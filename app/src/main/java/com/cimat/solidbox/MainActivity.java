package com.cimat.solidbox;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.cimat.solidbox.Utilities.PageAdapter;
import com.cimat.solidbox.Utilities.CustomViewPager;
import com.cimat.solidbox.Utilities.Preprocess.PreprocessFragment;

public class MainActivity extends AppCompatActivity
        implements PreprocessFragment.OnCADResultsSwitchPressedListener {

    Toolbar toolbar;
    TabLayout tabLayout;
    CustomViewPager viewPager;
    PageAdapter adapter;
    boolean areTabsVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // We get the toolbar from the layout. We are using a toolbar instead of an action bar
        // (remember set <item name="windowActionBar">false</item> and
        // <item name="windowNoTitle">true</item> in she file styles.xml
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //tabLayout.setVisibility(View.GONE);

        // Adds the tabs to the tabLayout
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Preprocess"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        areTabsVisible = true;
        tabLayout.setVisibility(View.GONE);

        // The viewPager handles the swipe tabs features
        viewPager = (CustomViewPager) findViewById(R.id.pager);
        viewPager.setPagingEnabled(false);

        // Number of offscreen pages to keep in memory (this way the viewPager does not destroy
        // any of the three tabs)
        //viewPager.setOffscreenPageLimit(1);

        adapter = new PageAdapter(getSupportFragmentManager(),
                tabLayout.getTabCount(), savedInstanceState);
        viewPager.setAdapter(adapter);

        Log.i("Main activity", "onCreate function");

        // The viewPager is attached to a tab selected listener.
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.i("Geometry fragment", "Asking for fragment");
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        // Check if the system supports OpenGL ES 2.0.
        ActivityManager activityManager =
                (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo configurationInfo =
                activityManager.getDeviceConfigurationInfo();
        // Even though the latest emulator supports OpenGL ES 2.0,
        // it has a bug where it doesn't set the reqGlEsVersion so
        // the above check doesn't work. The below will detect if the
        // app is running on an emulator, and assume that it supports
        // OpenGL ES 2.0.
        final boolean supportsEs2 =
                configurationInfo.reqGlEsVersion >= 0x20000
                        || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1
                        && (Build.FINGERPRINT.startsWith("generic")
                        || Build.FINGERPRINT.startsWith("unknown")
                        || Build.MODEL.contains("google_sdk")
                        || Build.MODEL.contains("Emulator")
                        || Build.MODEL.contains("Android SDK built for x86")));

        if (!supportsEs2) {

            // This is where you could create an OpenGL ES 1.x compatible
            // renderer if you wanted to support both ES 1 and ES 2. Since
            // we're not doing anything, the app will crash if the device
            // doesn't support OpenGL ES 2.0. If we publish on the market, we
            // should also add the following to AndroidManifest.xml:
            //
            // <uses-feature android:glEsVersion="0x00020000"
            // android:required="true" />
            //
            // This hides our app from those devices which don't support OpenGL
            // ES 2.0.

            Toast.makeText(this, "This device does not support OpenGL ES 2.0.",
                    Toast.LENGTH_LONG).show();
            return;
        }
    }

    public void onCADResultsSwitchActivated() {
        PreprocessFragment fragment =
                adapter.getPreAndPostProcessFragment();
        if (null != fragment)
            fragment.switchPreAndPro();
        else
            Log.i("Geometry fragment", "The fragment is null!!");


        //viewPager.setCurrentItem(2);
    }
}
