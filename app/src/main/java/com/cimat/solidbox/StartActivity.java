package com.cimat.solidbox;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class StartActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String arch = System.getProperty("os.arch");
        Log.i("CPU's architecture", arch);

        setContentView(R.layout.activity_start);
    }

    @Override
    public void onClick(View v) {

        Intent i;
        i = new Intent(this, MainActivity.class);
        startActivity(i);

    }

    public void test(View v) {

        Intent i;
        i = new Intent(this, MainActivity.class);
        startActivity(i);

    }
}
