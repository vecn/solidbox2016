package com.cimat.solidbox.DataStructures;

/**
 * Created by ernesto on 2/02/16.
 */
public interface DataInterface {
    int getId();
    void setId(int id);
    void setDepth(int id);
    int getDepth();
}
