package com.cimat.solidbox.DataStructures;


import com.cimat.solidbox.OpenGL.Objects.VisualObject;
import com.cimat.solidbox.OpenGL.Programs.ColorShaderProgram;
import com.cimat.solidbox.Utilities.Constants.BooleanOperation;
import com.cimat.solidbox.Utilities.Preprocess.Geometry;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.BooleanBuiltShape;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Shape;

import java.util.ArrayList;

import nb.geometricBot.Model;

/**
 * Created by ernesto on 1/07/16.
 */

public class ShapeTree {
    private BinaryTreeNode<Shape> root;

    private void initializeVariables() {
        root = null;
    }

    public ShapeTree() {
        super();
        initializeVariables();
    }

    public void addNode(Shape shape) {
        BinaryTreeNode<Shape> node = new BinaryTreeNode(
                BooleanOperation.NONE,
                shape,
                null,
                null
        );
        root = node;
    }

    public void addNode(Shape leftShape,
                        Shape rightShape,
                        Shape booleanBuiltShape,
                        BooleanOperation booleanOperation) {
        /*
        BinaryTreeNode<Shape> node = new BinaryTreeNode(
                booleanOperation,
                booleanBuiltShape,
                leftShape.getTree().getRoot(),
                rightShape.getTree().getRoot()
        );

        root = node;
        //*/ // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }

    public BinaryTreeNode<Shape> getRoot() {
        return root;
    }

    /*
    public void handleDragInteractivePoint(Geometry.InteractivePoint ip,
                                           float x, float y) {
        root.getObject().handleDragInteractivePoint(ip, x, y);
    }

    public void draw(ColorShaderProgram colorProgram,
                     float[] viewProjectionMatrix,
                     float[] modelViewProjectionMatrix,
                     VisualObject visualJoiningPoint,
                     VisualObject controlPoint) {
        root.getObject().draw(colorProgram,
                viewProjectionMatrix,
                modelViewProjectionMatrix,
                visualJoiningPoint,
                controlPoint);
    }

    public Model getModel() {
        return root.getObject().getModel();
    }

    public ArrayList<Geometry.Vertex> getVertices() {
        return root.getObject().getVertices();
    }

    public ArrayList<Geometry.InteractivePoint> getInteractivePoints() {
        return root.getObject().getInteractivePoints();
    }

    public ShapeTree getTree() {
        return this;
    }

    public void handleSelection() {
        root.getObject().handleSelection();
    }

    public void handleDoubleSelection() {
        root.getObject().handleDoubleSelection();
    }
    //*/
}
