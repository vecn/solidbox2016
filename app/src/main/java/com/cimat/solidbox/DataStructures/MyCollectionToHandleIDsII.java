package com.cimat.solidbox.DataStructures;

import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by ernesto on 27/07/16.
 */

public class MyCollectionToHandleIDsII<T extends DataInterface> {
    private Queue<Integer> availableIds;
    private CopyOnWriteArrayList<T> elements;
    private int counter;

    private void initializeVariables() {
        availableIds = new LinkedList();
        elements = new CopyOnWriteArrayList();
        counter = 0;
    }

    public MyCollectionToHandleIDsII() {
        super();
        initializeVariables();
    }

    public void addElement(T element) {
        int availableId;
        // Looks for available last positions in the array
        if (availableIds.size() > 0) {
            availableId = availableIds.poll();
            element.setId(availableId);
        } else {
            element.setId(counter);
            counter++;
        }
        elements.add(element);
    }

    public void removeElement(T element) {
        availableIds.add(element.getId());
        elements.remove(element);
    }

    public int getSize(){
        return elements.size();
    }

    public CopyOnWriteArrayList<T> getElements() {
        return elements;
    }
}
