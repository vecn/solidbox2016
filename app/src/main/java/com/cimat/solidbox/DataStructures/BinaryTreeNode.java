package com.cimat.solidbox.DataStructures;

import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.Constants.BooleanOperation;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Shape;

/**
 * Created by ernesto on 5/07/16.
 */

public class BinaryTreeNode<T> {
    private BinaryTreeNode<T> leftChild;
    private BinaryTreeNode<T> rightChild;
    private T object;
    private BooleanOperation booleanOperation;

    private void initializeVariables() {
        leftChild = null;
        rightChild = null;
        object = null;
        booleanOperation = BooleanOperation.NONE;
    }

    public BinaryTreeNode(BooleanOperation booleanOperation,
                          T object,
                          BinaryTreeNode<T> leftChild,
                          BinaryTreeNode<T> rightChild) {
        this.booleanOperation = booleanOperation;
        this.object = object;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }

    public BooleanOperation getBooleanOperation() {
        return booleanOperation;
    }

    public T getObject() { return object; }
    public BinaryTreeNode<T> getLeftChild() { return leftChild; }
    public BinaryTreeNode<T> getRightChild() { return rightChild; }
}
