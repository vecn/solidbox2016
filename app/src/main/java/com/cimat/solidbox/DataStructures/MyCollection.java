package com.cimat.solidbox.DataStructures;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by ernesto on 5/18/16.
 */
public class MyCollection<T extends DataInterface> {
    protected ArrayList<T> elements;

    protected void initializeVariables() {
        elements = new ArrayList();
    }

    public MyCollection() {
        initializeVariables();
    }

    public T getElement(int index) {
        // Out of bounds
        if (index < 0) return null;
        if (index > elements.size() - 1) return null;
        return elements.get(index);
    }

    public void addElement(T element) {
        elements.add(element);
    }

    public void removeElement(T element) {
        removeElementById(element.getId());
    }

    public void removeElementById(int id){
        T element;
        Iterator<T> it = elements.iterator();
        while (it.hasNext()) {
            element = it.next();
            if (element.getId() == id) {
                it.remove();
                break;
            }
        }
    }

    public T getElementById(int id) {
        T element = null;
        Iterator<T> it = elements.iterator();
        while (it.hasNext()) {
            element = it.next();
            if (element.getId() == id) break;
        }
        return element;
    }

    public int getSize(){ return elements.size(); }

    public void clear() {
        elements.clear();
    }
}

