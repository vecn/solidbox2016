package com.cimat.solidbox.DataStructures;

import com.cimat.solidbox.Utilities.Preprocess.Geometry;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;

/**
 * Created by ernesto on 2/02/16.
 */
public class MyCollectionToHandleIDs<T extends DataInterface> {
    protected int counter;
    protected int numberOfElements;
    protected Vector<Integer> positionInArray;
    protected Queue<Integer> availableIds;
    protected ArrayList<T> elements;

    public void clear() {
        counter = 0;
        numberOfElements = 0;
        positionInArray.clear();
        availableIds.clear();
        elements.clear();
    }

    protected void initializeVariables() {
        counter = 0;
        numberOfElements = 0;
        positionInArray = new Vector();
        availableIds = new LinkedList();
        elements = new ArrayList();
    }

    public MyCollectionToHandleIDs() {
        initializeVariables();
    }

    public ArrayList<T> getElements() { return elements; }

    public T getElement(int index) {
        // Out of bounds
        if (index < 0) return null;
        if (index > numberOfElements - 1) return null;
        return elements.get(index);
    }

    public void addElement(T element) {
        // Looks for available last positions in the array
        if (availableIds.size() > 0) {
            int availablePosition = counter - availableIds.size();
            int availableId = availableIds.poll();

            element.setId(availableId);
            elements.set(availablePosition, element);

            positionInArray.set(availableId, availablePosition);
            numberOfElements++;
        } else {
            element.setId(counter);
            elements.add(element);

            positionInArray.add(counter);

            counter++;
            numberOfElements++;
        }
    }

    // Used when the data is being loaded from a file
    public void addElementWithId(T element) {
        elements.add(element);
        numberOfElements++;
    }

    public void removeElement(T element) {
        removeElementById(element.getId());
    }

    public void removeElementById(int id){
        if (availableIds.contains(id)) return;
        // We add the id to que queue to use it later
        availableIds.add(id);
        // We move the last element to this position
        elements.set(positionInArray.get(id), elements.get(numberOfElements-1));
        positionInArray.set(elements.get(numberOfElements-1).getId(), positionInArray.get(id));
        // We set the reference to null!!!
        elements.set(numberOfElements - 1, null);
        positionInArray.set(id, -1);
        numberOfElements--;
    }

    public T getElementById(int id) {
        if ((id > (positionInArray.size() - 1)) || (id < 0)) return null;
        return elements.get(positionInArray.get(id));
    }

    public int getSize(){ return numberOfElements; }
}
