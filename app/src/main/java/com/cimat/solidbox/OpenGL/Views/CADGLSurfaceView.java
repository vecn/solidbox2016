package com.cimat.solidbox.OpenGL.Views;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.Vector;

import com.cimat.solidbox.OpenGL.Utilities.TouchHelper;
import com.cimat.solidbox.Utilities.Preprocess.Conditions.Force;
import com.cimat.solidbox.Utilities.Preprocess.Conditions.FixedDisplacement;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.PlotParameters;
import com.cimat.solidbox.Utilities.Preprocess.PreprocessFragment;
import com.cimat.solidbox.Utilities.ProblemData;

import com.cimat.solidbox.OpenGL.Utilities.TouchHelper.*;
import com.cimat.solidbox.Utilities.Preprocess.GeometrySet;
import com.cimat.solidbox.Utilities.Tools;

/**
 * Created by ernesto on 17/02/16.
 */
public class CADGLSurfaceView extends GLSurfaceView {
    private Context context;

    public boolean rendererSet = false;
    public CADRenderer renderer;

    private ArrayList<Pointer> pointers;
    private ArrayList<Tap> quickTaps;
    private ArrayList<Tap> longTaps;
    private ArrayList<Tap> previousQuickTaps;
    private ArrayList<DoubleTap> doubleTaps;
    private Drags drags;

    private PreprocessFragment parentFragment;

    public CADGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        Log.i("Constructor", "Creating glSurfaceView");

        // Request an OpenGL ES 2.0 compatible context.
        setEGLContextClientVersion(2);

        renderer = null;
        rendererSet = false;

        pointers = new ArrayList();
        quickTaps = new ArrayList();
        previousQuickTaps = new ArrayList();
        longTaps = new ArrayList();
        doubleTaps = new ArrayList();
        drags = new Drags();

        parentFragment = null;

        final View v = this.getRootView();

        setOnTouchListener(new OnTouchListener() {
            Tap tap;
            DoubleTap doubleTap;
            Point p;
            // When a user touches that view, we'll receive a call to onTouch()
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event != null) {
                    // First we disable any button
                    if (null != parentFragment) parentFragment.hideAndUnselectCurrentGroups();

                    // Adds pointers or updates them
                    handlePointer(event, v.getWidth(), v.getHeight(), pointers);

                    // Identifies the current taps
                    identifyTaps(pointers, quickTaps, longTaps);

                    // Identifies the drags
                    identifyDrags(pointers, drags);

                    // Identifies the double taps and the single taps.
                    // It also removes the taps
                    // that have already been used and those ones that have
                    // exceeded the limit time to become a double tap
                    identifySingleAndDoubleTaps(quickTaps, previousQuickTaps, doubleTaps);

                    // Reports all the single taps
                    Iterator<Tap> it = quickTaps.iterator();
                    while (it.hasNext()) {
                        tap = it.next();
                        p = tap.getPosition();
                        renderer.handleTouchPress(p.x, p.y);
                    }

                    // Reports all the drags
                    renderer.handleTouchDrags(drags);

                    // Reports only one double tap
                    Iterator<DoubleTap> itD = doubleTaps.iterator();
                    if (itD.hasNext()) {
                        doubleTap = itD.next();
                        p = doubleTap.getPosition();
                        renderer.handleDoubleTouchPress(p.x, p.y);
                    }

                    // We need to store the current taps
                    previousQuickTaps.addAll(quickTaps);

                    // Removes the gestures that have already been performed
                    quickTaps.clear();
                    doubleTaps.clear();
                    drags.removeStoppedDrags();

                    // Ask if the action that the user wanted to do was performed correctly

                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        Log.i("TOUCH EVENT", "Ernesto");
        /*
        boolean retVal = mScaleGestureDetector.onTouchEvent(e);
        retVal = mGestureDetector.onTouchEvent(e) || retVal;
        // Why to call super.onTouchEvent ??
        return retVal || super.onTouchEvent(e);
        //*/
        return true;
    }

    // The double tap is composed by two quick taps
    public static void identifySingleAndDoubleTaps(ArrayList<Tap> newQuickTaps,
                                                   ArrayList<Tap> previousQuickTaps,
                                                   ArrayList<DoubleTap> doubleTaps) {
        Point p;
        Tap oldTap, newTap;
        Iterator<Tap> oldIt, newIt;
        float distance;
        long currentTime;

        // Removes the taps that have exceeded the interval time
        newIt = newQuickTaps.iterator();
        if (newIt.hasNext()) {
            currentTime = newIt.next().getInitialTime();
            removeOldQuickTaps(previousQuickTaps, currentTime);
        }

        // Every quick tap may become a double tap.
        newIt = newQuickTaps.iterator();
        while (newIt.hasNext()) {
            newTap = newIt.next();

            oldIt = previousQuickTaps.iterator();
            while (oldIt.hasNext()) {
                oldTap = oldIt.next();
                distance =
                        Tools.distanceBetweenPoints(newTap.getPosition(), oldTap.getPosition());
                Log.i("Double taps", "Distance = " + distance);
                if (distance < Constants.MAX_TAP_DISTANCE) {
                    p = newTap.getPosition();
                    doubleTaps.add(new DoubleTap(p.x, p.y,
                            newTap.getEndingTime() - oldTap.getEndingTime()));

                    // We remove the quick taps
                    oldIt.remove();
                    newIt.remove();
                }
            }
        }
    }

    public static void removeOldQuickTaps(ArrayList<Tap> quickTaps,
                                          final long currentTime) {
        Tap tap;
        Iterator<Tap> it = quickTaps.iterator();
        long intervalTime;

        while (it.hasNext()) {
            tap = it.next();
            intervalTime = currentTime - tap.getEndingTime();
            if (intervalTime > Constants.MAX_DOUBLE_TAP_INTERVAL_TIME) {
                it.remove();
            }
        }
    }

    public static void identifyTaps(ArrayList<Pointer> pointers,
                                    ArrayList<Tap> quickTaps,
                                    ArrayList<Tap> longTaps) {
        Pointer pointer;
        Iterator<Pointer> it = pointers.iterator();
        while (it.hasNext()) {
            pointer = it.next();
            // To be a tap the pointer must have been short in time and
            // in distance
            if (pointer.hasStopped()) {
                if (pointer.getMovedDistance() < Constants.MAX_TAP_DISTANCE) {
                    if (pointer.getPressDuration() < Constants.MAX_TAP_DURATION) {
                        quickTaps.add(new Tap(
                                pointer.getStartX(),
                                pointer.getStartY(),
                                pointer.getStartTime(),
                                pointer.getLastTime()
                        ));
                    } else {
                        longTaps.add(new Tap(
                                pointer.getStartX(),
                                pointer.getStartY(),
                                pointer.getStartTime(),
                                pointer.getLastTime()
                        ));
                    }
                    // We remove the stopped pointer
                    it.remove();
                }
            }
        }
    }

    public static void identifyDrags(ArrayList<Pointer> pointers, Drags drags) {
        Drag drag;
        Pointer pointer;
        Iterator<Pointer> it = pointers.iterator();
        while (it.hasNext()) {
            pointer = it.next();
            // To be a drag the pointer must have been long in distance
            // and not necessarily the pointer must have stopped
            if (pointer.getMovedDistance() > Constants.MAX_TAP_DISTANCE) {
                // If the drag is not contained in the collection we add it
                if (!drags.contains(pointer.getID())) {
                    drag = new Drag(pointer.getID());
                    drags.addDrag(drag);
                    drag.setBeginningPosition(pointer.getStartX(), pointer.getStartY());
                } else {
                    drag = drags.getDragByID(pointer.getID());
                }

                // Update the last and previous positions
                drag.setPreviousPosition(pointer.getPreviousLastX(), pointer.getPreviousLastY());
                drag.setEndingPosition(pointer.getLastX(), pointer.getLastY());

                // If pointer related to the drag has stopped, we also stop the drag
                if (pointer.hasStopped()) {
                    drag.stop();
                    // We delete the pointer, it's not needed anymore
                    it.remove();
                }
            }
        }
    }

    public static void handlePointer(MotionEvent event,
                                     float width,
                                     float height,
                                     ArrayList<Pointer> pointers) {
        int pointerID, pointerIndex;
        Pointer pointer = null;
        float x, y;

        // Get the index for this action
        pointerIndex = MotionEventCompat.getActionIndex(event);
        pointerID = MotionEventCompat.getPointerId(event, pointerIndex);
        // Convert touch coordinates into normalized device coordinates, keeping in mind that
        // Android's Y coordinates are inverted.
        x =   (MotionEventCompat.getX(event, pointerIndex) / width) * 2f - 1f;
        y = -((MotionEventCompat.getY(event, pointerIndex) / height) * 2f - 1f);

        // Determines if the pointer must be added to the array or finished
        //Log.i("ACTIONS", "Pointer ID = " + pointerID);
        switch (MotionEventCompat.getActionMasked(event)) {

            // Triggered by the firs pointer touching the screen
            case MotionEvent.ACTION_DOWN: {
                Log.i("ACTIONS", "ACTION_DOWN");

                // Creates a new pointer and sets the start time and the initial position
                pointer = new Pointer(pointerID);
                pointer.startIt(x, y, System.currentTimeMillis());
                pointers.add(pointer);

                break;
            }

            // Extra pointer touching the screen after the primary pointer
            case MotionEvent.ACTION_POINTER_DOWN: {
                Log.i("ACTIONS", "ACTION_POINTER_DOWN");

                // Creates a new pointer and sets the start time and the initial position
                pointer = new Pointer(pointerID);
                pointer.startIt(x, y, System.currentTimeMillis());
                pointers.add(pointer);

                break;
            }

            // Triggered by the last pointer leaving the screen
            case MotionEvent.ACTION_UP: {
                Log.i("ACTIONS", "ACTION_UP");

                // Stop the pointer
                pointer = TouchHelper.getPointerByID(pointerID, pointers);
                pointer.stopIt(x, y, System.currentTimeMillis());

                break;
            }

            // Non-primary pointer goes up
            case MotionEvent.ACTION_POINTER_UP: {
                Log.i("ACTIONS", "ACTION_POINTER_UP");

                // Stop the pointer
                pointer = TouchHelper.getPointerByID(pointerID, pointers);
                pointer.stopIt(x, y, System.currentTimeMillis());

                break;
            }

            // Can be triggered by all the pointers at the same time
            case MotionEvent.ACTION_MOVE: {
                //Log.i("ACTIONS", "-----------------------------");
                Log.i("ACTIONS", "ACTION_MOVE");
                //Log.i("ACTIONS", "Number of pointers = " + event.getPointerCount());

                for (int i = 0; i < event.getPointerCount(); i++) {
                    x =   (MotionEventCompat.getX(event, i) / width) * 2f - 1f;
                    y = -((MotionEventCompat.getY(event, i) / height) * 2f - 1f);
                    pointerID = MotionEventCompat.getPointerId(event, i);
                    //Log.i("ACTIONS", "Pointer ID = " + pointerID);
                    // The pointer is still alive
                    pointer = TouchHelper.getPointerByID(pointerID, pointers);
                    pointer.continueIt(x, y, System.currentTimeMillis());
                }

                break;
            }
        }
    }

    private Pointer getPointerWithThisId(final int id,
                                         ArrayList<Pointer> pointers) {
        Pointer pointer = null;
        Iterator<Pointer> it = pointers.iterator();
        while (it.hasNext()) {
            pointer = it.next();
            if (id == pointer.getID()) break;
        }
        return pointer;
    }

    private float distance(float x1, float y1, float x2, float y2) {
        float dx = x1 - x2;
        float dy = y1 - y2;
        float distanceInPx = (float) Math.sqrt(dx * dx + dy * dy);
        return pxToDp(distanceInPx);
    }

    private float pxToDp(float px) {
        return px / getResources().getDisplayMetrics().density;
    }

    public void setParameters(GeometrySet geometry,
                              PlotParameters plotParameters,
                              ProblemData problemData) {
        // Once we have received the data needed we can create the renderer
        renderer = new CADRenderer(context, geometry, plotParameters, problemData);

        // We set this renderer
        //setRenderer(renderer);
        rendererSet = true;

        // Render the view only when there is a change in the drawing data
        //setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    public void setTypeOfAction(Constants.CADOperation action) {
        renderer.setCurrentAction(action);
    }

    public void setCurrentForce(Force f) {
        renderer.setCurrentForce(f);
    }

    public void setCurrentFixedDisplacement(FixedDisplacement disp) {
        renderer.setCurrentFixedDisplacement(disp);
    }

    public void setReferenceToParentFragment(PreprocessFragment fragment) {
        this.parentFragment = fragment;
    }
}
