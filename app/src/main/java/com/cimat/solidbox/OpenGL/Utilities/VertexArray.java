package com.cimat.solidbox.OpenGL.Utilities;

import android.util.Log;

import com.cimat.solidbox.Utilities.Constants;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glVertexAttribPointer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class VertexArray {

    // To store our vertex data in native code
    private final FloatBuffer floatBuffer;
    private int size;

    public VertexArray(float[] vertexData) {
        floatBuffer = ByteBuffer
                .allocateDirect(vertexData.length * Constants.BYTES_PER_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer()
                .put(vertexData);
        size = vertexData.length;
    }

    public VertexArray(int length) {
        size = length;
        floatBuffer = ByteBuffer
                .allocateDirect(length * Constants.BYTES_PER_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
    }

    public void setValue(int index, float value) {
        floatBuffer.put(index, value);
    }

    // Generic method to associate an attribute in our shader with the data
    public void setVertexAttribPointer(int dataOffset, int attributeLocation,
                                       int componentCount, int stride) {
        // Where OpenGL will start to read the data
        floatBuffer.position(dataOffset);
        // Tells to OpenGL where can read the data for the attribute
        glVertexAttribPointer(attributeLocation, componentCount, GL_FLOAT, false, stride, floatBuffer);
        // Enables the attribute
        glEnableVertexAttribArray(attributeLocation);
        floatBuffer.position(0);
    }

    public void printVertexArray() {
        Log.i("Vertex array", "VERTEX ARRAY");
        for (int i = 0; i < size; i++)
            Log.i("Vertex array", "" + floatBuffer.get(i));
    }
}