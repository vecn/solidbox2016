package com.cimat.solidbox.OpenGL.Utilities;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.opengl.GLES20;
import android.util.Log;

import com.cimat.solidbox.OpenGL.Objects.ObjectBuilder;
import com.cimat.solidbox.OpenGL.Objects.ObjectBuilder.DrawCommand;
import com.cimat.solidbox.OpenGL.Objects.VisualObject;
import com.cimat.solidbox.OpenGL.Programs.ColorShaderProgram;
import com.cimat.solidbox.OpenGL.Programs.TextShaderProgram;
import com.cimat.solidbox.OpenGL.Programs.TextureShaderProgram;
import com.cimat.solidbox.OpenGL.Programs.VaryingColorShaderProgram;
import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.Preprocess.Geometry;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.Triangle;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.Vertex;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.LineSegment;

import nb.geometricBot.Mesh;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.GL_TRIANGLE_FAN;
import static android.opengl.GLES20.GL_TRIANGLE_STRIP;
import static android.opengl.GLES20.GL_UNSIGNED_INT;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glDrawElements;
import static android.opengl.GLES20.glLineWidth;
import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.multiplyMV;
import static android.opengl.Matrix.rotateM;
import static android.opengl.Matrix.setIdentityM;
import static android.opengl.Matrix.translateM;

public class Utilities {

	public static final int BYTES_PER_FLOAT = 4;
	public static final int BYTES_PER_SHORT = 2;
	private static final String TAG = "Utilities";

    public static class Texture {
        public int ID;
        public float ratio;

        public Texture() {
            ID = 0;
            ratio = 4f;
        }
    }
	
	public static int createProgram(int vertexShaderHandle, int fragmentShaderHandle, AttribVariable[] variables) {
		int  mProgram = GLES20.glCreateProgram();
		
		if (mProgram != 0) {
	        GLES20.glAttachShader(mProgram, vertexShaderHandle);
	        GLES20.glAttachShader(mProgram, fragmentShaderHandle);
	
	        for (AttribVariable var: variables) {
	        	GLES20.glBindAttribLocation(mProgram, var.getHandle(), var.getName());
	        }   
	        
	        GLES20.glLinkProgram(mProgram);
	     
	        final int[] linkStatus = new int[1];
	        GLES20.glGetProgramiv(mProgram, GLES20.GL_LINK_STATUS, linkStatus, 0);
	
	        if (linkStatus[0] == 0)
	        {
	        	Log.v(TAG, GLES20.glGetProgramInfoLog(mProgram));
	            GLES20.glDeleteProgram(mProgram);
	            mProgram = 0;
	        }
	    }
	     
	    if (mProgram == 0)
	    {
	        throw new RuntimeException("Error creating program.");
	    }
		return mProgram;
	}

	public static int loadShader(int type, String shaderCode){
	    int shaderHandle = GLES20.glCreateShader(type);
	     
	    if (shaderHandle != 0)
	    {
	        GLES20.glShaderSource(shaderHandle, shaderCode);
	        GLES20.glCompileShader(shaderHandle);
	    
	        // Get the compilation status.
	        final int[] compileStatus = new int[1];
	        GLES20.glGetShaderiv(shaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);
	     
	        // If the compilation failed, delete the shader.
	        if (compileStatus[0] == 0)
	        {
	        	Log.v(TAG, "Shader fail info: " + GLES20.glGetShaderInfoLog(shaderHandle));
	            GLES20.glDeleteShader(shaderHandle);
	            shaderHandle = 0;
	        }
	    }
	    
	     
	    if (shaderHandle == 0)
	    {
	        throw new RuntimeException("Error creating shader " + type);
	    }
	    return shaderHandle;
	}

	public static FloatBuffer newFloatBuffer(float[] verticesData) {
		FloatBuffer floatBuffer;
		floatBuffer = ByteBuffer.allocateDirect(verticesData.length * BYTES_PER_FLOAT)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		floatBuffer.put(verticesData).position(0);
		return floatBuffer;
	}

	// Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
	public static void bindData(ColorShaderProgram colorProgram, VertexArray vertexArray) {
		if (null != vertexArray) {
			vertexArray.setVertexAttribPointer(
					0,
					colorProgram.getPositionAttributeLocation(),
					Constants.POSITION_COMPONENT_COUNT,
					0);
		}
	}

	public static void bindData(TextureShaderProgram program, VertexArray vertexArray,
								VertexArray textureArray) {
		if (null == vertexArray) return;

		vertexArray.setVertexAttribPointer(
				0,
				program.getPositionAttributeLocation(),
				Constants.POSITION_COMPONENT_COUNT,
				0);

		textureArray.setVertexAttribPointer(
				0,
				program.getTextureCoordinatesAttributeLocation(),
				Constants.TEXTURE_COORDINATES_COMPONENT_COUNT,
				0);
	}

	public static Geometry.Point convertNormalized2DPointToWorldSpace2DPoint(float normalizedX,
																			 float normalizedY,
																			 float[] invertedTransformationMatrix) {
		final float[] normalizedPoint = {normalizedX, normalizedY, 0, 1};
		final float[] worldSpacePoint = new float[4];
		multiplyMV(worldSpacePoint, 0, invertedTransformationMatrix, 0, normalizedPoint, 0);
		return new Geometry.Point(worldSpacePoint[0], worldSpacePoint[1], worldSpacePoint[2]);
	}

	// Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
	public static void bindData(VaryingColorShaderProgram varyingColorProgram, VertexArray vertexArray) {
		int propertiesPerVertex = Constants.POSITION_COMPONENT_COUNT + Constants.COLOR_COMPONENT_COUNT;
		int STRIDE = propertiesPerVertex * Constants.BYTES_PER_FLOAT;
		if (null != vertexArray) {
			// To read the position
			vertexArray.setVertexAttribPointer(
					0,
					varyingColorProgram.getPositionAttributeLocation(),
					Constants.POSITION_COMPONENT_COUNT,
					STRIDE);
			// To read the color of each vertex
			vertexArray.setVertexAttribPointer(
					Constants.POSITION_COMPONENT_COUNT,
					varyingColorProgram.getColorAttributeLocation(),
					Constants.COLOR_COMPONENT_COUNT,
					STRIDE);
		}
	}


	public static void appendLineSegmentToDrawList(final int startVertex,
												   List<ObjectBuilder.DrawCommand> drawList,
												   final int typeOfLine) {
		drawList.add(new ObjectBuilder.DrawCommand() {
			@Override
			public void draw() {
				glLineWidth(Constants.LINE_WIDTH_NOT_SELECTED_VISUAL_LINE);
				glDrawArrays(typeOfLine, startVertex, 2);
			}
		});
	}

	public static void appendTriangleStripToDrawList(final int startVertex,
												 final int numOfVertices,
												 List<ObjectBuilder.DrawCommand> drawList) {
		drawList.add(new ObjectBuilder.DrawCommand() {
			@Override
			public void draw() {
				glDrawArrays(GL_TRIANGLE_STRIP, startVertex, numOfVertices);
                //glDrawElements(GL_TRIANGLE_STRIP, numOfVertices, GL_UNSIGNED_INT,);
			}
		});
	}

	public static void positionObjectInScene(float x,
											 float y,
											 float z,
											 float[] matrixIn,
											 float[] matrixOut) {
		float[] modelMatrix = new float[16];
		setIdentityM(modelMatrix, 0);
		translateM(modelMatrix, 0, x, y, z);
		multiplyMM(matrixOut, 0, matrixIn, 0, modelMatrix, 0);
	}

	/*
	public static void appendTriangle(Triangle triangle,
									  VertexArray vertexArray,
									  final int startVertex) {
		Point point;
		final int floatsPerVertex = Constants.FLOATS_PER_VERTEX;
		final int startIndex = startVertex*floatsPerVertex;

		for (int i = 0; i < 3; i++) {
			point = triangle.getVertex(i);
			vertexArray.setValue(startIndex + i*floatsPerVertex + 0, point.x);
			vertexArray.setValue(startIndex + i*floatsPerVertex + 1, point.y);
			vertexArray.setValue(startIndex + i*floatsPerVertex + 2, point.z);
		}
	}
	//*/

	public static void appendTriangle(List<DrawCommand> drawList,
									  final int startVertex) {

		drawList.add(new DrawCommand() {
			@Override
			public void draw() {
				glDrawArrays(GL_TRIANGLES, startVertex, 3);
			}
		});
	}


	public static void buildDrawListForMesh(Mesh mesh,
											List<DrawCommand> drawList,
											int startVertex) {
		int[] connMtx = mesh.getConnMtxRef();
		float[] vertices = mesh.getVerticesRef();
		final int numberOfElements = mesh.getNElements();
		final int dim = Constants.PROBLEM_DIMENSION;
		/*
		Geometry.Triangle triangle = new Geometry.Triangle();
		Geometry.Point point = new Geometry.Point();

		drawList.clear();
		for (int i = 0; i < numberOfElements; i++) {
			buildMeshTriangle(i, triangle, connMtx, vertices, dim, point);

			appendTriangle(drawList, startVertex);
			startVertex += 3;
		}
		//*/ // PENDING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	}


	public static void setValuesForMeshVertexArray(Mesh mesh,
												   VertexArray vertexArray,
												   int startVertex) {
		int[] connMtx = mesh.getConnMtxRef();
		float[] vertices = mesh.getVerticesRef();
		final int numberOfElements = mesh.getNElements();
		final int dim = Constants.PROBLEM_DIMENSION;

		/*
		Geometry.Triangle triangle = new Geometry.Triangle();
		Geometry.Point point = new Geometry.Point();

		for (int i = 0; i < numberOfElements; i++) {
			buildMeshTriangle(i, triangle, connMtx, vertices, dim, point);

			appendTriangle(triangle, vertexArray, startVertex);
			startVertex += 3;
		}
		//*/ // PENDING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	}

	/*
	public static void buildMeshTriangle(int i,
										 Triangle triangle,
										 int[] connMtx,
										 float[] vertices,
										 final int dim,
										 Point point) {
		int index;

		index = connMtx[i*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT + 0];
		point.setCoordinates(vertices[index*dim], vertices[index*dim+1], 0f);
		triangle.copyCoordinates(0, point);

		index = connMtx[i*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT + 1];
		point.setCoordinates(vertices[index*dim], vertices[index*dim + 1], 0f);
		triangle.copyCoordinates(1, point);

		index = connMtx[i*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT + 2];
		point.setCoordinates(vertices[index*dim], vertices[index*dim+1], 0f);
		triangle.copyCoordinates(2, point);

	}
	//*/

	public static void buildDrawListForTriangleFan(List<DrawCommand> drawList,
											final int numberOfSides) {
		drawList.clear();
		drawList.add(new DrawCommand() {
			@Override
			public void draw() {
				glDrawArrays(GL_TRIANGLE_FAN, 0, numberOfSides + 2);
			}
		});
	}

	public static void setValuesForTriangleFanVertexArray(VertexArray vertexArray,
														  ArrayList<Point> vertices) {
		// Here we assume that the vertices have a given order (clockwise or
		// counterclockwise)
		if (null == vertices) return;

		int index = 0;
		Point v;

		Iterator<Point> it = vertices.iterator();
		if (it.hasNext()) {
			v = it.next();

			// The center
			vertexArray.setValue(index++, v.getX());
			vertexArray.setValue(index++, v.getY());
			vertexArray.setValue(index++, v.getZ());

			// The vertices
			while (it.hasNext()) {
				v = it.next();
				vertexArray.setValue(index++, v.getX());
				vertexArray.setValue(index++, v.getY());
				vertexArray.setValue(index++, v.getZ());
			}

			// Repeats the first vertex
			v = vertices.get(0);
			vertexArray.setValue(index++, v.getX());
			vertexArray.setValue(index++, v.getY());
			vertexArray.setValue(index++, v.getZ());
		}
	}

	public static <T extends Point> void drawPoints(ColorShaderProgram colorProgram,
													float[] viewProjectionMatrix,
													float[] modelViewProjectionMatrix,
													VisualObject visualElement,
													ArrayList<T> elements,
													float depth,
													float R, float G, float B) {
		visualElement.bindData(colorProgram);

		T e;
		Iterator<T> it = elements.iterator();
		while (it.hasNext()) {
			e = it.next();
			Utilities.positionObjectInScene(e.getX(), e.getY(),
					depth, viewProjectionMatrix, modelViewProjectionMatrix);
			colorProgram.setUniforms(modelViewProjectionMatrix, R, G, B);
			visualElement.draw();
		}
	}

	public static <T extends Point> void drawPoint(ColorShaderProgram colorProgram,
													float[] viewProjectionMatrix,
													float[] modelViewProjectionMatrix,
													VisualObject visualElement,
													T element,
													float depth,
													float R, float G, float B) {
		visualElement.bindData(colorProgram);

		Utilities.positionObjectInScene(element.getX(), element.getY(),
				depth, viewProjectionMatrix, modelViewProjectionMatrix);
		colorProgram.setUniforms(modelViewProjectionMatrix, R, G, B);
		visualElement.draw();
	}

	public static <T extends Point> void drawSelectedPoints(ColorShaderProgram colorProgram,
													float[] viewProjectionMatrix,
													float[] modelViewProjectionMatrix,
													VisualObject visualElement,
													ArrayList<T> elements,
													float depth,
													float R, float G, float B) {
		visualElement.bindData(colorProgram);

		T e;
		Iterator<T> it = elements.iterator();
		while (it.hasNext()) {
			e = it.next();
			Utilities.positionObjectInScene(e.getX(), e.getY(),
					depth, viewProjectionMatrix, modelViewProjectionMatrix);
			colorProgram.setUniforms(modelViewProjectionMatrix, R, G, B);
			//if (e.isSelected()) visualElement.draw(); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		}
	}

	public static void drawLineSegments(ColorShaderProgram colorProgram,
										float[] viewProjectionMatrix,
										float[] modelViewProjectionMatrix,
										VertexArray vertexArray,
										List<DrawCommand> drawList,
										float depth,
										float R, float G, float B) {
		Utilities.positionObjectInScene(0, 0, depth, viewProjectionMatrix,
				modelViewProjectionMatrix);

		Utilities.bindData(colorProgram, vertexArray);
		colorProgram.setUniforms(modelViewProjectionMatrix, R, G, B);

		for (DrawCommand drawCommand : drawList)
			drawCommand.draw();
	}

	public static void drawTextures(TextureShaderProgram program,
									float[] viewProjectionMatrix,
									float[] modelViewProjectionMatrix,
									VertexArray vertexArray,
									VertexArray textureArray,
									List<DrawCommand> drawList,
									float depth,
									int texture,
									final Colors.ColorRGB color) {
		Utilities.positionObjectInScene(0, 0, depth, viewProjectionMatrix,
				modelViewProjectionMatrix);

		Utilities.bindData(program, vertexArray, textureArray);
		program.setUniforms(modelViewProjectionMatrix, texture,
				color.getRed(), color.getGreen(), color.getBlue(), 0f, 0f, 0f, 0f, 0f);

		for (DrawCommand drawCommand : drawList)
			drawCommand.draw();
	}

	public static void rotate(float angle,
							  float x, float y, float z,
							  float[] R,
							  float[] matrixIn,
							  float[] matrixOut) {
		setIdentityM(R, 0);
		rotateM(R, 0, angle, x, y, z);
		multiplyMM(matrixOut, 0, matrixIn, 0, R, 0);
	}

	/*
	public static <T extends Point> boolean isClockwiseOrder(ArrayList<T> elements) {
		float sum = 0f;
		Iterator<T> it = elements.iterator();
		T e1, e2;

		if (it.hasNext()) {
			e1 = it.next();
			while (it.hasNext()) {
				e2 = it.next();
				sum += (e2.getX() - e1.getX())*(e2.getY() - e1.getY());
				e1 = e2;
			}

			e2 = elements.get(0);
			sum += (e2.getX() - e1.getX())*(e2.getY() - e1.getY());

			if (sum > 0) return true;
			else         return false;
		} else {
			return false;
		}
	}
	//*/

	// Be careful with the polygons with area equal to zero!!
	public static boolean isClockwiseOrder(ArrayList<Point> elements) {
		float sum = 0f;
		Iterator<Point> it = elements.iterator();
		Point e1, e2;

		if (it.hasNext()) {
			e1 = it.next();
			while (it.hasNext()) {
				e2 = it.next();
				sum += e1.x*e2.y - e1.y*e2.x;
				e1 = e2;
			}

			e2 = elements.get(0);
			sum += e1.x*e2.y - e1.y*e2.x;

			if (sum > -1e-5) return true;
			else             return false;
		} else {
			return false;
		}
	}

	public static Point getTransformedPoint(float[] T, Point a) {
		float [] x = new float[4];
		float [] y = new float[4];
		x[0] = a.x;
		x[1] = a.y;
		x[2] = a.z;
		x[3] = 1f;
		multiplyMV(y, 0, T, 0, x, 0);
		return new Geometry.Point(y[0], y[1], y[2]);
	}
}
