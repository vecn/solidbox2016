package com.cimat.solidbox.OpenGL.Objects;

import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.OpenGL.Utilities.VertexArray;
import com.cimat.solidbox.OpenGL.Programs.ColorShaderProgram;
import com.cimat.solidbox.OpenGL.Programs.VaryingColorShaderProgram;
import com.cimat.solidbox.OpenGL.Utilities.Colors.ColorPalette;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ernesto on 22/02/16.
 */
public class VisualSurfaceResults {
    private int STRIDE;
    private VertexArray vertexArray;
    private List<ObjectBuilder.DrawCommand> drawList;
    private List<ObjectBuilder.DrawCommand> drawListBorder;
    ObjectBuilder.GeneratedData generatedData;
    private int numberOfElements;
    private int propertiesPerVertex;
    boolean meshSet;
    boolean valuesSet;
    boolean drawListCreated;
    boolean working;

    public VisualSurfaceResults() {
        vertexArray = null;
        drawList = new ArrayList();
        numberOfElements = 0;
        propertiesPerVertex = Constants.POSITION_COMPONENT_COUNT + Constants.COLOR_COMPONENT_COUNT;
        STRIDE = propertiesPerVertex * Constants.BYTES_PER_FLOAT;
        meshSet = false;
        valuesSet = false;
        drawListCreated = false;
        //temporal = null;
        working = false;
    }

    public void setNumberOfElements(int numberOfElements) {
        if (numberOfElements <= 0) return;
        this.numberOfElements = numberOfElements;
        // Space to save the data for OpenGL
        vertexArray = new VertexArray(
                numberOfElements*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT*propertiesPerVertex);
        drawListCreated = false;
    }

    public void setMesh(float[] vertices,
                        int[] connMtx) {

        working = true;

        int numberOfElements, nPreviousVertices, beginIndex;
        final int nVerticesPerElement = Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT;
        final int dimension = Constants.PROBLEM_DIMENSION;
        final int beginAttributeIndex = 0;
        float x, y, z;

        numberOfElements = connMtx.length / nVerticesPerElement;
        for (int i = 0; i < numberOfElements; i++) {
            nPreviousVertices = i*nVerticesPerElement;
            for (int j = 0; j < nVerticesPerElement; j++) {
                beginIndex = beginAttributeIndex + propertiesPerVertex*(nPreviousVertices + j);
                // For OpenGL we give the three coordinates
                x = vertices[connMtx[nPreviousVertices + j]*dimension];
                y = vertices[connMtx[nPreviousVertices + j]*dimension + 1];
                z = 0f;
                vertexArray.setValue(beginIndex + 0, x);
                vertexArray.setValue(beginIndex + 1, y);
                vertexArray.setValue(beginIndex + 2, z);
            }
        }

        meshSet = true;

        working = false;
    }

    public void setValues( char direction,
                           float[] values,
                           int[] connMtx,
                           ColorPalette colorPalette) {
        working = true;

        for (int i = 0; i < numberOfElements; i++)
            for (int j = 0; j < Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT; j++)
                changeColor(
                        i,
                        j,
                        connMtx,
                        values,
                        colorPalette,
                        direction,
                        Constants.POSITION_COMPONENT_COUNT,
                        propertiesPerVertex,
                        vertexArray
                );

        valuesSet = true;

        if (meshSet && valuesSet && (!drawListCreated)){
            createDrawList();
        }

        working = false;
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(VaryingColorShaderProgram varyingColorProgram) {
        if (null != vertexArray) {
            // To read the position
            vertexArray.setVertexAttribPointer(
                    0,
                    varyingColorProgram.getPositionAttributeLocation(),
                    Constants.POSITION_COMPONENT_COUNT,
                    STRIDE);
            // To read the color of each vertex
            vertexArray.setVertexAttribPointer(
                    Constants.POSITION_COMPONENT_COUNT,
                    varyingColorProgram.getColorAttributeLocation(),
                    Constants.COLOR_COMPONENT_COUNT,
                    STRIDE);
        }
    }

    public void bindData(ColorShaderProgram colorProgram) {
        if (null != vertexArray) {
            // To read the position
            vertexArray.setVertexAttribPointer(
                    0,
                    colorProgram.getPositionAttributeLocation(),
                    Constants.POSITION_COMPONENT_COUNT,
                    STRIDE);
        }
    }

    // Draws everything in the draw list
    public void draw() {
        if (drawList.size() == 0) return;
        if (working) return;
        for (ObjectBuilder.DrawCommand drawCommand : drawList) {
            drawCommand.draw();
        }
    }

    public boolean hasBeenSet(){
        return meshSet && valuesSet;
    }

    private void setNewValues(int beginAttributeIndex,
                              VertexArray vertexArray,
                              float[] temporal) {

        int count = 0;
        int countStride = 0;
        for(int i = 0; i < temporal.length; i++) {
            vertexArray.setValue(beginAttributeIndex + countStride + count%3, temporal[i]);
            if (count == 2){
                count = 0;
                countStride += propertiesPerVertex;
            } else {
                count++;
            }
        }
    }

    private void changeColor(int element,
                             int localVertex,
                             int[] connMtx,
                             float[] values,
                             ColorPalette colorPalette,
                             char direction,
                             int beginAttributeIndex,
                             int propertiesPerVertex,
                             VertexArray vertexArray) {
        final int dimension = Constants.PROBLEM_DIMENSION;
        int nPreviousVertices, beginIndex;

        nPreviousVertices = element*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT;

        switch (direction) {
            // Values in all directions
            case Constants.ALL_COMPONENTS:
                colorPalette.setValues(
                        values[connMtx[nPreviousVertices+localVertex]*dimension],
                        values[connMtx[nPreviousVertices+localVertex]*dimension + 1]
                );
                break;
            // Values in X
            case Constants.X_COMPONENT:
                colorPalette.setValues(values[connMtx[nPreviousVertices+localVertex]*dimension]);
                break;
            // Values in Y
            case Constants.Y_COMPONENT:
                colorPalette.setValues(values[connMtx[nPreviousVertices+localVertex]*dimension + 1]);
                break;
            case Constants.ONE_COMPONENT:
                colorPalette.setValues(values[connMtx[nPreviousVertices+localVertex]]);
                break;
        }

        beginIndex = beginAttributeIndex + propertiesPerVertex*(nPreviousVertices + localVertex);
        vertexArray.setValue(beginIndex + 0, colorPalette.getRed());
        vertexArray.setValue(beginIndex + 1, colorPalette.getGreen());
        vertexArray.setValue(beginIndex + 2, colorPalette.getBlue());
    }

    private void createDrawList() {
        int startVertex = 0;
        drawList.clear();
        for (int i = 0; i < numberOfElements; i++) {
            ObjectBuilder.appendTriangle(startVertex, drawList);
            startVertex += Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT;
        }
        drawListCreated = true;
        //Log.i("Draw list", "size = " + drawList.size());
    }
}
