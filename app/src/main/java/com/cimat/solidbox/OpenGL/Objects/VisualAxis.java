package com.cimat.solidbox.OpenGL.Objects;

import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.OpenGL.Utilities.VertexArray;
import com.cimat.solidbox.OpenGL.Programs.ColorShaderProgram;
import com.cimat.solidbox.Utilities.Preprocess.Axis;

import java.util.List;

/**
 * Created by ernesto on 15/02/16.
 */
public class VisualAxis {
    private VertexArray vertexArray;
    private List<ObjectBuilder.DrawCommand> drawListAxis;
    private List<ObjectBuilder.DrawCommand> drawListGrid;

    public VisualAxis(Axis axis) {
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createVisualAxis(axis);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawListAxis = generatedData.getDrawList(0);
        drawListGrid = generatedData.getDrawList(1);
    }

    // Update the data for OpenGL
    public void updateOpenGLData(Axis axis){
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createVisualAxis(axis);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawListAxis = generatedData.getDrawList(0);
        drawListGrid = generatedData.getDrawList(1);
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(ColorShaderProgram colorProgram) {
        vertexArray.setVertexAttribPointer(
                0,
                colorProgram.getPositionAttributeLocation(),
                Constants.POSITION_COMPONENT_COUNT,
                0);
    }

    // Draws everything in the draw list
    public void drawAxis() {
        for (ObjectBuilder.DrawCommand drawCommand : drawListAxis) {
            drawCommand.draw();
        }
    }

    public void drawGrid() {
        for (ObjectBuilder.DrawCommand drawCommand : drawListGrid) {
            drawCommand.draw();
        }
    }
}
