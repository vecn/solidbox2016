package com.cimat.solidbox.OpenGL.Utilities;

import com.cimat.solidbox.OpenGL.Objects.ObjectBuilder.DrawCommand;
import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.opengl.GLES20.GL_LINES;

/**
 * Created by ernesto on 21/06/16.
 */

/** This only works when the vertices are in order clockwise
 * or counterclockwise, because a line is always drawn
 * between two consecutive vertices. This way the vertices
 * are not repeated when is built the vertex array.
 */

public class OpenGLineSegmentHelper {
    private List<DrawCommand> drawList;
    private VertexArray vertexArray;

    private ArrayList<Point> vertices;
    private boolean isClosed;
    private boolean isOK;

    private void initializeVariables() {
        drawList = new ArrayList();
        vertexArray = null;
        vertices = null;
        isClosed = false;
        isOK = false;
    }

    public void clear() {
        drawList.clear();
        vertexArray = null;
        vertices = null;
        isClosed = false;
        isOK = false;
    }

    public OpenGLineSegmentHelper() {
        initializeVariables();
    }

    public void setData(ArrayList<Point> vertices, boolean isClosed) {
        this.vertices = vertices;
        this.isClosed = isClosed;

        isOK = true;
        if (null == vertices) isOK = false;
        if (0 == vertices.size()) isOK = false;
        if (isClosed && (2 > vertices.size())) isOK = false;

    }

    public boolean buildOpenGLData(int typeOfLine) {
        if (!isOK) return false;

        buildOpenGLDrawList(GL_LINES);
        buildOpenGLVertexArray();

        return true;
    }

    private void buildOpenGLDrawList(int typeOfLine) {
        if (null == vertices) return;

        int numberOfSides;
        if (isClosed) numberOfSides = vertices.size();
        else          numberOfSides = vertices.size() - 1;

        drawList.clear();
        for (int i = 0; i < numberOfSides; i++)
            Utilities.appendLineSegmentToDrawList(i, drawList, typeOfLine);
    }

    private void buildOpenGLVertexArray() {
        if (null == vertices) return;

        // We need an extra vertex if the shape is closed
        int totalOfVertices = vertices.size();
        if (isClosed) totalOfVertices++;

        vertexArray = new VertexArray(
                new float[totalOfVertices*Constants.FLOATS_PER_VERTEX]);

        setValuesVertexArray();
    }

    private void setValuesVertexArray() {
        int i = 0;
        Point v;
        final int n = Constants.FLOATS_PER_VERTEX;
        Iterator<Point> it = vertices.iterator();
        while (it.hasNext()) {
            v = it.next();
            vertexArray.setValue(i*n + 0, v.getX());
            vertexArray.setValue(i*n + 1, v.getY());
            vertexArray.setValue(i*n + 2, v.getZ());
            i++;
        }

        // If the shape is closed we need to copy the first vertex
        if (isClosed) {
            v = vertices.get(0);
            vertexArray.setValue(i * n + 0, v.getX());
            vertexArray.setValue(i * n + 1, v.getY());
            vertexArray.setValue(i * n + 2, v.getZ());
        }
    }

    public List<DrawCommand> getDrawList() { return drawList; }
    public VertexArray getVertexArray() { return vertexArray; }

    public void updateVertexArray() {
        setValuesVertexArray();
    }
    public void updateLineType(int typeOfLine) { buildOpenGLDrawList(typeOfLine); }
}
