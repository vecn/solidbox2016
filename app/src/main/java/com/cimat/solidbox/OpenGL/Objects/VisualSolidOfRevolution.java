package com.cimat.solidbox.OpenGL.Objects;

import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.OpenGL.Utilities.VertexArray;
import com.cimat.solidbox.OpenGL.Programs.ColorShaderProgram;
import com.cimat.solidbox.OpenGL.Programs.VaryingColorShaderProgram;
import com.cimat.solidbox.OpenGL.Utilities.Colors.ColorPalette;
import com.cimat.solidbox.Utilities.Preprocess.Geometry;

import java.util.ArrayList;
import java.util.List;

import nb.pdeBot.finiteElement.MeshResults;

/**
 * Created by ernesto on 29/03/16.
 */
public class VisualSolidOfRevolution {
    private VertexArray vertexArray;
    private List<ObjectBuilder.DrawCommand> drawList;
    private int[] rotatingVerticesIds;
    private int numberOfSlices;

    public VisualSolidOfRevolution() {
        vertexArray = null;
        drawList = null;
        rotatingVerticesIds = null;
        numberOfSlices = 0;
    }

    public void create(int[][] modelSgm, int numberOfSlices) {
        ArrayList<int[]> holderRotatingVerticesIds = new ArrayList();
        ArrayList<List<ObjectBuilder.DrawCommand>> holderDrawList = new ArrayList();
        ArrayList<VertexArray> holderVertexArray = new ArrayList();

        this.numberOfSlices = numberOfSlices;
        ObjectBuilder.createElementsForSolidOfRevolution(holderVertexArray, holderDrawList,
                holderRotatingVerticesIds, modelSgm, numberOfSlices);

        vertexArray = holderVertexArray.get(0);
        rotatingVerticesIds = holderRotatingVerticesIds.get(0);
        drawList = holderDrawList.get(0);
    }

    public void setVerticesCoordinates(Geometry.Ray axis, float[] vertices) {
        ObjectBuilder.setVerticesCoordinatesSolidOfRevolution(axis, vertexArray, vertices,
                rotatingVerticesIds, numberOfSlices);
    }

    public void setVerticesValues(char direction, float[] values,
                                  ColorPalette colorPalette) {
        ObjectBuilder.setVerticesValuesSolidOfRevolution(vertexArray, direction, values,
                numberOfSlices, rotatingVerticesIds, colorPalette);
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(VaryingColorShaderProgram varyingColorProgram) {
        int propertiesPerVertex = Constants.POSITION_COMPONENT_COUNT + Constants.COLOR_COMPONENT_COUNT;
        int STRIDE = propertiesPerVertex * Constants.BYTES_PER_FLOAT;
        if (null != vertexArray) {
            // To read the position
            vertexArray.setVertexAttribPointer(
                    0,
                    varyingColorProgram.getPositionAttributeLocation(),
                    Constants.POSITION_COMPONENT_COUNT,
                    STRIDE);
            // To read the color of each vertex
            vertexArray.setVertexAttribPointer(
                    Constants.POSITION_COMPONENT_COUNT,
                    varyingColorProgram.getColorAttributeLocation(),
                    Constants.COLOR_COMPONENT_COUNT,
                    STRIDE);
        }
    }

    // Draws everything in the draw list
    public void draw() {
        if (null != drawList) {
            for (ObjectBuilder.DrawCommand drawCommand : drawList) {
                drawCommand.draw();
            }
        }
    }
}
