package com.cimat.solidbox.OpenGL.Utilities;

import android.util.Log;

import com.cimat.solidbox.OpenGL.Objects.ObjectBuilder.DrawCommand;
import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.LoggerConfig;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
import com.cimat.solidbox.Utilities.Preprocess.MyModel;

import java.util.ArrayList;
import java.util.List;

import nb.geometricBot.Mesh;

/**
 * Created by ernesto on 21/06/16.
 */

public class OpenGLSurfaceMeshHelper {
    private List<DrawCommand> drawList;
    private VertexArray vertexArray;
    private ArrayList<Point> boundary;
    private ArrayList<ArrayList<Point>> holes;

    private MyModel model;
    private Mesh mesh;

    private boolean isOK;

    private void inititializeVariables() {
        drawList = new ArrayList();
        vertexArray = null;
        boundary = null;
        holes = null;

        model = new MyModel();
        mesh = null;

        isOK = false;
    }

    public void clear() {
        drawList.clear();
        vertexArray = null;
        boundary = null;
        holes = null;

        model = new MyModel();
        mesh = null;

        isOK = false;
    }

    public OpenGLSurfaceMeshHelper() {
        inititializeVariables();
    }

    public void setData(ArrayList<Point> boundary) {
        this.boundary = boundary;
        isOK = true;
        if (null == boundary) isOK = false;
        if (2 > boundary.size()) isOK = false;
    }

    public void setData(ArrayList<Point> boundary,
                        ArrayList<ArrayList<Point>> holes) {
        this.boundary = boundary;
        this.holes = holes;

        isOK = true;
        if (null == boundary) isOK = false;
        if (2 > boundary.size()) isOK = false;
        if (null == holes) isOK = false;
    }

    public boolean buildOpenGLData(boolean isConvex, boolean hasHoles) {
        if (!isOK) return false;

        if (hasHoles || !isConvex) {
            model.create(boundary, holes);
            if (model.isAValidModel()) {
                buildMesh();
                buildOpenGLDataForConcavePolygon();
            } else {
                return false;
            }
        } else {
            buildOpenGLDataForConvexPolygon();
        }

        return true;
    }

    private void buildOpenGLDataForConcavePolygon() {
        vertexArray = new VertexArray(new float[mesh.getNElements()
                *Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT
                *Constants.FLOATS_PER_VERTEX]);
        drawList.clear();
        Utilities.buildDrawListForMesh(mesh, drawList, 0);
        Utilities.setValuesForMeshVertexArray(mesh, vertexArray, 0);
    }

    private boolean buildMesh() {
        if (model.isAValidModel()) {
            mesh = Mesh.getSimplestMesh(model);
        }
        else {
            if (LoggerConfig.ON)
                Log.i("SurfaceShape", "WARNING: Mesh not created!! Invalid model");
            return false;
        }

        return true;
    }

    private void buildOpenGLDataForConvexPolygon() {
        // We'll use a triangle fan to create the mesh to draw the surface
        vertexArray = new VertexArray(
                new float[(boundary.size() + 2)* Constants.FLOATS_PER_VERTEX]);

        Utilities.buildDrawListForTriangleFan(drawList, boundary.size());
        Utilities.setValuesForTriangleFanVertexArray(vertexArray, boundary);
    }

    public void updateVertexArray(boolean isConvex, boolean hasHoles) {
        if (hasHoles || !isConvex)
            // If the polygon is concave or it has holes, then we
            // need to update everything
            buildOpenGLDataForConcavePolygon();
        else
            Utilities.setValuesForTriangleFanVertexArray(vertexArray, boundary);
    }

    public Mesh getMesh() { return mesh; }
    public MyModel getModel() { return model; }
    public List<DrawCommand> getDrawList() { return drawList; }
    public VertexArray getVertexArray() { return vertexArray; }
}
