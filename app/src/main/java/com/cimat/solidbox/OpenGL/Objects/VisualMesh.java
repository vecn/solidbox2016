package com.cimat.solidbox.OpenGL.Objects;

import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.OpenGL.Utilities.VertexArray;
import com.cimat.solidbox.OpenGL.Programs.ColorShaderProgram;

import java.util.List;

import nb.geometricBot.Mesh;

/**
 * Created by ernesto on 27/01/16.
 */
public class VisualMesh {
    private VertexArray vertexArray;
    private List<ObjectBuilder.DrawCommand> drawList;
    private int size;
    private boolean isCreated;

    public VisualMesh() {
        size = 0;
        vertexArray = null;
        drawList = null;
        isCreated = false;
    }

    public VisualMesh(Mesh mesh) {
        size = mesh.getNEdges();
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createVisualMesh(mesh);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
        isCreated = true;
    }

    public void createMesh(Mesh mesh){
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createVisualMesh(mesh);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
        size = mesh.getNEdges();
        isCreated = true;
    }

    public void changeCoordinates(Mesh mesh) {
        float[] vertices = mesh.getVerticesRef();
        int[] connEdges = mesh.getConnEdgesRef();
        final int nMeshEdges = mesh.getNEdges();
        int beginIndex;
        float x1, y1, x2, y2;

        for (int i = 0; i < nMeshEdges; i++) {
            x1 = vertices[connEdges[i*2]*2];
            y1 = vertices[connEdges[i*2]*2+1];
            x2 = vertices[connEdges[i*2+1]*2];
            y2 = vertices[connEdges[i*2+1]*2+1];
            beginIndex = 2*i*Constants.POSITION_COMPONENT_COUNT;
            vertexArray.setValue(beginIndex + 0, x1);
            vertexArray.setValue(beginIndex + 1, y1);
            vertexArray.setValue(beginIndex + 2, 0f);
            vertexArray.setValue(beginIndex + 3, x2);
            vertexArray.setValue(beginIndex + 4, y2);
            vertexArray.setValue(beginIndex + 5, 0f);
        }
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(ColorShaderProgram colorProgram) {
        if (null != vertexArray) {
            vertexArray.setVertexAttribPointer(
                    0,
                    colorProgram.getPositionAttributeLocation(),
                    Constants.POSITION_COMPONENT_COUNT,
                    0);
        }
    }

    // Draws everything in the draw list
    public void draw() {
        if (null != drawList) {
            for (ObjectBuilder.DrawCommand drawCommand : drawList) {
                drawCommand.draw();
            }
        }
    }

    public boolean hasBeenCreated() {
        return isCreated;
    }
}
