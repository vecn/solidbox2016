package com.cimat.solidbox.OpenGL.Programs;

import android.content.Context;

import com.cimat.solidbox.R;

import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniform4f;
import static android.opengl.GLES20.glUniformMatrix4fv;

/**
 * Created by ernesto on 12/02/16.
 */
public class VaryingColorShaderProgram extends ShaderProgram {
    // Uniform locations
    private final int uMatrixLocation;

    // Attribute locations
    private final int aPositionLocation;
    private int aColorLocation;

    public VaryingColorShaderProgram(Context context) {
        super(context, R.raw.varying_vertex_shader, R.raw.varying_fragment_shader);

        // Retrieve uniform locations for the shader program.
        uMatrixLocation = glGetUniformLocation(program, U_MATRIX);

        // Retrieve attribute locations for the shader program.
        aPositionLocation = glGetAttribLocation(program, A_POSITION);
        aColorLocation = glGetAttribLocation(program, A_COLOR);
    }

    public void setUniforms(float[] matrix) {
        glUniformMatrix4fv(uMatrixLocation, 1, false, matrix, 0);
    }

    public int getPositionAttributeLocation() { return aPositionLocation; }
    public int getColorAttributeLocation() {
        return aColorLocation;
    }
}
