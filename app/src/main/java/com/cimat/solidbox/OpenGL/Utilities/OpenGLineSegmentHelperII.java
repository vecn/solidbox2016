package com.cimat.solidbox.OpenGL.Utilities;

import com.cimat.solidbox.OpenGL.Objects.ObjectBuilder;
import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.Preprocess.Geometry;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ernesto on 23/06/16.
 */

/** In this helper a line will be drawn for each
 * pair of vertices. This way the vertices do not
 * have to be in order.
 */

public class OpenGLineSegmentHelperII {
    private List<ObjectBuilder.DrawCommand> drawList;
    private VertexArray vertexArray;

    private ArrayList<Geometry.Point> vertices;
    private boolean isOK;

    private void initializeVariables() {
        drawList = new ArrayList();
        vertexArray = null;
        vertices = null;
        isOK = false;
    }

    public void clear() {
        drawList.clear();
        vertexArray = null;
        vertices = null;
        isOK = false;
    }

    public OpenGLineSegmentHelperII() {
        initializeVariables();
    }

    public void setData(ArrayList<Geometry.Point> vertices) {
        if (null == vertices) return;
        this.vertices = vertices;

        isOK = true;
        if (null == vertices) isOK = false;
        if (0 == vertices.size()) isOK = false;
    }

    public boolean buildOpenGLData(int typeOfLine) {
        if (null == vertices) return false;

        buildOpenGLDrawList(typeOfLine);
        buildOpenGLVertexArray();

        return true;
    }

    private void buildOpenGLDrawList(int typeOfLine) {
        if (null == vertices) return;
        int numberOfSides = vertices.size()/2;

        drawList.clear();
        for (int i = 0; i < numberOfSides; i++)
            Utilities.appendLineSegmentToDrawList(2*i, drawList, typeOfLine);
    }

    private void buildOpenGLVertexArray() {
        vertexArray = new VertexArray(
                new float[vertices.size()*Constants.FLOATS_PER_VERTEX]);

        setValuesVertexArray();
    }

    private void setValuesVertexArray() {
        if (null == vertices) return;
        int i = 0;
        Geometry.Point v;
        final int n = Constants.FLOATS_PER_VERTEX;
        Iterator<Geometry.Point> it = vertices.iterator();
        while (it.hasNext()) {
            v = it.next();
            vertexArray.setValue(i*n + 0, v.getX());
            vertexArray.setValue(i*n + 1, v.getY());
            vertexArray.setValue(i*n + 2, v.getZ());
            i++;
        }
    }

    public List<ObjectBuilder.DrawCommand> getDrawList() {
        return drawList;
    }
    public VertexArray getVertexArray() {
        return vertexArray;
    }

    public void updateVertexArray() {
        setValuesVertexArray();
    }
    public void updateLineType(int typeOfLine) { buildOpenGLDrawList(typeOfLine); }
}
