package com.cimat.solidbox.OpenGL.Utilities;

import android.util.Log;

/**
 * Created by ernesto on 16/06/16.
 */

public class Colors {
    public static abstract class ColorPalette {
        protected float maxValue;
        protected float minValue;
        protected final float oneSixth = 1f/6f;
        protected float myValue;

        public ColorPalette() {
            minValue = maxValue = myValue = 0f;
        }

        public void setMinMaxValues(float minValue, float maxValue){
            this.minValue = minValue;
            this.maxValue = maxValue;
            if (Math.abs(maxValue - minValue) < 1e-10){
                this.minValue = 0f;
                this.maxValue = 1f;
            }
            //Log.i("COLOR SCALE", "Setting min and max values.");
        }

        protected void calculateMyValue(int nComponents, float[] values) {
            myValue = 0f;
            for (int i = 0; i < nComponents; i++) myValue += (values[i]*values[i]);
            myValue = (float) Math.sqrt(myValue);

            myValue = (myValue - minValue) / (maxValue - minValue);
        }

        protected void calculateMyValue(float value1, float value2) {
            myValue = (float) Math.sqrt(value1*value1 + value2*value2);

            myValue = (myValue - minValue) / (maxValue - minValue);
        }

        protected void calculateMyValue(float value) {
            myValue = (value - minValue) / (maxValue - minValue);
        }

        public float getMaxValue() { return maxValue; }
        public float getMinValue() {
            return minValue;
        }

        public abstract void setValues(float value1, float value2);
        public abstract void setValues(float value);
        public abstract float getRed();
        public abstract float getGreen();
        public abstract float getBlue();

    }

    public static class ColorPaletteA extends ColorPalette {

        public ColorPaletteA() {
            super();
        }

        public void setValues(int nComponents, float[] values) {
            calculateMyValue(nComponents, values);
        }

        public void setValues(float value1, float value2) {
            calculateMyValue(value1, value2);
        }

        public void setValues(float value){ calculateMyValue(value); }

        public float getRed() {
            float color;

            if (myValue > 0.5f) color = 0f;
            else                color = 2f *(0.5f - myValue);

            return color;
        }


        public float getGreen() {
            float color;

            if      (myValue < oneSixth)        color = 0f;
            else if (myValue < 0.5f)            color = 3f * (myValue - oneSixth);
            else if (myValue < (1f - oneSixth)) color = 3f * (5f*oneSixth - myValue);
            else                                color = 0f;

            return color;
        }

        public float getBlue() {
            float color;

            if (myValue < 0.5f) color = 0f;
            else                color = 2f * (myValue - 0.5f);

            return color;
        }
    }

    public static class ColorPaletteRainbow extends ColorPalette {
        private final float[][] colors = {
                {0.00f,   0,   0, 128},
                {0.10f,   0,   0, 255},
                {0.20f,   0, 128, 255},
                {0.37f,   0, 255,  25},
                {0.50f,   0, 255,   0},
                {0.63f, 255, 255,   0},
                {0.80f, 255, 128,   0},
                {0.90f, 255,   0,   0},
                {1.00f, 100,   0,   0}
        };

        public ColorPaletteRainbow() {
            super();
            final float factor = 1f / 255f;
            for (int i = 0; i < colors.length; i++)
                for (int j = 1; j < colors[i].length; j++)
                    colors[i][j] *= factor;
        }

        private int locateLowerLimit(float x) {
            if (x > 1f) return colors.length - 2;
            if (x < 0f) return 0;
            int i = 0;
            while (i < (colors.length - 1)) {
                if ((x >= colors[i][0]) && (x <= colors[i+1][0])) break;
                i++;
            }
            if (i == (colors.length - 1)) i--;
            return i;
        }

        private float calculateColor(int colorIndex) {
            int i = locateLowerLimit(myValue);
            if (myValue > 1f) return colors[colors.length-1][colorIndex];
            if (myValue < 0f) return colors[0][colorIndex];
            return colors[i][colorIndex] + (colors[i+1][colorIndex] - colors[i][colorIndex]) /
                    (colors[i+1][0] - colors[i][0]) * (myValue - colors[i][0]);
        }

        public void setValues(float value1, float value2) {
            calculateMyValue(value1, value2);
        }
        public void setValues(float value) {
            calculateMyValue(value);
        }

        public float getRed(){
            return calculateColor(1);
        }

        public float getGreen() {
            return calculateColor(2);
        }

        public float getBlue() {
            return calculateColor(3);
        }
    }

    public static class ColorPaletteSunset extends ColorPalette {
        private final float[][] colors = {
                {0.00f,   0,   0,   0},
                {0.15f,  20,   0, 100},
                {0.30f, 100,   0, 200},
                {0.80f, 220, 100,   0},
                {1.00f, 255, 255,   0}
        };

        public ColorPaletteSunset() {
            super();
            final float factor = 1f / 255f;
            for (int i = 0; i < colors.length; i++)
                for (int j = 1; j < colors[i].length; j++)
                    colors[i][j] *= factor;
        }

        private int locateLowerLimit(float x) {
            if (x > 1f) return colors.length - 2;
            if (x < 0f) return 0;
            int i = 0;
            while (i < (colors.length - 1)) {
                if ((x >= colors[i][0]) && (x <= colors[i+1][0])) break;
                i++;
            }
            if (i == (colors.length - 1)) i--;
            return i;
        }

        private float calculateColor(int colorIndex) {
            int i = locateLowerLimit(myValue);
            if (myValue > 1f) return colors[colors.length-1][colorIndex];
            if (myValue < 0f) return colors[0][colorIndex];
            return colors[i][colorIndex] + (colors[i+1][colorIndex] - colors[i][colorIndex]) /
                    (colors[i+1][0] - colors[i][0]) * (myValue - colors[i][0]);
        }

        public void setValues(float value1, float value2) {
            calculateMyValue(value1, value2);
        }
        public void setValues(float value) {
            calculateMyValue(value);
        }

        public float getRed(){
            return calculateColor(1);
        }

        public float getGreen() {
            return calculateColor(2);
        }

        public float getBlue() {
            return calculateColor(3);
        }
    }

    public static class ColorRGB {
        protected  float R, G, B;

        public ColorRGB(){
            R = G = B = 0f;
        }

        public void setRed(float value)   { R = value; }
        public void setGreen(float value) { G = value; }
        public void setBlue(float value)  { B = value; }

        public float getRed()   { return R; }
        public float getGreen() { return G; }
        public float getBlue()  { return B; }
    }

    public static class ColorRGBT extends ColorRGB {
        private float T;

        public ColorRGBT(){
            super();
            T = 1f;
        }

        public void setTransparency(float value) { T = value; }
        public float getTransparency() { return T; }
    }
}
