package com.cimat.solidbox.OpenGL.Views;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.graphics.PointF;

import com.cimat.solidbox.OpenGL.Views.CADGLSurfaceView;
import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.PlotParameters;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
import com.cimat.solidbox.OpenGL.Utilities.TouchHelper.Tap;
import com.cimat.solidbox.OpenGL.Utilities.TouchHelper.Drag;
import com.cimat.solidbox.OpenGL.Utilities.TouchHelper.Drags;
import com.cimat.solidbox.OpenGL.Utilities.TouchHelper.Pointer;
import com.cimat.solidbox.OpenGL.Utilities.TouchHelper.Pointers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by ernesto on 17/02/16.
 */
public class ResultsGLSurfaceView extends GLSurfaceView {

    private class ImportantFlags {
        private boolean newAction;
        private boolean newPanning;

        ImportantFlags() { setAllFalse(); }

        public void setAllTrue() {
            newAction = true;
            newPanning = true;
        }

        public void setAllFalse() {
            newAction = false;
            newPanning = false;
        }
    }

    public ImportantFlags flags;
    private Context context;
    public ResultsRenderer renderer;
    private int typeOfAction;
    public boolean rendererSet;
    long referenceTime;

    // State objects and values related to gesture tracking.
    //private ScaleGestureDetector mScaleGestureDetector;
    //private GestureDetectorCompat mGestureDetector;
    private boolean scaleOnGoing;

    private ArrayList<Point> previousPointersInNDC;
    private ArrayList<Point> currentPointersInNDC;

    Pointers pointers;
    Queue<Tap> taps;
    Drags drags;

    // The gesture listener, used for handling simple gestures such as double touches, scrolls,
    // and flings
    private final GestureDetector.SimpleOnGestureListener
            mGestureListener = new GestureDetector.SimpleOnGestureListener() {

        float previousPointerPositionOnX;
        float previousPointerPositionOnY;
        float x, y;

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return true;
        }
    };

    // Gesture listener to handle two pointers on the screen
    private final ScaleGestureDetector.OnScaleGestureListener
            mScaleGestureListener = new ScaleGestureDetector.SimpleOnScaleGestureListener() {

        private float normalizedX, normalizedY;

        @Override
        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
            return true;
        }

        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            scaleOnGoing = false;
        }

    };

    public ResultsGLSurfaceView(Context context,
                                AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        Log.i("ResultsGLSurfaceView", "Step 1");

        // Request an OpenGL ES 2.0 compatible context.
        setEGLContextClientVersion(2);

        Log.i("ResultsGLSurfaceView", "Step 2");

        flags = new ImportantFlags();

        renderer = null;

        typeOfAction = Constants.DOING_NOTHING;

        flags.setAllFalse();

        referenceTime = 0;

        // Sets up interactions
        //mScaleGestureDetector = new ScaleGestureDetector(context, mScaleGestureListener);
        //mGestureDetector = new GestureDetectorCompat(context, mGestureListener);

        scaleOnGoing = false;

        previousPointersInNDC = new ArrayList();
        currentPointersInNDC = new ArrayList();

        previousPointersInNDC.add(new Point());
        previousPointersInNDC.add(new Point());
        currentPointersInNDC.add(new Point());
        currentPointersInNDC.add(new Point());

        pointers = new Pointers();
        taps = new LinkedList();
        drags = new Drags();

        Log.i("ResultsGLSurfaceView", "Step 3");

        // Render the view only when there is a change in the drawing data
        //setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        setOnTouchListener(new OnTouchListener() {
            Tap tap;
            Drag drag;
            Point p, p1, p2;
            // When a user touches that view, we'll receive a call to onTouch()
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event != null) {
                    // Adds pointers or updates them
                    //CADGLSurfaceView.handlePointer(event, v.getWidth(), v.getHeight(), pointers);

                    // Identifies the taps (quick or long, no matters) and drags
                    //CADGLSurfaceView.identifyTapsAndDrags(pointers, taps, drags);

                    // Reports all the taps
                    while (taps.size() > 0) {
                        tap = taps.poll();
                        p = tap.getPosition();
                        //renderer.handleTouchPress(p.x, p.y, Constants.DOING_NOTHING);
                    }

                    // Reports all the drags
                    //renderer.handleTouchDrags(drags);

                    // Removes the pointers and drags that have being stopped
                    pointers.removeStoppedPointers();
                    drags.removeStoppedDrags();

                    return true;
                } else {
                    return false;
                }
            }
        });

        Log.i("ResultsGLSurfaceView", "Step 4");

    }

    private float distance(float x1, float y1, float x2, float y2) {
        float dx = x1 - x2;
        float dy = y1 - y2;
        float distanceInPx = (float) Math.sqrt(dx * dx + dy * dy);
        return pxToDp(distanceInPx);
    }

    private float pxToDp(float px) {
        return px / getResources().getDisplayMetrics().density;
    }

    public void setParameters(PlotParameters plotParameters) {
        // Once we have received the data needed we can create the renderer
        renderer = new ResultsRenderer(context, plotParameters);

        // We set this renderer
        //setRenderer(renderer);
        rendererSet = true;
    }

    /*
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean retVal = mScaleGestureDetector.onTouchEvent(event);
        retVal = mGestureDetector.onTouchEvent(event) || retVal;
        return retVal || super.onTouchEvent(event);
    }
    //*/
}
