package com.cimat.solidbox.OpenGL.Objects;

/**
 * Created by ernesto on 6/01/16.
 */
import static android.opengl.GLES20.GL_LINES;
import static android.opengl.GLES20.GL_LINE_STRIP;
import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.GL_TRIANGLE_FAN;
import static android.opengl.GLES20.GL_TRIANGLE_STRIP;
import static android.opengl.GLES20.GL_TRIANGLE_STRIP;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glLineWidth;
import static com.cimat.solidbox.Utilities.Tools.largestMagnitude;
import static com.cimat.solidbox.Utilities.Tools.smallestMagnitude;
import static com.cimat.solidbox.Utilities.Tools.smallestValue;
import static com.cimat.solidbox.Utilities.Tools.largestValue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.CompletionService;

import android.util.Log;

import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.Preprocess.Axis;
import com.cimat.solidbox.OpenGL.Utilities.ColorScale;
import com.cimat.solidbox.Utilities.Preprocess.Geometry;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.SimpleCircle;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Triangle;
import com.cimat.solidbox.OpenGL.Utilities.Colors.ColorPalette;
import com.cimat.solidbox.OpenGL.Utilities.VertexArray;

import nb.geometricBot.Mesh;
import nb.pdeBot.finiteElement.MeshResults;

public class ObjectBuilder {

    // Interface to do the draw list.
    public interface DrawCommand {
        void draw();
    }

    public static class GeneratedData {
        public final float[] vertexData;
        public final List<List<DrawCommand>> drawLists;
        public float height, width;

        GeneratedData(float[] vertexData, List<DrawCommand> drawList,
                      float width, float height) {
            this.vertexData = vertexData;
            this.drawLists = new ArrayList();
            this.drawLists.add(drawList);
            this.width = width;
            this.height = height;
        }

        public List<DrawCommand> getDrawList(int index) {
            if (index < 0) return null;
            if (index > (drawLists.size() - 1)) return null;
            return drawLists.get(index);
        }

        public void addDrawList(List<DrawCommand> drawList) {
            this.drawLists.add(drawList);
        }
    }

    private final float[] vertexData;
    private List<List<DrawCommand>> drawLists;
    private int offset = 0;
    private float width, height;

    // Constructor
    private ObjectBuilder(int sizeInVertices, int floatsPerVertex) {
        vertexData = new float[sizeInVertices * floatsPerVertex];
        this.drawLists = new ArrayList();
        this.drawLists.add(new ArrayList<DrawCommand>());
    }

    public void setDrawingDimensions(float width, float height) {
        this.width = width;
        this.height = height;
    }

    static GeneratedData createVisualCircumference(Point center,
                                                   float radius,
                                                   int numPoints) {
        int size = numPoints + 1;

        ObjectBuilder builder = new ObjectBuilder(size, 3);

        SimpleCircle circle = new SimpleCircle(center,radius);

        builder.appendCircumference(circle, numPoints, 10);

        return builder.build();
    }

    static GeneratedData createVisualCircle(Point center, float radius, int numPoints) {

        int size = sizeOfCircleInVertices(numPoints);

        ObjectBuilder builder = new ObjectBuilder(size, 3);

        SimpleCircle visualCircle = new SimpleCircle(center, radius);

        builder.appendCircle(visualCircle, numPoints);

        return builder.build();

    }

    static GeneratedData createVisualCircle(Point center, float radius, int numPoints,
                                            float scaleOnX, float scaleOnY) {

        int size = sizeOfCircleInVertices(numPoints);

        ObjectBuilder builder = new ObjectBuilder(size, 3);

        SimpleCircle visualCircle = new SimpleCircle(center, radius);

        builder.appendCircle(visualCircle, numPoints, scaleOnX, scaleOnY);

        return builder.build();

    }

    static GeneratedData createVisualAxis(Axis axis) {
        int size;

        /*
        Log.i("AXIS", "Before -> Quantity on x = " + axis.mainTics.getQuantityOnX());
        Log.i("AXIS", "Before -> Quantity on y = " + axis.mainTics.getQuantityOnY());
        //*/

        // The axis lines and the tics
        size = (2 + axis.mainTics.getQuantityOnX() + axis.mainTics.getQuantityOnY())*2;
        // The grid lines
        size += ((axis.mainTics.getQuantityOnX() + axis.mainTics.getQuantityOnY())*2);

        /*
        Log.i("AXIS", "Size = " + size);

        Log.i("AXIS", "After 1 -> Quantity on x = " + axis.mainTics.getQuantityOnX());
        Log.i("AXIS", "After 1 -> Quantity on y = " + axis.mainTics.getQuantityOnY());
        //*/

        ObjectBuilder builder = new ObjectBuilder(size, 3);

        // Axis' lines
        builder.appendLine(axis.mainTics.getBeginOnX(), 0f, 0f, axis.mainTics.getEndOnX(), 0f, 0f,
                Constants.LINE_WIDTH_VISUAL_AXIS, builder.drawLists.get(0));
        builder.appendLine(0f, axis.mainTics.getBeginOnY(), 0f, 0f, axis.mainTics.getEndOnY(), 0f,
                Constants.LINE_WIDTH_VISUAL_AXIS, builder.drawLists.get(0));

        // Main tics' lines
        addTicsLinesOnX(axis.mainTics.getLength(),
                axis.mainTics.getIntervalSize(),
                axis.mainTics.getBeginOnX(),
                axis.mainTics.getQuantityOnX(),
                0f,
                Constants.LINE_WIDTH_VISUAL_AXIS,
                builder.drawLists.get(0),
                builder
        );

        addTicsLinesOnY(axis.mainTics.getLength(),
                axis.mainTics.getIntervalSize(),
                axis.mainTics.getBeginOnY(),
                axis.mainTics.getQuantityOnY(),
                0f,
                Constants.LINE_WIDTH_VISUAL_AXIS,
                builder.drawLists.get(0),
                builder
        );

        // The grid
        builder.drawLists.add(new ArrayList<DrawCommand>());
        addGridLinesOnX(
                axis.getMinY(),
                axis.getMaxY(),
                axis.mainTics.getIntervalSize(),
                axis.mainTics.getBeginOnX(),
                axis.mainTics.getQuantityOnX(),
                Constants.LINE_WIDTH_GRID,
                builder.drawLists.get(1),
                builder
        );

        addGridLinesOnY(
                axis.getMinX(),
                axis.getMaxX(),
                axis.mainTics.getIntervalSize(),
                axis.mainTics.getBeginOnY(),
                axis.mainTics.getQuantityOnY(),
                Constants.LINE_WIDTH_GRID,
                builder.drawLists.get(1),
                builder
        );

        return builder.build();
    }

    static void addGridLinesOnX(float yMin,
                                float yMax,
                                float intervalSize,
                                float beginX,
                                int numberOfTics,
                                float lineWidth,
                                List<DrawCommand> drawList,
                                ObjectBuilder builder) {
        float x1, y1, z1, x2, y2, z2;
        for (int i = 0; i < numberOfTics; i++) {
            x1 = beginX + (float)i*intervalSize;
            y1 = yMin;
            z1 = 0f;
            x2 = x1;
            y2 = yMax;
            z2 = z1;
            builder.appendLine(x1, y1, z1, x2, y2, z2, lineWidth, drawList);
        }
    }

    static void addGridLinesOnY(float xMin,
                                float xMax,
                                float intervalSize,
                                float beginY,
                                int numberOfTics,
                                float lineWidth,
                                List<DrawCommand> drawList,
                                ObjectBuilder builder) {
        float x1, y1, z1, x2, y2, z2;
        for (int i = 0; i < numberOfTics; i++) {
            x1 = xMin;
            y1 = beginY + (float)i*intervalSize;
            z1 = 0f;
            x2 = xMax;
            y2 = y1;
            z2 = z1;
            builder.appendLine(x1, y1, z1, x2, y2, z2, lineWidth, drawList);
        }
    }

    static void addTicsLinesOnX(float ticsLength,
                                float intervalSize,
                                float beginX,
                                int numberOfTics,
                                float y,
                                float lineWidth,
                                List<DrawCommand> drawList,
                                ObjectBuilder builder) {
        float x1, y1, z1, x2, y2, z2;
        for (int i = 0; i < numberOfTics; i++) {
            x1 = beginX + (float)i*intervalSize;
            y1 = y - 0.5f*ticsLength;
            z1 =  0f;
            x2 =  x1;
            y2 = -y1;
            z2 =  z1;
            builder.appendLine(x1, y1, z1, x2, y2, z2, lineWidth, drawList);
        }
    }

    static void addTicsLinesOnY(float ticsLength,
                                float intervalSize,
                                float beginY,
                                int numberOfTics,
                                float x,
                                float lineWidth,
                                List<DrawCommand> drawList,
                                ObjectBuilder builder) {
        float x1, y1, z1, x2, y2, z2;
        for (int i = 0; i < numberOfTics; i++) {
            x1 = x - 0.5f*ticsLength;
            y1 = beginY + (float)i*intervalSize;
            z1 =  0f;
            x2 = -x1;
            y2 =  y1;
            z2 =  z1;
            builder.appendLine(x1, y1, z1, x2, y2, z2, lineWidth, drawList);
        }
    }

    static GeneratedData createAxisOrientation(float centerX,
                                               float centerY,
                                               float width,
                                               float height,
                                               float scaleOnX,
                                               float scaleOnY) {
        int size;
        Point p1 = new Point();
        Point p2 = new Point();
        Point p3 = new Point();
        final double pi = 3.1416;

        float triangleBase = 0.25f*width;
        float triangleHeight = (float)Math.tan(pi/3.0) * triangleBase * 0.5f;

        size = 10;
        ObjectBuilder builder = new ObjectBuilder(size, 3);

        Triangle triangle = new Triangle();

        p1.setCoordinates(centerX  + (-width*0.5f + triangleBase*0.5f)*scaleOnX, centerY + (height*0.5f                 )*scaleOnY, 0f);
        p2.setCoordinates(centerX  + (-width*0.5f                    )*scaleOnX, centerY + (height*0.5f - triangleHeight)*scaleOnY, 0f);
        p3.setCoordinates(centerX  + (-width*0.5f + triangleBase     )*scaleOnX, centerY + (height*0.5f - triangleHeight)*scaleOnY, 0f);
        triangle.copyCoordinates(0, p1);
        triangle.copyCoordinates(1, p2);
        triangle.copyCoordinates(2, p3);
        builder.appendTriangle(triangle);

        p1.setCoordinates(centerX + (width*0.5f                 )*scaleOnX, centerY + (-height * 0.5f + triangleBase * 0.5f)*scaleOnY, 0f);
        p2.setCoordinates(centerX + (width*0.5f - triangleHeight)*scaleOnX, centerY + (-height*0.5f + triangleBase         )*scaleOnY, 0f);
        p3.setCoordinates(centerX + (width*0.5f - triangleHeight)*scaleOnX, centerY + (-height*0.5f                        )*scaleOnY, 0f);
        triangle.copyCoordinates(0, p1);
        triangle.copyCoordinates(1, p2);
        triangle.copyCoordinates(2, p3);
        builder.appendTriangle(triangle);

        builder.appendLine(
                centerX + (-width * 0.5f + triangleBase * 0.5f)*scaleOnX,
                centerY + (height * 0.5f - triangleHeight)*scaleOnY,
                0f,
                centerX + (-width * 0.5f + triangleBase * 0.5f)*scaleOnX,
                centerY + (-height * 0.5f + triangleBase * 0.5f)*scaleOnY,
                0f,
                Constants.LINE_WIDTH_AXIS_ORIENTATION
        );

        builder.appendLine(
                centerX + (-width * 0.5f + triangleBase * 0.5f)*scaleOnX,
                centerY + (-height * 0.5f + triangleBase * 0.5f)*scaleOnY,
                0f,
                centerX + (width*0.5f - triangleHeight)*scaleOnX,
                centerY + (-height*0.5f + triangleBase*0.5f)*scaleOnY,
                0f,
                Constants.LINE_WIDTH_AXIS_ORIENTATION
        );

        return builder.build();
    }

    static GeneratedData createPartiallyFixedDisplacement(float x,
                                                          float y,
                                                          float base) {
        int size;
        int numberOfSidesPerWheel = 20;
        int numberOfWheels = 3;
        float radius;
        float yIni = y;

        // Number of vertices (1 triangle + 3 wheels + 4 lines)
        size = 3 + numberOfWheels*sizeOfCircleInVertices(numberOfSidesPerWheel)
                + 4*2;

        ObjectBuilder builder = new ObjectBuilder(size, 3);

        radius = 0.2f*base;

        builder.putTheTwoLines(x, y, base);
        y -= 0.5f*base;
        builder.putTheTwoWheels(x, y, radius, base, numberOfSidesPerWheel);
        y -= 2f*radius;
        builder.mirrorTheTwoLines(x, y, base);
        y -= 0.5f*base;
        builder.putTheTriangle(x, y, base);
        y -= (float)Math.sin(Math.toRadians(60.0))*base;
        builder.putTheEndingCircle(x, y, radius, numberOfSidesPerWheel);

        builder.setDrawingDimensions(base, Math.abs(y - yIni));

        return builder.build();
    }

    static GeneratedData createTotallyFixedDisplacement(float x,
                                                        float y,
                                                        float base) {
        int size;
        int numberOfSidesPerCircle = 20;
        float radius;
        ObjectBuilder builder;
        float yIni = y;

        radius = 0.4f*base;

        // Number of vertices (1 line + 1 triangle + 1 circle)
        size = 2 + 3 + sizeOfCircleInVertices(numberOfSidesPerCircle);

        builder = new ObjectBuilder(size, 3);

        builder.appendLine(x, y, 0, x, y - 1.5f*base, 0, 5);
        y -= 1.5*base;
        builder.putTheTriangle(x, y, base);
        y -= (float)Math.sin(Math.toRadians(60.0))*base;
        builder.putTheEndingCircle(x, y, radius, numberOfSidesPerCircle);

        builder.setDrawingDimensions(base, Math.abs(y - yIni));

        return builder.build();
    }

    static GeneratedData createPartialFixedSupport(float nodeX,
                                                   float nodeY,
                                                   float base) {
        int size;
        int numberOfSidesPerWheel = 20;
        int numberOfWheels = 3;
        float spaceBetweenWheels = 0.05f*base;
        int numberOfSupportLines = 6;
        float radius;

        // Vertices(Triangle + wheels + surface line + support lines)
        size = 3 + numberOfWheels*sizeOfCircleInVertices(numberOfSidesPerWheel) +
                2 + 2*numberOfSupportLines;
        ObjectBuilder builder = new ObjectBuilder(size, 3);

        radius = 0.5f*(base/(float)numberOfWheels - spaceBetweenWheels);

        putTheTriangle(nodeX, nodeY, base, builder);
        putTheWheels(nodeX - 0.5f * base,
                nodeY - (float) Math.sin(Math.toRadians(60.0)) * base,
                radius, base, numberOfWheels, numberOfSidesPerWheel, builder);
        putSupportLines(nodeX - 0.6f*base,
                nodeY - (float)Math.sin(Math.toRadians(60.0))*base - 2f*radius,
                1.2f*base, numberOfSupportLines, builder);

        return builder.build();
    }

    static GeneratedData createFixedSupport(float nodeX,
                                            float nodeY,
                                            float base) {
        int size;
        int numberOfSupportLines = 6;

        // Vertices(Triangle + surface line + support lines)
        size = 3 + 2 + 2*numberOfSupportLines;
        ObjectBuilder builder = new ObjectBuilder(size, 3);

        putTheTriangle(nodeX, nodeY, base, builder);
        putSupportLines(nodeX - 0.6f * base, nodeY - (float) Math.sin(Math.toRadians(60.0)) * base,
                1.2f * base, numberOfSupportLines, builder);

        return builder.build();
    }

    private static void putTheTriangle(float startX,
                                       float startY,
                                       float base,
                                       ObjectBuilder builder) {
        Triangle triangle = new Triangle();
        Point point = new Point();
        float height = (float)Math.sin(Math.toRadians(60.0))*base;

        point.setCoordinates(startX, startY, 0f);
        triangle.copyCoordinates(0, point);

        point.setCoordinates(startX - 0.5f * base, startY - height, 0f);
        triangle.copyCoordinates(1, point);

        point.setCoordinates(startX + 0.5f * base, startY - height, 0f);
        triangle.copyCoordinates(2, point);

        builder.appendTriangle(triangle);
    }

    private static void putTheWheels(float startX,
                                     float startY,
                                     float radius,
                                     float base,
                                     int numberOfWheels,
                                     int numberOfSidesPerWheel,
                                     ObjectBuilder builder) {
        float distanceBetweenCenters = base/(float)numberOfWheels;
        float centerY = startY - radius;
        SimpleCircle circle = new SimpleCircle();

        circle.setRadius(radius);

        for (int i = 0; i < numberOfWheels; i++) {
            circle.setCenter(startX + (0.5f + (float)i)*distanceBetweenCenters, centerY, 0f);
            builder.appendCircle(circle, numberOfSidesPerWheel);
        }
    }

    private static void putSupportLines(float startX,
                                 float startY,
                                 float length,
                                 int numberOfSupportLines,
                                 ObjectBuilder builder) {
        float separation = length / (float) numberOfSupportLines;
        float height = 0.2f*length;

        // Line touching the wheels
        builder.appendLine(startX, startY, 0f, startX + length, startY, 0f, Constants.LINE_WIDTH_VISUAL_AXIS);

        // The lines to simulate ground under the line touching the wheels
        for (int i = 0; i < numberOfSupportLines; i++) {
            builder.appendLine(
                    startX + (float) i * separation, startY - height, 0f,
                    startX + (1f + (float) i) * separation, startY, 0f,
                    Constants.LINE_WIDTH_VISUAL_AXIS
            );
        }
    }

    private void putTheTwoLines(float startX, float startY, float base) {
        float x,  y;
        x = startX;
        y = startY;
        appendLine(x, y, 0, x, y - 0.5f*base, 0, 5);

        x = x;
        y = y - 0.5f*base;
        appendLine(x - 0.5f*base, y, 0, x + 0.5f*base, y, 0, 5);
    }

    private void mirrorTheTwoLines(float startX, float startY, float base) {
        float x,  y;
        x = startX;
        y = startY;

        appendLine(x - 0.5f*base, y, 0, x + 0.5f*base, y, 0, 5);

        y = y - 0.5f*base;
        appendLine(x, y, 0, x, y + 0.5f*base, 0, 5);
    }

    private void putTheTwoWheels(float startX,
                                 float startY,
                                 float radius,
                                 float base,
                                 int numberOfSidesPerWheel) {
        float distanceBetweenCenters = base/(float)2;
        float centerY = startY - radius;
        SimpleCircle circle = new SimpleCircle();

        circle.setRadius(radius);

        // Appends the wheels
        circle.setCenter(startX - 0.5f*distanceBetweenCenters, centerY, 0f);
        appendCircle(circle, numberOfSidesPerWheel);

        circle.setCenter(startX + 0.5f*distanceBetweenCenters, centerY, 0f);
        appendCircle(circle, numberOfSidesPerWheel);
    }

    private void putTheTriangle(float startX, float startY, float base) {
        Triangle triangle =  new Triangle();
        Point point = new Point();
        float height = (float)Math.sin(Math.toRadians(60.0))*base;

        point.setCoordinates(startX - 0.5f*base, startY, 0f);
        triangle.copyCoordinates(0, point);

        point.setCoordinates(startX, startY - height, 0f);
        triangle.copyCoordinates(1, point);

        point.setCoordinates(startX + 0.5f*base, startY, 0f);
        triangle.copyCoordinates(2, point);

        appendTriangle(triangle);
    }

    private void putTheEndingCircle(float startX,
                                    float startY,
                                    float radius,
                                    int numberOfSides) {
        SimpleCircle circle = new SimpleCircle();

        circle.setRadius(radius);
        circle.setCenter(startX, startY - radius, 0f);

        appendCircle(circle, numberOfSides);
    }

    public static GeneratedData createColorScale(ColorScale colorScale) {
        float triangleStripWidth, stripCenterX, stripCenterY;
        // Number of vertices to create the color scale using triangle strips
        int size = colorScale.getNumberOfDivisionsOnX()*
                (2*(colorScale.getNumberOfDivisionsOnY() + 1));

        // Builder
        ObjectBuilder builder = new ObjectBuilder(size, 6);

        // We have to cover all the colors along the color scale
        colorScale.colorPalette.setMinMaxValues(colorScale.getCenterY() - 0.5f*colorScale.getHeight(),
                colorScale.getCenterY() + 0.5f*colorScale.getHeight());

        // Append the triangle strips
        triangleStripWidth = colorScale.getWidth() / colorScale.getNumberOfDivisionsOnX();
        stripCenterY = colorScale.getCenterY();
        stripCenterX = colorScale.getCenterX() - 0.5f*colorScale.getWidth() + 0.5f*triangleStripWidth;
        for (int i = 0; i < colorScale.getNumberOfDivisionsOnX(); i++) {
            builder.appendTriangleStrip(stripCenterX, stripCenterY, triangleStripWidth,
                    colorScale.getHeight(), colorScale.getNumberOfDivisionsOnY() + 1,
                    colorScale.colorPalette);
            stripCenterX += triangleStripWidth;
        }

        return builder.build();
    }

    private void appendLine(float x1, float y1, float z1, float x2, float y2, float z2,
                            final float width, List<DrawCommand> drawList) {
        final int startVertex = offset / Constants.FLOATS_PER_VERTEX;
        final int numVertices = 2;

        vertexData[offset++] = x1;
        vertexData[offset++] = y1;
        vertexData[offset++] = z1;

        vertexData[offset++] = x2;
        vertexData[offset++] = y2;
        vertexData[offset++] = z2;

        drawList.add(new DrawCommand() {
            @Override
            public void draw() {
                glLineWidth(width);
                glDrawArrays(GL_LINES, startVertex, numVertices);
            }
        });
    }

    private void appendLine(float x1, float y1, float z1, float x2, float y2, float z2,
                            final float width) {
        final int startVertex = offset / Constants.FLOATS_PER_VERTEX;
        final int numVertices = 2;

        vertexData[offset++] = x1;
        vertexData[offset++] = y1;
        vertexData[offset++] = z1;

        vertexData[offset++] = x2;
        vertexData[offset++] = y2;
        vertexData[offset++] = z2;

        drawLists.get(0).add(new DrawCommand() {
            @Override
            public void draw() {
                glLineWidth(width);
                glDrawArrays(GL_LINES, startVertex, numVertices);
            }
        });
    }

    private void appendCommandToDrawLine(final int startIndex,
                                         final int numVertices,
                                         final float width,
                                         List<DrawCommand> drawList) {
        drawList.add(new DrawCommand() {
            @Override
            public void draw() {
                glLineWidth(width);
                glDrawArrays(GL_LINES, startIndex, numVertices);
            }
        });
    }

    // Number of points needed to define a fan circle
    private static int sizeOfCircleInVertices(int numPoints) {
        return 1 + (numPoints + 1);
    }

    private static int sizeOfTriangleStripInVertices(int numPoints) { return numPoints * 2; }

    // Creates the the list of points to approximate a circle in the XY plane
    private void appendCircle(SimpleCircle circle, int numPoints) {
        final int startVertex = offset / Constants.FLOATS_PER_VERTEX;
        final int numVertices = sizeOfCircleInVertices(numPoints);
        float angleInRadians;

        // Center point of fan
        vertexData[offset++] = circle.center.x;
        vertexData[offset++] = circle.center.y;
        vertexData[offset++] = circle.center.z;

        // Fan around center point. <= is used because we want to generate
        // the point at the starting angle twice to complete the fan.
        for (int i = 0; i <= numPoints; i++) {
            angleInRadians = ((float) i / (float) numPoints) * ((float) Math.PI * 2f);

            vertexData[offset++] = circle.center.x + circle.radius * (float)Math.cos(angleInRadians);
            vertexData[offset++] = circle.center.y + circle.radius * (float)Math.sin(angleInRadians);
            vertexData[offset++] = circle.center.z;
        }
        drawLists.get(0).add(new DrawCommand() {
            @Override
            public void draw() {
                glDrawArrays(GL_TRIANGLE_FAN, startVertex, numVertices);
            }
        });
    }

    private void appendCircle(SimpleCircle circle, int numPoints, float scaleOnX, float scaleOnY) {
        final int startVertex = offset / Constants.FLOATS_PER_VERTEX;
        final int numVertices = sizeOfCircleInVertices(numPoints);
        float angleInRadians;

        // Center point of fan
        vertexData[offset++] = circle.center.x;
        vertexData[offset++] = circle.center.y;
        vertexData[offset++] = circle.center.z;

        // Fan around center point. <= is used because we want to generate
        // the point at the starting angle twice to complete the fan.
        for (int i = 0; i <= numPoints; i++) {
            angleInRadians = ((float) i / (float) numPoints) * ((float) Math.PI * 2f);

            vertexData[offset++] = circle.center.x + circle.radius*(float)Math.cos(angleInRadians)*scaleOnX;
            vertexData[offset++] = circle.center.y + circle.radius*(float)Math.sin(angleInRadians)*scaleOnY;
            vertexData[offset++] = circle.center.z;
        }
        drawLists.get(0).add(new DrawCommand() {
            @Override
            public void draw() {
                glDrawArrays(GL_TRIANGLE_FAN, startVertex, numVertices);
            }
        });
    }

    private void appendCircumference(SimpleCircle circle,
                                     int numPoints,
                                     float width) {
        float angleInRadians, x, y;
        List<DrawCommand> drawList = drawLists.get(0);

        for (int i = 0; i <= numPoints; i++) {
            angleInRadians = ((float) i / (float) numPoints) * ((float) Math.PI * 2f);

            x = circle.center.x + circle.radius*(float)Math.cos(angleInRadians);
            y = circle.center.y + circle.radius*(float)Math.sin(angleInRadians);

            vertexData[offset++] = x;
            vertexData[offset++] = y;
            vertexData[offset++] = 0f;

            appendCommandToDrawLine(i, 2, width, drawList);
        }
    }

    private void appendTriangleStrip(float centerX, float centerY, float width, float height,
                                     int numPoints, ColorPalette colorPalette) {
        final int startVertex = offset / Constants.FLOATS_PER_COLOR_VERTEX;
        final int numVertices = sizeOfTriangleStripInVertices(numPoints);
        final float yStart = centerY - 0.5f*height;
        final float yEnd = centerY + 0.5f*height;
        final float xStart = centerX - 0.5f*width;
        final float xEnd = centerX + 0.5f*width;
        final float interval = height / (float)(numPoints - 1);
        float y;

        y = yStart;
        for (int i = 0; i < numPoints; i++) {
            colorPalette.setValues(y);
            vertexData[offset++] = xStart;
            vertexData[offset++] = y;
            vertexData[offset++] = 0f;
            vertexData[offset++] = colorPalette.getRed();
            vertexData[offset++] = colorPalette.getGreen();
            vertexData[offset++] = colorPalette.getBlue();

            colorPalette.setValues(y);
            vertexData[offset++] = xEnd;
            vertexData[offset++] = y;
            vertexData[offset++] = 0f;
            vertexData[offset++] = colorPalette.getRed();
            vertexData[offset++] = colorPalette.getGreen();
            vertexData[offset++] = colorPalette.getBlue();

            y += interval;
        }

        drawLists.get(0).add(new DrawCommand() {
            @Override
            public void draw() {
                // Here we use GL_TRIANGLE_STRIP to do a triangle strip
                glDrawArrays(GL_TRIANGLE_STRIP, startVertex, numVertices);
            }
        });
    }

    private void appendTriangle(Triangle triangle) {
        final int startVertex = offset / Constants.FLOATS_PER_VERTEX;
        final int numVertices = Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT;
        Point point;

        for (int i = 0; i < numVertices; i++) {
            point = triangle.getVertex(i);
            vertexData[offset++] = point.x;
            vertexData[offset++] = point.y;
            vertexData[offset++] = point.z;
        }

        drawLists.get(0).add(new DrawCommand() {
            @Override
            public void draw() {
                glDrawArrays(GL_TRIANGLES, startVertex, numVertices);
            }
        });
    }

    private void setValuesInColorPalette(final int vertexId, final char direction, final int dim,
                                         float[] values, ColorPalette colorPalette) {
        switch (direction) {
            // Values in all directions
            case Constants.ALL_COMPONENTS:
                colorPalette.setValues(
                        values[vertexId*dim],
                        values[vertexId*dim + 1]
                );
                break;
            // Values in X
            case Constants.X_COMPONENT:
                colorPalette.setValues(values[vertexId*dim]);
                break;
            // Values in Y
            case Constants.Y_COMPONENT:
                colorPalette.setValues(values[vertexId*dim + 1]);
                break;
            case Constants.ONE_COMPONENT:
                colorPalette.setValues(values[vertexId]);
                break;
        }
    }

    private float[] getMatrixToRotateAroundArbitraryAxis(float angle, Geometry.Ray axis) {
        float[] m = new float[16];
        float length = axis.vector.length();

        float a = axis.point.x;
        float b = axis.point.y;
        float c = axis.point.z;

        // Normalize the axis' vector
        float u = axis.vector.x / length;
        float v = axis.vector.y / length;
        float w = axis.vector.z / length;

        // Set some intermediate values.
        float u2 = u*u;
        float v2 = v*v;
        float w2 = w*w;
        float cosT = (float)Math.cos(angle);
        float oneMinusCosT = 1f - cosT;
        float sinT = (float)Math.sin(angle);

        // Build the matrix entries element by element.
        m[0] = u2 + (v2 + w2) * cosT;
        m[1] = u*v * oneMinusCosT - w*sinT;
        m[2] = u*w * oneMinusCosT + v*sinT;
        m[3] = (a*(v2 + w2) - u*(b*v + c*w))*oneMinusCosT + (b*w - c*v)*sinT;

        m[4] = u*v * oneMinusCosT + w*sinT;
        m[5] = v2 + (u2 + w2) * cosT;
        m[6] = v*w * oneMinusCosT - u*sinT;
        m[7] = (b*(u2 + w2) - v*(a*u + c*w))*oneMinusCosT + (c*u - a*w)*sinT;

        m[8]  = u*w * oneMinusCosT - v*sinT;
        m[9]  = v*w * oneMinusCosT + u*sinT;
        m[10] = w2 + (u2 + v2) * cosT;
        m[11] = (c*(u2 + v2) - w*(a*u + b*v))*oneMinusCosT + (a*v - b*u)*sinT;

        m[12] = 0f;
        m[13] = 0f;
        m[14] = 0f;
        m[15] = 1f;

        return m;
    }

    private void multiplySpecialMv(float[] x,
                                   float[] M,
                                   float[] v) {
        x[0] = x[1] = x[2] = x[3] = 0f;
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                x[i] += M[i*4+j]*v[j];
    }

    private GeneratedData build() {
        GeneratedData generatedData = new GeneratedData(
                vertexData, drawLists.get(0), width, height);
        for (int i = 1; i < drawLists.size(); i++)
            generatedData.addDrawList(drawLists.get(i));
        return generatedData;
    }

    // Creates the info needed by OpenGL to draw a mesh
    static GeneratedData createVisualMesh(Mesh mesh) {
        int size, nMeshEdges;
        float x1, y1, x2, y2;
        float[] vertices = mesh.getVerticesRef();
        int[] connEdges = mesh.getConnEdgesRef();

        // Total of vertices
        nMeshEdges = mesh.getNEdges();
        size = nMeshEdges * 2;

        // Builder object
        ObjectBuilder builder = new ObjectBuilder(size, 3);

        for (int i = 0; i < nMeshEdges; i++) {
            x1 = vertices[connEdges[i*2]*2];
            y1 = vertices[connEdges[i*2]*2+1];
            x2 = vertices[connEdges[i*2+1]*2];
            y2 = vertices[connEdges[i*2+1]*2+1];
            builder.appendLine(x1, y1, 0f, x2, y2, 0f, Constants.LINE_WIDTH_VISUAL_MESH);
        }

        return builder.build();
    }

    public static void appendTriangle(final int startVertex,
                                      List<DrawCommand> drawList) {
        drawList.add(new ObjectBuilder.DrawCommand() {
            @Override
            public void draw() {
                glDrawArrays(
                        GL_TRIANGLES,
                        startVertex,
                        Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT
                );
            }
        });
    }

    static GeneratedData createSolidOfRevolution(Geometry.Ray axis,
                                                 MeshResults meshResults,
                                                 int numberOfSlices,
                                                 int typeOfSurfaceResult,
                                                 ColorPalette colorPalette) {
        char direction;
        int size;
        int[] rotatingVerticesIds;
        int[][] modelSgm = meshResults.getModelSgmRef();
        float[] vertices = meshResults.getVerticesRef();
        float[] values;
        float startAngle, endAngle, angleInterval, smallestValue, largestValue;
        final int dimension = Constants.PROBLEM_DIMENSION;

        // Type of surface result
        direction = Constants.ONE_COMPONENT;
        values = null;
        smallestValue = largestValue = 0f;
        switch (typeOfSurfaceResult) {
            case Constants.DISPLACEMENTS:
                values = meshResults.getDisplacementRef();
                direction = Constants.ALL_COMPONENTS;
                smallestValue = smallestMagnitude(dimension, values);
                largestValue = largestMagnitude(dimension, values);
                break;
            case Constants.DISPLACEMENTS_ON_X:
                values = meshResults.getDisplacementRef();
                direction = Constants.X_COMPONENT;
                smallestValue = smallestValue(dimension, values, 0);
                largestValue = largestValue(dimension, values, 0);
                break;
            case Constants.DISPLACEMENTS_ON_Y:
                values = meshResults.getDisplacementRef();
                direction = Constants.Y_COMPONENT;
                smallestValue = smallestValue(dimension, values, 1);
                largestValue = largestValue(dimension, values, 1);
                break;
            case Constants.DEFORMATIONS:
                values = meshResults.getStrainRef();
                direction = Constants.ALL_COMPONENTS;
                smallestValue =smallestMagnitude(dimension, values);
                largestValue = largestMagnitude(dimension, values);
                break;
            case Constants.VON_MISES:
                values = meshResults.getVonMisesStressRef();
                direction = Constants.ONE_COMPONENT;
                smallestValue = smallestValue(values);
                largestValue = largestValue(values);
                break;
        }
        colorPalette.setMinMaxValues(smallestValue, largestValue);

        // Minimum number of slices to construct the solid
        if (numberOfSlices < 4) return null;

        // Vertices that will be rotated in order
        rotatingVerticesIds = getRotatingVerticesIds(modelSgm);

        size = numberOfSlices*2*rotatingVerticesIds.length;
        ObjectBuilder builder = new ObjectBuilder(size, 6);

        angleInterval = 2f*(float)Math.PI / (float)numberOfSlices;
        startAngle = endAngle = 2f*(float)Math.PI - 0.5f*angleInterval;
        for (int i = 0; i < numberOfSlices; i++) {
            endAngle += angleInterval;
            builder.appendSliceSurface(axis, startAngle, endAngle,
                    rotatingVerticesIds, vertices, direction, values, colorPalette);
            startAngle += angleInterval;
        }

        return builder.build();
    }

    private void appendSliceSurface(Geometry.Ray axis,
                                    float startAngle,
                                    float endAngle,
                                    int[] rotatingVerticesIds,
                                    float[] vertices,
                                    char direction,
                                    float[] values,
                                    ColorPalette colorPalette) {
        final float[] matFirstFace, matSecondFace;
        final int dim = Constants.PROBLEM_DIMENSION;

        // Matrices for the first and second faces of the slice
        matFirstFace = getMatrixToRotateAroundArbitraryAxis(startAngle, axis);
        matSecondFace = getMatrixToRotateAroundArbitraryAxis(endAngle, axis);

        // Append a triangle strip
        final int startVertex = offset / Constants.FLOATS_PER_COLOR_VERTEX;
        final int numVertices = 2*rotatingVerticesIds.length;
        float[] v = new float[4];
        float[] x = new float[4];
        float[] y = new float[4];

        for (int i = 0; i < rotatingVerticesIds.length; i++) {
            v[0] = vertices[rotatingVerticesIds[i]*dim];
            v[1] = vertices[rotatingVerticesIds[i]*dim+1];
            v[2] = 0f;
            v[3] = 1f;

            multiplySpecialMv(x, matFirstFace, v);
            multiplySpecialMv(y, matSecondFace, v);

            setValuesInColorPalette(rotatingVerticesIds[i], direction, dim, values, colorPalette);

            vertexData[offset++] = x[0];
            vertexData[offset++] = x[1];
            vertexData[offset++] = x[2];
            vertexData[offset++] = colorPalette.getRed();
            vertexData[offset++] = colorPalette.getBlue();
            vertexData[offset++] = colorPalette.getGreen();

            vertexData[offset++] = y[0];
            vertexData[offset++] = y[1];
            vertexData[offset++] = y[2];
            vertexData[offset++] = colorPalette.getRed();
            vertexData[offset++] = colorPalette.getBlue();
            vertexData[offset++] = colorPalette.getGreen();
        }

        drawLists.get(0).add(new DrawCommand() {
            @Override
            public void draw() {
                // Here we use GL_TRIANGLE_STRIP to do a triangle strip
                glDrawArrays(GL_TRIANGLE_STRIP, startVertex, numVertices);
            }
        });
    }

    static int[] getRotatingVerticesIds(int[][] modelSgm) {
        int[] previousModelEdge, currentModelEdge, rotatingVertices;
        int modelEdgeId, start, increment, sA, eA, sB, eB, count, numberOfVisitedModelEdges;
        Set<Integer> visitedModelEdgesIds = new HashSet();

        // Number of vertices that will be rotated
        count = 0;
        for (int i = 0; i < modelSgm.length; i++) count += modelSgm[i].length;

        rotatingVertices = new int[count];

        previousModelEdge = modelSgm[0];

        visitedModelEdgesIds.add(0);
        numberOfVisitedModelEdges = 1;

        // We get the edge's id next to the rotating axis
        modelEdgeId = getNextEdgeModelId(visitedModelEdgesIds, 0, modelSgm);

        count = 0;
        //while ((modelEdgeId != axisIdOnModel) && (numberOfVisitedModelEdges < modelSgm.length)) {
        while (numberOfVisitedModelEdges < modelSgm.length) {

            currentModelEdge = modelSgm[modelEdgeId];

            // Auxiliary variables to write less
            sA = 0;
            eA = previousModelEdge.length - 1;
            sB = 0;
            eB = currentModelEdge.length - 1;

            // We see which node is common to the edges and on which direction we should
            // go through the vertices in the edge
            if (previousModelEdge[sA] == currentModelEdge[sA]) {
                start = 0;
                increment = 1;
            } else if (previousModelEdge[sA] == currentModelEdge[eB]) {
                start = modelSgm[modelEdgeId].length - 1;
                increment = -1;
            } else if (previousModelEdge[eA] == currentModelEdge[sB]) {
                start = 0;
                increment = 1;
            } else {
                start = modelSgm[modelEdgeId].length - 1;
                increment = -1;
            }

            // We add the vertices ids in the right order
            for (int i = 0; i < currentModelEdge.length; i++) {
                rotatingVertices[count++] = currentModelEdge[start];
                start += increment;
            }
            numberOfVisitedModelEdges++;

            previousModelEdge = currentModelEdge;

            // We look for the next adjacent edge
            modelEdgeId = getNextEdgeModelId(visitedModelEdgesIds, modelEdgeId, modelSgm);
        }
        return rotatingVertices;
    }

    static int getNextEdgeModelId(Set<Integer> visitedEdgesIds,
                                  int currentEdgeModelId,
                                  int[][] modelSgm) {
        int nextEdgeModelId, sA, eA, sB, eB;
        int[] nextEdge;
        int[] currentEdge = modelSgm[currentEdgeModelId];

        sA = 0;
        eA = currentEdge.length - 1;
        for (nextEdgeModelId = 0; nextEdgeModelId < modelSgm.length; nextEdgeModelId++) {
            nextEdge = modelSgm[nextEdgeModelId];
            sB = 0;
            eB = nextEdge.length - 1;
            if ((!visitedEdgesIds.contains(nextEdgeModelId)) &&
                    ((currentEdge[sA] == nextEdge[sB] || (currentEdge[sA] == nextEdge[eB])) ||
                            (currentEdge[eA] == nextEdge[sB] || (currentEdge[eA] == nextEdge[eB])))) {
                visitedEdgesIds.add(nextEdgeModelId);
                break;
            }
        }

        return nextEdgeModelId;
    }

    static void createElementsForSolidOfRevolution(ArrayList<VertexArray> holderVertexArray,
                                                   ArrayList<List<ObjectBuilder.DrawCommand>> holderDrawList,
                                                   ArrayList<int[]> holderRotatingVerticesIds,
                                                   int[][] modelSgm,
                                                   int numberOfSlices) {
        final int numberOfVerticesPerSlice;
        final int propertiesPerVertex;

        // Vertices that will be rotated. We exclude the axis' vertices.
        holderRotatingVerticesIds.add(getRotatingVerticesIds(modelSgm));

        numberOfVerticesPerSlice = 2*holderRotatingVerticesIds.get(0).length;
        propertiesPerVertex = 6;

        holderVertexArray.add(new VertexArray(numberOfSlices*numberOfVerticesPerSlice*propertiesPerVertex));

        ArrayList<DrawCommand> drawList = new ArrayList<DrawCommand>();
        for (int i = 0; i < numberOfSlices; i++) {
            final int startVertex = i*numberOfVerticesPerSlice;
            drawList.add(new DrawCommand() {
                @Override
                public void draw() {
                    glDrawArrays(GL_TRIANGLE_STRIP, startVertex, numberOfVerticesPerSlice);
                }
            });
        }
        holderDrawList.add(drawList);
    }

    static void setVerticesCoordinatesSolidOfRevolution(Geometry.Ray axis,
                                                        VertexArray vertexArray,
                                                        float[] vertices,
                                                        int[] rotatingVerticesIds,
                                                        int numberOfSlices) {
        float startAngle, endAngle, angleInterval;

        // Minimum number of slices to construct the solid
        if (numberOfSlices < 4) return;

        ObjectBuilder builder = new ObjectBuilder(0, 0);

        angleInterval = 2f*(float)Math.PI / (float)numberOfSlices;
        startAngle = endAngle = 2f*(float)Math.PI - 0.5f*angleInterval;
        for (int i = 0; i < numberOfSlices; i++) {
            endAngle += angleInterval;
            builder.setVerticesCoordinatesSlice(axis, i,
                    rotatingVerticesIds.length * 2, 6, vertexArray,
                    startAngle, endAngle, rotatingVerticesIds, vertices);
            startAngle += angleInterval;
        }
    }

    private void setVerticesCoordinatesSlice(Geometry.Ray axis,
                                             int numberOfSlice,
                                             int numberOfVerticesPerSlice,
                                             int propertiesPerVertex,
                                             VertexArray vertexArray,
                                             float startAngle,
                                             float endAngle,
                                             int[] rotatingVerticesIds,
                                             float[] vertices) {
        final float[] matFirstFace, matSecondFace;
        final int dim = Constants.PROBLEM_DIMENSION;

        // Matrices for the first and second faces of the slice
        matFirstFace = getMatrixToRotateAroundArbitraryAxis(startAngle, axis);
        matSecondFace = getMatrixToRotateAroundArbitraryAxis(endAngle, axis);

        float[] v = new float[4];
        float[] x = new float[4];
        float[] y = new float[4];

        final int beginning = numberOfSlice*numberOfVerticesPerSlice*propertiesPerVertex;
        for (int i = 0; i < rotatingVerticesIds.length; i++) {
            v[0] = vertices[rotatingVerticesIds[i]*dim];
            v[1] = vertices[rotatingVerticesIds[i]*dim+1];
            v[2] = 0f;
            v[3] = 1f;

            multiplySpecialMv(x, matFirstFace, v);
            multiplySpecialMv(y, matSecondFace, v);

            vertexArray.setValue(beginning + i*6*2 + 0, x[0]);
            vertexArray.setValue(beginning + i*6*2 + 1, x[1]);
            vertexArray.setValue(beginning + i*6*2 + 2, x[2]);
            vertexArray.setValue(beginning + i*6*2 + 6, y[0]);
            vertexArray.setValue(beginning + i*6*2 + 7, y[1]);
            vertexArray.setValue(beginning + i*6*2 + 8, y[2]);
        }
    }

    static void setVerticesValuesSolidOfRevolution(VertexArray vertexArray,
                                                   char direction,
                                                   float[] values,
                                                   int numberOfSlices,
                                                   int[] rotatingVerticesIds,
                                                   ColorPalette colorPalette) {

        // Minimum number of slices to construct the solid
        if (numberOfSlices < 4) return;

        ObjectBuilder builder = new ObjectBuilder(0, 0);

        for (int i = 0; i < numberOfSlices; i++)
            builder.setVerticesValuesSlice(i, rotatingVerticesIds.length*2, 6, vertexArray,
                    rotatingVerticesIds, direction, values, colorPalette);
    }

    private void setVerticesValuesSlice(int numberOfSlice,
                                        int numberOfVerticesPerSlice,
                                        int propertiesPerVertex,
                                        VertexArray vertexArray,
                                        int[] rotatingVerticesIds,
                                        char direction,
                                        float[] values,
                                        ColorPalette colorPalette) {
        final int dim = Constants.PROBLEM_DIMENSION;

        final int beginning = numberOfSlice*numberOfVerticesPerSlice*propertiesPerVertex;
        for (int i = 0; i < rotatingVerticesIds.length; i++) {
            setValuesInColorPalette(rotatingVerticesIds[i], direction, dim, values, colorPalette);

            vertexArray.setValue(beginning + i*6*2 + 3, colorPalette.getRed());
            vertexArray.setValue(beginning + i*6*2 + 4, colorPalette.getGreen());
            vertexArray.setValue(beginning + i*6*2 + 5, colorPalette.getBlue());

            vertexArray.setValue(beginning + i*6*2 + 9,  colorPalette.getRed());
            vertexArray.setValue(beginning + i*6*2 + 10, colorPalette.getGreen());
            vertexArray.setValue(beginning + i*6*2 + 11, colorPalette.getBlue());
        }
    }

    static GeneratedData createMeshForSolidOfRevolution(Geometry.Ray axis,
                                                        MeshResults meshResults,
                                                        int numberOfSlices) {
        int size;
        int[] rotatingVerticesIds;
        int[][] modelSgm = meshResults.getModelSgmRef();
        float[] vertices = meshResults.getVerticesRef();
        float startAngle, endAngle, angleInterval;

        // Minimum number of slices to construct the solid
        if (numberOfSlices < 4) return null;

        // Vertices that will be rotated. We exclude the axis' vertices.
        rotatingVerticesIds = getRotatingVerticesIds(modelSgm);

        Log.i("SolidOfRevolution", "Rotating vertices");
        for (int i = 0; i < rotatingVerticesIds.length; i++) {
            Log.i("SolidOfRevolution", "" + i + " " + rotatingVerticesIds[i] + " " +
                    vertices[rotatingVerticesIds[i]*2+0] + " " + vertices[rotatingVerticesIds[i]*2+1]);
        }

        size = numberOfSlices*(4*(rotatingVerticesIds.length - 1) + 2);
        ObjectBuilder builder = new ObjectBuilder(size, 3);

        angleInterval = 2f*(float)Math.PI / (float)numberOfSlices;
        startAngle = endAngle = 2f*(float)Math.PI - 0.5f*angleInterval;
        for (int i = 0; i < numberOfSlices; i++) {
            endAngle += angleInterval;
            builder.appendMeshForSliceSurface(axis, startAngle,
                    endAngle,rotatingVerticesIds, vertices);
            startAngle += angleInterval;
        }

        return builder.build();
    }

    private void appendMeshForSliceSurface(Geometry.Ray axis,
                                           float startAngle,
                                           float endAngle,
                                           int[] rotatingVerticesIds,
                                           float[] vertices) {
        final float[] matFirstFace, matSecondFace;
        final int dim = Constants.PROBLEM_DIMENSION;

        // Matrices for the first and second faces of the slice
        matFirstFace = getMatrixToRotateAroundArbitraryAxis(startAngle, axis);
        matSecondFace = getMatrixToRotateAroundArbitraryAxis(endAngle, axis);

        // Append a triangle strip
        final int startVertex = offset / Constants.FLOATS_PER_VERTEX;
        final int numVertices = 2*rotatingVerticesIds.length;
        float[] v = new float[4];
        float[] x = new float[4];
        float[] y = new float[4];
        float[] z = new float[4];

        // First line
        v[0] = vertices[rotatingVerticesIds[0]*dim];
        v[1] = vertices[rotatingVerticesIds[0]*dim+1];
        v[2] = 0f;
        v[3] = 1f;

        multiplySpecialMv(x, matFirstFace, v);
        multiplySpecialMv(y, matSecondFace, v);

        appendLine(x[0], x[1], x[2], y[0], y[1], y[2], 5f);
        for (int j = 0; j < 4; j++) z[j] = x[j];

        for (int i = 1; i < rotatingVerticesIds.length; i++) {
            v[0] = vertices[rotatingVerticesIds[i]*dim];
            v[1] = vertices[rotatingVerticesIds[i]*dim+1];

            multiplySpecialMv(x, matFirstFace, v);
            multiplySpecialMv(y, matSecondFace, v);

            appendLine(z[0], z[1], z[2], x[0], x[1], x[2], 5f);
            appendLine(x[0], x[1], x[2], y[0], y[1], y[2], 5f);

            for (int j = 0; j < 4; j++) z[j] = x[j];
        }
    }
}