package com.cimat.solidbox.OpenGL.Views;

import android.content.Context;
import android.opengl.GLSurfaceView;

import com.cimat.solidbox.OpenGL.Utilities.TouchHelper;
import com.cimat.solidbox.Utilities.PlotParameters;
import com.cimat.solidbox.Utilities.Preprocess.GeometrySet;
import com.cimat.solidbox.Utilities.ProblemData;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by ernesto on 13/09/16.
 */

public class PrePostRenderer implements GLSurfaceView.Renderer {
    private boolean isCADActive;
    public CADRenderer cadRenderer;
    public ResultsRenderer resultsRenderer;

    public PrePostRenderer(Context context,
                           GeometrySet geometry,
                           PlotParameters plotParametersCAD,
                           PlotParameters plotParametersRes,
                           ProblemData problemData) {
        isCADActive = true;
        cadRenderer = new CADRenderer(context, geometry, plotParametersCAD,
                problemData);
        resultsRenderer = new ResultsRenderer(context, plotParametersRes);
    }

    public void handleTouchPress(float normalizedX, float normalizedY) {
        if (isCADActive)
            cadRenderer.handleTouchPress(normalizedX, normalizedY);
        else
            resultsRenderer.handleTouchPress(normalizedX, normalizedY);
    }

    public void handleTouchDrags(TouchHelper.Drags drags) {
        if (isCADActive)
            cadRenderer.handleTouchDrags(drags);
        else
            resultsRenderer.handleTouchDrags(drags);
    }

    public void handleDoubleTouchPress(float normalizedX, float normalizedY) {
        if (isCADActive)
            cadRenderer.handleDoubleTouchPress(normalizedX, normalizedY);
    }

    public void activeCADRenderer() {
        isCADActive = true;
    }

    public void activateResultsRenderer() {
        isCADActive = false;
    }

    @Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
        cadRenderer.onSurfaceCreated(glUnused, config);
        resultsRenderer.onSurfaceCreated(glUnused, config);
    }

    @Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
        cadRenderer.onSurfaceChanged(glUnused, width, height);
        resultsRenderer.onSurfaceChanged(glUnused, width, height);
    }

    @Override
    public void onDrawFrame(GL10 glUnused) {
        if (isCADActive) cadRenderer.onDrawFrame(glUnused);
        else             resultsRenderer.onDrawFrame(glUnused);
    }


}
