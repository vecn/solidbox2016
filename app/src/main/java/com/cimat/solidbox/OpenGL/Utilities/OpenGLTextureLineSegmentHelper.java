package com.cimat.solidbox.OpenGL.Utilities;

import com.cimat.solidbox.OpenGL.Objects.ObjectBuilder;
import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
import com.cimat.solidbox.Utilities.Tools;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ernesto on 10/05/17.
 *
 */

public class OpenGLTextureLineSegmentHelper {
    private float width;
    private float ratio;
    private List<ObjectBuilder.DrawCommand> drawList;
    private VertexArray vertexArray;
    private VertexArray textureArray;

    private ArrayList<Point> vertices;

    private ArrayList<Point> dividedLine;
    private ArrayList<Point> offset1;
    private ArrayList<Point> offset2;
    private Point firstVertexPreviousPosition;
    private boolean isClosed;

    private void initializeVariables() {
        width = 1f;
        ratio = 4f;
        drawList = new ArrayList();
        vertexArray = null;
        textureArray = null;
        vertices = null;
        dividedLine = null;
        offset1 = null;
        offset2 = null;
        firstVertexPreviousPosition = new Point(0f, 0f, 0f);
        isClosed = false;
    }

    public OpenGLTextureLineSegmentHelper() {
        initializeVariables();
    }

    public void setData(float width, ArrayList<Point> vertices,
                        float ratio,
                        boolean isClosed) {
        if (1e-3 > width) this.width = 1f;
        else              this.width = width;
        this.ratio = ratio;
        if (null != vertices) this.vertices = vertices;
        this.isClosed = isClosed;
    }

    // It has to be used when the line's size changes
    public void buildOpenGLData() {
        if (null == vertices) return;
        if (0 == vertices.size()) return;

        float division = width/ratio;
        dividedLine = Tools.divideVertices(vertices, division, isClosed);

        vertexArray = new VertexArray(
                new float[2*dividedLine.size()*Constants.POSITION_COMPONENT_COUNT]);
        textureArray = new VertexArray(
                new float[2*dividedLine.size()*Constants.TEXTURE_COORDINATES_COMPONENT_COUNT]);

        offset1 = Tools.copyVerticesCoordinates(dividedLine);
        offset2 = Tools.copyVerticesCoordinates(dividedLine);

        Tools.offsetNonClosedVertices2D(offset1,  0.5f*width);
        Tools.offsetNonClosedVertices2D(offset2, -0.5f*width);

        setValuesVertexArray();

        drawList.clear();
        Utilities.appendTriangleStripToDrawList(0, dividedLine.size()*2, drawList);

        firstVertexPreviousPosition.x = vertices.get(0).getX();
        firstVertexPreviousPosition.y = vertices.get(0).getY();
        firstVertexPreviousPosition.z = vertices.get(0).getZ();
    }

    private void setValuesVertexArray() {
        if (null == vertexArray) return;
        if (null == textureArray) return;
        if (null == offset1) return;
        if (null == offset2) return;
        //if (null == colorArray) return;

        //Tools.printToLog("---------Divided line", dividedLine);
        //Tools.printToLog("---------Offset 1", offset1);
        //Tools.printToLog("---------Offset 2", offset2);

        Iterator<Point> it1 = offset1.iterator();
        Iterator<Point> it2 = offset2.iterator();

        int count = 0;
        final int n = Constants.POSITION_COMPONENT_COUNT;
        final int m = Constants.TEXTURE_COORDINATES_COMPONENT_COUNT;
        final int c = 4;
        Point v1, v2;
        while (it1.hasNext() && it2.hasNext()) {
            v1 = it1.next();
            v2 = it2.next();

            vertexArray.setValue(2*count*n + 0, v1.getX());
            vertexArray.setValue(2*count*n + 1, v1.getY());
            vertexArray.setValue(2*count*n + 2, v1.getZ());

            vertexArray.setValue(2*count*n + 3, v2.getX());
            vertexArray.setValue(2*count*n + 4, v2.getY());
            vertexArray.setValue(2*count*n + 5, v2.getZ());

            textureArray.setValue(2*count*m + 0, count);
            textureArray.setValue(2*count*m + 1, 0f);
            textureArray.setValue(2*count*m + 2, count);
            textureArray.setValue(2*count*m + 3, 1f);

            count++;
        }
    }

    public List<ObjectBuilder.DrawCommand> getDrawList() {
        return drawList;
    }
    public VertexArray getVertexArray() {
        return vertexArray;
    }
    public VertexArray getTextureArray() { return textureArray; }

    // It can only be used when the line is moved
    public void updateVertexArray() {
        if (null == vertices) return;
        if (0 == vertices.size()) return;

        float dx = vertices.get(0).getX() - firstVertexPreviousPosition.x;
        float dy = vertices.get(0).getY() - firstVertexPreviousPosition.y;
        float dz = vertices.get(0).getZ() - firstVertexPreviousPosition.z;

        Tools.translatePoints(dx, dy, dz, dividedLine);
        Tools.translatePoints(dx, dy, dz, offset1);
        Tools.translatePoints(dx, dy, dz, offset2);

        firstVertexPreviousPosition.x = vertices.get(0).getX();
        firstVertexPreviousPosition.y = vertices.get(0).getY();
        firstVertexPreviousPosition.z = vertices.get(0).getZ();

        setValuesVertexArray();
    }
}
