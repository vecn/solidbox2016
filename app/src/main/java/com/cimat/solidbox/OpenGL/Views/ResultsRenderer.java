package com.cimat.solidbox.OpenGL.Views;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.Log;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import nb.geometricBot.Mesh;
import nb.pdeBot.finiteElement.MeshResults;

import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_DEPTH_BUFFER_BIT;
import static android.opengl.GLES20.GL_DEPTH_TEST;
import static android.opengl.GLES20.GL_LESS;
import static android.opengl.GLES20.glBlendFunc;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glDepthFunc;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glViewport;
import static android.opengl.Matrix.invertM;
import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.multiplyMV;
import static android.opengl.Matrix.orthoM;
import static android.opengl.Matrix.rotateM;
import static android.opengl.Matrix.setIdentityM;
import static android.opengl.Matrix.setLookAtM;
import static android.opengl.Matrix.translateM;
import static com.cimat.solidbox.Utilities.Preprocess.Axis.getLabel;
import static com.cimat.solidbox.Utilities.Tools.largestMagnitude;
import static com.cimat.solidbox.Utilities.Tools.smallestMagnitude;
import static com.cimat.solidbox.Utilities.Tools.smallestValue;
import static com.cimat.solidbox.Utilities.Tools.largestValue;

import com.cimat.solidbox.OpenGL.Objects.VisualObject;
import com.cimat.solidbox.OpenGL.Objects.VisualMesh;
import com.cimat.solidbox.OpenGL.Objects.VisualSurfaceResults;
import com.cimat.solidbox.OpenGL.Objects.VisualBorderShell;
import com.cimat.solidbox.OpenGL.Objects.VisualSolidOfRevolution;
import com.cimat.solidbox.OpenGL.Programs.ColorShaderProgram;
import com.cimat.solidbox.OpenGL.Programs.VaryingColorShaderProgram;
import com.cimat.solidbox.OpenGL.Programs.BatchTextProgram;
import com.cimat.solidbox.OpenGL.Utilities.Colors;
import com.cimat.solidbox.OpenGL.Utilities.GLText;
import com.cimat.solidbox.OpenGL.Utilities.ColorScale;
import com.cimat.solidbox.OpenGL.Utilities.Colors.ColorPalette;

import com.cimat.solidbox.OpenGL.Utilities.TouchHelper;
import com.cimat.solidbox.Utilities.PlotParameters;
import com.cimat.solidbox.Utilities.ProblemData;
import com.cimat.solidbox.Utilities.Constants;

import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
import com.cimat.solidbox.Utilities.Preprocess.Geometry;
import com.cimat.solidbox.Utilities.Tools;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by ernesto on 16/08/16.
 */

public class ResultsRenderer {

    private class ImportantFlags {
        public boolean dataHasBeenSet;
        public boolean showMesh;
        public boolean clipAreaChanged;
        public boolean newZooming;
        public boolean doingZooming;

        ImportantFlags() { setAllFalse(); }

        public void setAllTrue() {
            dataHasBeenSet = true;
            showMesh = true;
            clipAreaChanged = true;
            newZooming = true;
            doingZooming = true;
        }

        public void setAllFalse() {
            dataHasBeenSet = false;
            showMesh = false;
            clipAreaChanged = false;
            newZooming = false;
            doingZooming = false;
        }
    }

    private final Context context;

    private final float[] projectionMatrix = new float[16];
    private final float[] modelMatrix = new float[16];
    private final float[] rotationMatrix = new float[16];
    private final float[] viewMatrix = new float[16];
    private final float[] viewProjectionMatrix = new float[16];
    private final float[] invertedProjectionMatrix = new float[16];
    private final float[] invertedViewProjectionMatrix = new float[16];
    private final float[] invertedRotationMatrix = new float[16];
    private final float[] rotationViewProjectionMatrix = new float[16];
    private final float[] modelRotationViewProjectionMatrix = new float[16];

    private VisualMesh visualMesh;
    private VisualSurfaceResults visualSurfaceResults;
    private VisualBorderShell visualBorderShell;
    private VisualObject visualColorScale;
    private VisualObject visualSectionLines;
    private VisualSolidOfRevolution visualSolidOfRevolution;
    //private VisualObject visualSolidOfRevolution;
    private VisualObject visualMeshForSolidOfRevolution;

    // For text
    private GLText glText;

    private int typeOfSurfaceResultsToShow;

    private ColorShaderProgram colorProgram;
    private VaryingColorShaderProgram varyingColorShaderProgram;
    private BatchTextProgram batchTextShaderProgram;

    private PlotParameters plotParameters;
    private PlotParameters initialPlotParameters;
    private ProblemData problemData;
    private ColorScale colorScale;

    // Field of vision of camera in degrees
    private float angleOfVisionOfCamera;

    private ImportantFlags flags;

    private ColorPalette colorPalette;

    private MeshResults meshResults;

    // Queue with the needed updates
    private BlockingQueue<Integer> updates;

    public ResultsRenderer(Context context,
                           PlotParameters plotParameters) {
        this.context = context;
        this.plotParameters = plotParameters;
        initialPlotParameters = new PlotParameters();
        visualMesh = new VisualMesh();

        problemData = null;
        glText = null;

        visualSurfaceResults = new VisualSurfaceResults();
        visualBorderShell = new VisualBorderShell();
        visualColorScale = new VisualObject();
        visualSectionLines = new VisualObject();
        visualSolidOfRevolution = new VisualSolidOfRevolution();
        visualMeshForSolidOfRevolution = new VisualObject();

        colorScale = new ColorScale();

        typeOfSurfaceResultsToShow = Constants.NO_SURFACE_RESULT;

        angleOfVisionOfCamera = 90f;

        // Most of the flags are used to update the screen
        flags = new ImportantFlags();
        flags.setAllFalse();
        flags.newZooming = true;

        colorPalette = new Colors.ColorPaletteRainbow();
        //colorPalette = new ColorPaletteSunset();

        meshResults = null;

        updates = new LinkedBlockingQueue();

        Log.i("ColorScale", "Creating a results renderer!!!");
    }

    public void setResultsData(MeshResults meshResults,
                               ProblemData problemData) {
        this.meshResults = meshResults;
        this.problemData = problemData;
    }

    public void setTypeOfSurfaceResultsToShow(int typeOfSurfaceResultsToShow) {
        this.typeOfSurfaceResultsToShow =
                typeOfSurfaceResultsToShow;
    }

    public void handleTouchPress(float normalizedX,
                                 float normalizedY) {

    }

    public void handleTouchDrags(TouchHelper.Drags drags) {
        switch (drags.size()) {
            case 1:
                handleOneDrag(drags.getDrag(0));
                break;

            case 2:
                handleTwoDrags(drags);
                break;
        }
    }

    private void handleOneDrag(TouchHelper.Drag drag) {
        rotateScene(
                drag.getPreviousPosition().x,
                drag.getPreviousPosition().y,
                drag.getEndingPosition().x,
                drag.getEndingPosition().y
        );
    }

    public void rotateScene(float normalizedStartX, float normalizedStartY,
                            float normalizedEndX, float normalizedEndY) {

        Geometry.Vector dragVectorNDC =
                new Geometry.Vector(normalizedEndX - normalizedStartX,
                normalizedEndY - normalizedStartY, 0f);
        Geometry.Vector rotationAxis = new Geometry.Vector();
        float sign = 1;

        // Rotation axis on the XYZ original coordinate system
        if (Math.abs(dragVectorNDC.x) > 1e-5) {
            rotationAxis.y = 1f;
            rotationAxis.z = 0f;
            rotationAxis.x = -dragVectorNDC.y * rotationAxis.y / dragVectorNDC.x;
            if (dragVectorNDC.x > 0) sign = -1f;
        } else if (Math.abs(dragVectorNDC.y) > 1e-5) {
            rotationAxis.x = 1f;
            rotationAxis.z = 0f;
            rotationAxis.y = -dragVectorNDC.x * rotationAxis.x / dragVectorNDC.y;
            if (dragVectorNDC.y < 0) sign = -1f;
        } else {
            return;
        }

        // Calculates the angle
        float angleInRadians = dragVectorNDC.length() / Constants.DISTANCE_FOR_PI_RAD_ANGLE * sign;

        Tools.addRotationAroundArbitraryAxis(plotParameters.getCenterX(),
                plotParameters.getCenterY(), plotParameters.getCenterZ(),
                rotationAxis.x, rotationAxis.y, rotationAxis.z, angleInRadians, viewMatrix);
    }

    public void handleTwoDrags(TouchHelper.Drags drags) {
        Tools.scaleView(
                drags.getDrag(0).getPreviousPosition(),
                drags.getDrag(1).getPreviousPosition(),
                drags.getDrag(0).getEndingPosition(),
                drags.getDrag(1).getEndingPosition(),
                viewMatrix,
                projectionMatrix,
                plotParameters
        );
        // The color scale is change every time we zoom in or zoom out
        setDimensionsColorScale();
        visualColorScale.createColorScale(colorScale);
    }

    //@Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
        // Background color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        // OpenGL program (with a shader to draw uniform color over the triangles)
        colorProgram = new ColorShaderProgram(context);

        // OpenGL program (with a shader to draw a varying color over the triangles)
        varyingColorShaderProgram = new VaryingColorShaderProgram(context);

        batchTextShaderProgram = new BatchTextProgram();
        batchTextShaderProgram.init();

        glText = new GLText(batchTextShaderProgram, context.getAssets());
        glText.load("Roboto-Regular.ttf", Constants.FONT_SIZE, 2, 2);
    }

    //@Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
        // Set the OpenGL viewport to fill the entire surface.
        glViewport(0, 0, width, height);

        // Ortographic projection
        plotParameters.setScreenValues(width, height);
        orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                plotParameters.getMinY(), plotParameters.getMaxY(),
                plotParameters.getMinZ(), plotParameters.getMaxZ());
        /*
        MatrixHelper.perspectiveM(
                projectionMatrix,
                angleOfVisionOfCamera,
                (float)width / (float)height,
                1f,
                10f
        );
        //*/

        setLookAtM(viewMatrix, 0, 0f, 0f, 3f, 0f, 0f, 0f, 0f, 1f, 0f);

        setIdentityM(viewMatrix, 0);

        setIdentityM(rotationMatrix, 0);

        setDimensionsColorScale();

        Log.i("ColorScale", "onSurfaceChanged");
    }

    public void setDimensionsColorScale() {
        float normUnitLength = plotParameters.getUnitLengthNormalizedCoordinateSystem();
        colorScale.setCenter(
                plotParameters.getMaxX() - (Constants.COLOR_SCALE_LEFT_MARGIN +
                        0.5f * Constants.COLOR_SCALE_WIDTH) / normUnitLength,
                plotParameters.getCenterY());
        colorScale.setDimensions(Constants.COLOR_SCALE_WIDTH / normUnitLength,
                Constants.COLOR_SCALE_HEIGHT / normUnitLength);
        colorScale.setNumberOfDivisions(Constants.COLOR_SCALE_DIVISONS_ON_X,
                Constants.COLOR_SCALE_DIVISONS_ON_Y);
    }

    //@Override
    public void onDrawFrame(GL10 glUnused) {

        handleUpdates();

        glClearColor(0.9f, 0.9f, 0.9f, 1f);

        // Clear the rendering surface.
        glClear(GL_COLOR_BUFFER_BIT);

        if (!flags.dataHasBeenSet) return;

        glClear(GL_DEPTH_BUFFER_BIT);
        // Enable depth test
        glEnable(GL_DEPTH_TEST);
        // Accept fragment if it closer to the camera than the former one
        glDepthFunc(GL_LESS);

        // Update the viewProjection matrix, and create an inverted matrix for
        // touch picking.
        multiplyMM(viewProjectionMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
        //invertM(invertedViewProjectionMatrix, 0, projectionMatrix, 0);

        switch (problemData.getTypeOfProblem()) {
            case Constants.PLANE_STRESS:
                drawPlate();
                break;
            case Constants.PLANE_STRAIN:
                drawSection();
                break;
            case Constants.SOLID_OF_REVOLUTION:
                drawSolidOfRevolution();
                break;
        }
        glDisable(GL_DEPTH_TEST);

        drawColorScale();
    }

    private void drawPlate() {

        if (flags.showMesh) {
            colorProgram.useProgram();
            visualMesh.bindData(colorProgram);

            rotateScene();
            positionObjectInScene(0f, 0f, -0.5f*problemData.plate.getThickness());
            colorProgram.setUniforms(modelRotationViewProjectionMatrix, 0f, 0f, 0f);
            visualMesh.draw();

            rotateScene();
            positionObjectInScene(0f, 0f, 0.5f*problemData.plate.getThickness());
            colorProgram.setUniforms(modelRotationViewProjectionMatrix, 0f, 0f, 0f);
            visualMesh.draw();
        }

        varyingColorShaderProgram.useProgram();
        visualSurfaceResults.bindData(varyingColorShaderProgram);

        rotateScene();
        positionObjectInScene(0f, 0f, -0.5f * problemData.plate.getThickness());
        varyingColorShaderProgram.setUniforms(modelRotationViewProjectionMatrix);
        visualSurfaceResults.draw();

        rotateScene();
        positionObjectInScene(0f, 0f, 0.5f * problemData.plate.getThickness());
        varyingColorShaderProgram.setUniforms(modelRotationViewProjectionMatrix);
        visualSurfaceResults.draw();
        //*
        visualBorderShell.bindData(varyingColorShaderProgram);
        rotateScene();
        positionObjectInScene(0f, 0f, 0f);
        varyingColorShaderProgram.setUniforms(modelRotationViewProjectionMatrix);
        visualBorderShell.draw();
        //*/
    }

    private void drawSection() {

        if (flags.showMesh) {
            colorProgram.useProgram();
            visualMesh.bindData(colorProgram);

            rotateScene();
            positionObjectInScene(0f, 0f, 0);
            colorProgram.setUniforms(modelRotationViewProjectionMatrix, 0f, 0f, 0f);
            visualMesh.draw();
        }

        varyingColorShaderProgram.useProgram();
        visualSurfaceResults.bindData(varyingColorShaderProgram);
        rotateScene();
        positionObjectInScene(0f, 0f, 0f);
        varyingColorShaderProgram.setUniforms(modelRotationViewProjectionMatrix);
        visualSurfaceResults.draw();

        colorProgram.useProgram();
        visualSectionLines.bindData(colorProgram);
        rotateScene();
        positionObjectInScene(0f, 0f, 0f);
        colorProgram.setUniforms(modelRotationViewProjectionMatrix, 1f, 1f, 1f);
        visualSectionLines.draw();
    }

    private void drawSolidOfRevolution() {

        if (flags.showMesh) {
            colorProgram.useProgram();
            visualMeshForSolidOfRevolution.bindData(colorProgram);
            rotateScene();
            positionObjectInScene(0f, 0f, 0f);
            colorProgram.setUniforms(modelRotationViewProjectionMatrix, 0f, 0f, 0f);
            visualMeshForSolidOfRevolution.draw();
        }

        varyingColorShaderProgram.useProgram();
        visualSolidOfRevolution.bindData(varyingColorShaderProgram);
        rotateScene();
        positionObjectInScene(0f, 0f, 0f);
        varyingColorShaderProgram.setUniforms(modelRotationViewProjectionMatrix);
        visualSolidOfRevolution.draw();
    }

    private void drawColorScale() {

        varyingColorShaderProgram.useProgram();
        visualColorScale.bindData(varyingColorShaderProgram);

        positionColorScaleInScene();
        varyingColorShaderProgram.setUniforms(modelRotationViewProjectionMatrix);
        visualColorScale.draw();

        final float unitLength = plotParameters.getUnitLengthNormalizedCoordinateSystem();
        if (flags.dataHasBeenSet) {
            glEnable(GLES20.GL_BLEND);
            glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            glText.setScale(Constants.FONT_SIZE_ON_SCREEN / (Constants.FONT_SIZE * unitLength));
            glText.begin(0.0f, 0.0f, 0.0f, 1.0f, modelRotationViewProjectionMatrix);
            glText.draw("                               ", 0f, 1f);
            glText.draw(getLabel(Math.round(colorPalette.getMaxValue()*10000000f)/10000000f),
                    colorScale.getCenterX() - 1.5f*colorScale.getWidth(),
                    colorScale.getCenterY() + 0.5f * colorScale.getHeight());
            glText.draw(getLabel(Math.round(colorPalette.getMinValue()*10000000f)/10000000f),
                    colorScale.getCenterX() - 1.5f*colorScale.getWidth(),
                    colorScale.getCenterY() - 0.6f * colorScale.getHeight());
            glText.draw("                               ", 0f, 1f);
            glDisable(GLES20.GL_BLEND);
        }
    }

    public void drawMesh() {
        flags.showMesh = true;
    }

    public void hideMesh() {
        flags.showMesh = false;
    }

    public void changeVerticesCoordinates() {
        if (null == meshResults) return;

        switch (problemData.getTypeOfProblem()) {
            case Constants.PLANE_STRAIN:
                visualMesh.changeCoordinates(meshResults);
                visualSurfaceResults.setMesh(meshResults.getVerticesRef(), meshResults.getConnMtxRef());
                visualBorderShell.setMesh(meshResults.getVerticesRef(), meshResults.getConnEdgesRef());
                break;
            case Constants.PLANE_STRESS:
                visualMesh.changeCoordinates(meshResults);
                visualSurfaceResults.setMesh(meshResults.getVerticesRef(), meshResults.getConnMtxRef());
                visualBorderShell.setMesh(meshResults.getVerticesRef(), meshResults.getConnEdgesRef());
                break;
            case Constants.SOLID_OF_REVOLUTION:
                visualSolidOfRevolution.setVerticesCoordinates(
                        problemData.solidOfRevolution.getAxis(), meshResults.getVerticesRef());
                visualMeshForSolidOfRevolution.createMeshForSolidOfRevolution(
                        problemData.solidOfRevolution.getAxis(), meshResults,
                        problemData.solidOfRevolution.getNumberOfSlices());
                break;
        }
    }

    public void addUpdate(int resultUpdate) {
        updates.add(resultUpdate);
    }

    public void setInitialSurfaceResult() {
        if (null == meshResults) return;
        if (null == problemData) return;

        colorScale.setColorPalette(new Colors.ColorPaletteRainbow());
        Log.i("ColorScale", "numberDivX 2 = " +
                colorScale.getNumberOfDivisionsOnX() +
                " numberDivY 2 = " +
                colorScale.getNumberOfDivisionsOnY());
        visualColorScale.createColorScale(colorScale);

        switch (problemData.getTypeOfProblem()) {
            case Constants.PLANE_STRESS:
                visualBorderShell.setNumberOfElements(meshResults.getNEdges() * 2,
                        -0.5f * problemData.plate.getThickness(), 0.5f * problemData.plate.getThickness());
                visualBorderShell.setMesh(meshResults.getVerticesRef(), meshResults.getConnEdgesRef());
                // Memory for OpenGL to show the results
                visualSurfaceResults.setNumberOfElements(meshResults.getNElements());
                // Creates the triangles used to show the surface results
                visualSurfaceResults.setMesh(meshResults.getVerticesRef(), meshResults.getConnMtxRef());
                // Sets the values on each vertex of each triangle
                setSurfaceResult();
                // Creates the mesh
                visualMesh.createMesh(meshResults);
                break;

            case Constants.PLANE_STRAIN:
                //visualSectionLines.createSectionLines(modelSurface, meshResults);
                // Memory for OpenGL to show the results
                visualSurfaceResults.setNumberOfElements(meshResults.getNElements());
                // Creates the triangles used to show the surface results
                visualSurfaceResults.setMesh(meshResults.getVerticesRef(), meshResults.getConnMtxRef());
                // Sets the values on each vertex of each triangle
                setSurfaceResult();
                // Creates the mesh
                visualMesh.createMesh(meshResults);
                break;

            case Constants.SOLID_OF_REVOLUTION:
                //final int axisIdOnModel =
                //        modelSurface.getEgeIdOnModel(problemData.solidOfRevolution.getAxis());
                final int numberOfSlices = problemData.solidOfRevolution.getNumberOfSlices();
                visualSolidOfRevolution.create(meshResults.getModelSgmRef(), numberOfSlices);
                visualSolidOfRevolution.setVerticesCoordinates(
                        problemData.solidOfRevolution.getAxis(), meshResults.getVerticesRef());
                setSurfaceResult();
                visualMeshForSolidOfRevolution.createMeshForSolidOfRevolution(
                        problemData.solidOfRevolution.getAxis(), meshResults, numberOfSlices);
                break;
        }

        if (Constants.SOLID_OF_REVOLUTION == problemData.getTypeOfProblem())
            plotParameters.setDepth(meshResults, true);
        else
            plotParameters.setDepth(meshResults, false);

        // Here I have to do the copy, because now plotParameters have the right values
        initialPlotParameters.copy(plotParameters);

        orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                plotParameters.getMinY(), plotParameters.getMaxY(),
                plotParameters.getMinZ(), plotParameters.getMaxZ());

        flags.dataHasBeenSet = true;
    }

    public void setSurfaceResult() {
        if (null == meshResults) return;
        if (null == problemData) return;

        switch (typeOfSurfaceResultsToShow) {
            case Constants.DISPLACEMENTS:
                // Set min and max values for the color palette
                setMinAndMaxValuesForColorPalette(meshResults.getDisplacementRef(),
                        Constants.ALL_COMPONENTS, colorPalette);
                updateSurfaceResults(Constants.ALL_COMPONENTS, meshResults.getDisplacementRef(),
                        meshResults.getConnMtxRef(), meshResults.getConnEdgesRef(), colorPalette);
                break;

            case Constants.DISPLACEMENTS_ON_X:
                setMinAndMaxValuesForColorPalette(meshResults.getDisplacementRef(),
                        Constants.X_COMPONENT, colorPalette);
                updateSurfaceResults(Constants.X_COMPONENT, meshResults.getDisplacementRef(),
                        meshResults.getConnMtxRef(), meshResults.getConnEdgesRef(), colorPalette);
                break;

            case Constants.DISPLACEMENTS_ON_Y:
                setMinAndMaxValuesForColorPalette(meshResults.getDisplacementRef(),
                        Constants.Y_COMPONENT, colorPalette);
                updateSurfaceResults(Constants.Y_COMPONENT, meshResults.getDisplacementRef(),
                        meshResults.getConnMtxRef(), meshResults.getConnEdgesRef(), colorPalette);
                break;

            case Constants.VON_MISES:
                setMinAndMaxValuesForColorPalette(meshResults.getVonMisesStressRef(),
                        Constants.ONE_COMPONENT, colorPalette);
                updateSurfaceResults(Constants.ONE_COMPONENT, meshResults.getVonMisesStressRef(),
                        meshResults.getConnMtxRef(), meshResults.getConnEdgesRef(), colorPalette);
                break;
            case Constants.PLASTIFIED_ELEMS:
                setMinAndMaxValuesForColorPalette(meshResults.getPlasticElemsRef(),
                        Constants.ONE_COMPONENT, colorPalette);
                updateSurfaceResults(Constants.ONE_COMPONENT, meshResults.getPlasticElemsRef(),
                        meshResults.getConnMtxRef(), meshResults.getConnEdgesRef(), colorPalette);
                break;
            case Constants.DAMAGE:
                setMinAndMaxValuesForColorPalette(meshResults.getDamageRef(),
                        Constants.ONE_COMPONENT, colorPalette);
                updateSurfaceResults(Constants.ONE_COMPONENT, meshResults.getDamageRef(),
                        meshResults.getConnMtxRef(), meshResults.getConnEdgesRef(), colorPalette);
                break;
            /*
            case Constants.VON_MISES:
                visualSurfaceResults.setValues(
                        Constants.ONE_COMPONENT,
                        meshResults.getVonMisesStressRef(),
                        meshResults.getConnMtxRef(),
                        colorPalette
                );
                break;
            */
        }
    }

    private void updateSurfaceResults(char direction,
                                      float[] values,
                                      int[] connMtx,
                                      int[] connEdges,
                                      ColorPalette colorPalette) {
        switch (problemData.getTypeOfProblem()) {
            case Constants.PLANE_STRESS:
                visualSurfaceResults.setValues(
                        direction,
                        values,
                        connMtx,
                        colorPalette
                );
                visualBorderShell.setValues(
                        direction,
                        values,
                        connEdges,
                        colorPalette
                );
                break;

            case Constants.PLANE_STRAIN:
                visualSurfaceResults.setValues(
                        direction,
                        values,
                        connMtx,
                        colorPalette
                );
                break;

            case Constants.SOLID_OF_REVOLUTION:
                //*
                visualSolidOfRevolution.setVerticesValues(
                        direction,
                        values,
                        colorPalette
                );
                //*/
                break;
        }
    }

    private void positionObjectInScene(float x, float y, float z) {
        setIdentityM(modelMatrix, 0);
        translateM(modelMatrix, 0, x, y, z);
        multiplyMM(modelRotationViewProjectionMatrix, 0, rotationViewProjectionMatrix, 0, modelMatrix, 0);
    }

    private void positionColorScaleInScene() {
        for (int i = 0; i < 16; i++)
            modelRotationViewProjectionMatrix[i] = projectionMatrix[i];
    }

    private void rotateScene() {
        multiplyMM(rotationViewProjectionMatrix, 0, viewProjectionMatrix, 0, rotationMatrix, 0);
    }

    private void doNotRotateScene() {
        setIdentityM(modelMatrix, 0);
        multiplyMM(rotationViewProjectionMatrix, 0, viewProjectionMatrix, 0, modelMatrix, 0);
    }

    private void setRotationMatrix(float angle, float x, float y, float z) {
        /*
        float[] A = new float[16];
        float[] B = new float[16];
        for (int i = 0; i < 16; i++) A[i] = rotationMatrix[i];
        rotateM(B, 0, angle, x, y, z);
        multiplyMM(rotationMatrix, 0, B, 0, A, 0);
        //*/
        rotateM(rotationMatrix, 0, angle, x, y, z);

        Log.i("ROTATION", "Begin ---------------------------------");
        Log.i("ROTATION", "Last object rotation");
        Log.i("ROTATION", "Angle = " + angle);
        Log.i("ROTATION", "Axis  = " + x + "   " + y + "   " + z);
        printSquareMatrixSize4("ROTATION", "Rotation", rotationMatrix);
        Log.i("ROTATION", "End ---------------------------------");
    }

    public void clearTransformations() {
        plotParameters.copy(initialPlotParameters);
        setIdentityM(viewMatrix, 0);
        setIdentityM(rotationMatrix, 0);
        // We update the projection matrix
        orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                plotParameters.getMinY(), plotParameters.getMaxY(),
                plotParameters.getMinZ(), plotParameters.getMaxZ());
        // The color scale will change every time we zoom in or zoom out
        setDimensionsColorScale();
        visualColorScale.createColorScale(colorScale);
    }

    private void setMinAndMaxValuesForColorPalette(float[] values,
                                                   int direction,
                                                   ColorPalette colorPalette) {
        float smallestValue, largestValue;
        final int dimension = Constants.PROBLEM_DIMENSION;

        // Smallest and largest displacement
        smallestValue = largestValue = 0f;
        switch (direction){
            case Constants.ALL_COMPONENTS:
                smallestValue = smallestMagnitude(dimension, values);
                largestValue = largestMagnitude(dimension, values);
                break;
            case Constants.X_COMPONENT:
                smallestValue = smallestValue(dimension, values, 0);
                largestValue = largestValue(dimension, values, 0);
                break;
            case Constants.Y_COMPONENT:
                smallestValue = smallestValue(dimension, values, 1);
                largestValue = largestValue(dimension, values, 1);
                break;
            case Constants.ONE_COMPONENT:
                smallestValue = smallestValue(values);
                largestValue = largestValue(values);
        }
        colorPalette.setMinMaxValues(smallestValue, largestValue);
    }

    public void printPlotParameters() {
        plotParameters.print("MATRICES", "Plot parameters");
    }

    public void printMatrices() {
        printSquareMatrixSize4("MATRICES", "Projection", projectionMatrix);
        printSquareMatrixSize4("MATRICES", "View", viewMatrix);
        printSquareMatrixSize4("MATRICES", "Rotation", rotationMatrix);
        printSquareMatrixSize4("MATRICES", "Model", modelMatrix);
        printSquareMatrixSize4("MATRICES", "ModelRotationViewProjection", modelRotationViewProjectionMatrix);
    }

    public static void printSquareMatrixSize4(String TAG, String label, float[] M) {
        Log.i(TAG, "Begin --------------------------------------------------");
        Log.i(TAG, label);
        for (int j = 0; j < 4; j++) {
            Log.i(TAG, M[0*4+j] + " " + M[1*4+j] + " " + M[2*4+j] + " " + M[3*4+j]);
        }
        Log.i(TAG, "End ---------------------------------------------------");
    }

    public static Point convertNormalized2DPointToWorldSpace2DPoint(float normalizedX,
                                                                          float normalizedY,
                                                                          float[] invertedTransformationMatrix) {
        final float[] normalizedPoint = {normalizedX, normalizedY, 0, 1};
        final float[] worldSpacePoint = new float[4];
        multiplyMV(worldSpacePoint, 0, invertedTransformationMatrix, 0, normalizedPoint, 0);
        return new Point(worldSpacePoint[0], worldSpacePoint[1], worldSpacePoint[2]);
    }

    private void handleUpdates() {
        final int numberOfUpdatesToDo = 3;
        for (int i = 0; i < numberOfUpdatesToDo; i++)
            processUpdate(Tools.getNextUpdate(updates));
    }

    private void processUpdate(int update) {
        switch (update) {
            case Constants.RESULTS_UPDATE_SET_INITIAL_SURFACE_RESULT:
                setInitialSurfaceResult();
                break;

            case Constants.RESULTS_UPDATE_SET_NEW_SURFACE_RESULTS:
                setSurfaceResult();
                break;

            case Constants.RESULTS_UPDATE_SET_NEW_COORDINATES:
                changeVerticesCoordinates();
                break;
        }
    }

}
