package com.cimat.solidbox.OpenGL.Utilities;


import java.util.List;

import java.util.ArrayList;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
import com.cimat.solidbox.Utilities.Preprocess.MyModel;
import com.cimat.solidbox.OpenGL.Programs.ColorShaderProgram;
import com.cimat.solidbox.OpenGL.Objects.ObjectBuilder.DrawCommand;

import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.setIdentityM;
import static android.opengl.Matrix.translateM;

/**
 * Created by ernesto on 31/05/17.
 */

public class Mesh {
    private OpenGLSurfaceMeshHelper openGLHelper;
    private float[] translationMatrix;

    private void initializeVariables() {
        openGLHelper = new OpenGLSurfaceMeshHelper();
        translationMatrix = new float[16];
        setIdentityM(translationMatrix, 0);
    }

    public Mesh() {
        initializeVariables();
    }

    public void build(ArrayList<Point> boundary,
                      boolean isConvex) {
        if (null == boundary) return;
        openGLHelper.setData(boundary);
        openGLHelper.buildOpenGLData(isConvex, false);
        setIdentityM(translationMatrix, 0);
    }

    public void build(ArrayList<Point> boundary,
                      ArrayList<ArrayList<Point>> holes) {
        if (null == boundary) return;
        if (null == holes) return;
        openGLHelper.setData(boundary, holes);
        openGLHelper.buildOpenGLData(false, true);
        setIdentityM(translationMatrix, 0);
    }

    public void translate(float dx, float dy, float dz) {
        translateM(translationMatrix, 0, dx, dy, dz);
    }

    public void draw(ColorShaderProgram colorProgram,
                     float[] viewProjectionMatrix,
                     float[] modelViewProjectionMatrix,
                     Colors.ColorRGB color,
                     float depth) {
        float[] aux = new float[16];

        colorProgram.useProgram();
        Utilities.bindData(colorProgram, openGLHelper.getVertexArray());
        multiplyMM(modelViewProjectionMatrix, 0,
                viewProjectionMatrix, 0, translationMatrix, 0);

        for (int i = 0; i < 16; i++)
            aux[i] = modelViewProjectionMatrix[i];
        Utilities.positionObjectInScene(0, 0, depth, aux,
                modelViewProjectionMatrix);

        colorProgram.setUniforms(modelViewProjectionMatrix,
                color.getRed(), color.getGreen(), color.getBlue());

        List<DrawCommand> drawList = openGLHelper.getDrawList();
        for (DrawCommand drawCommand : drawList)
            drawCommand.draw();
    }

    public MyModel getModel() {
        return openGLHelper.getModel();
    }
}
