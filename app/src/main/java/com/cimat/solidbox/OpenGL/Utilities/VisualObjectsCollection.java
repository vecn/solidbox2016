package com.cimat.solidbox.OpenGL.Utilities;

import com.cimat.solidbox.OpenGL.Objects.VisualObject;

/**
 * Created by ernesto on 5/09/16.
 */

public class VisualObjectsCollection {
    private VisualObject interactivePoint;
    private VisualObject interactivePointSelected;
    private VisualObject fixedSupport;
    private VisualObject partialFixedSupport;
    private VisualObject vertex;
    private VisualObject supportWheel;

    private void initializeVariables() {
        interactivePoint = new VisualObject();
        interactivePointSelected = new VisualObject();
        fixedSupport = new VisualObject();
        partialFixedSupport = new VisualObject();
        vertex = new VisualObject();
        supportWheel = new VisualObject();
    }

    public VisualObjectsCollection() {
        initializeVariables();
    }

    public void setInteractivePoint(final float r,
                                    final int n) {
        interactivePoint.createVisualCircle(r, n);
    }

    public void setInteractivePointSelected(final float r,
                                            final int n) {
        interactivePointSelected.createVisualCircumference(r, n);
    }

    public void setVertex(final float r,
                          final int n) {
        vertex.createVisualCircle(r, n);
    }

    public void setPartialFixedSupport(final float baseLength) {
        partialFixedSupport.createPartialFixedSupport(0f, 0f, baseLength);
    }

    public void setFixedSupport(final float baseLength) {
        fixedSupport.createFixedSupport(0f, 0f, baseLength);
    }

    public void setSupportWheel(final int r,
                                final int n) {
        supportWheel.createVisualCircle(r, n);
    }

    public VisualObject getInteractivePoint() {
        return interactivePoint;
    }

    public VisualObject getInteractivePointSelected() {
        return interactivePointSelected;
    }

    public VisualObject getFixedSupport() {
        return fixedSupport;
    }

    public VisualObject getPartialFixedSupport() {
        return partialFixedSupport;
    }

    public VisualObject getVertex() {
        return vertex;
    }

    public VisualObject getSupportWheel() { return supportWheel; }
}
