package com.cimat.solidbox.OpenGL.Objects;

import com.cimat.solidbox.OpenGL.Programs.ColorShaderProgram;
import com.cimat.solidbox.OpenGL.Programs.VaryingColorShaderProgram;
import com.cimat.solidbox.OpenGL.Utilities.ColorScale;
import com.cimat.solidbox.OpenGL.Utilities.Colors;
import com.cimat.solidbox.Utilities.Constants;

import com.cimat.solidbox.OpenGL.Utilities.VertexArray;
import com.cimat.solidbox.Utilities.Preprocess.Geometry;

import java.util.List;

import nb.pdeBot.finiteElement.MeshResults;

/**
 * Created by ernesto on 2/03/16.
 */
public class VisualObject {

    private VertexArray vertexArray;
    private List<ObjectBuilder.DrawCommand> drawList;
    private float height, width;

    public VisualObject() {
        vertexArray = null;
        drawList = null;
        height = width = 0f;
    }

    public float getHeight() { return height; }
    public float getWidth() { return width; }

    public void createVisualCircumference(float radius, int numPoints) {
        ObjectBuilder.GeneratedData generatedData =
                ObjectBuilder.createVisualCircumference(
                        new Geometry.Point(0f, 0f, 0f),
                        radius,
                        numPoints
                );
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void createVisualCircle(float radius, int numPoints) {
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createVisualCircle(
                new Geometry.Point(0f, 0f, 0f),
                radius,
                numPoints);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void createVisualCircle(Geometry.Point center,
                                   float radius,
                                   int numPoints,
                                   float scaleOnX,
                                   float scaleOnY) {
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createVisualCircle(
                center,
                radius,
                numPoints,
                scaleOnX,
                scaleOnY);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void createPartialFixedSupport(float nodeX, float nodeY, float base) {
        ObjectBuilder.GeneratedData generatedData
                = ObjectBuilder.createPartialFixedSupport(nodeX, nodeY, base);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void createFixedSupport(float nodeX, float nodeY, float base) {
        ObjectBuilder.GeneratedData generatedData
                = ObjectBuilder.createFixedSupport(nodeX, nodeY, base);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void createTotallyFixedDisplacement(float x, float y, float base) {
        ObjectBuilder.GeneratedData generatedData
                = ObjectBuilder.createTotallyFixedDisplacement(x, y, base);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
        width = generatedData.width;
        height = generatedData.height;
    }

    public void createPartiallyFixedDisplacement(float x, float y, float base) {
        ObjectBuilder.GeneratedData generatedData
                = ObjectBuilder.createPartiallyFixedDisplacement(x, y, base);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
        width = generatedData.width;
        height = generatedData.height;
    }

    public void createColorScale(ColorScale colorScale) {
        ObjectBuilder.GeneratedData generatedData
                = ObjectBuilder.createColorScale(colorScale);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    /*
    public void createSectionLines(Geometry.Surface surface, MeshResults meshResults) {
        ObjectBuilder.GeneratedData generatedData =
                ObjectBuilder.createSectionLines(surface, meshResults);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }
    //*/

    //*
    public void createSolidOfRevolution(Geometry.Ray axis,
                                        MeshResults meshResults,
                                        int numberOfSlices,
                                        int typeOfSurfaceResult,
                                        Colors.ColorPalette colorPalette) {
        ObjectBuilder.GeneratedData generatedData =
               ObjectBuilder.createSolidOfRevolution(axis, meshResults, numberOfSlices,
                       typeOfSurfaceResult, colorPalette);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void createMeshForSolidOfRevolution(Geometry.Ray axis,
                                               MeshResults meshResults,
                                               int numberOfSlices) {
        ObjectBuilder.GeneratedData generatedData =
                ObjectBuilder.createMeshForSolidOfRevolution(axis, meshResults, numberOfSlices);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }
    //*/

    /*
    public void createForces(Points points, float unitLengthNDC, float maxForce) {
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createForces(points, unitLengthNDC, maxForce);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void updateForces(Points points, float unitLengthNDC, float maxForce) {
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createForces(points, unitLengthNDC, maxForce);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void createForces(Lines lines, float unitLengthNDC, float maxForce) {
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createForces(lines, unitLengthNDC, maxForce);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void updateForces(Lines lines, float unitLengthNDC, float maxForce) {
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createForces(lines, unitLengthNDC, maxForce);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }
    //*/

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(ColorShaderProgram colorProgram) {
        if (null != vertexArray) {
            vertexArray.setVertexAttribPointer(
                    0,
                    colorProgram.getPositionAttributeLocation(),
                    Constants.POSITION_COMPONENT_COUNT,
                    0);
        }
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(VaryingColorShaderProgram varyingColorProgram) {
        int propertiesPerVertex = Constants.POSITION_COMPONENT_COUNT + Constants.COLOR_COMPONENT_COUNT;
        int STRIDE = propertiesPerVertex * Constants.BYTES_PER_FLOAT;
        if (null != vertexArray) {
            // To read the position
            vertexArray.setVertexAttribPointer(
                    0,
                    varyingColorProgram.getPositionAttributeLocation(),
                    Constants.POSITION_COMPONENT_COUNT,
                    STRIDE);
            // To read the color of each vertex
            vertexArray.setVertexAttribPointer(
                    Constants.POSITION_COMPONENT_COUNT,
                    varyingColorProgram.getColorAttributeLocation(),
                    Constants.COLOR_COMPONENT_COUNT,
                    STRIDE);
        }
    }

    // Draws everything in the draw list
    public void draw() {
        if (null != drawList) {
            for (ObjectBuilder.DrawCommand drawCommand : drawList) {
                drawCommand.draw();
            }
        }
    }
}
