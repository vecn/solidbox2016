package com.cimat.solidbox.OpenGL.Views;

import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_DEPTH_BUFFER_BIT;
import static android.opengl.GLES20.GL_DEPTH_TEST;
import static android.opengl.GLES20.GL_LEQUAL;
import static android.opengl.GLES20.GL_LESS;
import static android.opengl.GLES20.glBlendFunc;
import static android.opengl.GLES20.glCheckFramebufferStatus;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glDepthFunc;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glViewport;
import static android.opengl.Matrix.invertM;
import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.orthoM;
import static android.opengl.Matrix.rotateM;
import static android.opengl.Matrix.setIdentityM;
import static android.opengl.Matrix.setLookAtM;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import com.cimat.solidbox.OpenGL.Programs.TextShaderProgram;
import com.cimat.solidbox.OpenGL.Programs.TextureShaderProgram;
import com.cimat.solidbox.OpenGL.Utilities.Utilities;
import com.cimat.solidbox.OpenGL.Utilities.VisualObjectsCollection;
import com.cimat.solidbox.R;
import com.cimat.solidbox.Utilities.Commands;
import com.cimat.solidbox.Utilities.LoggerConfig;
import com.cimat.solidbox.Utilities.PlotParameters;
import com.cimat.solidbox.Utilities.ProblemData;
import com.cimat.solidbox.OpenGL.Objects.AxisOrientation;
import com.cimat.solidbox.OpenGL.Objects.VisualAxis;

import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.Constants.BooleanOperation;
import com.cimat.solidbox.Utilities.Constants.CADOperation;
import com.cimat.solidbox.Utilities.Constants.ShapeType;
import com.cimat.solidbox.Utilities.Constants.CADShape;
import com.cimat.solidbox.Utilities.Preprocess.Geometry;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.Line;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.RegularPolygon;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.Circle;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.InteractivePoint;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Shape;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.Vertex;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.LinearShape;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.SurfaceShape;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.BooleanBuiltShape;
import com.cimat.solidbox.Utilities.Preprocess.Conditions;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Zoom;
import com.cimat.solidbox.Utilities.Preprocess.Axis;
import com.cimat.solidbox.Utilities.Preprocess.Conditions.*;
import com.cimat.solidbox.Utilities.Preprocess.GeometrySet;

import com.cimat.solidbox.OpenGL.Objects.VisualObject;
import com.cimat.solidbox.OpenGL.Utilities.GLText;
import com.cimat.solidbox.OpenGL.Utilities.TouchHelper.Drag;
import com.cimat.solidbox.OpenGL.Utilities.TouchHelper.Drags;
import com.cimat.solidbox.OpenGL.Programs.BatchTextProgram;
import com.cimat.solidbox.OpenGL.Programs.ColorShaderProgram;
import com.cimat.solidbox.Utilities.Tools;
import com.cimat.solidbox.Utilities.UndoRedo;
import com.cimat.solidbox.OpenGL.Utilities.TextureHelper;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class CADRenderer {

    private class ImportantFlags {
        // Variables to handle the type of drawing
        public boolean anActionHasBeenExecuted;
        public boolean aSurfaceHasBeenAdded;
        public boolean magneticGrid;
        public boolean newDrag;
        public boolean newZooming;
        public boolean doingZooming;
        public boolean zoomingFinished;
        public boolean showGrid;
        public boolean newExtrude;

        ImportantFlags() { }

        public void setAllTrue() {
            anActionHasBeenExecuted = true;

            aSurfaceHasBeenAdded = true;
            magneticGrid = true;
            newDrag = true;
            newZooming = true;
            doingZooming = true;
            zoomingFinished = true;
            showGrid = true;
            newExtrude = true;
        }

        public void setAllFalse() {
            anActionHasBeenExecuted = false;
            aSurfaceHasBeenAdded = false;
            magneticGrid = false;
            newDrag = false;
            newZooming = false;
            doingZooming = false;
            zoomingFinished = false;
            showGrid = false;
            newExtrude = false;
        }
    }

    private final Context context;

    // Matrices for OpenGL
    private final float[] projectionMatrix = new float[16];
    private final float[] modelMatrix = new float[16];
    private final float[] viewMatrix = new float[16];
    private final float[] viewProjectionMatrix = new float[16];
    private final float[] invertedViewProjectionMatrix = new float[16];
    private final float[] modelViewProjectionMatrix = new float[16];
    private final float[] aux = new float[16];

    // Objects that can be drawn on the screen
    private AxisOrientation axisOrientation;
    private VisualObject deletionArea;
    private VisualAxis visualAxis;
    private VisualObjectsCollection visualObjects;

    private VisualObject visualForces;
    private VisualObject visualLinearForces;

    private BatchTextProgram batchTextShaderProgram;

    // Flags used to update the screen
    private ImportantFlags flags;

    // OpenGL programs
    private ColorShaderProgram colorProgram;
    private TextureShaderProgram textureProgram;

    // Geometry (contains the set of nodes, lines and surfaces)
    private GeometrySet geometry;

    // Axis (the x and y axis and the grid)
    Axis axis;

    // Queue used when a line is being drawn
    private Queue<Point> pointsToDrawLines;

    // Current selected shape
    private Shape currentSelectedShape;

    // Selected shapes
    private Queue<Shape> selectedShapes;

    // Queue used when drawing a predefined figure (rectangle, circle)
    private Queue<Point> pointsToDrawShape;

    // Previous and current action done by the user (action means that the user pressed a button
    // to do something)
    private CADOperation previousAction;
    private CADOperation currentAction;

    // Previous and current event done by the user (an event means if the user did a touch or
    // a drag on the screen)
    private int previousEvent;
    private int currentEvent;

    private Conditions.Force currentForce;
    private Conditions.FixedDisplacement currentFixedDisplacement;

    // To handle when zooming
    private Zoom zoom;
    private Commands.ZoomCommand zoomCommand;

    // To extrude lines
    //private ExtrudeLine extrudeLine;

    // Colors to draw the nodes
    private float[] colorNode;
    private float[] colorSelectedNode;

    // Colors to draw lines
    private float[] colorLine;
    private float[] colorSelectedLine;

    // Plot parameters
    private PlotParameters plotParameters;

    // For text on the screen (labels, titles, etc)
    private GLText glText;

    // Problem data
    private ProblemData problemData;

    // Some important values
    private float largestForce;
    private int numberOfSidesRegularPolygon;
    private int numberOfDivisions;

    // Command stacks
    private UndoRedo undoRedo;

    // Queue with the needed updates
    private BlockingQueue<Integer> updates;

    // Color to draw the surfaces
    private float[] colorSurface;

    // To identify screen orientation
    private float width, height;

    // Line textures
    private Map<Constants.TextureName, Utilities.Texture> texturesID;

    public CADRenderer(Context context,
                       GeometrySet geometry,
                       PlotParameters plotParameters,
                       ProblemData problemData) {
        this.context = context;

        // Those will contain all the data for the problem
        this.geometry = geometry;
        this.plotParameters = plotParameters;
        this.problemData = problemData;

        // Auxiliary collections
        pointsToDrawLines = new LinkedList();
        currentSelectedShape = null;
        selectedShapes = new LinkedList();
        pointsToDrawShape = new LinkedList();

        // Flags
        flags = new ImportantFlags();
        flags.setAllFalse();
        flags.showGrid = true;
        flags.newDrag = true;
        flags.newZooming = true;
        flags.newExtrude = true;

        // Actions
        previousAction = currentAction = CADOperation.DOING_NOTHING;
        previousEvent = currentEvent = Constants.INITIAL_VALUE_INT;

        // Zoom handler
        zoom = new Zoom();
        zoomCommand = new Commands.ZoomCommand();

        // Objects to impose boundary conditions
        this.currentForce = new Force();
        this.currentFixedDisplacement = new FixedDisplacement();

        // Colors
        setColors();

        // Auxiliary axis
        axisOrientation = null;
        deletionArea = null;

        // Visual objects
        visualObjects = new VisualObjectsCollection();

        // To render text
        glText = null;

        // Important values
        largestForce = 0f;
        numberOfSidesRegularPolygon = 5;
        numberOfDivisions = 1;

        // Queue updates
        updates = new LinkedBlockingQueue();

        // Command stack
        undoRedo = new UndoRedo();

        // Screen's size
        width = height = 0f;

        // Line textures
        texturesID = new HashMap<>();
    }

    private void setColors() {
        // Color for nodes
        colorNode = new float[3];
        colorSelectedNode = new float[3];
        colorNode[0] = 0.7f;
        colorNode[1] = 0f;
        colorNode[2] = 0f;
        colorSelectedNode[0] = 1f;
        colorSelectedNode[1] = 0.1f;
        colorSelectedNode[2] = 0.1f;

        // Color for lines
        colorLine = new float[3];
        colorSelectedLine = new float[3];
        colorLine[0] = 0f;
        colorLine[1] = 0.7f;
        colorLine[2] = 0f;
        colorSelectedLine[0] = 0.8f;
        colorSelectedLine[1] = 1f;
        colorSelectedLine[2] = 0.8f;
        colorSelectedLine[2] = 0.8f;

        // Color for the surfaces
        colorSurface = new float[3];
        colorSurface[0] = 1f;
        colorSurface[1] = 0f;
        colorSurface[2] = 0f;
    }

    public void setCurrentAction(CADOperation cadOperation) {
        // Saves the last action
        this.previousAction = this.currentAction;
        // Records what type of action is being executed
        this.currentAction = cadOperation;
    }

    public void switchStatusMagneticGrid() {
        if (flags.magneticGrid) flags.magneticGrid = false;
        else                    flags.magneticGrid = true;
    }

    public void setSurfaceColor(float r, float g, float b) {
        colorSurface[0] = r;
        colorSurface[1] = g;
        colorSurface[2] = b;
    }

    public boolean isInDeletionArea(Point pInNDC) {
        if (Math.abs(width) < 1e-10 || Math.abs(height) < 1e-10)
            return false;

        float dx = pInNDC.x - Constants.CENTER_DELETION_AREA[0];
        float dy = pInNDC.y - Constants.CENTER_DELETION_AREA[1];

        float scaleOnX = 1f;
        float scaleOnY = 1f;
        float aspectRatio = height / width;

        if (width > height) {
            // Landscape
            scaleOnX = aspectRatio;
        } else {
            // Portrait
            scaleOnY = aspectRatio;
        }

        dx *= scaleOnX;
        dy *= scaleOnY;
        if ((float)Math.sqrt(dx*dx + dy*dy) <= Constants.RADIUS_DELETION_AREA) return true;
        return false;
    }

    private Point processPoint(float normalizedX,
                                     float normalizedY,
                                     float[] invertedViewProjectionMatrix) {
        Point worldSpacePoint;
        Point gridPoint;
        Point finalPoint;

        // Converts to worls-space coordinates
        worldSpacePoint = Utilities.convertNormalized2DPointToWorldSpace2DPoint(
                normalizedX,
                normalizedY,
                invertedViewProjectionMatrix
        );
        finalPoint = worldSpacePoint;

        // Handles the magnetic grid
        if (flags.magneticGrid) {
            gridPoint = getPointOnTheGrid(worldSpacePoint.x, worldSpacePoint.y);
            finalPoint = gridPoint;
        }

        return finalPoint;
    }

    public void handleDoubleTouchPress(float normalizedX, float normalizedY) {
        Log.i("GESTURES", "Double tap");

        Iterator<Shape> it = selectedShapes.iterator();
        while (!selectedShapes.isEmpty()) selectedShapes.poll().unSelect();

        Point touchPointWSC, finalPointWSC;
        Shape shape;

        touchPointWSC = processPoint(normalizedX, normalizedY, invertedViewProjectionMatrix);

        //Log.i("SELECTION", "Detecting a boolean built shape!!");
        shape = detectSelectedBooleanBuiltShape(touchPointWSC.x, touchPointWSC.y);

        if (null != shape) {
            undoRedo.insertInUndoRedoForSplitBooleanBuiltShape(shape, geometry);
            // Shows the smoke wall behind the sons...
            // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }
    }

    public void handleTouchPress(float normalizedX, float normalizedY) {
        //Vertex vertex;
        LinearShape linearShape;
        SurfaceShape surface;
        Shape shape;
        //Geometry.LineSegment lineSegment;
        Point p1, p2, touchPointWSC, finalPointWSC;
        Iterator<Shape> itShape;

        // If the current and last action are different we
        // may have to do something
        if (previousAction != CADOperation.DOING_NOTHING)
            manageChangeInActions();

        manageEvents(Constants.EVENT_PRESS);

        touchPointWSC = processPoint(normalizedX, normalizedY, invertedViewProjectionMatrix);
        finalPointWSC = Utilities.convertNormalized2DPointToWorldSpace2DPoint(normalizedX,
                normalizedY, invertedViewProjectionMatrix);

        Log.i("Touches", "Here one touch!!");
        Log.i("Touches", "Current action = " + currentAction);

        // Actions
        switch (currentAction) {

            case DRAWING_A_RECTANGLE:
                // We use the finalPoints to draw the shape because it may be on the magnetic grid.
                pointsToDrawShape.add(new Point(touchPointWSC.x, touchPointWSC.y, touchPointWSC.z));

                if (pointsToDrawShape.size() == 2) {
                    p1 = pointsToDrawShape.poll();
                    p2 = pointsToDrawShape.poll();
                    undoRedo.insertInUndoRedoForCreateRectangle(
                            p1.x, p1.y, p2.x, p2.y, geometry);
                    currentAction = CADOperation.DOING_NOTHING;
                }
                break;

            case DRAWING_A_REGULAR_POLYGON:
                // We use the finalPoints to draw the shape because it may be on the magnetic grid.
                pointsToDrawShape.add(new Point(touchPointWSC.x, touchPointWSC.y, touchPointWSC.z));

                if (pointsToDrawShape.size() == 2) {
                    p1 = pointsToDrawShape.poll();
                    p2 = pointsToDrawShape.poll();
                    undoRedo.insertInUndoRedoForCreateRegularPolygon(
                            numberOfSidesRegularPolygon, p1.x, p1.y, p2.x, p2.y, geometry);
                    currentAction = CADOperation.DOING_NOTHING;
                }
                break;

            case DRAWING_A_CIRCLE:
                // We use the finalPoints to draw the shape because it may be on the magnetic grid.
                pointsToDrawShape.add(new Point(touchPointWSC.x, touchPointWSC.y, touchPointWSC.z));
                if (pointsToDrawShape.size() == 2) {
                    p1 = pointsToDrawShape.poll();
                    p2 = pointsToDrawShape.poll();
                    undoRedo.insertInUndoRedoForCreateCircle(p1.x, p1.y, p2.x, p2.y,
                            geometry);
                    currentAction = CADOperation.DOING_NOTHING;
                }
                break;

            case SELECTING_ELEMENT_TO_FIX_MATERIAL:
                surface = detectSelectedSurface(touchPointWSC.x, touchPointWSC.y);
                if (null != surface) {
                    undoRedo.insertInUndoRedoForApplyColorToElement(
                            colorSurface[0],
                            colorSurface[1],
                            colorSurface[2],
                            surface
                    );
                    currentAction = CADOperation.DOING_NOTHING;
                }
                break;

            /*
            case SELECTING_ELEMENT_TO_APPLY_FORCE:
                vertex = detectSelectedVertex(touchPointWSC.x, touchPointWSC.y);
                if (null != vertex) {
                    undoRedo.insertInUndoRedoForApplyForceOnElement(
                            vertex, currentForce, geometry);
                } else {
                    linearShape =
                            detectSelectedLinearShape(touchPointWSC.x, touchPointWSC.y);
                    if (null != linearShape) {
                        if (linearShape.canBeSplitIntoLineSegments()) {
                            lineSegment =
                                    detectLineSegment(touchPointWSC.x, touchPointWSC.y, linearShape);
                            if (null != lineSegment)
                                undoRedo.insertInUndoRedoForApplyForceOnElement(
                                        lineSegment, currentForce, geometry);
                            else
                                currentAction = CADOperation.DOING_NOTHING;
                        } else {
                            undoRedo.insertInUndoRedoForApplyForceOnElement(
                                    linearShape, currentForce, geometry);
                        }
                    } else {
                        currentAction = CADOperation.DOING_NOTHING;
                    }
                }

                break;

            case SELECTING_ELEMENT_TO_FIX_DISPLACEMENT:
                vertex = detectSelectedVertex(touchPointWSC.x, touchPointWSC.y);
                if (null != vertex) {
                    undoRedo.insertInUndoRedoForApplyDisplacementOnElement(
                            vertex, currentFixedDisplacement, geometry);
                } else {
                    // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    currentFixedDisplacement.fixNormal(0);
                    currentFixedDisplacement.fixTangent(0);
                    linearShape =
                            detectSelectedLinearShape(touchPointWSC.x, touchPointWSC.y);
                    if (null != linearShape) {
                        if (linearShape.canBeSplitIntoLineSegments()) {
                            lineSegment =
                                    detectLineSegment(touchPointWSC.x, touchPointWSC.y, linearShape);
                            if (null != lineSegment)
                                undoRedo.insertInUndoRedoForApplyDisplacementOnElement(
                                        lineSegment, currentFixedDisplacement, geometry);
                            else
                                currentAction = CADOperation.DOING_NOTHING;
                        } else {
                            undoRedo.insertInUndoRedoForApplyDisplacementOnElement(
                                    linearShape, currentFixedDisplacement, geometry);
                        }
                    } else {
                        currentAction = CADOperation.DOING_NOTHING;
                    }
                }
                break;
            //*/

            default:
                if (LoggerConfig.ON) {
                    Log.i("Touches", "Selecting something");
                }

                if (CADOperation.isABooleanOperation(currentAction)) {
                    surface = detectSelectedSurface(touchPointWSC.x,touchPointWSC.y);
                    if (null != surface) {
                        surface.select();
                        selectedShapes.add(surface);
                    } else {
                        for (Shape s : selectedShapes) s.unSelect();
                        selectedShapes.clear();
                    }
                } else {
                    shape = detectSelectedShape(touchPointWSC.x,touchPointWSC.y);
                    if (null != shape) {
                        if (null != currentSelectedShape) {
                            currentSelectedShape.unSelect();
                            if (shape == currentSelectedShape) {
                                currentSelectedShape = null;
                            } else {
                                shape.select();
                                currentSelectedShape = shape;
                            }
                        } else {
                            shape.select();
                            currentSelectedShape = shape;
                        }
                    } else {
                        if (null != currentSelectedShape)
                            currentSelectedShape.unSelect();
                        currentSelectedShape = null;
                    }
                }
        }
    }

    public void performBooleanOperation(BooleanOperation booleanOperation) {
        Shape s1, s2;

        if (2 == selectedShapes.size()) {
            s1 = selectedShapes.poll();
            s2 = selectedShapes.poll();
            undoRedo.insertInUndoRedoForBooleanOperation(s1, s2, booleanOperation, geometry);
            s1.unSelect();
            s2.unSelect();
            geometry.deleteShape(s1);
            geometry.deleteShape(s2);
        }
        else {
            Log.i("BooleanOperation", "Not performed!!!");
            Log.i("BooleanOperation", "Size = " + selectedShapes.size());
            while (!selectedShapes.isEmpty()) selectedShapes.poll().unSelect();
            currentAction = CADOperation.DOING_NOTHING;
        }
    }

    /*
    private Vertex detectSelectedVertex(float x, float y) {
        Vertex vertex, selectedVertex;
        Iterator<Vertex> it = geometry.getVertices().iterator();
        float selectionRadius =
                Constants.SELECTION_DISTANCE /
                plotParameters.getUnitLengthNormalizedCoordinateSystem();

        selectedVertex = null;
        while (it.hasNext()) {
            vertex = it.next();
            if (vertex.areYouSelected(x, y, selectionRadius)) {
                selectedVertex = vertex;
                break;
            }
        }
        return selectedVertex;
    }
    //*/

    private Shape detectSelectedShape(float x, float y) {
        Shape selectedShape = null;
        CopyOnWriteArrayList<Shape> shapes = geometry.getShapes();

        float selectionDistance =
                Constants.SELECTION_DISTANCE / plotParameters.getUnitLengthNormalizedCoordinateSystem();

        ArrayList<Shape> candidateShapes = new ArrayList<>();
        for (Shape shape : shapes) {
            if (shape.areYouSelected(x, y, selectionDistance))
                candidateShapes.add(shape);
        }

        int maximumDepth = Integer.MIN_VALUE;
        for (Shape shape : candidateShapes) {
            if (maximumDepth < shape.getDepth()) {
                selectedShape = shape;
                maximumDepth = shape.getDepth();
            }
        }

        return selectedShape;
    }

    private SurfaceShape detectSelectedSurface(float x, float y) {

        SurfaceShape surface;
        Iterator<SurfaceShape> it =
                geometry.getSurfaceShapes().iterator();
        ArrayList<SurfaceShape> candidateSurfaces = new ArrayList<>();
        while (it.hasNext()) {
            surface = it.next();
            /*
            if (surface.areYouSelected(x, y, 0f))
                candidateSurfaces.add(surface);
            //*/
        }

        int maximumDepth = Integer.MIN_VALUE;
        SurfaceShape selectedSurface = null;
        it = candidateSurfaces.iterator();
        while (it.hasNext()) {
            surface = it.next();
            if (maximumDepth < surface.getDepth()) {
                selectedSurface = surface;
                maximumDepth = surface.getDepth();
            }
        }

        return selectedSurface;
    }

    /*
    private Shape detectSelectedSurface(float x, float y) {
        Shape shape, selectedShape;
        Iterator<Shape> it = geometry.getShapes().iterator();

        selectedShape = null;
        while (it.hasNext()) {
            shape = it.next();
            if (CADShape.isASurface(shape.getCADShape())) {
                if (shape.areYouSelected(x, y, 1f)) {
                    selectedShape = shape;
                    break;
                }
            }
        }
        return selectedShape;
    }
    //*/


    private LinearShape detectSelectedLinearShape(float x, float y) {
        LinearShape shape, selectedShape;
        Iterator<LinearShape> it = geometry.getLinearShapes().iterator();
        float selectionDistance = Constants.SELECTION_DISTANCE /
                plotParameters.getUnitLengthNormalizedCoordinateSystem();

        selectedShape = null;
        /*
        while (it.hasNext()) {
            shape = it.next();
            if (CADShape.isALinearShape(shape.getCADShape())) {
                if (shape.areYouSelected(x, y, selectionDistance)) {
                    selectedShape = shape;
                    break;
                }
            }
        }
        //*/
        return selectedShape;
    }

    /*
    private Geometry.LineSegment detectLineSegment(float x, float y,
                                                   LinearShape linearShape) {
        Geometry.LineSegment lineSegment, selectedLineSegment;
        Iterator<Geometry.LineSegment> it =
                linearShape.getLineSegments().iterator();
        float selectionDistance = Constants.SELECTION_DISTANCE /
                plotParameters.getUnitLengthNormalizedCoordinateSystem();

        //if (linearShape.getLineSegments().size() > 20) return null;

        selectedLineSegment = null;
        while (it.hasNext()) {
            lineSegment = it.next();
            if (lineSegment.areYouSelected(x, y, selectionDistance)) {
                selectedLineSegment = lineSegment;
                break;
            }
        }

        return selectedLineSegment;
    }
    //*/

    //*
    private Shape detectSelectedBooleanBuiltShape(float x, float y) {
        Shape shape, selectedShape;
        Iterator<Shape> it = geometry.getShapes().iterator();

        selectedShape = null;
        /*
        while (it.hasNext()) {
            shape = it.next();
            if (CADShape.isABooleanBuiltShape(shape.getCADShape())) {
                // The third argument is the selection distance. In this case it
                // does not matter because the point has to be inside the area
                if (shape.areYouSelected(x, y, 1f)) {
                    selectedShape = shape;
                    break;
                }
            }
        }
        //*/

        return selectedShape;
    }

    private void manageEvents(int eventHappening) {
        previousEvent = currentEvent;
        currentEvent = eventHappening;
    }

    // Do something if the previous and the current action are different. This is only useful for
    // those actions which are performed on more than one step.
    private void manageChangeInActions( ){
        if(previousAction != currentAction){
            switch (previousAction) {
                case DRAWING_A_NODE:
                    break;
                case DRAWING_A_LINE:
                    // Removes all the points in the queue
                    pointsToDrawLines.clear();
                    break;
                case DRAWING_A_REGULAR_POLYGON:
                    pointsToDrawShape.clear();
                    break;
                case DRAWING_A_RECTANGLE:
                    pointsToDrawShape.clear();
                    break;
                case DRAWING_A_CIRCLE:
                    pointsToDrawShape.clear();
                    break;
            }
        }
    }

    public void handleTouchDrags(Drags drags) {
        manageEvents(Constants.EVENT_DRAG);

        switch (currentAction) {

            case DRAWING_PUNCTUAL_FORCE:
                //if (1 == drags.size()) handlePunctualForce(drags.getDrag(0));
                //else                   currentAction = CADOperation.DOING_NOTHING;
                break;

            default:
                //Log.i("DRAGGING", "Dragging something");

                // Relates each drag to a given point
                detectDragSelectedInteractivePoints(drags);

                switch (drags.size()) {
                    case 1:
                        // If there is only one drag then the user may be panning or dragging
                        // one object
                        handleOneDrag(drags.getDrag(0));
                        break;
                    case 2:
                        // If there are two drags the user may be dragging two objects or
                        // zooming
                        handleTwoDrags(drags);
                        break;
                    default:
                        // When there are 0 or more than two drags the user is dragging nodes
                        handleMoreThanTwoDrags(drags);
                        break;
                }
                break;
        }

        // When we stop the zooming, we create again the figures for supports and nodes
        if (flags.zoomingFinished) {
            visualObjects.setInteractivePoint(
                    Constants.VISUAL_NODE_RADIUS /
                            plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                    Constants.VISUAL_NODE_POINT_NUMPOINTS
            );

            visualObjects.setInteractivePointSelected(
                    Constants.VISUAL_NODE_RADIUS /
                            plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                    Constants.VISUAL_NODE_POINT_NUMPOINTS
            );

            visualObjects.setVertex(
                    Constants.VISUAL_VERTEX_RADIUS /
                            plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                    Constants.VISUAL_NODE_POINT_NUMPOINTS
            );

            visualObjects.setPartialFixedSupport(Constants.SUPPORT_TRIANGLE_BASE /
                            plotParameters.getUnitLengthNormalizedCoordinateSystem());
            visualObjects.setFixedSupport(Constants.SUPPORT_TRIANGLE_BASE /
                    plotParameters.getUnitLengthNormalizedCoordinateSystem());

            flags.zoomingFinished = false;
        }
    }

    // PENDING---------------------------------------
    // This function looks like it will not used anymore. I keep it just in case.
    /*
    private void handlePunctualForce(Drag drag) {
        Vertex vertex;
        Point touchPointWSC;
        PunctualForce punctualForce;

        if (drag.hasPunctualForceBeenSet()) {
            // If previously a punctual force was built, then we
            // only need update it
            punctualForce = drag.getPunctualForce();
            if (null != punctualForce) {
                touchPointWSC = getEndingWorldSpacePointsFromDrag(drag,
                        invertedViewProjectionMatrix);
                punctualForce.setForce(touchPointWSC.x, touchPointWSC.y);
            }
        } else {
            // If the punctual force has not been created, then first
            // we need to know which vertex was selected
            touchPointWSC = getInitialWorldSpacePointsFromDrag(drag,
                    invertedViewProjectionMatrix);
            vertex = detectSelectedElement(touchPointWSC.x, touchPointWSC.y,
                    geometry.getVertices());

            if (null != vertex) {
                touchPointWSC = getEndingWorldSpacePointsFromDrag(drag,
                        invertedViewProjectionMatrix);
                vertex.setForce(touchPointWSC.x, touchPointWSC.y);
                punctualForce = vertex.getForce();
                // We add the force to geomtry so it can be modified
                // through the screen
                geometry.addShape(punctualForce);
            } else {
                punctualForce = null;
            }

            // Then we relate this drag with the vertex's punctual force
            drag.setPunctualForce(punctualForce);
        }
    }
    //*/

    private InteractivePoint detectSelectedElement(float pX,
                                                      float pY,
                                                      ArrayList<InteractivePoint> elements) {
        boolean wasAnElementSelected;
        float distance;
        float smallestDistance;
        InteractivePoint element, closestElement;
        final float radiusSelection = Constants.VISUAL_NODE_RADIUS *
                        Constants.VISUAL_NODE_FACTOR_SELECTION /
                        plotParameters.getUnitLengthNormalizedCoordinateSystem();

        closestElement = null;
        wasAnElementSelected = false;
        Iterator<InteractivePoint> it = elements.iterator();
        if (it.hasNext()) {
            // Chooses a valid value for smallestDistance
            closestElement = it.next();
            smallestDistance = (float) Math.sqrt(
                    (pX - closestElement.getX()) * (pX - closestElement.getX()) +
                    (pY - closestElement.getY()) * (pY - closestElement.getY()));

            if (smallestDistance < radiusSelection) wasAnElementSelected = true;

            while (it.hasNext()) {
                element = it.next();
                distance = (float) Math.sqrt(
                        (pX - element.getX()) * (pX - element.getX()) +
                        (pY - element.getY()) * (pY - element.getY()));

                if (distance < smallestDistance) {
                    smallestDistance = distance;
                    closestElement = element;
                    if (smallestDistance < radiusSelection)
                        wasAnElementSelected = true;
                }
            }
        }

        if (wasAnElementSelected) return closestElement;
        else                      return null;
    }

    private void dragInteractivePoint(InteractivePoint ip, float x, float y, boolean hasStopped) {
        Log.i("Dragging", "Dragging interactive point");
        Shape shape = ip.getParentShape();
        if (null != shape)
            shape.handleDragInteractivePoint(ip, x, y);
    }

    private Shape getParentShape(InteractivePoint ip) {
        return ip.getParentShape();
    }

    private void handleOneDrag(Drag drag) {
        if (!drag.isEnabled()) return;

        ArrayList<InteractivePoint> interactivePoints;
        Point initialWSP, previousWSP, endingWSP, endingNDC;
        Point finalPoint, gridPoint;
        Shape parentShape;

        // Gets the drag points (initially in normalized device coordinates) in world-space coordinates
        previousWSP = Tools.getPreviousWorldSpacePointsFromDrag(drag, invertedViewProjectionMatrix);
        endingWSP = Tools.getEndingWorldSpacePointsFromDrag(drag, invertedViewProjectionMatrix);

        // If the magnetic grid is activated we choose the closest point on the grid
        finalPoint = endingWSP;
        if (flags.magneticGrid){
            gridPoint = getPointOnTheGrid(endingWSP.x, endingWSP.y);
            finalPoint =  gridPoint;
        }

        // Node related to the drag
        interactivePoints = drag.getInteractivePoints();
        if (0 == interactivePoints.size()) {
            // The user is panning because didn't select a node
            handlePanning(
                    previousWSP.x, previousWSP.y,
                    endingWSP.x, endingWSP.y,
                    plotParameters,
                    projectionMatrix
            );
            // If the drag has stopped we report it so we can undo/redo
            if (drag.hasStopped()) {
                initialWSP = getInitialWorldSpacePointsFromDrag(drag,
                        invertedViewProjectionMatrix);

                undoRedo.insertInUndoRedoForPanConform(
                        initialWSP.x,
                        initialWSP.y,
                        endingWSP.x,
                        endingWSP.y,
                        plotParameters,
                        projectionMatrix
                );
            }
            updates.add(Constants.CAD_UPDATE_AXIS);
        } else {
            // The user is dragging an interactive point
            for (InteractivePoint ip : interactivePoints) {
                parentShape = ip.getParentShape();
                if (null != parentShape) {
                    parentShape.handleDragInteractivePoint(ip, finalPoint.x, finalPoint.y);
                    initialWSP = getInitialWorldSpacePointsFromDrag(drag, invertedViewProjectionMatrix);
                    endingNDC = drag.getEndingPosition();

                    if (isInDeletionArea(endingNDC)) {
                        // If it reaches the deletion area
                        undoRedo.insertInUndoRedoForDeleteShape(parentShape, geometry, initialWSP.x,
                                initialWSP.y, ip);
                        // We have to disable the drag
                        drag.disable();
                    } else {
                        // If the drag has stopped we report it so we can undo/redo
                        if (drag.hasStopped()) {
                            undoRedo.insertInUndoRedoForConformDragInteractivePoint(
                                    initialWSP.x,
                                    initialWSP.y,
                                    ip
                            );
                        }
                    }
                }
            }
        }
    }

    private void handleTwoDrags(Drags drags) {
        Point aInNDC, bInNDC, aInWSC, bInWSC;
        Point zoomingCenterInNDC, zoomingCenterInWSC;
        float radiusInNDC;

        if ((0 == drags.getDrag(0).getInteractivePoints().size()) ||
                (0 == drags.getDrag(1).getInteractivePoints().size())) {
            // In this case the user is zooming because there are not
            // interactive points selected
            aInNDC = drags.getDrag(0).getPreviousPosition();
            bInNDC = drags.getDrag(1).getPreviousPosition();
            aInWSC = Utilities.convertNormalized2DPointToWorldSpace2DPoint(
                    aInNDC.x,
                    aInNDC.y,
                    invertedViewProjectionMatrix
            );
            bInWSC = Utilities.convertNormalized2DPointToWorldSpace2DPoint(
                    bInNDC.x,
                    bInNDC.y,
                    invertedViewProjectionMatrix
            );

            radiusInNDC = 0.5f*Tools.distanceBetweenPoints(aInNDC, bInNDC);
            zoomingCenterInNDC = Tools.middlePoint(aInNDC, aInNDC);
            zoomingCenterInWSC = Tools.middlePoint(aInWSC, bInWSC);
            /*
            zoom.setData(plotParameters.getZoomFactor(),radiusInNDC, zoomingCenterInNDC, zoomingCenterInWSC,
                    new Point(plotParameters.getCenterX(), plotParameters.getCenterY(),
                            plotParameters.getCenterZ()));
            */
            zoomCommand.setData(plotParameters.getZoomFactor(), radiusInNDC,
                    zoomingCenterInNDC, zoomingCenterInWSC, plotParameters.getCenter());

            /*
            Log.i("ZOOMING", "Setting data for zooming");
            Log.i("ZOOMING", "WSC zooming center = " + zoomingCenterInNDC.x + "   " + zoomingCenterInNDC.y);
            Log.i("ZOOMING", "WSC zooming center = " + zoomingCenterInWSC.x + "   " + zoomingCenterInWSC.y);
            //*/

            // When one of the pointers is close to a node, then the drag will be related to this
            // node. But after we finish zooming we do not want the node to be moved, so we have
            // to disconnect the node with the pointer.
            if (drags.getDrag(0).hasPointsBeenSet()) drags.getDrag(0).removePoints();
            if (drags.getDrag(1).hasPointsBeenSet()) drags.getDrag(1).removePoints();

            aInNDC = drags.getDrag(0).getEndingPosition();
            bInNDC = drags.getDrag(1).getEndingPosition();

            //Log.i("ZOOMING", "Old view center = " + plotParameters.getCenterX() + "  " + plotParameters.getCenterY());
            //Tools.handleZooming(aInNDC, bInNDC, zoom, plotParameters, projectionMatrix);
            zoomCommand.execute(aInNDC, bInNDC, plotParameters, projectionMatrix);

            // We report that the clip area has changed
            updates.add(Constants.CAD_UPDATE_AXIS);
            updates.add(Constants.CAD_UPDATE_INTERACTIVE_POINTS);
        } else {
            // In this case the user is dragging two nodes
            handleOneDrag(drags.getDrag(0));
            handleOneDrag(drags.getDrag(1));
        }
    }

    private void handleMoreThanTwoDrags(Drags drags) {
        for (int i = 0; i < drags.size(); i++) handleOneDrag(drags.getDrag(i));
    }

    public static Point getInitialWorldSpacePointsFromDrag(Drag drag,
                                                                 float[] invertedTransformationMatrix) {
        Point initialNormSpacePoint = drag.getBeginningPosition();
        Point initialWorldSpacePoint =
                Utilities.convertNormalized2DPointToWorldSpace2DPoint(
                        initialNormSpacePoint.x,
                        initialNormSpacePoint.y,
                        invertedTransformationMatrix
                );
        return initialWorldSpacePoint;
    }

    private void detectDragSelectedInteractivePoints(Drags drags) {
        if (null == currentSelectedShape) return;

        Point initialWorldSpacePoint;
        Drag drag;
        ArrayList<InteractivePoint> ips;

        final float selctionRadius = Constants.VISUAL_NODE_RADIUS *
                Constants.VISUAL_NODE_FACTOR_SELECTION /
                plotParameters.getUnitLengthNormalizedCoordinateSystem();

        // Detect selected nodes
        for (int i = 0; i < drags.size(); i++) {
            drag = drags.getDrag(i);
            // We try to find the selected node only at the beginning
            if (drag.hasJustStarted()) {
                if (!drag.hasPointsBeenSet()) {
                    initialWorldSpacePoint = getInitialWorldSpacePointsFromDrag(drag,
                            invertedViewProjectionMatrix);
                    ips = currentSelectedShape.detectSelectedInteractivePoints(
                            initialWorldSpacePoint.x,
                            initialWorldSpacePoint.y,
                            selctionRadius);
                    drag.setPointsRelatedToDrag(ips);
                }
            }
        }
    }

    public static void handlePanning(float oldX, float oldY,
                                     float newX, float newY,
                                     PlotParameters plotParameters,
                                     float[] projectionMatrix) {
        float displacementOnX, displacementOnY;

        // Displacements from the start dragging point
        displacementOnX = newX - oldX;
        displacementOnY = newY - oldY;

        plotParameters.setCenter(plotParameters.getCenterX() - displacementOnX,
                plotParameters.getCenterY() - displacementOnY, plotParameters.getCenterZ());

        orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                plotParameters.getMinY(), plotParameters.getMaxY(),
                plotParameters.getMinZ(), plotParameters.getMaxZ());
    }

    private float clamp(float value, float min, float max) {
        return Math.min(max, Math.max(value, min));
    }

    private void loadTextures() {
        // Textures
        float ratio[] = {0};
        int textureID = 0;

        textureID = TextureHelper.loadTexture(context, R.drawable.air_hockey_surface, ratio);
        if (0 != textureID) {
            Utilities.Texture texture = new Utilities.Texture();
            texture.ID = textureID;
            texture.ratio = ratio[0];
            texturesID.put(Constants.TextureName.HOCKEY_TABLE, texture);
        }

        textureID = TextureHelper.loadTexture(context, R.drawable.dashed_line_6_div, ratio);
        if (0 != textureID) {
            Utilities.Texture texture = new Utilities.Texture();
            texture.ID = textureID;
            texture.ratio = ratio[0];
            texturesID.put(Constants.TextureName.DASHED_LINE_6_DIV, texture);
        }
    }

    //@Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        colorProgram = new ColorShaderProgram(context);
        textureProgram = new TextureShaderProgram(context);

        batchTextShaderProgram = new BatchTextProgram();
        batchTextShaderProgram.init();

        glText = new GLText(batchTextShaderProgram, context.getAssets());

        loadTextures();

        // Load the font from file (set size + padding), creates the texture
        // NOTE: after a successful call to this the font is ready for rendering!
        // Create Font (Height: Constants.FONT_SIZE Pixels / X+Y Padding 2 Pixels)
        glText.load( "Roboto-Regular.ttf", Constants.FONT_SIZE,  2, 2 );
    }

    //@Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {

        this.width = width;
        this.height = height;

        // Set the OpenGL viewport to fill the entire surface.
        glViewport(0, 0, width, height);

        // Projection through a Frustum
        //MatrixHelper.perspectiveM(projectionMatrix, 45, (float) width / (float) height, 1f, 10f);

        float aspectRatio = (float)height / (float)width;
        if (width > height) {
            // Landscape
            axisOrientation = new AxisOrientation(-0.8f, -0.6f, 0.3f, 0.3f, aspectRatio, 1);
            deletionArea = new VisualObject();
            deletionArea.createVisualCircle(
                    new Point(Constants.CENTER_DELETION_AREA[0],
                            Constants.CENTER_DELETION_AREA[1],
                            Constants.CENTER_DELETION_AREA[2]),
                    Constants.RADIUS_DELETION_AREA,
                    15,
                    aspectRatio,
                    1f
            );
        } else {
            // Portrait
            axisOrientation = new AxisOrientation(-0.8f, -0.6f, 0.3f, 0.3f, 1, aspectRatio);
            deletionArea = new VisualObject();
            deletionArea.createVisualCircle(
                    new Point(Constants.CENTER_DELETION_AREA[0],
                            Constants.CENTER_DELETION_AREA[1],
                            Constants.CENTER_DELETION_AREA[2]),
                    Constants.RADIUS_DELETION_AREA,
                    15,
                    1f,
                    aspectRatio
            );
        }

        // Ortographic projection
        plotParameters.setScreenValues(width, height);
        orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                plotParameters.getMinY(), plotParameters.getMaxY(),
                -2f, 2f); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        /*
            orthoM(float[] m, int mOffset, float left, float right, float bottom, float top, float near, float far)

            float[] m       The destination array—this array’s length should be at least
                            sixteen elements so it can store the orthographic projection
                            matrix.
            int mOffset     The offset into m into which the result is written
            float left      The minimum range of the x-axis
            float right     The maximum range of the x-axis
            float bottom    The minimum range of the y-axis
            float top       The maximum range of the y-axis
            float near      The minimum range of the z-axis
            float far       The maximum range of the z-axis
        */

        /*
            perspectiveM(float[] m, float yFovInDegrees, float aspect, float n, float f)
            m               Array to store the matrix

            yFovInDegrees   Field of vision of the camera in degrees
            aspect          Aspect ratio of the screen
            n               This should be set to the distance to the near plane and must be
                            positive. For example, if this is set to 1, the near plane will be
                            located at z of -1.
            f               This should be set to the distance to the far plane and must be positive
                            and greater than the distance to the near plane
        */

        setLookAtM(viewMatrix, 0, 0f, 0f, 3f, 0f, 0f, 0f, 0f, 1f, 0f);

        /*
            setLookAtM(float[] rm, int rmOffset, float eyeX, float eyeY, float eyeZ, float centerX,
                       float centerY, float centerZ, float upX, float upY, float upZ)

            rm                              Destination array (rm)
            rmOffset                        Offset where the function will start the writing in the
                                            array
            eyeX, eyeY, eyeZ                This is where the eye will be
            centerX, centerY, centerZ       This is where the eye is looking, this position will
                                            appear in the center of the scene
            upX, upY, upZ                   If we were talking about your eyes, then this is where
                                            your head will be pointing. An upY of 1 means your head
                                            would be pointing straight up.

        */

        setIdentityM(viewMatrix, 0);

        // Interactive points
        visualObjects.setInteractivePoint(
                Constants.VISUAL_NODE_RADIUS /
                        plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                Constants.VISUAL_NODE_POINT_NUMPOINTS
        );

        visualObjects.setInteractivePointSelected(
                Constants.VISUAL_HALO_RADIUS /
                        plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                Constants.VISUAL_NODE_POINT_NUMPOINTS
        );

        // Vertices
        visualObjects.setVertex(
                Constants.VISUAL_VERTEX_RADIUS /
                        plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                Constants.VISUAL_NODE_POINT_NUMPOINTS
        );

        // Supports
        visualObjects.setPartialFixedSupport( Constants.SUPPORT_TRIANGLE_BASE /
                plotParameters.getUnitLengthNormalizedCoordinateSystem());

        visualObjects.setFixedSupport(Constants.SUPPORT_TRIANGLE_BASE /
                plotParameters.getUnitLengthNormalizedCoordinateSystem());

        // Axis
        axis = new Axis();
        axis.setPlotParameters(plotParameters);
        visualAxis = new VisualAxis(axis);
    }

    //@Override
    public void onDrawFrame(GL10 glUnused) {
        boolean updateVisualLines;
        boolean updateVisualSurfaces;
        boolean updateVisualForces;
        boolean updateVisualAxis;

        glClearColor(0.9f, 0.9f, 0.9f, 1f);

        // Clear the rendering surface.
        glClear(GL_COLOR_BUFFER_BIT);

        // Update the viewProjection matrix, and create an inverted matrix for
        // touch picking.
        multiplyMM(viewProjectionMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
        invertM(invertedViewProjectionMatrix, 0, viewProjectionMatrix, 0);

        handleUpdates();

        glText.setScale(Constants.FONT_SIZE_ON_SCREEN /
                (Constants.FONT_SIZE *
                        plotParameters.getUnitLengthNormalizedCoordinateSystem()));
        glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        Utilities.positionObjectInScene(0f, 0f, 0f, viewProjectionMatrix, modelViewProjectionMatrix);
        if (flags.showGrid) drawGrid();
        drawAxis();

        //glClear(GL_DEPTH_BUFFER_BIT);
        // Enable depth test
        //glEnable(GL_DEPTH_TEST);
        // Accept fragment if it closer to the camera than the former one
        //glDepthFunc(GL_LEQUAL);

        Iterator<Shape> itShape = geometry.getShapes().iterator();
        while (itShape.hasNext()) {
            itShape.next().draw(
                    colorProgram,
                    textureProgram,
                    glText,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    visualObjects,
                    texturesID,
                    plotParameters.getUnitLengthNormalizedCoordinateSystem()
            );
        }

        //glDisable(GL_DEPTH_TEST);

        drawAxisOrientation();
        drawDeletionArea();
    }

    private void handleUpdates() {
        final int numberOfUpdatesToDo = 3;
        for (int i = 0; i < numberOfUpdatesToDo; i++)
            processUpdate(Tools.getNextUpdate(updates));
    }

    private void processUpdate(int update) {
        switch (update) {
            case Constants.CAD_UPDATE_AXIS:
                axis.setPlotParameters(plotParameters);
                visualAxis.updateOpenGLData(axis);
                Log.i("Updates", "Axis");
                break;
            case Constants.CAD_UPDATE_INTERACTIVE_POINTS:
                visualObjects.setInteractivePoint(
                        Constants.VISUAL_NODE_RADIUS /
                                plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                        Constants.VISUAL_NODE_POINT_NUMPOINTS
                );

                visualObjects.setInteractivePointSelected(
                        Constants.VISUAL_NODE_RADIUS /
                                plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                        Constants.VISUAL_NODE_POINT_NUMPOINTS
                );
                break;
        }
    }

    private void drawForces() {
        visualForces.bindData(colorProgram);
        colorProgram.setUniforms(modelViewProjectionMatrix, 125f / 255f, 240f / 255f, 217f / 255f);

        visualForces.draw();

        visualLinearForces.bindData(colorProgram);
        colorProgram.setUniforms(modelViewProjectionMatrix, 125f / 255f, 255f / 255f, 150f / 255f);
        visualLinearForces.draw();
    }

    private void drawAxis() {
        colorProgram.useProgram();

        visualAxis.bindData(colorProgram);
        colorProgram.setUniforms(modelViewProjectionMatrix, 0.7f, 0.7f, 0.7f);
        visualAxis.drawAxis();

        glEnable(GLES20.GL_BLEND);
        glText.begin(0.7f, 0.7f, 0.7f, 1.0f, modelViewProjectionMatrix);
        glText.draw("                  ", 1f, 0f);
        axis.mainTics.drawLabels(glText);
        glText.draw("                  ", 0f, 0f);
        glDisable(GLES20.GL_BLEND);
    }

    private void drawGrid() {
        colorProgram.useProgram();
        visualAxis.bindData(colorProgram);
        colorProgram.setUniforms(modelViewProjectionMatrix, 0.7f, 0.7f, 0.7f);
        visualAxis.drawGrid();
    }

    private void drawAxisOrientation() {
        colorProgram.useProgram();
        axisOrientation.bindData(colorProgram);
        setIdentityM(modelViewProjectionMatrix, 0);
        colorProgram.setUniforms(modelViewProjectionMatrix, 0.5f, 0.5f, 0.2f);
        axisOrientation.draw();
    }

    private void drawDeletionArea() {
        float grey = 170f/255f;
        colorProgram.useProgram();
        deletionArea.bindData(colorProgram);
        setIdentityM(modelViewProjectionMatrix, 0);
        colorProgram.setUniforms(modelViewProjectionMatrix, grey, grey, grey);
        deletionArea.draw();
    }

    private void drawSupports() {
    }

    private void drawLines() {
    }

    private void drawSurfaces() {
    }

    private void rotateScene(float angle, float x, float y, float z, float[] matrixIn, float[] matrixOut) {
        setIdentityM(modelMatrix, 0);
        rotateM(modelMatrix, 0, angle, x, y, z);
        multiplyMM(matrixOut, 0, matrixIn, 0, modelMatrix, 0);
    }

    // Current force to be applied
    public void setCurrentForce(Force currentForce) {
        Log.i("Current Force Setting", "" + currentForce.getForceX() + "  " + currentForce.getForceY());
        this.currentForce.copyForce(currentForce);
    }

    // Current fixed displacement to be applied
    public void setCurrentFixedDisplacement(FixedDisplacement fixedDisplacement) {
        this.currentFixedDisplacement.setDisplacement(fixedDisplacement);
    }

    public void showGrid() {
        flags.showGrid = true;
    }

    public void hideGrid() {
        flags.showGrid = false;
    }

    public void setNumberOfSidesRegularPolygon(int numberSidesRegularPolygon) {
        this.numberOfSidesRegularPolygon = numberSidesRegularPolygon;
    }

    public void setNumberOfDivisions(int numberOfDivisions) {
        this.numberOfDivisions = numberOfDivisions;
    }

    private Point getPointOnTheGrid(float x, float y) {
        float intervalSize, previousX, previousY;
        int numberOfIntervalsOnX, numberOfIntervalsOnY;
        Point gridPoint = new Point();

        // When the second tics are shown I have to change this
        intervalSize = axis.mainTics.getIntervalSize();

        numberOfIntervalsOnX = (int)Math.floor(x / intervalSize);
        if (x < 0f) numberOfIntervalsOnX = (int)Math.ceil(x / intervalSize);
        numberOfIntervalsOnY = (int)Math.floor(y / intervalSize);
        if (y < 0f) numberOfIntervalsOnY = (int)Math.ceil(y / intervalSize);

        previousX = (float)numberOfIntervalsOnX*intervalSize;
        previousY = (float)numberOfIntervalsOnY*intervalSize;

        gridPoint.x = previousX;
        if (Math.abs(x) > Math.abs(previousX)+ 0.5f*intervalSize){
            if (x > 0f) gridPoint.x = previousX + intervalSize;
            else        gridPoint.x = previousX - intervalSize;
        }

        gridPoint.y = previousY;
        if (Math.abs(y) > Math.abs(previousY) + 0.5f*intervalSize){
            if (y > 0f) gridPoint.y = previousY + intervalSize;
            else        gridPoint.y = previousY - intervalSize;
        }

        gridPoint.z = 0f;

        return gridPoint;
    }

    public void unselectAllShapes() {
        Iterator<Shape> it = geometry.getShapes().iterator();
        while (it.hasNext()) it.next().unSelect();
    }

    /*
    private void handleExtrude(Line selectedLine, float pointerPosOnX, float pointerPosOnY,
                               int dragID, boolean hasStopped) {
        boolean canBeExtruded;
        if (flags.newExtrude) {
            // When we want to start a extrude
            canBeExtruded = extrudeLine.startExtrude(selectedLine, geometry, dragID);
            if (canBeExtruded) {
                Log.i("DRAGGING", "Extrude beginning");
                extrudeLine.extrude(pointerPosOnX, pointerPosOnY, hasStopped);
                flags.newExtrude = false;
                if (hasStopped) updates.add(Constants.CAD_UPDATE_SURFACES);
                updates.add(Constants.CAD_UPDATE_LINES);
                updates.add(Constants.CAD_UPDATE_FORCES);
            } else {
                Log.i("DRAGGING", "Extrude cannot be started");
                // If the extrude cannot be done we reset these variables
                flags.newExtrude = true;
                currentAction = Constants.DOING_NOTHING;
                extrudeLine.stopExtrude();
            }
        } else {
            // When we are doing a extrude. We perform the extrude only if the dragID is the same.
            if (dragID == extrudeLine.getDragID()) {
                Log.i("DRAGGING", "Extrude continuing");
                extrudeLine.extrude(pointerPosOnX, pointerPosOnY, hasStopped);
                if (hasStopped) updates.add(Constants.CAD_UPDATE_SURFACES);
                updates.add(Constants.CAD_UPDATE_LINES);
                updates.add(Constants.CAD_UPDATE_FORCES);
            } else {
                Log.i("DRAGGING", "Extrude cannot be continued");
                // If the extrude cannot be done we reset these variables
                flags.newExtrude = true;
                currentAction = Constants.DOING_NOTHING;
                extrudeLine.stopExtrude();
            }
        }
    }

    private void getLargestForce() {
        Points points = geometry.getPoints();
        Lines lines = geometry.getLines();
        Point point;
        Line line;
        float force;

        // Nodal forces
        largestForce = 0f;
        for (int i = 0; i < points.getSize(); i++) {
            point = points.getElement(i);
            if (point.hasConditions()) {
                if (point.getConditions().hasAppliedForce()){
                    force = point.getConditions().getForce().getMagnitude();
                    if (force > largestForce) largestForce = force;
                }
            }
        }

        // Linear forces
        for (int i = 0; i < lines.getSize(); i++) {
            line = lines.getElement(i);
            if (line.hasConditions()) {
                if (line.getConditions().hasAppliedForce()){
                    force = line.getConditions().getForce().getMagnitude();
                    if (force > largestForce) largestForce = force;
                }
            }
        }
    }
    //*/

    public void updateLines() { updates.add(Constants.CAD_UPDATE_LINES); }
    public void updateForces() { updates.add(Constants.CAD_UPDATE_FORCES); }
    public void updateSurfaces() { updates.add(Constants.CAD_UPDATE_SURFACES); }

    public void undo() { undoRedo.undo(1); }
    public void redo() { undoRedo.redo(1); }

}