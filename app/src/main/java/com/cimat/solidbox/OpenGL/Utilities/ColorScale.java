package com.cimat.solidbox.OpenGL.Utilities;

import android.util.Log;

import com.cimat.solidbox.OpenGL.Utilities.Colors.ColorPalette;

/**
 * Created by ernesto on 7/03/16.
 */
public class ColorScale {
    float width;
    float height;
    float centerX;
    float centerY;
    int numberOfDivisionsOnX;
    int numberOfDivisionsOnY;
    public ColorPalette colorPalette;

    public ColorScale() {
        width = height = 0f;
        centerX = centerY = 0f;
        numberOfDivisionsOnX = numberOfDivisionsOnY = 0;
        colorPalette = null;

        //Log.i("ColorScale", "Creating a color scale!!!");
    }

    public void setColorPalette(ColorPalette colorPalette){
        this.colorPalette = colorPalette;
    }

    public void setDimensions(float width, float height) {
        this.width = width;
        this.height = height;
        //Log.i("ColorScale", "width = " + width +
        //        "  height = " + height);
    }

    public void setCenter(float centerX, float centerY) {
        this.centerX = centerX;
        this.centerY = centerY;
    }

    // We'll create a mesh using triangle strips for OpenGL
    public void setNumberOfDivisions(int numberOfDivisionsOnX, int numberOfDivisionsOnY){
        this.numberOfDivisionsOnX = numberOfDivisionsOnX;
        this.numberOfDivisionsOnY = numberOfDivisionsOnY;
        //Log.i("ColorScale", "numberDivX = " + numberOfDivisionsOnX
        //        +  " numberDivY = " + numberOfDivisionsOnY);
    }

    public float getWidth() { return width; }
    public float getHeight() { return height; }
    public float getCenterX() { return centerX; }
    public float getCenterY() { return centerY; }
    public int getNumberOfDivisionsOnX() { return numberOfDivisionsOnX; }
    public int getNumberOfDivisionsOnY() { return numberOfDivisionsOnY; }
    public float getMaxValue() { return colorPalette.getMaxValue(); }
    public float getMinValue() { return colorPalette.getMinValue(); }
}
