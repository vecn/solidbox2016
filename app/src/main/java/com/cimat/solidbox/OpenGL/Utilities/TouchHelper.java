package com.cimat.solidbox.OpenGL.Utilities;

import com.cimat.solidbox.Utilities.Preprocess.Conditions;
import com.cimat.solidbox.Utilities.Preprocess.Geometry;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static android.opengl.Matrix.multiplyMV;
import static android.opengl.Matrix.setIdentityM;

/**
 * Created by ernesto on 16/06/16.
 */

public class TouchHelper {
    public static class Pointer {
        private int ID;
        private long startTime, lastTime;
        private Geometry.Point startPoint, previousLastPoint, lastPoint;
        private boolean hasStopped;
        private boolean wasUsedLikeATap;
        private float totalDistance;

        public Pointer(int ID) {
            this.ID = ID;
            startTime = lastTime = 0;
            startPoint = new Geometry.Point();
            previousLastPoint = new Geometry.Point();
            lastPoint  = new Geometry.Point();
            hasStopped = false;
            wasUsedLikeATap = false;
            totalDistance = 0f;
        }

        public int getID() { return ID; }

        public void startIt(float x, float y, long time) {
            startPoint.setCoordinates( x, y, 0f);
            previousLastPoint.setCoordinates(startPoint);
            lastPoint.setCoordinates(startPoint);
            startTime = time;
            totalDistance = 0f;
        }

        public void continueIt(float x, float y, long time) {
            previousLastPoint.setCoordinates(lastPoint);
            lastPoint.setCoordinates(x, y , 0f);
            lastTime = time;
            totalDistance += (float) Math.sqrt(Math.pow(lastPoint.x - previousLastPoint.x, 2) +
                    Math.pow(lastPoint.y - previousLastPoint.y, 2));
        }

        public void stopIt(float x, float y, long time) {
            continueIt(x, y, time);
            hasStopped = true;
        }

        public boolean hasStopped() { return hasStopped; }
        public long getStartTime() { return startTime; }
        public long getLastTime() { return lastTime; }
        public long getPressDuration() { return lastTime - startTime; }
        public float getStartX() { return startPoint.x; }
        public float getStartY() { return startPoint.y; }
        public float getLastX() { return lastPoint.x; }
        public float getLastY() { return lastPoint.y; }
        public float getPreviousLastX() { return previousLastPoint.x; }
        public float getPreviousLastY() { return previousLastPoint.y; }
        public boolean wasUsedLikeATap() { return wasUsedLikeATap; }

        public void setWasUsedLikeATap(boolean wasUsedLikeATap) {
            this.wasUsedLikeATap = wasUsedLikeATap;
        }

        public float getMovedDistance() {
            return totalDistance;
        }
    }

    public static Pointer getPointerByID(final int ID,
                                         ArrayList<Pointer> pointers) {
        Pointer pointer = null;
        for (int i = 0; i < pointers.size(); i++) {
            pointer = pointers.get(i);
            if (ID == pointer.getID()) break;
        }
        return pointer;
    }

    public static class Pointers {
        private ArrayList<Pointer> pointers;

        public Pointers() {
            pointers = new ArrayList();
        }



        public void removePointer(Pointer pointer) {
            pointers.remove(pointer);
        }

        public void removeStoppedPointers() {
            Pointer pointer;
            for (int i = 0; i < pointers.size(); i++) {
                pointer = pointers.get(i);
                if (pointer.hasStopped()) pointers.remove(i);
            }
        }

        public void addPointer(Pointer pointer) {
            pointers.add(pointer);
        }
        public ArrayList<Pointer> getPointers() { return pointers; }
        public int getSize() { return pointers.size(); }
        public Pointer getPointer(int index) { return pointers.get(index); }
    }

    public static class Tap {
        Geometry.Point p;
        long endingTime, initialTime;

        public Tap() {
            p = new Geometry.Point();
            initialTime = 0;
            endingTime = 0;
        }

        public Tap(float x,
                   float y,
                   long initialTime,
                   long endingTime){
            p = new Geometry.Point(x, y, 0);
            this.initialTime = initialTime;
            this.endingTime = endingTime;
        }

        public Geometry.Point getPosition() { return p; }
        public long getInitialTime() { return initialTime; }
        public long getEndingTime() { return endingTime; }
        public long getPressDuration() {
            return endingTime - initialTime;
        }
    }

    public static class DoubleTap {
        Geometry.Point p;
        long intervalTime;

        public DoubleTap(float x, float y, long intervalTime) {
            p = new Geometry.Point(x, y, 0);
            this.intervalTime = intervalTime;
        }

        public Geometry.Point getPosition() { return p; }
        public long getIntervalTime() { return intervalTime; }
    }

    public static class Drag {
        private int ID;
        private Geometry.Point begin, previous, end;

        private ArrayList<Geometry.InteractivePoint> ips;
        private boolean hasJustStarted;
        private boolean hasStopped;
        private boolean isEnabled;
        private boolean hasPointsBeenSet;
        private float[] transformationMatrix;

        public Drag(int ID) {
            this.ID = ID;
            begin = new Geometry.Point();
            previous = new Geometry.Point();
            end = new Geometry.Point();
            ips = new ArrayList<>();
            hasPointsBeenSet = false;
            hasJustStarted = true;
            hasStopped = false;
            isEnabled = true;
            transformationMatrix = new float[16];
            setIdentityM(transformationMatrix, 0);
        }

        public void setBeginningPosition(float x, float y) {
            begin.setCoordinates(x, y, 0f);
        }
        public void setPreviousPosition(float x, float y) {
            hasJustStarted = false;
            previous.setCoordinates(x, y, 0);
        }
        public void setEndingPosition(float x, float y) {
            end.setCoordinates(x, y, 0f);
        }

        public Geometry.Point getBeginningPosition() { return begin; }
        public Geometry.Point getPreviousPosition() { return previous; }
        public Geometry.Point getEndingPosition() { return end; }
        public boolean hasJustStarted() { return hasJustStarted; }

        public void setPointsRelatedToDrag(ArrayList<Geometry.InteractivePoint> points) {
            ips.clear();
            ips.addAll(points);
            hasPointsBeenSet = true;
        }

        public void addPoint(Geometry.InteractivePoint p) {
            ips.add(p);
            hasPointsBeenSet = true;
        }

        public void removePoints() {
            ips.clear();
        }

        public ArrayList<Geometry.InteractivePoint> getInteractivePoints() { return ips; }
        public boolean hasPointsBeenSet() { return hasPointsBeenSet; }

        public int getID() { return ID; }

        public void stop() { hasStopped = true; }
        public boolean hasStopped() { return hasStopped; }

        public void disable() { isEnabled = false; }
        public boolean isEnabled() { return isEnabled; }

        public void copyTransformationMatrix(final float[] T) {
            for (int i = 0; i < 16; i++)
                transformationMatrix[i] = T[i];
        }

        public Geometry.Point getTransformedBeginningPosition() {
            return Utilities.getTransformedPoint(transformationMatrix, begin);
        }

        public Geometry.Point getTransformedPreviousPosition() {
            return Utilities.getTransformedPoint(transformationMatrix, previous);
        }

        public Geometry.Point getTransformedEndingPosition() {
            return Utilities.getTransformedPoint(transformationMatrix, end);
        }
    }

    public static class Drags {
        private ArrayList<Drag> drags;
        private Set<Integer> idsSet;

        public Drags() {
            drags = new ArrayList();
            idsSet = new HashSet();
        }

        public Drag getDragByID(int ID) {
            Drag drag = null;
            if (!idsSet.contains(ID)) return drag;
            for (int i = 0; i < drags.size(); i++) {
                drag = drags.get(i);
                if (ID == drag.getID()) break;
            }
            return drag;
        }

        public void removeDragByID(int ID) {
            Drag drag;
            if (!idsSet.contains(ID)) return;
            for (int i = 0; i < drags.size(); i++) {
                drag = drags.get(i);
                if (ID == drag.getID()){
                    drags.remove(i);
                    idsSet.remove(ID);
                    break;
                }
            }
        }

        public void removeDrag(Drag drag) {
            removeDragByID(drag.getID());
        }

        public void removeDrag(int index) {
            if (index < 0) return;
            if (index > (drags.size() - 1)) return;
            Drag drag = drags.get(index);
            idsSet.remove(drag.getID());
            drags.remove(index);
        }

        public void addDrag(Drag drag) {
            if (idsSet.contains(drag.getID())) return;
            drags.add(drag);
            idsSet.add(drag.getID());
        }

        public int size() { return drags.size(); }
        public Drag getDrag(int index) {
            return drags.get(index);
        }

        public boolean contains(int ID) {
            return idsSet.contains(ID);
        }

        public void removeStoppedDrags() {
            Drag drag;
            for (int i = 0; i < drags.size(); i++) {
                drag = drags.get(i);
                if (drag.hasStopped()) removeDrag(i);
            }
        }
    }
}
