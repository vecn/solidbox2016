package com.cimat.solidbox.Utilities;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.cimat.solidbox.R;

import java.util.ArrayList;
import java.util.Iterator;

import static android.R.attr.resource;

/**
 * Created by ernesto on 15/08/16.
 */

public class ButtonGroup {
    ArrayList<MyImageButton> buttons;
    MyImageButton currentSelectedButton;

    public ButtonGroup() {
        buttons = new ArrayList();
        currentSelectedButton = null;
    }

    public void add(MyImageButton button) {
        buttons.add(button);
    }

    public void show() {
        Iterator<MyImageButton> it = buttons.iterator();
        while (it.hasNext()) it.next().show();
    }

    public void hide() {
        Iterator<MyImageButton> it = buttons.iterator();
        while (it.hasNext()) it.next().hide();
    }

    public void deactivate() {
        RelativeLayout.LayoutParams params;
        MyImageButton button;
        Iterator<MyImageButton> it = buttons.iterator();
        int marginLeft;

        if (it.hasNext()) {
            button = it.next();

            marginLeft = (int)convertDpToPixel(40, button.getContext());
            params = (RelativeLayout.LayoutParams) button.getLayoutParams();
            params.setMargins(-marginLeft, params.topMargin, params.rightMargin, params.rightMargin);
            button.setLayoutParams(params);
            button.setEnabled(false);

            while (it.hasNext()) {
                button = it.next();

                marginLeft = (int)convertDpToPixel(40, button.getContext());
                params = (RelativeLayout.LayoutParams) button.getLayoutParams();
                params.setMargins(-marginLeft, params.topMargin, params.rightMargin, params.rightMargin);
                button.setLayoutParams(params);
                button.setEnabled(false);
            }
        }
    }

    public void activate() {
        RelativeLayout.LayoutParams params;
        MyImageButton button;
        Iterator<MyImageButton> it = buttons.iterator();
        int marginLeft;

        if (it.hasNext()) {
            button = it.next();
            marginLeft = (int)convertDpToPixel(20, button.getContext());
            params = (RelativeLayout.LayoutParams) button.getLayoutParams();
            params.setMargins(-marginLeft, params.topMargin, params.rightMargin, params.rightMargin);
            button.setLayoutParams(params);
            button.setEnabled(true);

            while (it.hasNext()) {
                button = it.next();
                marginLeft = (int)convertDpToPixel(20, button.getContext());
                params = (RelativeLayout.LayoutParams) button.getLayoutParams();
                params.setMargins(-marginLeft, params.topMargin, params.rightMargin, params.rightMargin);
                button.setLayoutParams(params);
                button.setEnabled(true);
            }
        }
    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public void select(MyImageButton button) {
        if (null != currentSelectedButton) currentSelectedButton.unSelect();

        MyImageButton b;
        Iterator<MyImageButton> it = buttons.iterator();
        while (it.hasNext()) {
            b = it.next();
            if (b == button) {
                currentSelectedButton = button;
                currentSelectedButton.select();
                break;
            }
        }
    }

    public void unSelect() {
        if (null != currentSelectedButton) currentSelectedButton.unSelect();
    }

    public ImageButton getCurrentSelectedButton() {
        return currentSelectedButton;
    }
}
