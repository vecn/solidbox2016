package com.cimat.solidbox.Utilities;

/**
 * Created by ernesto on 26/01/16.
 * The view pager adapter handles the swipe tabs feature:
 */
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.cimat.solidbox.R;
import com.cimat.solidbox.Utilities.Preprocess.PreprocessFragment;

public class PageAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private PreprocessFragment preAndPostProcessFragment;

    private void initializeVariables() {
        mNumOfTabs = 0;
        preAndPostProcessFragment = null;
    }

    public PageAdapter(FragmentManager fm, int NumOfTabs,
                       Bundle savedInstanceState) {
        super(fm);

        Bundle args;
        Fragment fragment;
        initializeVariables();

        this.mNumOfTabs = NumOfTabs;
        //*
        if (null == savedInstanceState) {
            preAndPostProcessFragment = new PreprocessFragment();
            args = new Bundle();
            args.putInt("fragmentType", 0);
            preAndPostProcessFragment.setArguments(args);
        } else {
            Log.i("Geometry fragment", "Number of fragments " + fm.getFragments().size());
            fragment = fm.getFragments().get(0);
            if (0 == fragment.getArguments().getInt("fragmentType"))
                preAndPostProcessFragment = (PreprocessFragment) fragment;
        }
        //*/
    }

    //*
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return preAndPostProcessFragment;
            default:
                return null;
        }
    }

    public PreprocessFragment getPreAndPostProcessFragment() {
        return preAndPostProcessFragment;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
    //*/
}
