package com.cimat.solidbox.Utilities.Solver;

import android.view.Surface;

import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.Constants.SolverStatus;
import com.cimat.solidbox.Utilities.Preprocess.Conditions;
import com.cimat.solidbox.Utilities.Preprocess.Conditions.PunctualFixedDisplacement;
import com.cimat.solidbox.Utilities.Preprocess.Conditions.PunctualForce;
import com.cimat.solidbox.Utilities.Preprocess.Conditions.LinearFixedDisplacement;
import com.cimat.solidbox.Utilities.Preprocess.Conditions.UniformDistributedForce;
import com.cimat.solidbox.Utilities.Preprocess.Geometry;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.Vertex;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Shape;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.LinearShape;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.LineSegment;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.SurfaceShape;
import com.cimat.solidbox.Utilities.Preprocess.GeometrySet;
import com.cimat.solidbox.Utilities.ProblemData;
import com.cimat.solidbox.Utilities.Tools;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Iterator;

import nb.geometricBot.Model;
import nb.pdeBot.BoundaryConditions;
import nb.pdeBot.Material;
import nb.pdeBot.finiteElement.MeshResults;
import nb.pdeBot.finiteElement.solidMechanics.Analysis2D;
import nb.pdeBot.finiteElement.solidMechanics.StaticDamage;
import nb.pdeBot.finiteElement.solidMechanics.StaticElasticity2D;
import nb.pdeBot.finiteElement.solidMechanics.StaticPlasticity2D;

/**
 * Created by ernesto on 12/09/16.
 */

public class Solver {
    private GeometrySet geometry;
    private ProblemData problemData;
    private SurfaceShape surface;
    private Model model;
    private SolverStatus status;

    BoundaryConditions punctualForces;
    BoundaryConditions punctualFixedDisplacements;
    BoundaryConditions linearForces;
    BoundaryConditions linearFixedDisplacements;

    private Material material;
    private Analysis2D analysis2D;
    MeshResults meshResults;

    private void initializeVariables() {
        geometry = null;
        problemData = null;
        surface = null;
        model = null;
        punctualForces = null;
        punctualFixedDisplacements = null;
        linearForces = null;
        linearFixedDisplacements = null;
        status = SolverStatus.NOT_SOLVED;

        material = null;
        analysis2D = null;
        meshResults = null;
    }

    public Solver() {
        initializeVariables();
    }

    public void setData(GeometrySet geometry,
                        ProblemData problemData) {
        this.geometry = geometry;
        this.problemData = problemData;
    }

    public void solve() {
        // Gets the first surface found
        Iterator<SurfaceShape> it = geometry.getSurfaceShapes().iterator();
        if (it.hasNext()) {
            surface = it.next();
        } else {
            status = SolverStatus.MODEL_IS_INCOMPLETE;
            return;
        }

        // Gets the model from the first shape (it has to be a surface!!)
        //model = surface.getModel();
        // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        // Builds the boundary conditions
        constructBoundaryConditions();

        // Sets the material
        setMaterial();

        // Sets the problem data
        setProblemData();

        // Solves the problem
        switch(problemData.getTypeOfModel()) {
            case Constants.ELASTIC_PROBLEM:
                meshResults = StaticElasticity2D.solve(model, material, analysis2D,
                        punctualFixedDisplacements, punctualForces,
                        linearFixedDisplacements, linearForces,
                        problemData.getNumberOfMeshNodes());

                if (meshResults.FEMSuccess()) status = SolverStatus.OK;
                break;
            case Constants.PLASTIC_PROBLEM:
                meshResults = StaticPlasticity2D.solve(model, material, analysis2D,
                    punctualFixedDisplacements, punctualForces,
                    linearFixedDisplacements, linearForces,
                    problemData.getNumberOfMeshNodes());

                if (meshResults.FEMSuccess()) status = SolverStatus.OK;
                break;
            case Constants.DAMAGE_PROBLEM:
                meshResults = StaticDamage.solve(model, material, analysis2D,
                        punctualFixedDisplacements, punctualForces,
                        linearFixedDisplacements, linearForces,
                        problemData.getNumberOfMeshNodes());

                if (meshResults.FEMSuccess()) status = SolverStatus.OK;
                break;
        }
    }

    private void setMaterial() {
        material = new Material(2.11e6, 0.3, 0.3e6, 0.0, 500.0, 400e1, 400e1);
        /*
        switch(problemData.getTypeOfModel()){
            case Constants.ELASTIC_PROBLEM:
                material = new Material(2.11e6, 0.3, 0.0, 0.0, 0.0, 0.0, 0.0);
                break;
            case Constants.PLASTIC_PROBLEM:
                material = new Material(2.11e6, 0.3, 0.3e6, 0.0, 0.0, 0.0, 400e1);
                break;
            case Constants.DAMAGE_PROBLEM:
                material = new Material(2.11e6, 0.3, 0.0, 0.0, 500, 400e1, 0.0);
                break;
        }//*/
    }

    public SolverStatus getStatus() { return status; }

    private void setProblemData() {
        analysis2D = Analysis2D.PLANE_STRESS;
        switch (problemData.getTypeOfProblem()) {
            case Constants.PLANE_STRESS:
                analysis2D = Analysis2D.PLANE_STRESS;
                break;
            case Constants.PLANE_STRAIN:
                analysis2D = Analysis2D.PLANE_STRAIN;
                break;
            case Constants.SOLID_OF_REVOLUTION:
                analysis2D = Analysis2D.SOLID_OF_REVOLUTION;
                //analysis2D.setRevolutionEdgeID(
                //        geometry.getSurface(0).getEgeIdOnModel(problemData.solidOfRevolution.getAxis()));
                break;
        }
        analysis2D.setThickness(problemData.plate.getThickness());
    }

    /*private int getModelType() {
        int modelType = 0;
        switch (problemData.getTypeOfModel()) {
            case 0:
                modelType = 0;
                return modelType;
            case 1:
                modelType = 1;
                return modelType;
            case 2:
                modelType = 2;
                return modelType;
        }
        return 1;
    }*/

    public MeshResults getMeshResults() { return meshResults; }

    // Creates the boundary conditions
    private void constructBoundaryConditions() {
        constructBoundaryConditionsDirichlet();
        constructBoundaryConditionsNeumann();
    }

    private void constructBoundaryConditionsNeumann() {
        punctualForces = getImposedPunctualForces(surface);
        linearForces = getImposedLinearForces(surface);
        Tools.printBoundaryConditions("-------> Nodal forces",
                punctualForces);
        Tools.printBoundaryConditions("-------> Linear forces",
                linearForces);
    }

    private void constructBoundaryConditionsDirichlet() {
        punctualFixedDisplacements = getImposedPunctualDisplacements(surface);
        linearFixedDisplacements = getImposedLinearDisplacements(surface);
        Tools.printBoundaryConditions("-------> Fixed nodal displacements",
                punctualFixedDisplacements);
        Tools.printBoundaryConditions("-------> Fixed linear displacements",
                linearFixedDisplacements);
    }

    /*
    private BoundaryConditions getImposedPunctualDisplacements(Shape shape) {
        int size, count;
        Geometry.Vertex vertex;
        Conditions.PunctualFixedDisplacement fixedDisplacement;
        BoundaryConditions imposedDisplacements = new BoundaryConditions();
        ArrayList<Vertex> vertices = shape.getVertices();
        Iterator<Geometry.Vertex> it;

        // Number of vertices with a fixed displacement
        size = 0;
        it = vertices.iterator();
        while (it.hasNext()) {
            vertex = it.next();
            if (vertex.hasFixedDisplacement()) size++;
        }

        int ids[] = new int[size];
        char dof[] = new char[size];
        double val[] = new double[size*2];

        count = 0;
        while (it.hasNext()) {
            vertex = it.next();
            if (vertex.hasFixedDisplacement()) {
                fixedDisplacement = vertex.getFixedDisplacement();
                if (fixedDisplacement.isFixedX() && fixedDisplacement.isFixedY()) {
                    ids[count] = vertex.getId();
                    dof[count] = 'a';
                    val[count*2] = fixedDisplacement.getDisplacementX();
                    val[count*2 + 1] = fixedDisplacement.getDisplacementY();
                } else if (fixedDisplacement.isFixedX()) {
                    ids[count] = vertex.getId();
                    dof[count] = 'x';
                    val[count*2] = fixedDisplacement.getDisplacementX();
                    val[count*2 + 1] = 0f;
                } else if (fixedDisplacement.isFixedY()){
                    ids[count] = vertex.getId();
                    dof[count] = 'y';
                    val[count*2] = 0f;
                    val[count*2 + 1] = fixedDisplacement.getDisplacementY();
                }
                count++;
            }
        }
        imposedDisplacements.set(ids, dof, val);

        return imposedDisplacements;
    }
    //*/
    // PENDING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    private BoundaryConditions getImposedPunctualDisplacements(SurfaceShape surfaceShape) {
        // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        BoundaryConditions imposedDisplacements = new BoundaryConditions();
        /*
        int size, count;
        Iterator<LinearShape> it;
        Iterator<Vertex> is;
        Vertex vertex;
        PunctualFixedDisplacement fixedDisplacement;

        // Number of vertices with a a fixed displacement
        size = 0;
        it = surfaceShape.getContourLinearShapes().iterator();
        while (it.hasNext()) {
            is = it.next().getVertices().iterator();
            while (is.hasNext())
                if (is.next().hasFixedDisplacement()) size++;
        }

        int ids[] = new int[size];
        char dof[] = new char[size];
        double val[] = new double[size*2];

        count = 0;
        it = surfaceShape.getContourLinearShapes().iterator();
        while (it.hasNext()) {
            is = it.next().getVertices().iterator();
            while (is.hasNext()) {
                vertex = is.next();
                if (vertex.hasFixedDisplacement()) {
                    fixedDisplacement = vertex.getFixedDisplacement();
                    if (fixedDisplacement.isFixedX() && fixedDisplacement.isFixedY()) {
                        ids[count] = vertex.getId();
                        dof[count] = 'a';
                        val[count*2] = fixedDisplacement.getDisplacementX();
                        val[count*2+1] = fixedDisplacement.getDisplacementY();
                    } else if (fixedDisplacement.isFixedX()) {
                        ids[count] = vertex.getId();
                        dof[count] = 'x';
                        val[count*2] = fixedDisplacement.getDisplacementX();
                        val[count*2+1] = 0f;
                    } else if (fixedDisplacement.isFixedY()){
                        ids[count] = vertex.getId();
                        dof[count] = 'y';
                        val[count*2] = 0f;
                        val[count*2+1] = fixedDisplacement.getDisplacementY();
                    }
                    count++;
                }
            }
        }
        imposedDisplacements.set(ids, dof, val);
        //*/
        // PENDING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        return imposedDisplacements;
    }

    /*
    private BoundaryConditions getImposedPunctualForces(Shape shape) {
        int size, count;
        Vertex vertex;
        PunctualForce force;
        BoundaryConditions imposedForces = new BoundaryConditions();
        ArrayList<Vertex> vertices = shape.getVertices();
        Iterator<Vertex> it;

        // Number of vertices with a fixed displacement
        size = 0;
        it = vertices.iterator();
        while (it.hasNext()) {
            vertex = it.next();
            if (vertex.hasAppliedForce()) size++;
        }

        int ids[] = new int[size];
        char dof[] = new char[size];
        double val[] = new double[size*2];

        count = 0;
        it = vertices.iterator();
        while (it.hasNext()) {
            vertex = it.next();
            if (vertex.hasAppliedForce()) {
                force = vertex.getForce();
                // Always two forces are specified (some of them may be zero)
                ids[count] = vertex.getId();
                dof[count] = 'a';
                val[count*2] = force.geValueOnX();
                val[count*2 + 1] = force.geValueOnY();
                count++;
            }
        }
        imposedForces.set(ids, dof, val);

        return imposedForces;
    }
    //*/
    // PENDING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    private BoundaryConditions getImposedPunctualForces(SurfaceShape surfaceShape) {
        BoundaryConditions imposedForces = new BoundaryConditions();
        /*
        int size, count;
        PunctualForce force;
        Iterator<LinearShape> it;
        Iterator<Vertex> is;
        Vertex vertex;

        // Number of vertices with a punctual force
        size = 0;
        it = surfaceShape.getContourLinearShapes().iterator();
        while (it.hasNext()) {
            is = it.next().getVertices().iterator();
            while (is.hasNext())
                if (is.next().hasAppliedForce()) size++;
        }

        int ids[] = new int[size];
        char dof[] = new char[size];
        double val[] = new double[size*2];

        count = 0;
        it = surfaceShape.getContourLinearShapes().iterator();
        while (it.hasNext()) {
            is = it.next().getVertices().iterator();
            while (is.hasNext()) {
                vertex = is.next();
                if (vertex.hasAppliedForce()) {
                    force = vertex.getForce();
                    ids[count] = vertex.getId();
                    dof[count] = 'a';
                    val[count*2] = force.geValueOnX();
                    val[count*2+1] = force.geValueOnY();
                    count++;
                }
            }
        }
        imposedForces.set(ids, dof, val);
        //*/
        // PENDING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        return imposedForces;
    }

    private BoundaryConditions getImposedLinearForces(SurfaceShape surfaceShape) {
        BoundaryConditions imposedForces = new BoundaryConditions();
        /*
        int size, count;
        float fx, fy;
        Iterator<LinearShape> it;
        Iterator<LineSegment> is;
        LinearShape linearShape;
        LineSegment lineSegment;
        UniformDistributedForce uniformDistributedForce;

        // Number of line segments with a linear force
        size = 0;
        it = surfaceShape.getContourLinearShapes().iterator();
        while (it.hasNext()) {
            linearShape = it.next();
            if (linearShape.hasAppliedUniformForce())
                size += linearShape.getLineSegments().size();
            else {
                // The linear force may be applied only over
                // certain segments
                is = linearShape.getLineSegments().iterator();
                while (is.hasNext())
                    if (is.next().hasAppliedUniformDistributedForce())
                        size++;
            }
        }

        int ids[] = new int[size];
        char dof[] = new char[size];
        double val[] = new double[size*2];

        count = 0;
        it = surfaceShape.getContourLinearShapes().iterator();
        while (it.hasNext()) {
            linearShape = it.next();
            if (linearShape.hasAppliedUniformForce()) {
                fx = linearShape.getUniformForce().getValueOnX();
                fy = linearShape.getUniformForce().getValueOnX();
                is = linearShape.getLineSegments().iterator();
                while (is.hasNext()) {
                    // Always two forces are specified (some of them may be zero)
                    ids[count] = is.next().getId();
                    dof[count] = 'a';
                    val[count*2] = fx;
                    val[count*2 + 1] = fy;
                    count++;
                }
            } else {
                is = linearShape.getLineSegments().iterator();
                while (is.hasNext()) {
                    lineSegment = is.next();
                    if (lineSegment.hasAppliedUniformDistributedForce()) {
                        fx = lineSegment.getUniformDistributedForce().getValueOnX();
                        fy = lineSegment.getUniformDistributedForce().getValueOnY();
                        ids[count] = lineSegment.getId();
                        dof[count] = 'a';
                        val[count*2] = fx;
                        val[count*2 + 1] = fy;
                        count++;
                    }
                }
            }
        }
        imposedForces.set(ids, dof, val);
        //*/
        // PENDING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        return imposedForces;
    }

    private BoundaryConditions getImposedLinearDisplacements(SurfaceShape surfaceShape) {
        BoundaryConditions imposedDisplacements = new BoundaryConditions();
        /*
        int size, count;
        float dx, dy;
        Iterator<LinearShape> it;
        Iterator<LineSegment> is;
        LinearShape linearShape;
        LineSegment lineSegment;
        LinearFixedDisplacement fixedDisplacement;


        // Number of segments with a linear displacement
        size = 0;
        it = surfaceShape.getContourLinearShapes().iterator();
        while (it.hasNext()) {
            linearShape = it.next();
            if (linearShape.hasFixedDisplacement())
                size += linearShape.getLineSegments().size();
            else {
                is = linearShape.getLineSegments().iterator();
                while (is.hasNext())
                    if (is.next().hasLinearFixedDisplacement())
                        size++;
            }
        }

        int ids[] = new int[size];
        char dof[] = new char[size];
        double val[] = new double[size*2];

        count = 0;
        it = surfaceShape.getContourLinearShapes().iterator();
        while (it.hasNext()) {

            linearShape = it.next();

            // The displacement can be imposed over the entire
            // linear shape or...
            if (linearShape.hasFixedDisplacement()) {
                count = setFixedDisplacementValues(ids, dof, val, count,
                        linearShape);
            } else {
                // .. or only a few line segments
                is = linearShape.getLineSegments().iterator();
                while (is.hasNext()) {
                    lineSegment = is.next();
                    if (lineSegment.hasLinearFixedDisplacement()) {
                        count = setFixedDisplacementValues(ids, dof, val, count,
                                lineSegment);
                    }
                }
            }
        }
        imposedDisplacements.set(ids, dof, val);
        //*/
        // PENDING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        return imposedDisplacements;
    }

    private int setFixedDisplacementValues(int[] ids,
                                           char[] dof,
                                           double[] val,
                                           int count,
                                           LinearShape linearShape) {
        /*
        LinearFixedDisplacement fixedDisplacement =
                linearShape.getFixedDisplacement();
        Iterator<LineSegment> is = linearShape.getLineSegments().iterator();
        float dx, dy;

        if (fixedDisplacement.isFixedOnXY()) {
            dx = fixedDisplacement.getValueOnX();
            dy = fixedDisplacement.getValueOnY();

            while (is.hasNext()) {
                ids[count] = is.next().getId();
                dof[count] = 'a';
                val[count*2] = dx;
                val[count*2 + 1] = dy;
                count++;
            }

        } else if (fixedDisplacement.isFixedOnX()) {
            dx = fixedDisplacement.getValueOnX();

            while (is.hasNext()) {
                ids[count] = is.next().getId();
                dof[count] = 'x';
                val[count*2] = dx;
                val[count*2 + 1] = 0f;
                count++;
            }

        } else if (fixedDisplacement.isFixedOnY()) {
            dy = fixedDisplacement.getValueOnY();

            while (is.hasNext()) {
                ids[count] = is.next().getId();
                dof[count] = 'y';
                val[count*2] = 0f;
                val[count*2 + 1] = dy;
                count++;
            }

        }
        //*/
        // PENDING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        return count;
    }

    /*
    private int setFixedDisplacementValues(int[] ids,
                                           char[] dof,
                                           double[] val,
                                           int count,
                                           LineSegment lineSegment) {
        LinearFixedDisplacement fixedDisplacement =
                lineSegment.getLinearFixedDisplacement();
        float dx, dy;

        if (fixedDisplacement.isFixedOnXY()) {
            dx = fixedDisplacement.getValueOnX();
            dy = fixedDisplacement.getValueOnY();

            ids[count] = lineSegment.getId();
            dof[count] = 'a';
            val[count*2] = dx;
            val[count*2 + 1] = dy;

        } else if (fixedDisplacement.isFixedOnX()) {
            dx = fixedDisplacement.getValueOnX();

            ids[count] = lineSegment.getId();
            dof[count] = 'x';
            val[count*2] = dx;
            val[count*2 + 1] = 0f;

        } else if (fixedDisplacement.isFixedOnY()) {
            dy = fixedDisplacement.getValueOnY();

            ids[count] = lineSegment.getId();
            dof[count] = 'y';
            val[count*2] = 0f;
            val[count*2 + 1] = dy;
        }

        return count + 1;
    }
    //*/ // PENDING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

}
