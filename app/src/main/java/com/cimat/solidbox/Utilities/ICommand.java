package com.cimat.solidbox.Utilities;

/**
 * Created by ernesto on 8/08/16.
 */

public interface ICommand {
    void execute();
    void unExecute();
}
