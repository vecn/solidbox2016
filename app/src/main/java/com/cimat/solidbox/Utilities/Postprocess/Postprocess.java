package com.cimat.solidbox.Utilities.Postprocess;

import com.cimat.solidbox.OpenGL.Utilities.Colors;
import com.cimat.solidbox.OpenGL.Utilities.Colors.ColorPalette;
import com.cimat.solidbox.OpenGL.Utilities.Colors.ColorPaletteRainbow;
import com.cimat.solidbox.OpenGL.Utilities.Colors.ColorPaletteSunset;
import com.cimat.solidbox.OpenGL.Views.PrePostRenderer;
import com.cimat.solidbox.OpenGL.Views.ResultsRenderer;
import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.ProblemData;
import com.cimat.solidbox.Utilities.Tools;

import nb.pdeBot.finiteElement.MeshResults;

/**
 * Created by ernesto on 12/09/16.
 */

public class Postprocess {
    private MeshResults meshResults;
    private PrePostRenderer resultsRenderer;
    private ProblemData problemData;
    private float[] originalVertices;
    private float displacementsScaleFactor;

    boolean showMesh, showDistortion;
    private int typeOfSurfaceResultsToShow;
    //ColorPalette colorPalette;

    private void initializeVariables() {
        meshResults = null;
        resultsRenderer = null;
        problemData = null;
        originalVertices = null;
        displacementsScaleFactor = 10f;

        showMesh = false;
        typeOfSurfaceResultsToShow = Constants.DISPLACEMENTS;

        showDistortion = false;

        //colorPalette = new ColorPaletteRainbow();
        //colorPalette = new ColorPaletteSunset();

    }

    public Postprocess(ProblemData problemData,
                       PrePostRenderer resultsRenderer) {
        initializeVariables();
        this.problemData = problemData;
        this.resultsRenderer = resultsRenderer;
    }

    public void setData(MeshResults meshResults) {
        if (null == meshResults) return;
        if (!meshResults.FEMSuccess()) return;
        this.meshResults = meshResults;

        savesCopyOfVertices();
        if (showDistortion)
           addDisplacementsOnTheMesh(
                   displacementsScaleFactor,
                   originalVertices,
                   meshResults
           );

        resultsRenderer.resultsRenderer.setResultsData(
                meshResults,
                problemData
        );
    }

    public void setDisplacementsScaleFactor(float value) {
        if (null == meshResults) return;

        displacementsScaleFactor = value;
        if (!showDistortion) return;
        addDisplacementsOnTheMesh(displacementsScaleFactor,
                originalVertices, meshResults);
        //resultsRenderer.resultsRenderer.changeVerticesCoordinates(meshResults);
        resultsRenderer.resultsRenderer.addUpdate(
                Constants.RESULTS_UPDATE_SET_NEW_COORDINATES);
    }

    public void showInitialResults() {
        if (null == meshResults) return;

        resultsRenderer.resultsRenderer.
                setTypeOfSurfaceResultsToShow(typeOfSurfaceResultsToShow);
        resultsRenderer.resultsRenderer.addUpdate(
                Constants.RESULTS_UPDATE_SET_INITIAL_SURFACE_RESULT);
        if (showMesh) {
            resultsRenderer.resultsRenderer.drawMesh();
        }
    }

    public void switchShowMeshStatus() {
        if (null == meshResults) return;

        if (showMesh) {
            showMesh = false;
            resultsRenderer.resultsRenderer.hideMesh();
        } else {
            showMesh = true;
            resultsRenderer.resultsRenderer.drawMesh();
        }
    }

    public void switchShowDistortionStatus() {
        if (meshResults == null) return;

        if (showDistortion) {
            showDistortion = false;
            addDisplacementsOnTheMesh(0, originalVertices, meshResults);
            //resultsRenderer.resultsRenderer.changeVerticesCoordinates(meshResults);
            resultsRenderer.resultsRenderer.addUpdate(
                    Constants.RESULTS_UPDATE_SET_NEW_COORDINATES);
        } else {
            showDistortion = true;
            addDisplacementsOnTheMesh(displacementsScaleFactor,
                    originalVertices, meshResults);
            //resultsRenderer.resultsRenderer.changeVerticesCoordinates(meshResults);
            resultsRenderer.resultsRenderer.addUpdate(
                    Constants.RESULTS_UPDATE_SET_NEW_COORDINATES);
        }
    }

    public void showResults(int typeOfSurfaceResultsToShow) {
        if (null == meshResults) return;

        this.typeOfSurfaceResultsToShow = typeOfSurfaceResultsToShow;
        resultsRenderer.resultsRenderer.setTypeOfSurfaceResultsToShow(
                typeOfSurfaceResultsToShow);
        resultsRenderer.resultsRenderer.addUpdate(
                Constants.RESULTS_UPDATE_SET_NEW_SURFACE_RESULTS);
        //resultsRenderer.resultsRenderer.setSurfaceResult();
    }

    private void savesCopyOfVertices() {
        if (null == meshResults) return;

        float[] vertices = meshResults.getVerticesRef();
        originalVertices = new float[vertices.length];
        for (int i = 0; i < vertices.length; i++) originalVertices[i] = vertices[i];
    }

    private void addDisplacementsOnTheMesh(float displacementScaleFactor,
                                           float[] originalVertices,
                                           MeshResults meshResults) {
        if (null == meshResults) return;

        float[] displacements = meshResults.getDisplacementRef();
        float[] vertices = meshResults.getVerticesRef();

        for (int i = 0; i < vertices.length; i++)
            vertices[i] = originalVertices[i] + displacementScaleFactor*displacements[i];
    }
}
