package com.cimat.solidbox.Utilities.Preprocess;

/**
 * Created by ernesto on 26/01/16.
 */
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.cimat.solidbox.OpenGL.Utilities.TextureHelper;
import com.cimat.solidbox.OpenGL.Views.PrePostGLSurfaceView;
import com.cimat.solidbox.Utilities.ButtonGroup;
import com.cimat.solidbox.Utilities.MyImageButton;
import com.cimat.solidbox.Utilities.PlotParameters;
import com.cimat.solidbox.Utilities.Postprocess.Postprocess;
import com.cimat.solidbox.Utilities.ProblemData;
import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.Constants.CADOperation;

import java.io.File;
import java.util.ArrayList;
import java.util.Stack;

import com.cimat.solidbox.R;
import com.cimat.solidbox.Utilities.Solver.Solver;

import nb.geometricBot.Mesh;
import nb.geometricBot.Model;

public class PreprocessFragment extends Fragment
        implements View.OnClickListener {
    // Container Activity must implement this interface
    public interface OnCADResultsSwitchPressedListener {
        void onCADResultsSwitchActivated();
    }

    OnCADResultsSwitchPressedListener mCallback;

    /**
     * Hold a reference to our GLSurfaceView
     */
    private PrePostGLSurfaceView glSurfaceView;
    private GeometrySet geometry;
    private Solver solver;
    private Postprocess postprocess;
    private boolean isCADActive;

    ButtonGroup mainGroup;
    ButtonGroup mainGroupRes;
    ButtonGroup conditionsGroup;
    ButtonGroup displacementGroup;
    ButtonGroup supportGroup;
    ButtonGroup materialsGroup;
    ButtonGroup metalsGroup;
    ButtonGroup problemTypeGroup;
    ButtonGroup parametricShapesGroup;
    ButtonGroup booleanOperationsGroup;
    ButtonGroup linearShapeOperationsGroup;
    Stack<ButtonGroup> currentGroups;
    //*

    MyImageButton boundaryConditions;
    MyImageButton materials;
    MyImageButton problemType;
    MyImageButton parametricShapes;
    MyImageButton booleanOperations;
    MyImageButton linearShapeOperations;

    MyImageButton punctualForce;
    MyImageButton displacement;
    MyImageButton displacementOnXY;
    MyImageButton displacementOnX;
    MyImageButton displacementOnY;
    MyImageButton support;
    MyImageButton supportOnXY;
    MyImageButton supportOnX;
    MyImageButton supportOnY;

    MyImageButton metals;
    MyImageButton steel;
    MyImageButton iron;
    MyImageButton alum;

    MyImageButton planeStress;
    MyImageButton planeStrain;
    MyImageButton axiallySimmetric;

    MyImageButton rectangle;
    MyImageButton circle;
    MyImageButton regularPolygon;

    MyImageButton unify;
    MyImageButton combine;
    MyImageButton difference;
    MyImageButton intersect;
    MyImageButton substract;

    MyImageButton showResults;
    MyImageButton vonMises;
    MyImageButton plastic_damaged_elems;
    //*/

    SwitchCompat magneticGrid;
    SwitchCompat results;
    SwitchCompat showMesh;
    SwitchCompat showDistortion;

    ImageView magneticGridFigure, distortion, mesh;

    boolean boundaryConditionsIsActive;
    boolean materialsIsActive;
    boolean problemTypeIsActive;
    boolean parametricShapesIsActive;
    boolean booleanOperationsIsActive;
    boolean linearShapeOperationsIsActive;

    Conditions.Force currentForce;
    Conditions.FixedDisplacement currentFixedDisplacement;
    //Geometry.CleanPolyLine axisLine;

    PlotParameters plotParameters;
    PlotParameters plotParametersForResults;
    ProblemData problemData;


    boolean showGrid;

    public void saveInnerId() {
        Bundle args = new Bundle();
        args.putInt("myId", this.getId());
        this.setArguments(args);
        Log.i("Geometry fragment", "myId saved = " + getId());
    }

    public int getInnerId() {
        return getArguments().getInt("myId");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        geometry = new GeometrySet();

        isCADActive = true;

        // Initial plot parameters
        plotParameters = new PlotParameters();
        plotParameters.setZoomFactor(1f);
        plotParameters.setDepth(1f);
        plotParameters.setCenter(0f, 0f, 0f);

        // For the results
        plotParametersForResults = new PlotParameters();
        plotParametersForResults.setZoomFactor(1f);
        plotParametersForResults.setDepth(1f);
        plotParametersForResults.setCenter(0f, 0f, 0f);

        problemData = new ProblemData();
        currentForce = new Conditions.Force();
        currentFixedDisplacement = new Conditions.FixedDisplacement();
        //axisLine = null;

        showGrid = false;

        Log.i("Geometry fragment", "onCreate " + this.getId());

        //saveInnerId();
    }

    // This method is only called if  if this fragment needs
    // to be shown!!
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.preprocess_tab, container, false);

        Log.i("Geometry fragment", "onCreateView " + this.getId());

        mainGroup = new ButtonGroup();
        mainGroupRes = new ButtonGroup();
        conditionsGroup = new ButtonGroup();
        displacementGroup = new ButtonGroup();
        supportGroup = new ButtonGroup();
        materialsGroup = new ButtonGroup();
        metalsGroup = new ButtonGroup();
        problemTypeGroup = new ButtonGroup();
        parametricShapesGroup = new ButtonGroup();
        booleanOperationsGroup = new ButtonGroup();
        linearShapeOperationsGroup = new ButtonGroup();

        currentGroups = new Stack();

        setMainGroup(view);
        setMainGroupRes(view);
        setConditionsGroup(view);
            setDisplacementGroup(view);
        setSupportGroup(view);
        setMaterialsGroup(view);
            setMetalsGroup(view);
        setProblemTypeGroup(view);
        setParametricShapesGroup(view);
        setBooleanOperationsGroup(view);
        setLinearShapeOperationsGroup(view);
        if (isCADActive) deactivateResultButtons();

        magneticGrid = (SwitchCompat) view.findViewById(R.id.magneticGridSwitch);
        magneticGrid.setOnClickListener(this);

        results = (SwitchCompat) view.findViewById(R.id.CADResultsSwitch);
        results.setOnClickListener(this);

        showDistortion = (SwitchCompat) view.findViewById(R.id.showDistortionSwitch);
        showDistortion.setOnClickListener(this);

        showMesh = (SwitchCompat) view.findViewById(R.id.showMeshSwitch);
        showMesh.setOnClickListener(this);

        distortion = (ImageView) view.findViewById(R.id.distortion);
        mesh = (ImageView) view.findViewById(R.id.mesh);
        magneticGridFigure = (ImageView) view.findViewById(R.id.magneticGridFigure);

        boundaryConditionsIsActive = false;
        materialsIsActive = false;
        problemTypeIsActive = false;
        parametricShapesIsActive = false;
        boundaryConditionsIsActive = false;
        linearShapeOperationsIsActive = false;

        //fixDisplacement.setImageResource(R.drawable.supports_not_selected);

        // Creates the glSurfaceView and sets the data for the renders
        glSurfaceView =
                (PrePostGLSurfaceView) view.findViewById(R.id.glSurfaceView);
        glSurfaceView.setReferenceToParentFragment(this);
        glSurfaceView.setParameters(geometry, plotParameters,
                plotParametersForResults, problemData);

        // For the solver
        solver = new Solver();
        solver.setData(geometry, problemData);

        // For the postprocess
        postprocess = new Postprocess(problemData, glSurfaceView.renderer);

        // To enable menus for this fragment so they appear in the menu toolbar
        setHasOptionsMenu(true);

        magneticGrid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                glSurfaceView.renderer.cadRenderer.switchStatusMagneticGrid();
            }
        });

        results.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCallback.onCADResultsSwitchActivated();
            }
        });

        showMesh.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                postprocess.switchShowMeshStatus();
            }
        });

        showDistortion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                postprocess.switchShowDistortionStatus();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented the callback interface.
        // If not, it throws an exception
        try {
            mCallback = (OnCADResultsSwitchPressedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCreateMeshButtonPressed interface");
        }

        Log.i("Geometry fragment", "onAttach");
        if (null == mCallback)
            Log.i("Geometry fragment", "mCallBack is NULL");
        else
            Log.i("Geometry fragment", "mCallBack is NOT NULL");
    }

    public void switchPreAndPro() {
        //*
        if (isCADActive) {
            solver.solve();
            glSurfaceView.renderer.activateResultsRenderer();

            hideAndUnselectCurrentGroups();
            mainGroup.unSelect();
            mainGroup.deactivate();

            magneticGrid.setVisibility(View.GONE);
            magneticGridFigure.setVisibility(View.GONE);

            showMesh.setVisibility(View.VISIBLE);
            mesh.setVisibility(View.VISIBLE);
            showDistortion.setVisibility(View.VISIBLE);
            distortion.setVisibility(View.VISIBLE);

            mainGroupRes.activate();

            switch (solver.getStatus()) {
                case MODEL_IS_INCOMPLETE:
                    Toast.makeText(getActivity(),
                            "Model is incomplete!", Toast.LENGTH_LONG).show();
                    break;
                case NOT_SOLVED:
                    Toast.makeText(getActivity(),
                            "The solve function has not been called!",
                            Toast.LENGTH_LONG).show();
                    break;
                case OK:
                    postprocess.setData(solver.getMeshResults());
                    postprocess.showInitialResults();
                    break;
            }

            isCADActive = false;

        } else {
            glSurfaceView.renderer.activeCADRenderer();

            mainGroupRes.unSelect();
            mainGroupRes.deactivate();

            magneticGrid.setVisibility(View.VISIBLE);
            magneticGridFigure.setVisibility(View.VISIBLE);

            showMesh.setVisibility(View.GONE);
            mesh.setVisibility(View.GONE);
            showDistortion.setVisibility(View.GONE);
            distortion.setVisibility(View.GONE);

            mainGroup.activate();

            isCADActive = true;
        }
        //*/
    }

    private void setMainGroup(View view) {
        boundaryConditions = (MyImageButton) view.findViewById(R.id.conditionsButton);
        materials = (MyImageButton) view.findViewById(R.id.materialsButton);
        problemType = (MyImageButton) view.findViewById(R.id.problemTypeButton);
        parametricShapes = (MyImageButton) view.findViewById(R.id.parametricShapesButton);
        booleanOperations = (MyImageButton) view.findViewById(R.id.booleanOperationsButton);
        linearShapeOperations = (MyImageButton) view.findViewById(R.id.linearShapeOperationsButton);

        boundaryConditions.setOnClickListener(this);
        materials.setOnClickListener(this);
        problemType.setOnClickListener(this);
        parametricShapes.setOnClickListener(this);
        booleanOperations.setOnClickListener(this);
        linearShapeOperations.setOnClickListener(this);

        boundaryConditions.setStateImages(R.drawable.boundary_conditions_off, R.drawable.boundary_conditions_on);
        materials.setStateImages(R.drawable.materials_off, R.drawable.materials_on);
        problemType.setStateImages(R.drawable.probleam_type_off, R.drawable.probleam_type_on);
        parametricShapes.setStateImages(R.drawable.parametric_shapes_off, R.drawable.parametric_shapes_on);
        booleanOperations.setStateImages(R.drawable.boolean_operations_off, R.drawable.boolean_operations_on);
        linearShapeOperations.setStateImages(R.drawable.linear_shape_operations_off, R.drawable.linear_shape_operations_on);

        mainGroup.add(boundaryConditions);
        mainGroup.add(materials);
        mainGroup.add(problemType);
        mainGroup.add(parametricShapes);
        mainGroup.add(booleanOperations);
        mainGroup.add(linearShapeOperations);
    }

    public void setMainGroupRes(View view) {
        showResults =(MyImageButton) view.findViewById(R.id.showResults);
        //vonMises = (MyImageButton) view.findViewById(R.id.showVonMisesButton);
        //plastic_damaged_elems = (MyImageButton) view.findViewById(R.id.showElemsResults);

        showResults.setOnClickListener(this);
        //vonMises.setOnClickListener(this);
        //plastic_damaged_elems.setOnClickListener(this);

        showResults.setStateImages(R.drawable.nodal_displacements_off, R.drawable.nodal_displacements_on);
        //vonMises.setStateImages(R.drawable.stress_off, R.drawable.stress_on);
        //plastic_damaged_elems.setStateImages(R.drawable.stress_off, R.drawable.stress_on);

        mainGroupRes.add(showResults);
        //mainGroupRes.add(vonMises);
        //mainGroupRes.add(plastic_damaged_elems);
    }

    public void deactivateResultButtons() {
        mainGroupRes.deactivate();
    }

    private void setConditionsGroup(View view) {
        punctualForce = (MyImageButton) view.findViewById(R.id.punctualForceButton);
        displacement = (MyImageButton) view.findViewById(R.id.displacementButton);
        support = (MyImageButton) view.findViewById(R.id.supportsButton);

        punctualForce.setOnClickListener(this);
        displacement.setOnClickListener(this);
        support.setOnClickListener(this);

        punctualForce.setStateImages(R.drawable.punctual_force_off, R.drawable.punctual_force_on);
        displacement.setStateImages(R.drawable.displacement_off, R.drawable.displacement_on);
        support.setStateImages(R.drawable.completely_fixed_off, R.drawable.completely_fixed_on);

        conditionsGroup.add(punctualForce);
        conditionsGroup.add(displacement);
        conditionsGroup.add(support);
    }

    private void setDisplacementGroup(View view) {
        displacementOnXY = (MyImageButton) view.findViewById(R.id.displacementOnXYButton);
        //displacementOnX = (MyImageButton) view.findViewById(R.id.displacementOnXButton);
        //displacementOnY = (MyImageButton) view.findViewById(R.id.displacementOnYButton);

        displacementOnXY.setOnClickListener(this);
        //displacementOnX.setOnClickListener(this);
        //displacementOnY.setOnClickListener(this);

        displacementOnXY.setStateImages(R.drawable.displacement_on_xy_off, R.drawable.displacement_on_xy_on);
        //displacementOnX.setStateImages(R.drawable.displacement_on_x_off, R.drawable.displacement_on_x_on);
        //displacementOnY.setStateImages(R.drawable.displacement_on_y_off, R.drawable.displacement_on_y_on);

        displacementGroup.add(displacementOnXY);
        //displacementGroup.add(displacementOnX);
        //displacementGroup.add(displacementOnY);
    }

    private void setSupportGroup(View view) {
        supportOnXY = (MyImageButton) view.findViewById(R.id.supportOnXYButton);
        supportOnX = (MyImageButton) view.findViewById(R.id.supportOnXButton);
        supportOnY = (MyImageButton) view.findViewById(R.id.supportOnYButton);

        supportOnXY.setOnClickListener(this);
        supportOnX.setOnClickListener(this);
        supportOnY.setOnClickListener(this);

        supportOnXY.setStateImages(R.drawable.displacement_on_xy_off, R.drawable.displacement_on_xy_on);
        supportOnX.setStateImages(R.drawable.displacement_on_x_off, R.drawable.displacement_on_x_on);
        supportOnY.setStateImages(R.drawable.displacement_on_y_off, R.drawable.displacement_on_y_on);

        supportGroup.add(supportOnXY);
        supportGroup.add(supportOnX);
        supportGroup.add(supportOnY);
    }

    private void setMaterialsGroup(View view) {
        metals = (MyImageButton) view.findViewById(R.id.metalsButton);

        metals.setOnClickListener(this);

        metals.setStateImages(R.drawable.metals_off, R.drawable.metals_on);

        materialsGroup.add(metals);
    }

    private void setMetalsGroup(View view) {
        steel = (MyImageButton) view.findViewById(R.id.steelButton);
        iron = (MyImageButton) view.findViewById(R.id.ironButton);
        alum = (MyImageButton) view.findViewById(R.id.alumButton);

        steel.setOnClickListener(this);
        iron.setOnClickListener(this);
        alum.setOnClickListener(this);

        steel.setStateImages(R.drawable.steel_off, R.drawable.steel_on);
        iron.setStateImages(R.drawable.iron_off, R.drawable.iron_on);
        alum.setStateImages(R.drawable.alum_off, R.drawable.alum_on);

        metalsGroup.add(steel);
        metalsGroup.add(iron);
        metalsGroup.add(alum);
    }

    private void setProblemTypeGroup(View view) {
        planeStress = (MyImageButton) view.findViewById(R.id.planeStressButton);
        planeStrain = (MyImageButton) view.findViewById(R.id.planeStrainButton);
        axiallySimmetric = (MyImageButton) view.findViewById(R.id.axiallySimmetricButton);

        planeStress.setOnClickListener(this);
        planeStrain.setOnClickListener(this);
        axiallySimmetric.setOnClickListener(this);

        planeStress.setStateImages(R.drawable.plane_stress_off, R.drawable.plane_stress_on);
        planeStrain.setStateImages(R.drawable.plane_strain_off, R.drawable.plane_strain_on);
        axiallySimmetric.setStateImages(R.drawable.axially_symmetric_off, R.drawable.axially_symmetric_on);

        problemTypeGroup.add(planeStrain);
        problemTypeGroup.add(planeStress);
        problemTypeGroup.add(axiallySimmetric);
    }

    private void setParametricShapesGroup(View view) {
        rectangle = (MyImageButton) view.findViewById(R.id.rectangleButton);
        circle = (MyImageButton) view.findViewById(R.id.circleButton);
        regularPolygon = (MyImageButton) view.findViewById(R.id.regularPolygonButton);

        rectangle.setOnClickListener(this);
        circle.setOnClickListener(this);
        regularPolygon.setOnClickListener(this);

        rectangle.setStateImages(R.drawable.rectangle_off, R.drawable.rectangle_on);
        circle.setStateImages(R.drawable.circle_off, R.drawable.circle_on);
        regularPolygon.setStateImages(R.drawable.regular_polygon_off, R.drawable.regular_polygon_on);

        parametricShapesGroup.add(rectangle);
        parametricShapesGroup.add(circle);
        parametricShapesGroup.add(regularPolygon);
    }

    private void setBooleanOperationsGroup(View view) {
        unify = (MyImageButton) view.findViewById(R.id.unionButton);
        combine = (MyImageButton) view.findViewById(R.id.combineButton);
        difference = (MyImageButton) view.findViewById(R.id.differenceButton);
        substract = (MyImageButton) view.findViewById(R.id.substractButton);
        intersect = (MyImageButton) view.findViewById(R.id.intersectionButton);

        unify.setOnClickListener(this);
        combine.setOnClickListener(this);
        difference.setOnClickListener(this);
        substract.setOnClickListener(this);
        intersect.setOnClickListener(this);

        unify.setStateImages(R.drawable.union_off, R.drawable.union_on);
        combine.setStateImages(R.drawable.combine_off, R.drawable.combine_on);
        difference.setStateImages(R.drawable.difference_off, R.drawable.difference_on);
        substract.setStateImages(R.drawable.substract_off, R.drawable.substract_on);
        intersect.setStateImages(R.drawable.intersection_off, R.drawable.intersection_on);

        booleanOperationsGroup.add(unify);
        booleanOperationsGroup.add(combine);
        booleanOperationsGroup.add(substract);
        booleanOperationsGroup.add(intersect);
        booleanOperationsGroup.add(difference);
    }

    private void setLinearShapeOperationsGroup(View view) {
        // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.i("Geometry fragment", "onActivityCreated");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cad, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog dialog;
        final AlertDialog.Builder alertDialogBuilder;
        LayoutInflater inflater;
        final View viewDialog;

        glSurfaceView.renderer.cadRenderer.unselectAllShapes();
        // handle item selection
        switch (item.getItemId()) {
            case R.id.show_grid:
                if (showGrid){
                    showGrid = false;
                    glSurfaceView.renderer.cadRenderer.showGrid();
                }
                else {
                    showGrid = true;
                    glSurfaceView.renderer.cadRenderer.hideGrid();
                }
                return true;

            case R.id.problem_data:
                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                inflater = getActivity().getLayoutInflater();

                // Inflates and sets the layout for the dialog. Pass null as the parent view because
                // its going in the dialog layout.
                viewDialog = inflater.inflate(R.layout.problem_data, null);
                alertDialogBuilder.setView(viewDialog);

                final EditText numberOfMeshNodes = (EditText) viewDialog.findViewById(R.id.NumberOfMeshNodes);

                // Ok button
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        problemData.setMeshSize(
                                Integer.parseInt(numberOfMeshNodes.getText().toString()));
                    }
                });

                // Cancel button
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Do nothing
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                return true;

            case R.id.model_type:
                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                inflater = getActivity().getLayoutInflater();

                // Inflates and sets the layout for the dialog. Pass null as the parent view because
                // its going in the dialog layout.
                viewDialog = inflater.inflate(R.layout.model_type, null);
                alertDialogBuilder.setView(viewDialog);

                RadioGroup rg = (RadioGroup) viewDialog.findViewById(R.id.modelTypeGroup);

                rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int id) {
                        switch (id)
                        {
                            case R.id.elasticButton:
                                problemData.setModelType(Constants.ELASTIC_PROBLEM);
                                break;
                            case R.id.plasticButton:
                                problemData.setModelType(Constants.PLASTIC_PROBLEM);
                                break;
                            case R.id.damageButton:
                                problemData.setModelType(Constants.DAMAGE_PROBLEM);
                                break;
                        }
                    }
                });

                /*// Ok button
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

                // Cancel button
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Do nothing
                    }
                });*/
                dialog = alertDialogBuilder.create();
                dialog.show();
                return true;

            case R.id.undoAction:
                glSurfaceView.renderer.cadRenderer.undo();
                return true;

            case R.id.redoAction:
                glSurfaceView.renderer.cadRenderer.redo();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void handleCheckListTypeOfProblem(final int typeOfProblem,
                                              final CheckBox planeStress,
                                              final CheckBox planeStrain,
                                              final CheckBox solidOfRevo) {


        // There is always a default type of problem
        switch (typeOfProblem) {
            case Constants.PLANE_STRESS:
                planeStress.setChecked(true);
                planeStrain.setChecked(false);
                solidOfRevo.setChecked(false);
                break;
            case Constants.PLANE_STRAIN:
                planeStress.setChecked(false);
                planeStrain.setChecked(true);
                solidOfRevo.setChecked(false);
                break;
            case Constants.SOLID_OF_REVOLUTION:
                planeStress.setChecked(false);
                planeStrain.setChecked(false);
                solidOfRevo.setChecked(true);
                break;
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

    }

    @Override
    public void onStart () {
        super.onStart();
        Log.i("Geometry fragment", "onStart");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (glSurfaceView.rendererSet) {
            glSurfaceView.onPause();
            //Log.i("Saving", "" + getActivity().getFilesDir());
            //saveData(Constants.temporalFileNameGeometry);
        }

        Log.i("Geometry fragment", "onResume");
    }

    @Override
    public void onResume() {
        super.onResume();

        if (glSurfaceView.rendererSet) {
            //glSurfaceView.setGeometryFragment(geometry);
            glSurfaceView.onResume();
            if (null == glSurfaceView)
                Log.i("Geometry fragment", "glSurfaceView is NULL");
            else
                Log.i("Geometry fragment", "glSurfaceView is NOT NULL");
        }

        Log.i("Geometry fragment", "onResume");

        // Some tests
        //drawingALine();
        drawingARectangle();
        //drawingARegularPolygon();
        //drawingACircle();
        //drawingRectangle();
        //booleanOperationUnify();
        //booleanOperationCombine();
        //booleanOperationIntersect();
        //specialTestBooleanOperationDifference();
        //booleanOperationDifference(); // Check
        //booleanOperationSubstract();
        //booleanOperationsCombined();
        //rectangleWithForces();
        //regularPolygonWithForces();
        //circleWithForces();
        //interactivePointSelection();
        //linearShapesWithFixedDisplacements();
        //circleWithFixedDisplacement();
        //typesOfPunctualSupports();
        //cantileverBar();
        //test3BooleanOperationDifference();
        //splitBooleanBuiltShapeAfterDrag();
        //typesOfLines();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("Geometry fragment", "onStop");
    }

    @Override
    public void onDestroyView () {
        super.onDestroyView();
        Log.i("Geometry fragment", "onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("Geometry fragment", "onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.i("Geometry fragment", "onDetach");
    }

    @Override
    public void onClick(View view) {
        AlertDialog dialog;
        View viewDialog;
        final AlertDialog.Builder alertDialogBuilder;
        LayoutInflater inflater;

        switch (view.getId()) {

            case R.id.conditionsButton:
                glSurfaceView.renderer.cadRenderer.unselectAllShapes();
                hideAndUnselectCurrentGroups();
                mainGroup.select(boundaryConditions);
                conditionsGroup.show();
                currentGroups.add(conditionsGroup);
                break;

            case R.id.punctualForceButton:
                glSurfaceView.renderer.cadRenderer.unselectAllShapes();

                displacementGroup.hide();
                conditionsGroup.select(punctualForce);
                hideAndUnselectCurrentGroups();

                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                //alertDialogBuilder.setMessage("Forces");
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                alertDialogBuilder.setView(inflater.inflate(R.layout.force_dialog, null));

                // Add action buttons
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog f = (Dialog) dialog;
                        EditText captureMagnitude = (EditText) f.findViewById(R.id.ForceMagnitude);
                        // Then the user has to select a node to apply the force
                        currentForce.setForceX(Float.parseFloat(captureMagnitude.getText().toString()));
                        currentForce.setForceY(0f);
                        glSurfaceView.setCurrentForce(currentForce);
                        glSurfaceView.setTypeOfAction(CADOperation.SELECTING_ELEMENT_TO_APPLY_FORCE);
                    }
                });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the forces
                        glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                break;

            case R.id.supportsButton:
                glSurfaceView.renderer.cadRenderer.unselectAllShapes();

                conditionsGroup.select(support);
                displacementGroup.hide();
                supportGroup.unSelect();
                supportGroup.show();
                currentGroups.add(supportGroup);
                break;

            case R.id.supportOnXYButton:
                supportGroup.select(supportOnXY);
                currentFixedDisplacement.clear();
                currentFixedDisplacement.fixX(0);
                currentFixedDisplacement.fixY(0);
                Log.i("glSurfaceView", "Using glSurfaceView");
                if (null == glSurfaceView)
                    Log.i("glSurfaceView", "glSurfaceView is NULL!! What's happening!!!");
                glSurfaceView.setTypeOfAction(CADOperation.SELECTING_ELEMENT_TO_FIX_DISPLACEMENT);
                glSurfaceView.setCurrentFixedDisplacement(currentFixedDisplacement);
                break;

            case R.id.supportOnXButton:
                supportGroup.select(supportOnX);
                currentFixedDisplacement.clear();
                currentFixedDisplacement.fixX(0);
                glSurfaceView.setTypeOfAction(CADOperation.SELECTING_ELEMENT_TO_FIX_DISPLACEMENT);
                glSurfaceView.setCurrentFixedDisplacement(currentFixedDisplacement);
                break;

            case R.id.supportOnYButton:
                supportGroup.select(supportOnY);
                currentFixedDisplacement.clear();
                currentFixedDisplacement.fixY(0);
                glSurfaceView.setTypeOfAction(CADOperation.SELECTING_ELEMENT_TO_FIX_DISPLACEMENT);
                glSurfaceView.setCurrentFixedDisplacement(currentFixedDisplacement);
                break;

            case R.id.displacementButton:
                conditionsGroup.select(displacement);
                supportGroup.hide();
                displacementGroup.unSelect();
                displacementGroup.show();
                currentGroups.add(displacementGroup);
                break;

            case R.id.displacementOnXYButton:
                displacementGroup.select(displacementOnXY);

                currentFixedDisplacement.clear();

                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                //alertDialogBuilder.setMessage("Forces");
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                alertDialogBuilder.setView(inflater.inflate(R.layout.fix_both_displacements, null));

                // Add action buttons
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog f = (Dialog) dialog;
                        EditText displOnX = (EditText) f.findViewById(R.id.fixedOnBothDisplOnX);
                        EditText displOnY = (EditText) f.findViewById(R.id.fixedOnBothDisplOnY);
                        // Then the user has to select a node to apply the force
                        currentFixedDisplacement.fixX(
                                Float.parseFloat(displOnX.getText().toString()));
                        currentFixedDisplacement.fixY(
                                Float.parseFloat(displOnY.getText().toString()));
                        glSurfaceView.setCurrentFixedDisplacement(currentFixedDisplacement);
                        glSurfaceView.setTypeOfAction(CADOperation.SELECTING_ELEMENT_TO_FIX_DISPLACEMENT);
                        currentFixedDisplacement.clear();
                    }
                });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the forces
                        glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();
                break;

            case R.id.displacementOnXButton:
                displacementGroup.select(displacementOnX);
                break;

            case R.id.displacementOnYButton:
                displacementGroup.select(displacementOnY);
                break;

            case R.id.materialsButton:
                glSurfaceView.renderer.cadRenderer.unselectAllShapes();
                hideAndUnselectCurrentGroups();
                mainGroup.select(materials);
                materialsGroup.show();
                currentGroups.add(materialsGroup);
                break;

            case R.id.metalsButton:
                materialsGroup.select(metals);
                metalsGroup.unSelect();
                metalsGroup.show();
                currentGroups.add(metalsGroup);
                break;

            case R.id.ironButton:
                problemData.material.setIronProperties();
                glSurfaceView.renderer.cadRenderer.setSurfaceColor(
                        Constants.IRON_COLOR[0],
                        Constants.IRON_COLOR[1],
                        Constants.IRON_COLOR[2]
                );
                glSurfaceView.setTypeOfAction(CADOperation.SELECTING_ELEMENT_TO_FIX_MATERIAL);
                metalsGroup.select(iron);

                break;

            case R.id.steelButton:
                problemData.material.setSteelProperties();
                glSurfaceView.renderer.cadRenderer.setSurfaceColor(
                        Constants.STEEL_COLOR[0],
                        Constants.STEEL_COLOR[1],
                        Constants.STEEL_COLOR[2]
                );
                glSurfaceView.setTypeOfAction(CADOperation.SELECTING_ELEMENT_TO_FIX_MATERIAL);
                metalsGroup.select(steel);
                break;

            case R.id.alumButton:
                problemData.material.setAluminumProperties();
                glSurfaceView.renderer.cadRenderer.setSurfaceColor(
                        Constants.ALUMINUM_COLOR[0],
                        Constants.ALUMINUM_COLOR[1],
                        Constants.ALUMINUM_COLOR[2]
                );
                glSurfaceView.setTypeOfAction(CADOperation.SELECTING_ELEMENT_TO_FIX_MATERIAL);
                metalsGroup.select(alum);
                break;

            case R.id.problemTypeButton:
                glSurfaceView.renderer.cadRenderer.unselectAllShapes();

                hideAndUnselectCurrentGroups();
                mainGroup.select(problemType);
                problemTypeGroup.show();
                currentGroups.add(problemTypeGroup);
                break;

            case R.id.planeStressButton:
                problemTypeGroup.select(planeStress);
                problemData.setProblemType(Constants.PLANE_STRESS);
                removeRotationAxisFromGeometry();
                break;

            case R.id.planeStrainButton:
                problemTypeGroup.select(planeStrain);
                problemData.setProblemType(Constants.PLANE_STRAIN);
                removeRotationAxisFromGeometry();
                break;

            case R.id.axiallySimmetricButton:
                problemTypeGroup.select(axiallySimmetric);
                problemData.setProblemType(Constants.SOLID_OF_REVOLUTION);

                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                //alertDialogBuilder.setMessage("Forces");
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                alertDialogBuilder.setView(inflater.inflate(R.layout.rotation_axis_location, null));

                // Add action buttons
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog f = (Dialog) dialog;
                        EditText x = (EditText) f.findViewById(R.id.xAxisLocation);
                        Geometry.Ray axis =
                                new Geometry.Ray(new Geometry.Point(), new Geometry.Vector());
                        // For now the rotation axis is always parallel to y-axis
                        axis.vector.setComponents(0, 1, 0);
                        axis.point.setCoordinates(Float.parseFloat(x.getText().toString()),
                                0f, 0f);
                        problemData.solidOfRevolution.setAxis(axis);
                        addRotationAxisToGeometry(axis);
                    }
                });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the forces
                        glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                break;

            case R.id.parametricShapesButton:
                hideAndUnselectCurrentGroups();
                mainGroup.select(parametricShapes);
                parametricShapesGroup.show();
                currentGroups.add(parametricShapesGroup);
                break;

            case R.id.rectangleButton:
                parametricShapesGroup.select(rectangle);
                glSurfaceView.renderer.cadRenderer.
                        setCurrentAction(CADOperation.DRAWING_A_RECTANGLE);
                break;

            case R.id.circleButton:
                parametricShapesGroup.select(circle);
                glSurfaceView.renderer.cadRenderer.
                        setCurrentAction(CADOperation.DRAWING_A_CIRCLE);
                break;

            case R.id.regularPolygonButton:
                parametricShapesGroup.select(regularPolygon);
                hideAndUnselectCurrentGroups();

                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                viewDialog = inflater.inflate(R.layout.regular_polygon_dialog, null);
                alertDialogBuilder.setView(viewDialog);

                // The text fields and the checkboxes
                final EditText numberOfSides = (EditText) viewDialog.findViewById(R.id.NumberOfSidesRegularPolygon);

                // Ok button
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        glSurfaceView.setTypeOfAction(CADOperation.DRAWING_A_REGULAR_POLYGON);

                        glSurfaceView.renderer.cadRenderer.
                                setNumberOfSidesRegularPolygon(
                                        Integer.parseInt(numberOfSides.getText().toString()));
                    }
                });

                // Cancel button
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the displacement
                        glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();
                break;

            case R.id.booleanOperationsButton:
                hideAndUnselectCurrentGroups();
                mainGroup.select(booleanOperations);
                booleanOperationsGroup.show();
                currentGroups.add(booleanOperationsGroup);
                break;

            case R.id.unionButton:
                booleanOperationsGroup.select(unify);
                glSurfaceView.renderer.cadRenderer.
                        performBooleanOperation(Constants.BooleanOperation.UNIFY);
                hideAndUnselectCurrentGroups();
                break;

            case R.id.combineButton:
                booleanOperationsGroup.select(combine);
                glSurfaceView.renderer.cadRenderer.
                        performBooleanOperation(Constants.BooleanOperation.COMBINE);
                hideAndUnselectCurrentGroups();
                break;

            case R.id.differenceButton:
                booleanOperationsGroup.select(difference);
                glSurfaceView.renderer.cadRenderer.
                        performBooleanOperation(Constants.BooleanOperation.DIFFERENCE);
                hideAndUnselectCurrentGroups();
                break;

            case R.id.substractButton:
                booleanOperationsGroup.select(substract);
                glSurfaceView.renderer.cadRenderer.
                        performBooleanOperation(Constants.BooleanOperation.SUBSTRACT);
                hideAndUnselectCurrentGroups();
                break;

            case R.id.intersectionButton:
                booleanOperationsGroup.select(intersect);
                glSurfaceView.renderer.cadRenderer.
                        performBooleanOperation(Constants.BooleanOperation.INTERSECT);
                hideAndUnselectCurrentGroups();
                break;

            case R.id.linearShapeOperationsButton:
                hideAndUnselectCurrentGroups();
                mainGroup.select(linearShapeOperations);
                break;

            case R.id.showResults:
                hideAndUnselectCurrentGroups();
                mainGroupRes.select(showResults);
                postprocess.showResults(Constants.DISPLACEMENTS);
                break;

            /*
            case R.id.showVonMisesButton:
                hideAndUnselectCurrentGroups();
                mainGroupRes.select(vonMises);
                postprocess.showResults(Constants.VON_MISES);
                break;

            case R.id.showElemsResults:
                hideAndUnselectCurrentGroups();
                mainGroupRes.select(plastic_damaged_elems);
                switch(problemData.getTypeOfModel()){
                    case Constants.ELASTIC_PROBLEM:
                        //do nothing;
                        break;
                    case Constants.PLASTIC_PROBLEM:
                        postprocess.showResults(Constants.PLASTIFIED_ELEMS);
                        break;
                    case Constants.DAMAGE_PROBLEM:
                        postprocess.showResults(Constants.DAMAGE);
                        break;
                }
            //*/
            /*
            case R.id.InsertLine:
                deactivateOtherButtons(Constants.BUTTON_SELECTED_INSERT_LINE);
                if (insertLineIsPressed) {
                    insertLineIsPressed = false;
                    insertLine.setImageResource(R.drawable.insert_lines_not_selected);
                    glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                    break;
                }

                insertLineIsPressed = true;
                insertLine.setImageResource(R.drawable.line);

                glSurfaceView.setTypeOfAction(CADOperation.DRAWING_A_LINE);
                Log.i("Clicking", "Lines button");
                break;

            case R.id.InsertSurface:
                deactivateOtherButtons(Constants.BUTTON_SELECTED_INSERT_SURFACE);
                if (insertSurfaceIsPressed) {
                    insertSurfaceIsPressed = false;
                    insertSurface.setImageResource(R.drawable.insert_surfaces_not_selected);
                    glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                    break;
                }

                insertSurfaceIsPressed = true;
                insertSurface.setImageResource(R.drawable.surface);

                glSurfaceView.setTypeOfAction(CADOperation.DRAWING_A_SURFACE);
                Log.i("Clicking", "Surfaces button");
                break;

            case R.id.InsertForce:
                Log.i("Clicking", "Force button");

                deactivateOtherButtons(Constants.BUTTON_SELECTED_INSERT_NODAL_FORCE);
                if (insertForceIsPressed) {
                    insertForceIsPressed = false;
                    insertForce.setImageResource(R.drawable.nodal_force_not_selected);
                    glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                    break;
                }

                insertForceIsPressed = true;
                insertForce.setImageResource(R.drawable.nodal_force);

                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                //alertDialogBuilder.setMessage("Forces");
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                alertDialogBuilder.setView(inflater.inflate(R.layout.force_dialog, null));

                // Add action buttons
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog f = (Dialog) dialog;
                        EditText captureFX = (EditText) f.findViewById(R.id.ForceX);
                        EditText captureFY = (EditText) f.findViewById(R.id.ForceY);
                        // Then the user has to select a node to apply the force
                        currentForce.setForceX(Float.parseFloat(captureFX.getText().toString()));
                        currentForce.setForceY(Float.parseFloat(captureFY.getText().toString()));
                        glSurfaceView.setCurrentForce(currentForce);
                        glSurfaceView.setTypeOfAction(CADOperation.SELECTING_A_POINT_TO_APPLY_FORCE);
                    }
                });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the forces
                        glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                        insertForceIsPressed = false;
                        insertForce.setImageResource(R.drawable.nodal_force_not_selected);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                break;

            case R.id.FixedDisplacement:
                Log.i("Clicking", "Displacement button");

                deactivateOtherButtons(Constants.BUTTON_SELECTED_INSERT_SUPPORT);
                if (fixDisplacementIsPressed) {
                    fixDisplacementIsPressed = false;
                    fixDisplacement.setImageResource(R.drawable.supports_not_selected);
                    glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                    break;
                }

                fixDisplacementIsPressed = true;
                fixDisplacement.setImageResource(R.drawable.supports);

                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                //alertDialogBuilder.setMessage("Displacements");
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                viewDialog = inflater.inflate(R.layout.displacement_dialog, null);
                alertDialogBuilder.setView(viewDialog);

                // The text fields and the checkboxes
                final EditText captureDisplacementX = (EditText) viewDialog.findViewById(R.id.displacementX);
                final EditText captureDisplacementY = (EditText) viewDialog.findViewById(R.id.displacementY);
                final CheckBox fixX = (CheckBox) viewDialog.findViewById(R.id.fixDisplacementX);
                final CheckBox fixY = (CheckBox) viewDialog.findViewById(R.id.fixDisplacementY);

                // Text fields are disabled
                captureDisplacementX.setEnabled(false);
                captureDisplacementY.setEnabled(false);

                // When the first checkbox is checked
                fixX.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) captureDisplacementX.setEnabled(true);
                        Log.i("Clicking", "Checkbox clicked.");
                    }
                });

                // When the second checkbox is checked
                fixY.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) captureDisplacementY.setEnabled(true);
                        Log.i("Clicking", "Checkbox clicked.");
                    }
                });

                // Ok button
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Then the user has to select a node to apply the displacement
                        glSurfaceView.setTypeOfAction(
                                CADOperation.SELECTING_A_POINT_TO_FIX_DISPLACEMENT);

                        if (fixX.isChecked()){
                            currentFixedDisplacement.fixX(
                                    Float.parseFloat(captureDisplacementX.getText().toString()));
                        }
                        if (fixY.isChecked()) {
                            currentFixedDisplacement.fixY(
                                    Float.parseFloat(captureDisplacementY.getText().toString()));
                        }
                        glSurfaceView.setCurrentFixedDisplacement(currentFixedDisplacement);
                        currentFixedDisplacement.clear();
                    }
                });

                // Cancel button
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the displacement
                        glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                        fixDisplacementIsPressed = false;
                        fixDisplacement.setImageResource(R.drawable.supports_not_selected);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                break;

            case R.id.DrawRectangle:
                glSurfaceView.setTypeOfAction(CADOperation.DRAWING_A_RECTANGLE);
                break;

            case R.id.DrawRegularPolygon:
                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                viewDialog = inflater.inflate(R.layout.regular_polygon_dialog, null);
                alertDialogBuilder.setView(viewDialog);

                // The text fields and the checkboxes
                final EditText numberOfSides = (EditText) viewDialog.findViewById(R.id.NumberOfSidesRegularPolygon);

                // Ok button
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        glSurfaceView.setTypeOfAction(CADOperation.DRAWING_A_REGULAR_POLYGON);

                        glSurfaceView.renderer.setNumberOfSidesRegularPolygon(
                                Integer.parseInt(numberOfSides.getText().toString()));
                    }
                });

                // Cancel button
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the displacement
                        glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                break;

            case R.id.DrawCircle:
                glSurfaceView.setTypeOfAction(CADOperation.DRAWING_A_CIRCLE);
                break;

            case R.id.SplitSegment:
                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                viewDialog = inflater.inflate(R.layout.split_segment_dialog, null);
                alertDialogBuilder.setView(viewDialog);

                // The text fields and the checkboxes
                final EditText numberOfDivisions = (EditText) viewDialog.findViewById(R.id.NumberOfDivisions);

                // Ok button
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        glSurfaceView.setTypeOfAction(CADOperation.SPLITTING_SEGMENT);

                        glSurfaceView.renderer.setNumberOfDivisions(
                                Integer.parseInt(numberOfDivisions.getText().toString()));
                    }
                });

                // Cancel button
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the displacement
                        glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                break;

            case R.id.ExtrudeSegment:
                Log.i("EXTRUDE", "Setting the action");
                glSurfaceView.setTypeOfAction(CADOperation.EXTRUDING_SEGMENT);
                break;
            //*
            case R.id.InsertLinearForce:

                deactivateOtherButtons(Constants.BUTTON_SELECTED_INSERT_LINEAR_FORCE);
                if (insertLinearForceIsPressed) {
                    insertLinearForceIsPressed = false;
                    insertLinearForce.setImageResource(R.drawable.linear_force_not_selected);
                    glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                    break;
                }

                Log.i("Clicking", "Linear force button");
                insertLinearForceIsPressed = true;
                insertLinearForce.setImageResource(R.drawable.linear_force);

                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                //alertDialogBuilder.setMessage("Forces");
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                alertDialogBuilder.setView(inflater.inflate(R.layout.force_dialog, null));

                // Add action buttons
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog f = (Dialog) dialog;
                        EditText captureFX = (EditText) f.findViewById(R.id.ForceX);
                        EditText captureFY = (EditText) f.findViewById(R.id.ForceY);
                        // Then the user has to select a node to apply the force
                        currentForce.setForceX(Float.parseFloat(captureFX.getText().toString()));
                        currentForce.setForceY(Float.parseFloat(captureFY.getText().toString()));
                        glSurfaceView.setCurrentForce(currentForce);
                        glSurfaceView.setTypeOfAction(CADOperation.SELECTING_A_LINE_TO_APPLY_FORCE);
                    }
                });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the forces
                        glSurfaceView.setTypeOfAction(CADOperation.DOING_NOTHING);
                        insertLinearForceIsPressed = false;
                        insertLinearForce.setImageResource(R.drawable.linear_force_not_selected);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                break;

            case R.id.Conditions:
                conditionsButtonIsActive = conditionsButtonIsActive ? false : true;
                hideOtherGroupButtons(R.id.Conditions);

                if (conditionsButtonIsActive) setVisibilityConditionsButtonsGroup(View.VISIBLE);
                else                          setVisibilityConditionsButtonsGroup(View.GONE);

               break;

            case R.id.FreeDrawing:
                freeDrawingButtonIsActive = freeDrawingButtonIsActive ? false : true;
                hideOtherGroupButtons(R.id.FreeDrawing);

                if (freeDrawingButtonIsActive) setVisibilityFreeDrawingButtonsGroup(View.VISIBLE);
                else                           setVisibilityFreeDrawingButtonsGroup(View.GONE);

                break;

            case R.id.Shapes:
                shapesButtonIsActive = shapesButtonIsActive ? false : true;
                hideOtherGroupButtons(R.id.Shapes);

                if (shapesButtonIsActive) setVisibilityShapesButtonsGroup(View.VISIBLE);
                else                      setVisibilityShapesButtonsGroup(View.GONE);

                break;

            case R.id.LineOperations:
                lineOperationsButtonIsActive = lineOperationsButtonIsActive ? false : true;
                hideOtherGroupButtons(R.id.LineOperations);

                if (lineOperationsButtonIsActive) setVisibilityLineOperationsButtonsGroup(View.VISIBLE);
                else                              setVisibilityLineOperationsButtonsGroup(View.GONE);

                break;
                */
        }
    }

    public void hideAndUnselectCurrentGroups() {
        ButtonGroup bg;
        while (!currentGroups.isEmpty()) {
            bg = currentGroups.pop();
            bg.unSelect();
            bg.hide();
        }
        if (isCADActive) mainGroup.unSelect();
        else             mainGroupRes.unSelect();
    }

    public static File getStorageDir(String name) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStorageDirectory() + name);
        if (!file.mkdirs()) {
            Log.i("FILES", "Directory not created");
        }
        return file;
    }

    private void addRotationAxisToGeometry(Geometry.Ray axis) {
        /*
        if (null != axisLine) geometry.deleteShape(axisLine);
        axisLine = new Geometry.CleanPolyLine();
        axisLine.setStart(axis.point.x, plotParameters.getMinY(), 0);
        axisLine.createSegmentToHere(axis.point.x, plotParameters.getMaxY(), 0);
        geometry.addShape(axisLine);
        //*/
    }

    private void removeRotationAxisFromGeometry() {
        //if (axisLine != null) geometry.deleteShape(axisLine);
    }

    public GeometrySet getGeometry() { return geometry; }
    public ProblemData getProblemData() { return problemData; }

    private void drawingALine() {
        Geometry.OrderOneLinearShape shape = new Geometry.OrderOneLinearShape();
        shape.setStart(0f, 0f, 0f);
        shape.createSegmentToHere(5f, 0f, 0f);
        shape.createSegmentToHere(5f, 3f, 0f);
        shape.createSegmentToHere(4.5f, 3f, 0f);
        shape.setAsModifiable();

        geometry.addShape(shape);
    }

    private void drawingARectangle() {
        Geometry.Rectangle r = new Geometry.Rectangle(-3f, -2f, 3f, 2f);
        geometry.addShape(r);
    }

    private void drawingARegularPolygon() {
        Geometry.RegularPolygon p = new Geometry.RegularPolygon(5, 0f, 0f, 5f, 0f);
        geometry.addShape(p);
    }

    private void drawingACircle() {
        Geometry.Circle c = new Geometry.Circle(0f, 0f, 0f, 3f);
        geometry.addShape(c);
    }

    /*
    private void drawingTest1() {
        Geometry.Line l1 = new Geometry.Line(-1, 0, 0, -1, 4, 0);
        Geometry.Rectangle r = new Geometry.Rectangle(-1, -1, -4, -4);
        Geometry.RegularPolygon rp = new Geometry.RegularPolygon(5, 5, 5, 7, 7);
        Geometry.Circle c = new Geometry.Circle(2, -2, 3, -3);

        //l1.getFirstVertex().setForce(1);
        //l1.setUniformDistributedForce(-1, 0, 1, 0, 0.25f);
        l1.getLineSegments().get(0).setUniformDistributedForce(1f, 0.25f);
        //r.getContourLinearShapes().get(0).setUniformDistributedForce(-1, 0, 1, 0, 1f);

        geometry.addShape(l1);
        geometry.addShape(r);
        geometry.addShape(rp);
        geometry.addShape(c);
        //geometry.addShape(l1.getFirstVertex().getForce());
        //geometry.addShape(l1.getUniformForce());
        geometry.addShape(l1.getLineSegments().get(0).getUniformDistributedForce());
    }

    private void rectangleWithForces() {
        Geometry.Rectangle r = new Geometry.Rectangle(-4, -2, 4, 2);

        Conditions.FixedDisplacement displacement = new Conditions.FixedDisplacement();
        displacement.fixX(0f);
        displacement.fixY(0f);

        r.getVertices().get(0).setFixedDisplacement(displacement);
        r.getVertices().get(3).fixX(0);

        r.getVertices().get(1).setForce(1f);
        r.getContourLinearShapes().get(0).getLineSegments()
                .get(2).setUniformDistributedForce(1f, 0.25f);
        r.getContourLinearShapes().get(0).getLineSegments()
                .get(0).setUniformDistributedForce(1f, 0.25f);

        geometry.addShape(r);
        geometry.addShape(r.getContourLinearShapes().get(0)
                .getLineSegments().get(2).getUniformDistributedForce());
        geometry.addShape(r.getVertices().get(1).getForce());
        geometry.addShape(r.getContourLinearShapes().get(0)
                .getLineSegments().get(0).getUniformDistributedForce());
        geometry.addShape(r.getVertices().get(0).getFixedDisplacement());
        geometry.addShape(r.getVertices().get(3).getFixedDisplacement());
    }

    private void regularPolygonWithForces() {
        Geometry.RegularPolygon p = new Geometry.RegularPolygon(6, 0, 0, 0, 3);

        p.getVertices().get(0).setForce(4f);
        p.getContourLinearShapes().get(0).getLineSegments().
                get(2).setUniformDistributedForce(5f, 0.25f);

        geometry.addShape(p);
        geometry.addShape(p.getVertices().get(0).getForce());
        geometry.addShape(p.getContourLinearShapes().get(0).getLineSegments().
                get(2).getUniformDistributedForce());
    }

    private void circleWithForces() {
        Geometry.Circle c = new Geometry.Circle(0, 0, 0, 3);

        c.getContourLinearShapes().get(0).setUniformDistributedForce(3f, 0.25f);

        geometry.addShape(c);
        geometry.addShape(c.getContourLinearShapes().get(0).getUniformForce());
    }

    private void drawingRectangle() {
        Log.i("Rectangle", "Before creating it");
        Geometry.Rectangle r = new Geometry.Rectangle(-1, -1, -4, -4);
        geometry.addShape(r);
    }

    private void booleanOperationUnify() {
        Geometry.Circle c1 = new Geometry.Circle(0, 0, 3, 0);
        Geometry.Circle c2 = new Geometry.Circle(4, 0, 2, 0);

        Geometry.BooleanBuiltShape b = new Geometry.BooleanBuiltShape(
                c1, c2, Constants.BooleanOperation.UNIFY);

        geometry.addShape(b);
    }

    private void booleanOperationCombine() {
        Geometry.Circle c1 = new Geometry.Circle(0, 0, 3, 0);
        Geometry.Circle c2 = new Geometry.Circle(4, 0, 2, 0);

        Geometry.BooleanBuiltShape b = new Geometry.BooleanBuiltShape(
                c1, c2, Constants.BooleanOperation.COMBINE);

        geometry.addShape(b);
    }

    private void booleanOperationIntersect() {
        Geometry.Circle c1 = new Geometry.Circle(0, 0, 3, 0);
        Geometry.Circle c2 = new Geometry.Circle(4, 0, 2, 0);

        Geometry.BooleanBuiltShape b = new Geometry.BooleanBuiltShape(
                c1, c2, Constants.BooleanOperation.INTERSECT);

        geometry.addShape(b);
    }

    private void test1BooleanOperationDifference() {
        Geometry.Circle c1 = new Geometry.Circle(0, 0, 3, 0);
        Geometry.Circle c2 = new Geometry.Circle(4, 0, 2, 0);

        Geometry.BooleanBuiltShape b = new Geometry.BooleanBuiltShape(
                c1, c2, Constants.BooleanOperation.DIFFERENCE);

        geometry.addShape(b);
    }

    private void test2BooleanOperationDifference() {
        Model c1 = Model.getPolygon(0f, 0f, 3f, 30);
        Model c2 = Model.getPolygon(4f, 0f, 2f, 30);

        Model model = Model.difference(c1, c2);
        Mesh mesh = Mesh.getSimplestMesh(model);
    }

    private void test3BooleanOperationDifference() {
        Geometry.Circle c = new Geometry.Circle(0f, 0f, 2f, 0f);
        Geometry.Rectangle r = new Geometry.Rectangle(-2f, 0f, 2f, 4f);

        Geometry.BooleanBuiltShape b =
                new Geometry.BooleanBuiltShape(c, r,
                        Constants.BooleanOperation.DIFFERENCE);

        geometry.addShape(b);
    }

    private void booleanOperationSubstract() {
        Geometry.Circle c1 = new Geometry.Circle(0, 0, 3, 0);
        Geometry.Circle c2 = new Geometry.Circle(4, 0, 2, 0);

        Geometry.BooleanBuiltShape b = new Geometry.BooleanBuiltShape(
                c1, c2, Constants.BooleanOperation.SUBSTRACT);

        geometry.addShape(b);
    }

    private void splitBooleanBuiltShapeAfterDrag() {
        Geometry.Rectangle r1 = new Geometry.Rectangle(-3f, -1f,  1f,  1f);
        Geometry.Rectangle r2 = new Geometry.Rectangle( 3f,  1f, -1f, -1f);

        geometry.addShape(r1);
        geometry.addShape(r2);

        Geometry.BooleanBuiltShape b = new Geometry.BooleanBuiltShape(
                r1, r2, Constants.BooleanOperation.UNIFY);

        geometry.deleteShape(r1);
        geometry.deleteShape(r2);
        geometry.addShape(b);
    }

    private void booleanOperationsCombined() {
        Geometry.Rectangle r1 = new Geometry.Rectangle(-4, -1, 1, 1);
        Geometry.Rectangle r2 = new Geometry.Rectangle(0, -3, 3, 3);
        Geometry.Rectangle r3 = new Geometry.Rectangle(-1, -5, 1, 5);

        Geometry.BooleanBuiltShape b1 = new Geometry.BooleanBuiltShape(
                r1, r2, Constants.BooleanOperation.UNIFY);

        Geometry.BooleanBuiltShape b2 = new Geometry.BooleanBuiltShape(
                r3, b1, Constants.BooleanOperation.UNIFY);

        geometry.addShape(b2);
    }

    private void interactivePointSelection() {
        Geometry.Rectangle r = new Geometry.Rectangle(-2, -2, 2, 2);
        r.getInteractivePoints().get(0).select();

        geometry.addShape(r);
    }

    private void linearShapesWithFixedDisplacements() {
        Geometry.Rectangle r1 = new Geometry.Rectangle(-2, -2, 2, 2);
        r1.getContourLinearShapes().get(0).setNormalDisplacement(1, 0.25f);
        r1.getContourLinearShapes().get(0).getFixedDisplacement().build();

        geometry.addShape(r1);
        geometry.addShape(r1.getContourLinearShapes().get(0).getFixedDisplacement());
    }

    private void circleWithFixedDisplacement() {
        Geometry.Circle c1 = new Geometry.Circle(0, 0, 3, 0);
        //c1.getContourLinearShapes().get(0).setBothDisplacements(0, 0, 0.25f);
        c1.getContourLinearShapes().get(0).setNormalDisplacement(0f, 0.25f);
        c1.getContourLinearShapes().get(0).getFixedDisplacement().build();

        geometry.addShape(c1);
        geometry.addShape(c1.getContourLinearShapes().get(0).getFixedDisplacement());
    }

    private void typesOfPunctualSupports() {
        Geometry.Rectangle r1 = new Geometry.Rectangle(-2, -2, 2, 2);

        r1.getVertices().get(0).fixX(0f);
        r1.getVertices().get(1).fixX(1f);

        r1.getVertices().get(2).fixY(0f);
        r1.getVertices().get(3).fixY(1f);

        geometry.addShape(r1);
        geometry.addShape(r1.getVertices().get(0).getFixedDisplacement());
        geometry.addShape(r1.getVertices().get(1).getFixedDisplacement());
        geometry.addShape(r1.getVertices().get(2).getFixedDisplacement());
        geometry.addShape(r1.getVertices().get(3).getFixedDisplacement());
    }

    private void cantileverBar() {
        Geometry.Rectangle bar = new Geometry.Rectangle(-3, -2, 3, 2);

        bar.getContourLinearShapes().get(0).
                getLineSegments().get(3).setBothDisplacements(0f, 0f, 0.25f);

        Conditions.LinearFixedDisplacement linearFixedDisplacement =
                bar.getContourLinearShapes().get(0).
                        getLineSegments().get(3).getLinearFixedDisplacement();
        linearFixedDisplacement.build();

        bar.getVertices().get(2).setForce(1000f);

        problemData.setProblemType(Constants.PLANE_STRESS);
        //problemData.setProblemType(Constants.PLANE_STRAIN);
        //problemData.setProblemType(Constants.SOLID_OF_REVOLUTION);

        //ArrayList<Geometry.LinearShape> contour = bar.getContourLinearShapes();
        //contour.get(0).drawingLineProperties.setDashed6Div();

        geometry.addShape(bar);
        geometry.addShape(linearFixedDisplacement);
        geometry.addShape(bar.getVertices().get(2).getForce());
        //geometry.addShape(uniformDistributedForce);
    }

    private void typesOfLines() {

        Geometry.CleanPolyLine line1 = new Geometry.CleanPolyLine();

        line1.setStart(0f, 0f, 0f);
        line1.createSegmentToHere(5f, 0f, 0f);
        line1.createSegmentToHere(5f, 3f, 0f);
        line1.createSegmentToHere(-3f, 4f, 0f);
        line1.drawingLineProperties.setThickness(0.02f);
        line1.drawingLineProperties.setDashed6Div();
        //line1.drawingLineProperties.setHockeyTable();

        geometry.addShape(line1);
    }
    //*/
}