package com.cimat.solidbox.Utilities.Preprocess;

import android.opengl.GLES20;
import android.util.Log;

import com.cimat.solidbox.DataStructures.DataInterface;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.cimat.solidbox.OpenGL.Objects.ObjectBuilder.DrawCommand;
import com.cimat.solidbox.OpenGL.Objects.VisualObject;
import com.cimat.solidbox.OpenGL.Programs.ColorShaderProgram;
import com.cimat.solidbox.OpenGL.Programs.TextShaderProgram;
import com.cimat.solidbox.OpenGL.Programs.TextureShaderProgram;
import com.cimat.solidbox.OpenGL.Utilities.GLText;
import com.cimat.solidbox.OpenGL.Utilities.OpenGLineSegmentHelperII;
import com.cimat.solidbox.OpenGL.Utilities.TouchHelper;
import com.cimat.solidbox.OpenGL.Utilities.Utilities;
import com.cimat.solidbox.OpenGL.Utilities.Utilities.Texture;
import com.cimat.solidbox.OpenGL.Utilities.Vertices;
import com.cimat.solidbox.OpenGL.Utilities.VisualObjectsCollection;
import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.Constants.TextureName;
import com.cimat.solidbox.Utilities.LoggerConfig;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.ControlPoint;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Shape;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.InteractivePoint;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.LinearShape;
import com.cimat.solidbox.Utilities.Tools;
import com.cimat.solidbox.Utilities.UndoRedo;

import nb.geometricBot.Model;

import static android.opengl.GLES20.GL_LINES;
import static android.opengl.GLES20.glBlendFunc;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glDisableVertexAttribArray;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glGetProgramInfoLog;

/**
 * Created by ernesto on 4/02/16.
 */

public class Conditions {

    static public class Force {
        private float X, Y, Z;

        public Force() {
            X = Y = Z = 0f;
        }

        public void setForceX(float forceX) {
            X = forceX;
        }

        public void setForceY(float forceY) {
            Y = forceY;
        }

        public void setForceZ(float forceZ) {
            Z = forceZ;
        }

        public float getForceX() {
            return X;
        }

        public float getForceY() {
            return Y;
        }

        public float getForceZ() {
            return Z;
        }

        public void copyForce(Force force) {
            X = force.getForceX();
            Y = force.getForceY();
            Z = force.getForceZ();
        }

        public float getMagnitude(){
            return (float)Math.sqrt(X*X + Y*Y + Z*Z);
        }

        public void clear() {
            X = Y = Z = 0f;
        }

        public boolean writeToFile(DataOutputStream dos) {
            try {
                dos.writeFloat(X);
                dos.writeFloat(Y);
                dos.writeFloat(Z);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis) {
            try {
                X = dis.readFloat();
                Y = dis.readFloat();
                Z = dis.readFloat();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    static public class FixedDisplacement {
        private boolean isFixedX, isFixedY, isFixedZ;
        private float displacementX, displacementY, displacementZ;

        private boolean isNormalFixed, isTangentFixed;
        private float normalDisplacement, tangentDisplacement;

        private void initializeVariables() {
            isFixedX = isFixedY = isFixedZ = false;
            displacementX = displacementY = displacementZ = 0f;

            isNormalFixed = isTangentFixed = false;
            normalDisplacement = tangentDisplacement = 0f;
        }

        public FixedDisplacement() {
            initializeVariables();
        }

        public void fixX(float value) {
            this.isFixedX = true;
            this.displacementX = value;
        }

        public void fixY(float value) {
            this.isFixedY = true;
            this.displacementY = value;
        }

        public void fixZ(float value) {
            this.isFixedZ = true;
            this.displacementZ = value;
        }

        public void fixNormal(float value) {
            normalDisplacement = value;
            isNormalFixed = true;
        }

        public void fixTangent(float value) {
            tangentDisplacement = value;
            isTangentFixed = true;
        }

        public boolean isFixedX() {
            return isFixedX;
        }
        public boolean isFixedY() {
            return isFixedY;
        }
        public boolean isFixedZ() {
            return isFixedZ;
        }
        public float getDisplacementX() {
            return displacementX;
        }
        public float getDisplacementY() {
            return displacementY;
        }
        public float getDisplacementZ() {
            return displacementZ;
        }

        public void setDisplacement(FixedDisplacement fixedDisplacement) {
            isFixedX = fixedDisplacement.isFixedX;
            isFixedY = fixedDisplacement.isFixedY;
            isFixedZ = fixedDisplacement.isFixedZ;
            displacementX = fixedDisplacement.displacementX;
            displacementY = fixedDisplacement.displacementY;
            displacementZ = fixedDisplacement.displacementZ;

            isNormalFixed = fixedDisplacement.isNormalFixed;
            isTangentFixed = fixedDisplacement.isTangentFixed;
            normalDisplacement = fixedDisplacement.normalDisplacement;
            tangentDisplacement = fixedDisplacement.tangentDisplacement;
        }

        public void clear(){
            initializeVariables();
        }
    }

    static public class PunctualFixedDisplacement extends Shape {
        private boolean isFixedX, isFixedY, isFixedZ;
        private float displacementX, displacementY, displacementZ;
        private float magnitude, angle, initialAngle;
        private Point attachedVertex;

        private Point start;
        private ControlPoint rotationControl;
        private ArrayList<InteractivePoint> interactivePoints;

        // For drawing
        private float[] R;
        private float[] A;
        private float unitLengthNDC;
        private VisualObject totallyFixedSupport;
        private VisualObject partiallyFixedSupport;
        private VisualObject totallyImposedDisplacement;
        private VisualObject partiallyImposedDisplacement;
        private boolean buildOpenGLData;
        private boolean updateVertexArray;
        private float drawingHeight, drawingHeightDx, drawingHeightDy;

        private void initializeVariables() {
            isFixedX = isFixedY = isFixedZ = false;
            displacementX = displacementY = displacementZ = 0f;
            magnitude = 0f;
            // All the drawings were drawn using this orientation
            initialAngle = (float)(1.5*Math.PI);
            angle = 0f;
            attachedVertex = null;

            start = new Point();
            //start.parentShapeProperties.setParentShape(this); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            rotationControl = new ControlPoint(this);
            interactivePoints = new ArrayList();
            interactivePoints.add(rotationControl);

            R = new float[16];
            A = new float[16];
            unitLengthNDC = 0.25f;
            totallyFixedSupport = new VisualObject();
            partiallyFixedSupport = new VisualObject();
            partiallyImposedDisplacement = new VisualObject();
            totallyImposedDisplacement = new VisualObject();
            buildOpenGLData = true;
            updateVertexArray = false;
        }

        public PunctualFixedDisplacement() {
            super();
            initializeVariables();
        }

        public void setData(Point vertex, float unitLengthNDC) {
            float base = Constants.SUPPORT_TRIANGLE_BASE/unitLengthNDC;
            attachedVertex = vertex;
            this.unitLengthNDC = unitLengthNDC;
            totallyFixedSupport.createFixedSupport(0f, 0f, base);
            partiallyFixedSupport.createPartialFixedSupport(0f, 0f, base);
            partiallyImposedDisplacement.createPartiallyFixedDisplacement(
                    0f, 0f, 0.8f*base);
            totallyImposedDisplacement.createTotallyFixedDisplacement(
                    0f, 0f, 0.8f*base);
            start.setCoordinates(vertex);
        }

        public void itsVertexWasMoved() {
            float dx = attachedVertex.getX() - start.getX();
            float dy = attachedVertex.getY() - start.getY();
            translate(dx, dy, 0);
        }

        public void fixBoth(float displacementX, float displacementY) {
            isFixedX = true;
            isFixedY = true;
            this.displacementX = displacementX;
            this.displacementY = displacementY;
            angle = (float) Math.atan2(displacementY, displacementX);
            determineDrawingHeight();
            calculateDeltas();
            calculateMagnitude();
            setRotationControlCoordinates();
        }

        public void fixX(float displacementX) {
            this.isFixedX = true;
            this.displacementX = displacementX;
            if (displacementX >= 0) angle = 0f;
            else                    angle = (float)Math.PI;
            Log.i("SUPPORTS", "Angle = " + angle);
            isTotallyFixed();
            determineDrawingHeight();
            calculateDeltas();
            calculateMagnitude();
            setRotationControlCoordinates();
        }

        public void fixY(float displacementY) {
            this.isFixedY = true;
            this.displacementY = displacementY;
            if (displacementY >= 0) angle = (float)(0.5*Math.PI);
            else                    angle = (float)(1.5*Math.PI);
            isTotallyFixed();
            determineDrawingHeight();
            calculateDeltas();
            calculateMagnitude();
            setRotationControlCoordinates();
        }

        public void fixZ(float displacementZ) {
            this.isFixedZ = true;
            this.displacementZ = displacementZ;
            calculateMagnitude();
        }

        public void releaseX() {
            isFixedX = false;
            displacementX = 0f;
            calculateMagnitude();
        }

        public void releaseY() {
            isFixedY = false;
            displacementY = 0f;
            calculateMagnitude();
        }

        public void releaseZ() {
            isFixedZ = false;
            displacementZ = 0f;
            calculateMagnitude();
        }

        private void setRotationControlCoordinates() {
            if (displacementX == 0 && displacementY == 0) {

            } else {
                // This requires the correct drawingHeight
                rotationControl.setCoordinates(
                        attachedVertex.getX() + drawingHeightDx,
                        attachedVertex.getY() + drawingHeightDy,
                        attachedVertex.getZ()
                );
            }
        }

        private void isTotallyFixed() {
            if (isFixedX && isFixedY)
                angle = (float) Math.atan2(displacementY, displacementX);
        }

        private void determineDrawingHeight() {
            if (isFixedX && isFixedY)
                drawingHeight = totallyImposedDisplacement.getHeight();
            else
                drawingHeight = partiallyImposedDisplacement.getHeight();
            Log.i("DRAWING_HEIGHT", "h = " + drawingHeight);
        }

        private void calculateDeltas() {
            drawingHeightDx = drawingHeight*(float)Math.cos(angle);
            drawingHeightDy = drawingHeight*(float)Math.sin(angle);
        }

        private void calculateMagnitude() {
            magnitude = (float) Math.sqrt(displacementX*displacementX +
                    displacementY*displacementY + displacementZ*displacementZ);
        }

        public boolean isFixedX() {
            return isFixedX;
        }
        public boolean isFixedY() {
            return isFixedY;
        }
        public boolean isFixedZ() {
            return isFixedZ;
        }
        public float getDisplacementX() {
            return displacementX;
        }
        public float getDisplacementY() {
            return displacementY;
        }
        public float getDisplacementZ() {
            return displacementZ;
        }

        public void setDisplacement(PunctualFixedDisplacement fixedDisplacement) {
            isFixedX = fixedDisplacement.isFixedX();
            isFixedY = fixedDisplacement.isFixedY();
            isFixedZ = fixedDisplacement.isFixedZ();
            displacementX = fixedDisplacement.getDisplacementX();
            displacementY = fixedDisplacement.getDisplacementY();
            displacementZ = fixedDisplacement.getDisplacementZ();
        }

        public void setDisplacement(FixedDisplacement fixedDisplacement) {
            isFixedX = fixedDisplacement.isFixedX();
            isFixedY = fixedDisplacement.isFixedY();
            isFixedZ = fixedDisplacement.isFixedZ();
            displacementX = fixedDisplacement.getDisplacementX();
            displacementY = fixedDisplacement.getDisplacementY();
            displacementZ = fixedDisplacement.getDisplacementZ();
        }

        public void clear(){
            isFixedX = isFixedY = isFixedZ = false;
            displacementX = displacementY = displacementZ = 0f;
        }

        public void translate(float dx, float dy, float dz) {
            // Interactive points
            start.translate(dx, dy, dz);
            rotationControl.translate(dx, dy, dz);
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<TextureName, Texture> texturesID,
                         float unitLengthNDC) {

            colorProgram.useProgram();

            if (displacementX == 0f && displacementY == 0f) {
                if (isFixedX && isFixedY) {
                    totallyFixedSupport.bindData(colorProgram);
                    Utilities.positionObjectInScene(attachedVertex.getX(),
                            attachedVertex.getY(), attachedVertex.getZ(),
                            viewProjectionMatrix, modelViewProjectionMatrix);
                    colorProgram.setUniforms(modelViewProjectionMatrix, 0f, 0f, 0f);
                    totallyFixedSupport.draw();
                } else {
                    Utilities.positionObjectInScene(attachedVertex.getX(),
                            attachedVertex.getY(), attachedVertex.getZ(),
                            viewProjectionMatrix, A);
                    partiallyFixedSupport.bindData(colorProgram);
                    Utilities.rotate((float)Math.toDegrees(angle - initialAngle),
                            0f, 0f, 1f, R, A, modelViewProjectionMatrix);
                    colorProgram.setUniforms(modelViewProjectionMatrix, 0f, 0f, 0f);
                    partiallyFixedSupport.draw();
                }
            } else {
                if (isFixedX && isFixedY) {
                    totallyImposedDisplacement.bindData(colorProgram);
                    Utilities.positionObjectInScene(attachedVertex.getX(),
                            attachedVertex.getY(), attachedVertex.getZ(),
                            viewProjectionMatrix, A);
                    Utilities.rotate((float)Math.toDegrees(angle - initialAngle),
                            0f, 0f, 1f, R, A, modelViewProjectionMatrix);
                    colorProgram.setUniforms(modelViewProjectionMatrix,
                            0f, 0f, 0f);
                    totallyImposedDisplacement.draw();

                    // Shows the value
                    glEnable(GLES20.GL_BLEND);
                    Utilities.positionObjectInScene(0f, 0f, 0f,
                            viewProjectionMatrix, modelViewProjectionMatrix);
                    glText.begin(0f, 0f, 0f, 1.0f, modelViewProjectionMatrix);
                    glText.draw("                  ", 1f, 0f);
                    glText.draw(Float.toString(magnitude),
                            attachedVertex.getX() + drawingHeightDx,
                            attachedVertex.getY() + drawingHeightDy
                    );
                    glText.draw("                  ", 1f, 0f);

                } else {
                    Utilities.positionObjectInScene(attachedVertex.getX(),
                            attachedVertex.getY(), attachedVertex.getZ(),
                            viewProjectionMatrix, A);
                    partiallyImposedDisplacement.bindData(colorProgram);
                    Utilities.rotate((float)Math.toDegrees(angle - initialAngle),
                            0f, 0f, 1f, R, A, modelViewProjectionMatrix);
                    colorProgram.setUniforms(modelViewProjectionMatrix,
                            0f, 0f, 0f);
                    partiallyImposedDisplacement.draw();

                    // Shows the value
                    glEnable(GLES20.GL_BLEND);
                    Utilities.positionObjectInScene(0f, 0f, 0f,
                            viewProjectionMatrix, modelViewProjectionMatrix);
                    glText.begin(0f, 0f, 0f, 1.0f, modelViewProjectionMatrix);
                    glText.draw("                  ", 1f, 0f);
                    glText.draw(Float.toString(magnitude),
                            attachedVertex.getX() + drawingHeightDx,
                            attachedVertex.getY() + drawingHeightDy
                    );
                    glText.draw("                  ", 1f, 0f);
                    glDisable(GLES20.GL_BLEND);

                }
            }
        }

        public void handleDragInteractivePoint(InteractivePoint ip,
                                               float x,
                                               float y) {
            //Log.i("ControlPoint", "Punctual fixed displacement");
            if (ip == rotationControl) rotate(x, y);
        }

        private void rotate(float x, float y) {
            float dx, dy;
            dx = x - attachedVertex.getX();
            dy = y - attachedVertex.getY();
            angle = (float) Math.atan2(dy, dx);
            //Log.i("ControlPoint", "Angle = " + angle);
            calculateDeltas();
            setRotationControlCoordinates();
        }

        public void addElementsToGeometry(GeometrySet geometry) {
            geometry.addInteractivePoints(interactivePoints);
        }

        public void removeElementsFromGeometry(GeometrySet geometry) {
            geometry.removeInteractivePoints(interactivePoints);
        }

        public boolean areYouSelected(float x, float y,
                                      float selectionDistance) {
            return false;
        }

        public void applyTransformationMatrix(float[] transformationMatrix) {
            if (null == transformationMatrix) return;
            // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }

        public void handleTouchPress(float x,
                                     float y,
                                     float r,
                                     UndoRedo undoRedo) {

        }

        public Constants.DragStatus handleTouchDrag(TouchHelper.Drags drags,
                                                    float invertedViewProjectionMatrix,
                                                    UndoRedo undoRedo) {
            return Constants.DragStatus.NOTHING_DRAGGED;
        }

        public ArrayList<InteractivePoint> detectSelectedInteractivePoints(float x,
                                                                           float y,
                                                                           float r ) {
            return new ArrayList<>();
        }

        public ArrayList<InteractivePoint> getInteractivePoints() {
            return interactivePoints;
        }
        public ArrayList<Point> getVertices() {
            return null;
        }
        public Model getModel() { return null; }
        public void handleSelection() {}
        public void handleDoubleSelection() {}
        public void deleteBoundaryConditions(GeometrySet geomtry) {}
    }

    static public class Arrow {
        private ArrayList <Point> vertices;
        private float lengthSideLines;
        Point start;
        Point end;
        boolean setCoordinates;

        // Auxiliary variables
        Geometry.Vector f, a, b, c, F;

        private void initializeVariables() {
            vertices = new ArrayList();
            for (int i = 0; i < 6; i++) vertices.add(new Point());
            lengthSideLines = Constants.LENGTH_FORCES_SIDE_LINES_II;

            start = new Point();
            end = new Point();

            setCoordinates = true;

            f = new Geometry.Vector();
            a = new Geometry.Vector();
            b = new Geometry.Vector();
            c = new Geometry.Vector();
            F = new Geometry.Vector();
        }

        public Arrow() {
            initializeVariables();
        }

        public void translate(float dx, float dy, float dz) {
            start.translate(dx, dy, dz);
            end.translate(dx, dy, dz);
            Iterator<Point> it = vertices.iterator();
            while (it.hasNext()) it.next().translate(dx, dy, dz);
        }

        public void setStart(float x, float y, float z) {
            start.setCoordinates(x, y, z);
            setVertices();
        }

        public void setStart(Point p) {
            start.setCoordinates(p);
            setVertices();
        }

        public void setEnd(Point p) {
            end.setCoordinates(p);
            setVertices();
        }

        public void setEnd(float x, float y, float z) {
            end.setCoordinates(x, y, z);
            setVertices();
        }

        public void setUniLengthNDC(float unitLengthNDC) {
            lengthSideLines =
                    Constants.LENGTH_FORCES_SIDE_LINES_II/unitLengthNDC;
            setCoordinates = true;
        }

        public void setData(float x1, float y1, float z1,
                            float x2, float y2, float z2,
                            float unitLengthNDC) {
            start.setCoordinates(x1, y1, z1);
            end.setCoordinates(x2, y2, z2);
            lengthSideLines =
                    Constants.LENGTH_FORCES_SIDE_LINES_II/unitLengthNDC;
            setCoordinates = true;
        }

        public void setData(Point start,
                            Point end,
                            float unitLengthNDC) {
            this.start.setCoordinates(start);
            this.end.setCoordinates(end);
            lengthSideLines =
                    Constants.LENGTH_FORCES_SIDE_LINES_II/unitLengthNDC;
            setCoordinates = true;
        }

        public void moveToOrigin() {
            if (setCoordinates) {
                setVertices();
                setCoordinates = false;
            }

            float dx = -start.x;
            float dy = -start.y;
            float dz = -start.z;
            start.translate(dx, dy, dz);
            end.translate(dx, dy, dz);

            Iterator<Point> it = vertices.iterator();
            while (it.hasNext()) it.next().translate(dx, dy, dz);
        }

        private void setVertices() {
            // Set the coordinates for the vertices so
            // they look like an arrow
            F.setComponents(end.x - start.x, end.y - start.y, end.z - start.z);
            f.setComponents(F);
            f.scale(0.8f);

            if (Math.abs(f.y) > (float)1e-10)
                a.setComponents(1f, -f.x/f.y, 0f); // dotProduct(f,a) = 0
            else
                a.setComponents(-f.y/f.x, 1f, 0f); // dotProduct(f,a) = 0

            a.normalize();
            a.scale(0.1f * F.length());

            b.setComponents(f.x + a.x, f.y + a.y, f.z + a.z); // b = f + a
            c.setComponents(b.x - F.x, b.y - F.y, b.z - F.z); // c = b - F
            c.normalize();
            c.scale(lengthSideLines);

            vertices.get(0).setCoordinates(start.x, start.y, start.z);
            vertices.get(1).setCoordinates(end.x, end.y, end.z);

            vertices.get(2).setCoordinates(end.x, end.y, end.z);
            vertices.get(3).setCoordinates(end.x + c.x, end.y + c.y, end.z + c.z);

            a.scale(-1f);
            b.setComponents(f.x + a.x, f.y + a.y, f.z + a.z); // b = f + a
            c.setComponents(b.x - F.x, b.y - F.y, b.z - F.z); // c = b - F
            c.normalize();
            c.scale(lengthSideLines);

            vertices.get(4).setCoordinates(end.x, end.y, end.z);
            vertices.get(5).setCoordinates(end.x + c.x, end.y + c.y, end.z + c.z);
        }

        public ArrayList<Point> getVertices() {
            if (setCoordinates) {
                setVertices();
                setCoordinates = false;
            }
            return vertices;
        }
    }

    static public class PunctualForce extends Shape {
        private float value;
        private float currentUnitLengthInNDC;
        private Point start;
        private ControlPoint end;
        private Point attachedVertex;
        private ArrayList<InteractivePoint> interactivePoints;

        // Variables for drawing
        private Arrow arrow;
        private OpenGLineSegmentHelperII openGLineSegmentHelper;
        private boolean buildOpenGLData;
        private boolean updateVertexArray;

        private void initializeVariables() {
            value = 0f;
            currentUnitLengthInNDC = 0.25f;
            start = new Point();
            //start.parentShapeProperties.setParentShape(this); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            end = new ControlPoint(this);
            attachedVertex = null;
            interactivePoints = new ArrayList();

            arrow = new Arrow();
            arrow.setUniLengthNDC(currentUnitLengthInNDC);
            openGLineSegmentHelper = new OpenGLineSegmentHelperII();
            buildOpenGLData = true;
            updateVertexArray = false;

            //start.punctualForceProperties.setPunctualForce(this); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //end.punctualForceProperties.setPunctualForce(this);
        }

        public PunctualForce() {
            super();
            initializeVariables();
            build();
        }

        public void setVertex(Point vertex) {
            attachedVertex = vertex;
            start.setCoordinates(vertex.getX(), vertex.getY(),
                    vertex.getZ());
            arrow.setStart(vertex.getX(), vertex.getY(), vertex.getZ());
            scale(start.getX() + 1, start.getY(), currentUnitLengthInNDC);
        }

        public void itsVertexWasMoved() {
            float dx = attachedVertex.getX() - start.getX();
            float dy = attachedVertex.getY() - start.getY();
            translate(dx, dy, 0);
        }

        public void translate(float dx, float dy, float dz) {
            start.translate(dx, dy, dz);
            end.translate(dx, dy, dz);
            arrow.translate(dx, dy, dz);
            updateVertexArray = true;
        }

        public void setValue(float value) { this.value = value; }

        public void handleZoom(float unitLengthNDC) {
            currentUnitLengthInNDC = unitLengthNDC;
            arrow.setUniLengthNDC(unitLengthNDC);
            updateVertexArray = true;
        }

        private void build() {
            interactivePoints.add(end);
        }

        public Point getVertex() { return attachedVertex; }

        public void handleDragInteractivePoint(InteractivePoint ip, float x, float y) {
            if (ip == end) {
                scale(x, y, currentUnitLengthInNDC);
                updateVertexArray = true;
            }
        }

        public void addElementsToGeometry(GeometrySet geometry) {
            // Adds the interactive points
            geometry.addInteractivePoints(interactivePoints);
        }

        public void removeElementsFromGeometry(GeometrySet geometry) {
            geometry.removeInteractivePoints(interactivePoints);
        }

        public boolean areYouSelected(float x,
                                      float y,
                                      float selectionDistance) {
            return false;
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<TextureName, Texture> texturesID,
                         float unitLengthNCS) {

            if (buildOpenGLData) {
                //openGLineSegmentHelper.setData(arrow.getVertices()); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                openGLineSegmentHelper.buildOpenGLData(GL_LINES);
                buildOpenGLData = false;
                updateVertexArray = false;
            }

            if (updateVertexArray) {
                openGLineSegmentHelper.updateVertexArray();
                updateVertexArray = false;
            }

            colorProgram.useProgram();

            // Draws the lines
            Utilities.drawLineSegments(
                    colorProgram,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    openGLineSegmentHelper.getVertexArray(),
                    openGLineSegmentHelper.getDrawList(),
                    //getOpenGLDepth(),
                    1, // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    0.83f, 0.33f, 0f
            );

            // Draws the value
            glEnable(GLES20.GL_BLEND);
            Utilities.positionObjectInScene(0f, 0f, 0f,
                    viewProjectionMatrix, modelViewProjectionMatrix);
            glText.begin(0.83f, 0.33f, 0f, 1.0f, modelViewProjectionMatrix);
            glText.draw("                  ", 1f, 0f);
            glText.draw(Float.toString(value), end.getX(), end.getY());
            glText.draw("                  ", 1f, 0f);
            glDisable(GLES20.GL_BLEND);

            /*
            // Draws the control point
            Utilities.drawPoints(
                    colorProgram,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    visualControlPoint,
                    interactivePoints,
                    getOpenGLDepth()
            );
            //*/

        }

        private void scale(float x, float y, float unitLengthNDC) {
            float dx, dy, factor, d;
            dx = x - start.getX();
            dy = y - start.getY();
            d = (float)Math.sqrt(dx*dx + dy*dy);
            factor = Constants.ARROW_FORCE_LENGTH / (d*unitLengthNDC);
            dx *= factor;
            dy *= factor;
            end.setCoordinates(start.getX() + dx, start.getY() + dy, 0);
            arrow.setEnd(start.getX() + dx, start.getY() + dy, 0);
        }

        public float geValueOnX() {
            return (end.getX() - start.getX()) /
                    (Constants.ARROW_FORCE_LENGTH/currentUnitLengthInNDC)
                    * value;
        }

        public float geValueOnY() {
            return (end.getY() - start.getY()) /
                    (Constants.ARROW_FORCE_LENGTH/currentUnitLengthInNDC)
                    * value;
        }

        public float geValueOnZ() { return 0f; }

        public void applyTransformationMatrix(float[] transformationMatrix) {
            if (null == transformationMatrix) return;
            // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }

        public void handleTouchPress(float x,
                                     float y,
                                     float r,
                                     UndoRedo undoRedo) {

        }

        public Constants.DragStatus handleTouchDrag(TouchHelper.Drags drags,
                                                    float invertedViewProjectionMatrix,
                                                    UndoRedo undoRedo) {
            return Constants.DragStatus.NOTHING_DRAGGED;
        }

        public ArrayList<InteractivePoint> detectSelectedInteractivePoints(float x,
                                                                           float y,
                                                                           float r ) {
            return new ArrayList<>();
        }

        public ArrayList<InteractivePoint> getInteractivePoints() {
            return interactivePoints;
        }
        public ArrayList<Point> getVertices() {
            return arrow.getVertices();
        }
        public Model getModel() { return null; }
        public void handleSelection() {}
        public void handleDoubleSelection() {}
        public void deleteBoundaryConditions(GeometrySet geomtry) {}
    }

    static public class LinearFixedDisplacement extends Shape {
        private boolean isFixedNormalDisplacement, isFixedTangentDisplacement;
        private float normalDisplacement, tangentDisplacement;
        private Point referenceVertex;

        private final int NO_ATTACHED_SHAPE = 0;
        private final int LINEAR_SHAPE_WAS_ATTACHED = 1;
        private final int LINE_SEGMENT_WAS_ATTACHED = 2;

        // The fixed displacement can be imposed over a liner shape
        // or a line segment
        private int typeOfAttachedShape;
        //private LineSegment attachedLineSegment; // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        private LinearShape attachedLinearShape;

        // Variables for drawing
        private OpenGLineSegmentHelperII openGLineSegmentHelper;
        private ArrayList<Point> vertices;
        private ArrayList<Point> divisionPoints;
        private ArrayList<Point> centers;
        private float unitLengthNDC;

        private final int BUILD_OPENGL_DATA = 0;
        private final int UPDATE_VERTEX_ARRAY = 1;
        private final int DO_NOTHING = -1;
        private BlockingQueue<Integer> updates;
        private ReadWriteLock rwlock = new ReentrantReadWriteLock();

        private final float DX_NDC = 0.1f;
        private final float DY_NDC = 0.1f;
        private final float NORMAL_DISTANCE = 0.06f;

        private VisualObject supportWheel;

        private void initializeVariables() {
            referenceVertex = new Point();
            vertices = new ArrayList();
            divisionPoints = null;
            centers = new ArrayList();
            unitLengthNDC = 0.25f;

            typeOfAttachedShape = NO_ATTACHED_SHAPE;
            attachedLinearShape = null;
            //attachedLineSegment = null; // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            isFixedNormalDisplacement = isFixedTangentDisplacement = false;
            normalDisplacement = tangentDisplacement = 0f;

            supportWheel = new VisualObject();

            openGLineSegmentHelper = new OpenGLineSegmentHelperII();
            updates = new LinkedBlockingQueue();
            updates.add(BUILD_OPENGL_DATA);
        }

        public LinearFixedDisplacement() {
            super();
            initializeVariables();
        }

        public void setData(LinearShape linearShape,
                            float unitLengthNDC) {
            typeOfAttachedShape = LINEAR_SHAPE_WAS_ATTACHED;
            attachedLinearShape = linearShape;
            this.unitLengthNDC = unitLengthNDC;
            supportWheel.createVisualCircle(
                    0.5f*(NORMAL_DISTANCE - 0.009f)/unitLengthNDC,
                    Constants.NUMBER_OF_SIDES_TO_DRAW_SMALL_CIRCLE
            );
            //referenceVertex.setCoordinates(linearShape.getFirstVertex()); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }

        public void setData(//LineSegment lineSegment, // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            float unitLengthNDC) {
            typeOfAttachedShape = LINE_SEGMENT_WAS_ATTACHED;
            // attachedLineSegment = lineSegment; // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            this.unitLengthNDC = unitLengthNDC;
            supportWheel.createVisualCircle(
                    0.5f*(NORMAL_DISTANCE - 0.009f)/unitLengthNDC,
                    Constants.NUMBER_OF_SIDES_TO_DRAW_SMALL_CIRCLE
            );
            //referenceVertex.setCoordinates(lineSegment.getFirstVertex()); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }

        public void setDisplacement(FixedDisplacement displacement) {
            isFixedNormalDisplacement = displacement.isNormalFixed;
            isFixedTangentDisplacement = displacement.isTangentFixed;

            normalDisplacement = displacement.normalDisplacement;
            tangentDisplacement = displacement.tangentDisplacement;
        }

        public void setBothDisplacements(float normal, float tangent) {
            normalDisplacement = normal;
            tangentDisplacement = tangent;
            isFixedNormalDisplacement = true;
            isFixedTangentDisplacement = true;
        }

        public void setNormalDisplacement(float value) {
            normalDisplacement = value;
            isFixedNormalDisplacement = true;
        }

        public void setTangentDisplacement(float value) {
            tangentDisplacement = value;
            isFixedTangentDisplacement = true;
        }

        public boolean isFixedOnX() {
            // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            return true;
        }

        public boolean isFixedOnY() {
            // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            return true;
        }

        public boolean isFixedOnXY() {
            // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            return true;
        }

        public void attachedShapeWasChanged(Constants.CADOperation cadOperation) {
            float dx, dy;
            switch (cadOperation) {
                case SHAPE_WAS_TRANSLATED:
                    dx = dy = 0f;
                    switch (typeOfAttachedShape) {
                        case LINEAR_SHAPE_WAS_ATTACHED:
                            //dx = attachedLinearShape.getFirstVertex().getX()
                            //        - referenceVertex.getX();
                            //dy = attachedLinearShape.getFirstVertex().getY()
                            //        - referenceVertex.getY(); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            break;
                        case LINE_SEGMENT_WAS_ATTACHED:
                            //dx = attachedLineSegment.getFirstVertex().getX()
                            //        - referenceVertex.getX();
                            //dy = attachedLineSegment.getFirstVertex().getY()
                            //        - referenceVertex.getY(); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            break;
                    }
                    translate(dx, dy, 0);
                    break;
                case SHAPE_WAS_CHANGED:
                    build();
                    break;
            }
        }

        public void translate(float dx, float dy, float dz) {
            // The vertices
            Iterator<Point> itV = vertices.iterator();
            while (itV.hasNext()) itV.next().translate(dx, dy, dz);

            // The centers
            //Iterator<Point> itSP = centers.iterator();
            //while (itSP.hasNext()) itSP.next().translate(dx, dy, dz); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            // Reports a update
            updates.add(UPDATE_VERTEX_ARRAY);
        }

        public void build() {
            rwlock.writeLock().lock();
            try {
                if (isFixedNormalDisplacement && isFixedTangentDisplacement) {
                    createVerticesForTotallyFixedSupport();
                } else {
                    createVerticesForPartiallyFixedSupport();
                    createCentersForCircles();
                }
            } finally {
                rwlock.writeLock().unlock();
            }
            updates.add(BUILD_OPENGL_DATA);
        }

        private void setVectorForSupportLine(Point sp1,
                                             Point sp2,
                                             boolean clockwiseOrder,
                                             float distance,
                                             Geometry.Vector d) {
            Geometry.Vector a = new Geometry.Vector();
            Geometry.Vector b = new Geometry.Vector();

            a.x = sp2.x - sp1.x;
            a.y = sp2.y - sp1.y;
            a.z = 0;
            a.normalize();

            if (Math.abs(a.y) > 1e-5) {
                b.x = 1;
                b.y = (-0.7071f - a.x * b.x) / a.y;
                b.z = 0;
            } else {
                b.y = 1;
                b.x = (-0.7071f - a.y * b.y) / a.x;
                b.z = 0;
            }

            if (clockwiseOrder) {
                if ((a.x * b.y - b.x * a.y) > 0) {
                    if (Math.abs(a.y) > 1e-5) {
                        b.x = -1;
                        b.y = (-0.7071f - a.x * b.x) / a.y;
                        b.z = 0;
                    } else {
                        b.y = -1;
                        b.x = (-0.7071f - a.y * b.y) / a.x;
                        b.z = 0;
                    }
                }
            } else {
                if ((a.x * b.y - b.x * a.y) < 0) {
                    if (Math.abs(a.y) > 1e-5) {
                        b.x = -1;
                        b.y = (-0.7071f - a.x * b.x) / a.y;
                        b.z = 0;
                    } else {
                        b.y = -1;
                        b.x = (-0.7071f - a.y * b.y) / a.x;
                        b.z = 0;
                    }
                }
            }

            b.normalize();
            b.scale(distance);

            d.x = b.x;
            d.y = b.y;
            d.z = b.z;
        }

        private void createVerticesForTotallyFixedSupport() {
            Point sp1, sp2;
            Geometry.Vector d = new Geometry.Vector();
            float distance = Constants.LENGTH_SUPPORT_LINES/unitLengthNDC;
            boolean clockwiseOrder;

            switch (typeOfAttachedShape) {
                case LINEAR_SHAPE_WAS_ATTACHED:
                    //divisionPoints = attachedLinearShape.getDivisionPoints(
                    //        Constants.DISTANCE_BETWEEN_LINES_IN_LINEAR_SUPPORT/
                    //                unitLengthNDC);// PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
                case LINE_SEGMENT_WAS_ATTACHED:
                    //divisionPoints = attachedLineSegment.getDivisionPoints(
                    //        Constants.DISTANCE_BETWEEN_LINES_IN_LINEAR_SUPPORT/
                    //               unitLengthNDC); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
            }

            //clockwiseOrder = Utilities.isClockwiseOrder(divisionPoints); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            Iterator<Point> it = divisionPoints.iterator();

            //Log.i("Divison points", "-------------------------------------------");
            //Log.i("Supports", "Clockwise order = " + clockwiseOrder);

            vertices.clear();
            if (it.hasNext()) {
                sp1 = it.next();
                //Log.i("Supports", "" + sp1.x + "  " + sp1.y);
                while (it.hasNext()) {
                    sp2 = it.next();
                    //Log.i("Supports", "" + sp2.x + "  " + sp2.y);
                    // Here we do not need to extend further the
                    // drawing because it is attached to the linear shape
                    vertices.add(new Point(sp1.x, sp1.y, sp1.z));
                    //setVectorForSupportLine(sp1, sp2, clockwiseOrder, distance, d); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    vertices.add(new Point(sp1.x + d.x, sp1.y + d.y, sp1.z));
                    sp1 = sp2;
                }
                vertices.add(new Point(sp1.x, sp1.y, sp1.z));
                vertices.add(new Point(sp1.x + d.x, sp1.y + d.y, sp1.z));
            }
        }

        private void createCentersForCircles() {
            Point sp1, sp2, sp;
            Point projection = new Point();
            float distance = 0.5f*NORMAL_DISTANCE/unitLengthNDC;
            Iterator<Point> it1, it2;
            boolean clockwiseOrder;

            switch (typeOfAttachedShape) {
                case LINEAR_SHAPE_WAS_ATTACHED:
                    //divisionPoints = attachedLinearShape.getDivisionPoints(
                    //        1.5f*NORMAL_DISTANCE/unitLengthNDC); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
                case LINE_SEGMENT_WAS_ATTACHED:
                    //divisionPoints = attachedLineSegment.getDivisionPoints(
                    //        1.5f*NORMAL_DISTANCE/unitLengthNDC);// PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
            }

            //clockwiseOrder = Utilities.isClockwiseOrder(divisionPoints); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            it1 = divisionPoints.iterator();
            it2 = divisionPoints.iterator();

            if (LoggerConfig.ON) {
                //Log.i("Centers", "Centers----------------------------------------");
            }

            centers.clear();
            if (it1.hasNext() && it2.hasNext()) {
                sp1 = it1.next();
                sp = it2.next();

                if (it1.hasNext()) {
                    sp2 = it1.next();

                    //extendPoint(sp, sp1, sp2, projection, clockwiseOrder, distance); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    centers.add(new Point(projection));
                    if (LoggerConfig.ON) {
                        //Log.i("Centers", "" + projection.x + "  " + projection.y);
                    }

                    while (it1.hasNext() && it2.hasNext()) {
                        sp = it2.next();
                        sp2 = it1.next();
                        //extendPoint(sp, sp1, sp2, projection, clockwiseOrder, distance); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        centers.add(new Point(projection));
                        if (LoggerConfig.ON) {
                            //Log.i("Centers", "" + projection.x + "  " + projection.y);
                        }
                        sp1 = sp;
                    }

                    if (it2.hasNext()) {
                        sp = it2.next();
                        sp1 = sp;

                        //extendPoint(sp, sp1, sp2, projection, clockwiseOrder, distance); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        centers.add(new Point(projection));
                        if (LoggerConfig.ON) {
                            //Log.i("Centers", "" + projection.x + "  " + projection.y);
                        }
                    }
                }
            }
        }

        private void createVerticesForPartiallyFixedSupport() {
            Point sp1, sp2, sp;
            Point pr1 = new Point();
            Point pr2 = new Point();
            Point projection = new Point();
            Iterator<Point> it1, it2;
            float distance = NORMAL_DISTANCE/unitLengthNDC;
            float distance2 = Constants.LENGTH_SUPPORT_LINES/unitLengthNDC;
            Geometry.Vector d = new Geometry.Vector();
            boolean clockwiseOrder;

            // The division points
            switch (typeOfAttachedShape) {
                case LINEAR_SHAPE_WAS_ATTACHED:
                    //divisionPoints = attachedLinearShape.getDivisionPoints(
                    //        Constants.DISTANCE_BETWEEN_LINES_IN_LINEAR_SUPPORT
                    //                /unitLengthNDC); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
                case LINE_SEGMENT_WAS_ATTACHED:
                    //divisionPoints = attachedLineSegment.getDivisionPoints(
                    //        Constants.DISTANCE_BETWEEN_LINES_IN_LINEAR_SUPPORT
                    //                /unitLengthNDC); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
            }

            //clockwiseOrder = Utilities.isClockwiseOrder(divisionPoints); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            it1 = divisionPoints.iterator();
            it2 = divisionPoints.iterator();

            // The following vertices are used to draw the support lines
            vertices.clear();
            if (it1.hasNext() && it2.hasNext()) {
                sp1 = it1.next();
                sp = it2.next();
                if (it1.hasNext()) {
                    sp2 = it1.next();

                    // First extend. Because we have to draw circles between the
                    // support lines and the linear shape we have to extend each
                    // division point
                    //extendPoint(sp, sp1, sp2, projection, clockwiseOrder, distance); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    //setVectorForSupportLine(sp, sp2, clockwiseOrder, distance2, d); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    vertices.add(new Point(projection.x, projection.y, projection.z));
                    vertices.add(new Point(projection.x + d.x, projection.y + d.y,
                            projection.z));
                    pr1.setCoordinates(projection);

                    // Middle extends
                    while (it1.hasNext() && it2.hasNext()) {
                        sp = it2.next();
                        sp2 = it1.next();
                        //extendPoint(sp, sp1, sp2, projection, clockwiseOrder, distance);// PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        //setVectorForSupportLine(sp, sp2, clockwiseOrder,distance2, d);// PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                        // To define a line
                        vertices.add(new Point(projection.x, projection.y, projection.z));
                        vertices.add(new Point(projection.x + d.x, projection.y + d.y,
                                projection.z));

                        pr2.setCoordinates(projection);
                        vertices.add(new Point(pr1.x, pr1.y, pr1.z));
                        vertices.add(new Point(pr2.x, pr2.y, pr2.z));

                        sp1 = sp;
                        pr1.setCoordinates(pr2);
                    }

                    // Last extend
                    if (it2.hasNext()) {
                        sp = it2.next();
                        sp1 = sp;

                        //extendPoint(sp, sp1, sp2, projection, clockwiseOrder, distance);// PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        //setVectorForSupportLine(sp, sp2, clockwiseOrder, distance2, d);// PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        vertices.add(new Point(projection.x, projection.y, projection.z));
                        vertices.add(new Point(projection.x + d.x, projection.y + d.y,
                                projection.z));

                        pr2.setCoordinates(projection);
                        vertices.add(new Point(pr1.x, pr1.y, pr1.z));
                        vertices.add(new Point(pr2.x, pr2.y, pr2.z));
                    }
                }
            }
        }

        private void extendPoint(Point sp,
                                 Point sp1,
                                 Point sp2,
                                 Point projection,
                                 boolean clockwiseOrder,
                                 float distance) {
            Geometry.Vector a = new Geometry.Vector();
            Geometry.Vector b = new Geometry.Vector();

            a.x = sp2.x - sp1.x;
            a.y = sp2.y - sp1.y;
            a.z = 0;

            if (Math.abs(a.y) > 1e-10) {
                b.x = 1;
                b.y = -a.x * b.x / a.y;
                b.z = 0;
            } else {
                b.y = 1;
                b.x = -a.y*b.y/a.x;
                b.z = 0;
            }

            if (clockwiseOrder) {
                if ((a.x * b.y - b.x * a.y) > 0) {
                    b.x *= -1;
                    b.y *= -1;
                }
            } else {
                if ((a.x * b.y - b.x * a.y) < 0) {
                    b.x *= -1;
                    b.y *= -1;
                }
            }

            b.normalize();
            b.scale(distance);

            projection.x = sp.x + b.x;
            projection.y = sp.y + b.y;
            projection.z = 0;
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<TextureName, Texture> texturesID,
                         float unitLengthNDC) {
            switch (Tools.getNextUpdate(updates)) {
                case BUILD_OPENGL_DATA:
                    rwlock.readLock().lock();
                    try {
                        //openGLineSegmentHelper.setData(vertices);// PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        openGLineSegmentHelper.buildOpenGLData(GL_LINES);
                    } finally {
                        rwlock.readLock().unlock();
                    }
                    break;
                case UPDATE_VERTEX_ARRAY:
                    rwlock.readLock().lock();
                    try {
                        openGLineSegmentHelper.updateVertexArray();
                    } finally {
                        rwlock.readLock().unlock();
                    }
                    break;
            }

            colorProgram.useProgram();

            //*
            // Only draw the wheels when the support is partially fixed
            if ((isFixedTangentDisplacement && !isFixedNormalDisplacement) ||
                    (!isFixedTangentDisplacement && isFixedNormalDisplacement)) {

                rwlock.readLock().lock();
                try {
                    // Draws the wheels
                    Point sp;
                    Iterator<Point> it = centers.iterator();

                    supportWheel.bindData(colorProgram);
                    while(it.hasNext()) {
                        sp = it.next();
                        Utilities.positionObjectInScene(sp.x, sp.y, sp.z, viewProjectionMatrix,
                                modelViewProjectionMatrix);
                        colorProgram.setUniforms(modelViewProjectionMatrix, 0f, 0f, 0f);
                        supportWheel.draw();
                    }
                } finally {
                    rwlock.readLock().unlock();
                }
            }
            //*/

            // Draws the lines
            Utilities.drawLineSegments(
                    colorProgram,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    openGLineSegmentHelper.getVertexArray(),
                    openGLineSegmentHelper.getDrawList(),
                    //getOpenGLDepth(),
                    1, // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    0f, 0f, 0f
            );

        }

        public float getValueOnX() {
            // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            return 0;
        }

        public float getValueOnY() {
            // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            return 0;
        }


        public ArrayList<Point> getVertices() {
            return vertices;
        }

        public void applyTransformationMatrix(float[] transformationMatrix) {
            if (null == transformationMatrix) return;
            // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }

        public void addElementsToGeometry(GeometrySet geometry) {
            if (null == geometry) return;
            // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }

        public void removeElementsFromGeometry(GeometrySet geometry) {
            if (null == geometry) return;
            // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }

        public void handleTouchPress(float x,
                                     float y,
                                     float r,
                                     UndoRedo undoRedo) {

        }

        public Constants.DragStatus handleTouchDrag(TouchHelper.Drags drags,
                                                    float invertedViewProjectionMatrix,
                                                    UndoRedo undoRedo) {
            return Constants.DragStatus.NOTHING_DRAGGED;
        }

        public ArrayList<InteractivePoint> detectSelectedInteractivePoints(float x,
                                                                           float y,
                                                                           float r) {
            return new ArrayList<>();
        }

        // Unused methods
        public boolean areYouSelected(float x, float y, float selectionDistance) {
            return false;
        }
        public void handleDragInteractivePoint(InteractivePoint ip,
                                               float x, float y) {}
        public ArrayList<InteractivePoint> getInteractivePoints() { return null; }
        public Model getModel() { return null; }
        public void handleSelection() {}
        public void handleDoubleSelection() {}
        public void deleteBoundaryConditions(GeometrySet geomtry) {}
    }

    static public class UniformDistributedForce extends Shape {
        private final int NO_ATTACHED_SHAPE = 0;
        private final int LINEAR_SHAPE_WAS_ATTACHED = 1;
        private final int LINE_SEGMENT_WAS_ATTACHED = 2;

        private float value;

        // The force could be applied over a whole linear shape
        // or only over a line segment
        private int typeOfAttachedShape;
        private Geometry.LinearShape attachedLinearShape;
        //private Geometry.LineSegment attachedLineSegment; // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        private Point start, end;
        private Geometry.Point startMagnitudeAndDirectionControl;
        private ControlPoint magnitudeAndDirectionControl;
        private float unitLengthNDC;
        private float startForceFactorLocation;

        private Arrow arrow;
        private ArrayList<Point> divisionPoints;

        private ArrayList<Point> vertices;
        private ArrayList<InteractivePoint> interactivePoints;

        // Variables for drawing
        private OpenGLineSegmentHelperII openGLineSegmentHelper;

        private final int BUILD_OPENGL_DATA = 0;
        private final int UPDATE_VERTEX_ARRAY = 1;
        private final int DO_NOTHING = -1;
        private BlockingQueue<Integer> updates;
        private ReadWriteLock rwlock = new ReentrantReadWriteLock();

        private void initializeVariables() {
            value = 0f;

            typeOfAttachedShape = NO_ATTACHED_SHAPE;
            attachedLinearShape = null;
            //attachedLineSegment = null; // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            start = new Point();
            end = new Point();
            startMagnitudeAndDirectionControl = new Geometry.Point();
            magnitudeAndDirectionControl = new ControlPoint(this);
            unitLengthNDC = 0.25f;
            startForceFactorLocation = 0f;

            vertices = new ArrayList();
            interactivePoints = new ArrayList();
            divisionPoints = null;
            arrow = new Arrow();

            openGLineSegmentHelper = new OpenGLineSegmentHelperII();
            updates = new LinkedBlockingQueue();
            updates.add(BUILD_OPENGL_DATA);
        }

        public UniformDistributedForce() {
            super();
            initializeVariables();
            interactivePoints.add(magnitudeAndDirectionControl);
        }

        // This method should be triggered when the attached linear shape
        // or line segment is changed (was translated, some node or nodes were
        // moved)
        public void attachedShapeWasChanged(Constants.CADOperation cadOperation) {
            float dx, dy;

            switch (cadOperation) {
                case SHAPE_WAS_TRANSLATED:
                    dx = dy = 0f;
                    switch (typeOfAttachedShape) {
                        case LINEAR_SHAPE_WAS_ATTACHED:
                            /*
                            dx = attachedLinearShape.getFirstVertex().getX()
                                    - start.getX();
                            dy = attachedLinearShape.getFirstVertex().getY()
                                    - start.getY();
                                    //*/ // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            break;
                        case LINE_SEGMENT_WAS_ATTACHED:
                            /*
                            dx = attachedLineSegment.getFirstVertex().getX()
                                    - start.getX();
                            dy = attachedLineSegment.getFirstVertex().getY()
                                    - start.getY();
                                    //*/// PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            break;
                    }

                    translate(dx, dy, 0);
                    break;

                case SHAPE_WAS_CHANGED:
                    wholeUpdate();
                    break;
            }
        }

        public boolean areYouSelected(float x,
                                      float y,
                                      float selectionDistance) {
            return false;
        }

        public void addElementsToGeometry(GeometrySet geometry) {
            //super.addElementsToGeometry(geometry); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            geometry.addInteractivePoints(interactivePoints);
        }

        public void removeElementsFromGeometry(GeometrySet geometry) {
            //super.removeElementsFromGeometry(geometry); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            geometry.removeInteractivePoints(interactivePoints);
        }

        //*
        public void setStart(Point p) {
            Point v1, v2;
            Point projectionPoint = new Point();

            start.setCoordinates(p);
            switch (typeOfAttachedShape) {
                case LINEAR_SHAPE_WAS_ATTACHED:
                    /*
                    projectionPoint.setCoordinates(
                            attachedLinearShape.getDivisionPointFromStart(
                            startForceFactorLocation));
                            //*/// PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
                case LINE_SEGMENT_WAS_ATTACHED:
                    //v1 = attachedLineSegment.getFirstVertex();
                    //v2 = attachedLineSegment.getLastVertex(); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    /*
                    projectionPoint.setCoordinates(
                            v1.getX() + startForceFactorLocation *(v2.getX() - v1.getX()),
                            v2.getY() + startForceFactorLocation * (v2.getY() - v1.getY()),
                            0
                    );
                    //*/ // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
            }

            float dx = projectionPoint.x - startMagnitudeAndDirectionControl.x;
            float dy = projectionPoint.y - startMagnitudeAndDirectionControl.y;
            float dz = projectionPoint.z - startMagnitudeAndDirectionControl.z;

            startMagnitudeAndDirectionControl.translate(dx, dy, dz);
            magnitudeAndDirectionControl.translate(dx, dy, dz);

            rwlock.writeLock().lock();
            try {
                createVertices();
                setVertices();
            } finally {
                rwlock.writeLock().unlock();
            }

            updates.add(BUILD_OPENGL_DATA);
        }

        public void setEnd(Point p) {
            Point v1, v2;
            Point projectionPoint = new Point();

            end.setCoordinates(p);
            switch (typeOfAttachedShape) {
                case LINEAR_SHAPE_WAS_ATTACHED:
                    /*
                    projectionPoint.setCoordinates(
                            attachedLinearShape.getDivisionPointFromStart(
                                    startForceFactorLocation));
                                    //*/ // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
                case LINE_SEGMENT_WAS_ATTACHED:
                    /*
                    v1 = attachedLineSegment.getFirstVertex();
                    v2 = attachedLineSegment.getLastVertex(); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    projectionPoint.setCoordinates(
                            v1.getX() + startForceFactorLocation * (v2.getX() - v1.getX()),
                            v2.getY() + startForceFactorLocation * (v2.getY() - v1.getY()),
                            0
                    );
                    //*/
                    break;
            }

            float dx = projectionPoint.x - startMagnitudeAndDirectionControl.x;
            float dy = projectionPoint.y - startMagnitudeAndDirectionControl.y;
            float dz = projectionPoint.z - startMagnitudeAndDirectionControl.z;

            startMagnitudeAndDirectionControl.translate(dx, dy, dz);
            magnitudeAndDirectionControl.translate(dx, dy, dz);

            rwlock.writeLock().lock();
            try {
                createVertices();
                setVertices();
            } finally {
                rwlock.writeLock().unlock();
            }

            updates.add(BUILD_OPENGL_DATA);
        }

        private void wholeUpdate() {
            Point v1, v2;
            Point projectionPoint = new Point();

            switch (typeOfAttachedShape) {
                case LINEAR_SHAPE_WAS_ATTACHED:
                    /*
                    v1 = attachedLinearShape.getFirstVertex();
                    v2 = attachedLinearShape.getLastVertex();
                    projectionPoint.setCoordinates(
                            attachedLinearShape.getDivisionPointFromStart(
                                    startForceFactorLocation));
                    wholeUpdate(v1, v2, projectionPoint);
                    //*/ // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
                case LINE_SEGMENT_WAS_ATTACHED:
                    /*
                    v1 = attachedLineSegment.getFirstVertex();
                    v2 = attachedLineSegment.getLastVertex();
                    projectionPoint.setCoordinates(
                            v1.getX() + startForceFactorLocation * (v2.getX() - v1.getX()),
                            v1.getY() + startForceFactorLocation * (v2.getY() - v1.getY()),
                            0
                    );
                    wholeUpdate(v1, v2, projectionPoint);
                    //*/ // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
            }
        }

        private void wholeUpdate(Point v1, Point v2, Point projectionPoint) {
            float dx, dy;

            // Start and end updates
            start.setCoordinates(v1);
            end.setCoordinates(v2);

            // Control points update
            dx = projectionPoint.x - startMagnitudeAndDirectionControl.x;
            dy = projectionPoint.y - startMagnitudeAndDirectionControl.y;

            Log.i("Translation", "dx = " + dx);
            Log.i("Translation", "dy = " + dy);

            startMagnitudeAndDirectionControl.translate(dx, dy, 0);
            magnitudeAndDirectionControl.translate(dx, dy, 0);

            //*
            // New division points
            rwlock.writeLock().lock();
            try {
                createVertices();
                setVertices();
            } finally {
                rwlock.writeLock().unlock();
            }

            // New OpenGL data
            updates.add(BUILD_OPENGL_DATA);
            //*/
        }
        //*/

        public void setData(Geometry.LinearShape linearShape,
                            float unitLengthNDC) {

            typeOfAttachedShape = LINEAR_SHAPE_WAS_ATTACHED;
            attachedLinearShape = linearShape;

            //start.setCoordinates(linearShape.getFirstVertex());
            //end.setCoordinates(linearShape.getLastVertex()); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            startMagnitudeAndDirectionControl.setCoordinates(start.getX(),
                    start.getY(), start.getZ());
            magnitudeAndDirectionControl.setCoordinates(start.getX() + 1,
                    start.getY(), start.getZ());

            startForceFactorLocation = 0f;
            this.unitLengthNDC = unitLengthNDC;
            arrow.setUniLengthNDC(unitLengthNDC);

            scale(1, 0, unitLengthNDC);

            rwlock.writeLock().lock();
            try {
                createVertices();
                setVertices();
            } finally {
                rwlock.writeLock().unlock();
            }
        }

        public void setData(// Geometry.LineSegment lineSegment, // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            float unitLengthNDC) {
            typeOfAttachedShape = LINE_SEGMENT_WAS_ATTACHED;
            //attachedLineSegment = lineSegment; // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            //start.setCoordinates(lineSegment.getFirstVertex());
            //end.setCoordinates(lineSegment.getLastVertex()); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            startMagnitudeAndDirectionControl.setCoordinates(start.getX(),
                    start.getY(), 0);
            magnitudeAndDirectionControl.setCoordinates(start.getX() + 1,
                    start.getY(), start.getZ());
            startForceFactorLocation = 0f;
            this.unitLengthNDC = unitLengthNDC;
            arrow.setUniLengthNDC(unitLengthNDC);

            scale(1, 0, unitLengthNDC);

            rwlock.writeLock().lock();
            try {
                createVertices();
                setVertices();
            } finally {
                rwlock.writeLock().unlock();
            }
        }

        private void createVertices() {
            int numberOfForces, numberOfVertices;

            // The division points
            switch (typeOfAttachedShape) {
                case LINEAR_SHAPE_WAS_ATTACHED:
                    /*
                    divisionPoints = attachedLinearShape.getDivisionPoints(
                            Constants.DISTANCE_BETWEEN_LINES_IN_UNIFORM_FORCE/
                                    unitLengthNDC);
                    break;

                case LINE_SEGMENT_WAS_ATTACHED:
                    divisionPoints = attachedLineSegment.getDivisionPoints(
                            Constants.DISTANCE_BETWEEN_LINES_IN_UNIFORM_FORCE/
                                    unitLengthNDC);
                    break;
                    //*/ // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            }

            if (null == divisionPoints) divisionPoints = new ArrayList();

            // Calculates the number of vertices
            numberOfForces = divisionPoints.size();
            numberOfVertices = numberOfForces*6;

            vertices.clear();
            for (int i = 0; i < numberOfVertices; i++) vertices.add(new Point());
        }

        public void setVertices() {
            float x, y, z;
            Point arrowVertex;
            Point sp;

            // This arrow will be copied to draw the uniform force
            arrow.setStart(startMagnitudeAndDirectionControl);
            //arrow.setEnd(magnitudeAndDirectionControl); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            arrow.moveToOrigin();

            Iterator<Geometry.Point> itDivPoints = divisionPoints.iterator();
            Iterator<Point> itVertices = vertices.iterator(), itArrow;
            while (itDivPoints.hasNext()) {
                sp = itDivPoints.next();
                itArrow = arrow.getVertices().iterator();
                while (itArrow.hasNext()) {
                    arrowVertex = itArrow.next();
                    if (itVertices.hasNext()) {
                        x = arrowVertex.getX() + sp.x;
                        y = arrowVertex.getY() + sp.y;
                        z = arrowVertex.getZ() + sp.z;
                        itVertices.next().setCoordinates(x, y, z);
                    }
                }
            }
        }

        public void setValue(float value) {
            this.value = value;
        }

        public void handleDragInteractivePoint(InteractivePoint ip,
                                               float x, float y) {
            if (ip == magnitudeAndDirectionControl) {
                scale(x, y, unitLengthNDC);
                rwlock.writeLock().lock();
                try {
                    setVertices();
                    updates.add(UPDATE_VERTEX_ARRAY);
                } finally {
                    rwlock.writeLock().unlock();
                }
            }
        }

        // PENDING: I do not know what this function does!!!!!!!!!!!!!!!!!!!!
        private void scale(float x, float y, float unitLengthNDC) {
            float dx, dy, factor, d;
            dx = x - startMagnitudeAndDirectionControl.x;
            dy = y - startMagnitudeAndDirectionControl.y;
            d = (float)Math.sqrt(dx*dx + dy*dy);
            factor = Constants.ARROW_FORCE_LENGTH / (d*unitLengthNDC);
            dx *= factor;
            dy *= factor;
            magnitudeAndDirectionControl.setCoordinates(
                    startMagnitudeAndDirectionControl.x + dx,
                    startMagnitudeAndDirectionControl.y + dy,
                    0);
        }

        public void handleZoom(float unitLengthNDC) {
            this.unitLengthNDC = unitLengthNDC;
            arrow.setUniLengthNDC(unitLengthNDC);

            rwlock.writeLock().lock();
            try {
                setVertices();
            } finally {
                rwlock.writeLock().unlock();
            }

            updates.add(UPDATE_VERTEX_ARRAY);
        }

        public void translate(float dx, float dy, float dz) {
            start.translate(dx, dy, dz);
            end.translate(dx, dy, dz);
            startMagnitudeAndDirectionControl.translate(dx, dy, dz);
            magnitudeAndDirectionControl.translate(dx, dy, dz);

            Iterator<Point> itV = vertices.iterator();
            while (itV.hasNext()) itV.next().translate(dx, dy, dz);

            Iterator<Point> itSP = divisionPoints.iterator();
            while (itSP.hasNext()) itSP.next().translate(dx, dy, dz);

            updates.add(UPDATE_VERTEX_ARRAY);
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<TextureName, Texture> texturesID,
                         float unitLengthNDC) {

            switch (Tools.getNextUpdate(updates)) {
                case BUILD_OPENGL_DATA:
                    rwlock.readLock().lock();
                    try {
                        //openGLineSegmentHelper.setData(vertices);// PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        openGLineSegmentHelper.buildOpenGLData(GL_LINES);
                    } finally {
                        rwlock.readLock().unlock();
                    }
                    break;
                case UPDATE_VERTEX_ARRAY:
                    rwlock.readLock().lock();
                    try {
                        openGLineSegmentHelper.updateVertexArray();
                    } finally {
                        rwlock.readLock().unlock();
                    }
                    break;
            }

            colorProgram.useProgram();

            // Draws the lines
            Utilities.drawLineSegments(
                    colorProgram,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    openGLineSegmentHelper.getVertexArray(),
                    openGLineSegmentHelper.getDrawList(),
                    //getOpenGLDepth(),
                    1, // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    0.83f, 0.33f, 0f
            );

            /*
            // Draws the control point
            Utilities.drawPoints(
                    colorProgram,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    visualJoiningPoint,
                    interactivePoints,
                    getOpenGLDepth(),
                    0.83f, 0.33f, 0f
            );
            //*/

            // Draws the value
            glEnable(GLES20.GL_BLEND);
            Utilities.positionObjectInScene(0f, 0f, 0f,
                    viewProjectionMatrix, modelViewProjectionMatrix);
            glText.begin(0.83f, 0.33f, 0f, 1.0f, modelViewProjectionMatrix);
            glText.draw("                  ", 1f, 0f);
            glText.draw(Float.toString(value),
                    magnitudeAndDirectionControl.getX(),
                    magnitudeAndDirectionControl.getY());
            glText.draw("                  ", 1f, 0f);
            glDisable(GLES20.GL_BLEND);
        }

        public float getValueOnX() {
            float length, force;
            double angle;

            length = 0f;
            switch (typeOfAttachedShape) {
                case LINEAR_SHAPE_WAS_ATTACHED:
                    //length = attachedLinearShape.getLength(); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
                case LINE_SEGMENT_WAS_ATTACHED:
                    // length = attachedLineSegment.getLength(); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
            }

            force = length*value;
            angle = Math.atan2(magnitudeAndDirectionControl.getY() - startMagnitudeAndDirectionControl.y,
                    (magnitudeAndDirectionControl.getX() - startMagnitudeAndDirectionControl.x));

            return force*(float)Math.cos(angle);
        }
        public float getValueOnY() {
            float length, force;
            double angle;

            length = 0f;
            switch (typeOfAttachedShape) {
                case LINEAR_SHAPE_WAS_ATTACHED:
                    //length = attachedLinearShape.getLength(); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
                case LINE_SEGMENT_WAS_ATTACHED:
                    //length = attachedLineSegment.getLength(); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    break;
            }

            force = length*value;
            angle = Math.atan2(magnitudeAndDirectionControl.getY() - startMagnitudeAndDirectionControl.y,
                    (magnitudeAndDirectionControl.getX() - startMagnitudeAndDirectionControl.x));

            return force*(float)Math.sin(angle);
        }

        public void applyTransformationMatrix(float[] transformationMatrix) {
            if (null == transformationMatrix) return;
            // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }

        public void handleTouchPress(float x,
                                     float y,
                                     float r,
                                     UndoRedo undoRedo) {

        }

        public Constants.DragStatus handleTouchDrag(TouchHelper.Drags drags,
                                                    float invertedViewProjectionMatrix,
                                                    UndoRedo undoRedo) {
            return Constants.DragStatus.NOTHING_DRAGGED;
        }

        public ArrayList<InteractivePoint> detectSelectedInteractivePoints(float x,
                                                                           float y,
                                                                           float r) {
            return new ArrayList<>();
        }

        public float getValueOnZ() { return 1f; }

        public ArrayList<InteractivePoint> getInteractivePoints() {
            return interactivePoints;
        }

        public ArrayList<Point> getVertices() {
            return vertices;
        }

        public Model getModel() { return null; }
        public void handleSelection() {}
        public void handleDoubleSelection() {}
        public void deleteBoundaryConditions(GeometrySet geomtry) {}
    }

    /*
    static public class VertexConditions {
        private PunctualForce punctualForce;
        private FixedDisplacement fixedDisplacement;
        boolean hasAppliedForce;
        boolean hasFixedDisplacement;

        private void initializeVariables() {
            punctualForce = null;
            fixedDisplacement = null;
            hasAppliedForce = false;
            hasFixedDisplacement = false;
        }

        public VertexConditions() {
            initializeVariables();
        }

        public void createForce(Point v) {
            if (!hasAppliedForce)
                punctualForce = new PunctualForce();
            punctualForce.setVertex(v);
            hasAppliedForce = true;
        }

        public void setDisplacement(FixedDisplacement displacement) {
            if (!hasFixedDisplacement)
                fixedDisplacement = new FixedDisplacement();
            fixedDisplacement.setDisplacement(displacement);
            hasFixedDisplacement = true;
        }

        public boolean hasAppliedForce() { return hasAppliedForce; }
        public boolean hasFixedDisplacement() { return hasFixedDisplacement; }
        public PunctualForce getForce() { return punctualForce; }
        public FixedDisplacement getDisplacement() { return fixedDisplacement; }
    }

    // I DO NOT KNOW WHY THIS CLASS HAS TO IMPLEMENT THAT INTERFACE!!!!!!!!!!!!!
    static public class AppliedConditions implements DataInterface {
        // To know the element with restrictions. It could be a node, line or surface
        private int id;

        private Force force;
        private FixedDisplacement fixedDisplacement;

        boolean hasAppliedForce;
        boolean hasFixedDisplacement;

        public AppliedConditions(int idNode) {
            this.id = idNode;

            force = null;
            fixedDisplacement = null;

            hasAppliedForce = false;
            hasFixedDisplacement = false;
        }

        public int getId() {
            return id;
        }
        public void setId(int idNode) {
            this.id = idNode;
        }

        public void setDepth(int depth) { }
        public int getDepth() { return 0; }

        public void copyForce(Force force){
            if (!hasAppliedForce) this.force = new Force();
            this.force.copyForce(force);
            hasAppliedForce = true;
        }

        public void copyFixedDisplacement(FixedDisplacement fixedDisplacement) {
            if (!hasFixedDisplacement) this.fixedDisplacement = new FixedDisplacement();
            this.fixedDisplacement.setDisplacement(fixedDisplacement);
            hasFixedDisplacement = true;
        }

        public Force getForce() { return force; }
        public FixedDisplacement getFixedDisplacement() { return fixedDisplacement; }

        public boolean hasAppliedForce() { return hasAppliedForce; }
        public boolean hasFixedDisplacement() { return hasFixedDisplacement; }

        public boolean writeToFile(DataOutputStream dos) {
            try {
                dos.writeBoolean(hasAppliedForce);
                if (hasAppliedForce)
                    if (!force.writeToFile(dos)) return false;
                dos.writeBoolean(hasFixedDisplacement);
                if (hasFixedDisplacement)
                    if (!fixedDisplacement.writeToFile(dos)) return false;
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis) {
            try {
                hasAppliedForce = dis.readBoolean();
                if (hasAppliedForce) {
                    force = new Force();
                    if (!force.readFromFile(dis)) return false;
                }

                hasFixedDisplacement = dis.readBoolean();
                if (hasFixedDisplacement) {
                    fixedDisplacement = new FixedDisplacement();
                    if (!fixedDisplacement.readFromFile(dis)) return false;
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }
    //*/
}
