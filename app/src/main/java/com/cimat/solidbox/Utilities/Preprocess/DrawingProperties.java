package com.cimat.solidbox.Utilities.Preprocess;

import com.cimat.solidbox.OpenGL.Utilities.Colors;
import com.cimat.solidbox.Utilities.Constants;

/**
 * Created by ernesto on 31/05/17.
 */

public class DrawingProperties {
    private Colors.ColorRGB color;
    private Colors.ColorRGB selectedColor;

    private boolean useTexture;
    private Constants.TextureName textureName;

    private void initializeVariables() {
        color = new Colors.ColorRGB();
        color.setRed(0.5f);
        color.setGreen(0.5f);
        color.setBlue(0.5f);
        selectedColor = new Colors.ColorRGB();
        selectedColor.setRed(0.7f);
        selectedColor.setGreen(0.7f);
        selectedColor.setBlue(0.7f);
        useTexture = false;
        textureName = Constants.TextureName.NONE;
    }

    public DrawingProperties() {
        initializeVariables();
    }

    public void setColor(float r, float g, float b) {
        color.setRed(r);
        color.setRed(g);
        color.setRed(b);
    }

    public Colors.ColorRGB getColor() {
        return color;
    }

    public void setSelectedColor(float r, float g, float b) {
        selectedColor.setRed(r);
        selectedColor.setGreen(g);
        selectedColor.setBlue(b);
    }

    public Colors.ColorRGB getSelectedColor() {
        return selectedColor;
    }

    public void setTexture(Constants.TextureName texture) {
        textureName = texture;
    }

    public Constants.TextureName getTexture() {
        return textureName;
    }

    public void useColor() {
        useTexture = false;
    }

    public void useTexture() {
        useTexture = true;
    }

    public boolean needsTexturing() {
        return useTexture;
    }
}
