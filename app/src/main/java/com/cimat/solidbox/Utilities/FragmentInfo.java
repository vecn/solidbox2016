package com.cimat.solidbox.Utilities;

/**
 * Created by ernesto on 14/10/16.
 */

public interface FragmentInfo {
    int getType();
}
