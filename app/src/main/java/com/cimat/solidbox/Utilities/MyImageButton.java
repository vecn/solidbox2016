package com.cimat.solidbox.Utilities;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by ernesto on 15/08/16.
 */

public class MyImageButton extends ImageButton {
    private int offStateImgId;
    private int onStateImgId;

    public MyImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        offStateImgId = -1;
        onStateImgId = -1;
    }

    public void setStateImages(int offStateImgId, int onStateImgId) {
        this.offStateImgId = offStateImgId;
        this.onStateImgId = onStateImgId;
    }

    public void hide() {
        setVisibility(View.GONE);
    }

    public void show() {
        setVisibility(View.VISIBLE);
    }

    public void select() {
        setImageResource(onStateImgId);
    }

    public void unSelect() {
        setImageResource(offStateImgId);
    }
}
