package com.cimat.solidbox.Utilities;

import com.cimat.solidbox.DataStructures.BinaryTreeNode;
import com.cimat.solidbox.Utilities.Preprocess.Conditions;
import com.cimat.solidbox.Utilities.Preprocess.Geometry;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Shape;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Rectangle;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.RegularPolygon;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.Circle;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Zoom;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.BooleanBuiltShape;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.InteractivePoint;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.LinearShape;
import com.cimat.solidbox.Utilities.Preprocess.Conditions.Force;
import com.cimat.solidbox.Utilities.Preprocess.Conditions.FixedDisplacement;
import com.cimat.solidbox.Utilities.Preprocess.GeometrySet;
import com.cimat.solidbox.Utilities.Constants.BooleanOperation;
import com.cimat.solidbox.Utilities.Constants.ShapeType;

import java.util.ArrayList;

import static android.opengl.Matrix.orthoM;

/**
 * Created by ernesto on 8/08/16.
 */

public class Commands {
    public static class conformDragInteractivePointCommand implements ICommand {
        private InteractivePoint ip;
        private float initialX, initialY;

        public conformDragInteractivePointCommand(float initialX, float initialY,
                                                  InteractivePoint ip) {
            this.ip = ip;
            this.initialX = initialX;
            this.initialY = initialY;
        }

        public void execute() {
        }

        public void unExecute() {
            Shape shape = ip.getParentShape();
            if (null != shape)
                shape.handleDragInteractivePoint(ip, initialX, initialY);
        }
    }

    public static class dragInteractivePointCommand {
        private InteractivePoint ip;
        float x, y;

        public dragInteractivePointCommand(float x, float y, InteractivePoint ip) {
            this.ip = ip;
            this.x = x;
            this.y = y;
        }

        public void execute() {
            Shape shape = ip.getParentShape();
            if (null != shape)
                shape.handleDragInteractivePoint(ip, x, y);

        }
    }

    public static class createRectangleCommand implements ICommand {
        private float x1, y1, x2, y2;
        private GeometrySet geometry;
        private Shape shape;

        public createRectangleCommand(float x1, float y1,
                                      float x2, float y2,
                                      GeometrySet geometry) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
            this.geometry = geometry;
            shape = null;
        }

        public void execute() {
            shape = new Rectangle(x1, y1, x2, y2);
            geometry.addShape(shape);
        }

        public void unExecute() {
            geometry.deleteShape(shape);
        }
    }

    /*
    public static class createRegularPolygonCommand implements ICommand {
        private int numberOfSides;
        private float centerX, centerY;
        private float apothemX, apothemY;
        private GeometrySet geometry;
        private Shape shape;

        public createRegularPolygonCommand(int numberOfSides,
                                           float centerX,
                                           float centerY,
                                           float apothemX,
                                           float apothemY,
                                           GeometrySet geometry) {
            this.numberOfSides = numberOfSides;
            this.centerX = centerX;
            this.centerY = centerY;
            this.apothemX = apothemX;
            this.apothemY = apothemY;
            this.geometry = geometry;
            shape = null;
        }

        public void execute() {
            shape = new RegularPolygon(numberOfSides,
                    centerX, centerY, apothemX, apothemY);
            geometry.addShape(shape);
        }

        public void unExecute() {
            geometry.deleteShape(shape);
        }
    }

    public static class createCircleCommand implements ICommand {
        private float centerX, centerY;
        private float radiusPointX, radiusPointY;
        private GeometrySet geometry;
        private Shape shape;

        public createCircleCommand(float centerX,
                                   float centerY,
                                   float radiusPointX,
                                   float radiusPointY,
                                   GeometrySet geometry) {
            this.centerX = centerX;
            this.centerY = centerY;
            this.radiusPointX = radiusPointX;
            this.radiusPointY = radiusPointY;
            this.geometry = geometry;
            shape = null;
        }

        public void execute() {
            shape = new Circle(centerX, centerY, radiusPointX, radiusPointY);
            geometry.addShape(shape);
        }

        public void unExecute() {
            geometry.deleteShape(shape);
        }
    }
    //*/

    /*
    public static class booleanOperationCommand implements ICommand {
        private Shape a, b;
        private BooleanOperation booleanOperation;
        private GeometrySet geometry;
        private Shape shape;
        private boolean wasCommandPerformed;

        public booleanOperationCommand(Shape a, Shape b,
                                       BooleanOperation booleanOperation,
                                       GeometrySet geometry) {
            this.a = a;
            this.b = b;
            this.booleanOperation = booleanOperation;
            this.geometry = geometry;
            wasCommandPerformed = false;
        }

        public void execute() {
            // New boolean built shape
            shape = new BooleanBuiltShape(a, b, booleanOperation);

            // The shapes are deleted from the geometry set so they
            // are not drawn anymore
            a.deleteBoundaryConditions(geometry);
            b.deleteBoundaryConditions(geometry);
            geometry.deleteShape(a);
            geometry.deleteShape(b);

            // Adds the new shape
            geometry.addShape(shape);

            wasCommandPerformed = true;
        }

        public void unExecute() {
            if (!wasCommandPerformed) return;
            geometry.deleteShape(shape);
            geometry.addShape(a);
            geometry.addShape(b);
        }
    }
    //*/

    public static class extrudeLinearShapeCommand implements ICommand {

        public extrudeLinearShapeCommand() {

        }

        public void execute() {

        }

        public void unExecute() {

        }
    }

    public static class splitLinearShapeCommand implements ICommand {

        public splitLinearShapeCommand() {

        }

        public void execute() {

        }

        public void unExecute() {

        }
    }

    public static class sendToBackCommand implements ICommand {
        private Shape shape;
        private ArrayList<Shape> depthList;
        private int initialDepth;
        private boolean wasCommandPerformed;

        public sendToBackCommand(Shape shape, ArrayList<Shape> depthList) {
            this.shape = shape;
            this.depthList = depthList;
            initialDepth = shape.getDepth();
            wasCommandPerformed = false;
        }

        public void execute() {
            if (null == shape) return;
            final int depth = shape.getDepth();
            Shape s;

            // Moves forward the previous shapes
            for (int i = depth; i > 0; i--) {
                s = depthList.get(i - 1);
                s.setDepth(i);
                depthList.set(i, s);
            }

            // Moves to the very back the shape
            shape.setDepth(0);
            depthList.set(0, shape);

            wasCommandPerformed = true;
        }

        public void unExecute() {
            if (!wasCommandPerformed) return;
            Shape s;

            // Moves one level back the shapes
            for (int i = 1; i <= initialDepth; i++) {
                s = depthList.get(i);
                s.setDepth(i - 1);
                depthList.set(i - 1, s);
            }

            // Moves the shape to its original position
            shape.setDepth(initialDepth);
            depthList.set(initialDepth, shape);
        }
    }

    public static class bringToFrontCommand implements ICommand {
        private Shape shape;
        private ArrayList<Shape> depthList;
        private int initialDepth;
        private boolean wasCommandPerformed;

        public bringToFrontCommand(Shape shape, ArrayList<Shape> depthList) {
            this.shape = shape;
            this.depthList = depthList;
            initialDepth = shape.getDepth();
            wasCommandPerformed = false;
        }

        public void execute() {
            if (null == shape) return;
            final int depth = shape.getDepth();
            Shape s;

            // Moves back the forward shapes
            for (int i = depth; i < depthList.size() - 1; i++) {
                s = depthList.get(i + 1);
                s.setDepth(i);
                depthList.set(i, s);
            }

            // Moves to the very front the shape
            shape.setDepth(depthList.size() - 1);
            depthList.set(depthList.size() - 1, shape);

            wasCommandPerformed = true;
        }

        public void unExecute() {
            if (!wasCommandPerformed) return;
            Shape s;

            // Moves forward the shapes
            for (int i = depthList.size() - 2; i >= initialDepth; i--) {
                s = depthList.get(i);
                s.setDepth(i + 1);
                depthList.set(i + 1, s);
            }

            // Moves the shape to its initial position
            shape.setDepth(initialDepth);
            depthList.set(initialDepth, shape);
        }
    }

    public static class sendToBackOneLevelCommand implements ICommand {
        private Shape shape;
        private ArrayList<Shape> depthList;
        private boolean wasCommandPerformed;

        public sendToBackOneLevelCommand(Shape shape, ArrayList<Shape> depthList) {
            this.shape = shape;
            this.depthList = depthList;
            wasCommandPerformed = false;
        }

        public void execute() {
            if (null == shape) return;
            final int depth = shape.getDepth();
            if (depth > 0) {
                Tools.switchElements(depth, depth - 1, depthList);
                depthList.get(depth).setDepth(depth);
                depthList.get(depth - 1).setDepth(depth - 1);
                wasCommandPerformed = true;
            }
        }

        public void unExecute() {
            final int depth = shape.getDepth();
            if (wasCommandPerformed) {
                Tools.switchElements(depth, depth - 1, depthList);
                depthList.get(depth).setDepth(depth);
                depthList.get(depth - 1).setDepth(depth - 1);
            }
        }
    }

    public static class bringToFrontOneLevelCommand implements ICommand {
        private Shape shape;
        private ArrayList<Shape> depthList;
        private boolean wasCommandPerformed;

        public bringToFrontOneLevelCommand(Shape shape, ArrayList<Shape> depthList) {
            this.shape = shape;
            this.depthList = depthList;
            wasCommandPerformed = false;
        }

        public void execute() {
            if (null == shape) return;
            final int depth = shape.getDepth();
            if (depth < depthList.size() - 1) {
                Tools.switchElements(depth, depth + 1, depthList);
                depthList.get(depth).setDepth(depth);
                depthList.get(depth + 1).setDepth(depth + 1);
                wasCommandPerformed = true;
            }
        }

        public void unExecute() {
            final int depth = shape.getDepth();
            if (wasCommandPerformed) {
                Tools.switchElements(depth, depth + 1, depthList);
                depthList.get(depth).setDepth(depth);
                depthList.get(depth + 1).setDepth(depth + 1);
            }
        }
    }

    public static class ZoomCommand {
        Zoom zoom;

        public ZoomCommand() {
            zoom = new Zoom();
        }

        public void setData(float initialScaleFactor,
                            float initialRadiusInNDC,
                            Point zoomingCenterInNDC,
                            Point zoomingCenterInWSC,
                            Point centersViewInWSC)  {
            zoom.setData(initialScaleFactor, initialRadiusInNDC,
                    zoomingCenterInNDC, zoomingCenterInWSC, centersViewInWSC);
        }

        public void execute(Point a,
                            Point b,
                            PlotParameters plotParameters,
                            float[] projectionMatrix) {
            float scaleFactor = zoom.getScaleFactorFromPointsInNDC(a, b);

            plotParameters.setCenter(
                    zoom.getZoomingCenterInWSC().x - zoom.getDisplacementToCentersViewOnX(),
                    zoom.getZoomingCenterInWSC().y - zoom.getDisplacementToCentersViewOnY(),
                    plotParameters.getCenterZ()
            );
            plotParameters.setZoomFactor(scaleFactor);

            // We update the projection matrix
            orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                    plotParameters.getMinY(), plotParameters.getMaxY(),
                    plotParameters.getMinZ(), plotParameters.getMaxZ());
        }

        public Zoom getZoom() { return zoom; }
    }

    public static class ZoomConformCommand implements ICommand {
        private float initialScaleFactor;
        private Zoom zoom;
        private PlotParameters plotParameters;
        private float[] projectionMatrix;

        public ZoomConformCommand(float initialScaleFactor,
                                  Zoom zoom,
                                  PlotParameters plotParameters,
                                  float[] projectionMatrix) {
            this.initialScaleFactor = initialScaleFactor;
            this.zoom = zoom;
            this.plotParameters = plotParameters;
            this.projectionMatrix = projectionMatrix;
        }

        public void execute() {
        }

        public void unExecute() {
            plotParameters.setCenter(
                    zoom.getZoomingCenterInWSC().x - zoom.getDisplacementToCentersViewOnX(),
                    zoom.getZoomingCenterInWSC().y - zoom.getDisplacementToCentersViewOnY(),
                    plotParameters.getCenterZ()
            );
            plotParameters.setZoomFactor(initialScaleFactor);
            // We update the projection matrix
            orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                   plotParameters.getMinY(), plotParameters.getMaxY(),
                   plotParameters.getMinZ(), plotParameters.getMaxZ());
        }
    }

    public static class panCommand {
        PlotParameters plotParameters;
        float[] projectionMatrix;

        public panCommand(PlotParameters plotParameters,
                          float[] projectionMatrix) {
            this.plotParameters = plotParameters;
            this.projectionMatrix = projectionMatrix;
        }

        public void execute(float oldX, float oldY,
                            float newX, float newY) {
            float displacementOnX, displacementOnY;

            // Displacements from the start dragging point
            displacementOnX = newX - oldX;
            displacementOnY = newY - oldY;

            plotParameters.setCenter(plotParameters.getCenterX() - displacementOnX,
                    plotParameters.getCenterY() - displacementOnY, plotParameters.getCenterZ());

            orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                    plotParameters.getMinY(), plotParameters.getMaxY(),
                    plotParameters.getMinZ(), plotParameters.getMaxZ());
        }
    }

    public static class panConformCommand implements ICommand {
        float initialX, initialY, finalX, finalY;
        PlotParameters plotParameters;
        float[] projectionMatrix;

        public panConformCommand(float initialX, float initialY,
                                 float finalX, float finalY,
                                 PlotParameters plotParameters,
                                 float[] projectionMatrix) {
            this.initialX = initialX;
            this.initialY = initialY;
            this.finalX = finalX;
            this.finalY = finalY;
            this.plotParameters = plotParameters;
            this.projectionMatrix = projectionMatrix;
        }

        public void execute() {
        }

        public void unExecute() {
            float displacementOnX, displacementOnY;

            // Displacements from the start dragging point
            displacementOnX = initialX - finalX;
            displacementOnY = initialY - finalY;

            plotParameters.setCenter(plotParameters.getCenterX() - displacementOnX,
                    plotParameters.getCenterY() - displacementOnY, plotParameters.getCenterZ());

            orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                    plotParameters.getMinY(), plotParameters.getMaxY(),
                    plotParameters.getMinZ(), plotParameters.getMaxZ());
        }
    }

    /*
    public static class splitBooleanBuiltShapeCommand implements ICommand {
        private Shape booleanBuiltShape;
        private GeometrySet geometry;

        public splitBooleanBuiltShapeCommand(Shape booleanBuiltShape,
                                             GeometrySet geometry) {
            this.booleanBuiltShape = booleanBuiltShape;
            this.geometry = geometry;
        }

        public void execute() {
            if (null == booleanBuiltShape) return;
            Shape shape;

            // Removes the boundary conditions
            booleanBuiltShape.deleteBoundaryConditions(geometry);
            // Removes the boolean built shape from the geometry set
            geometry.deleteShape(booleanBuiltShape);

            shape = booleanBuiltShape.getLeftSonShape();
            if (null != shape) {
                // Pass the transformations
                shape.applyTransformationMatrix(booleanBuiltShape.getTransformationMatrix());
                geometry.addShape(shape);
            }

            shape = booleanBuiltShape.getRightSonShape();
            if (null != shape) {
                // Pass the transformations
                shape.applyTransformationMatrix(booleanBuiltShape.getTransformationMatrix());
                geometry.addShape(shape);
            }
        }

        public void unExecute() {
            if (null == booleanBuiltShape) return;
            Shape shape;

            shape = booleanBuiltShape.getLeftSonShape();
            if (null != shape) geometry.deleteShape(shape);

            shape = booleanBuiltShape.getRightSonShape();
            if (null != shape) geometry.deleteShape(shape);

            geometry.addShape(booleanBuiltShape);
        }
    }

    public static class applyForceOnElement implements ICommand{
        private Vertex vertex;
        private LineSegment lineSegment;
        private LinearShape linearShape;
        private Force force;
        private ShapeType shapeType;
        private GeometrySet geometry;

        private void initializeVariables() {
            vertex = null;
            lineSegment = null;
            linearShape = null;
            force = null;
            shapeType = ShapeType.INVALID_VALUE;
            geometry = null;
        }

        public applyForceOnElement(Vertex vertex,
                                   Force force,
                                   GeometrySet geometry) {
            initializeVariables();
            this.vertex = vertex;
            this.force = force;
            this.geometry = geometry;
            shapeType = vertex.getShapeType();
        }

        public applyForceOnElement(LineSegment lineSegment,
                                   Force force,
                                   GeometrySet geometry) {
            initializeVariables();
            this.lineSegment = lineSegment;
            this.force = force;
            this.geometry = geometry;
            shapeType = lineSegment.getShapeType();
        }

        public applyForceOnElement(LinearShape linearShape,
                                   Force force,
                                   GeometrySet geometry) {
            initializeVariables();
            this.linearShape = linearShape;
            this.force = force;
            this.geometry = geometry;
            shapeType = linearShape.getShapeType();
        }

        public void execute() {
            switch (shapeType) {
                case POINT:
                    vertex.setForce(force.getMagnitude());
                    geometry.addShape(vertex.getForce());
                    break;
                case LINEAR:
                    // The force could be applied over a
                    // line segment or over an entire linear shape
                    if (null != lineSegment) {
                        lineSegment.setUniformDistributedForce(
                                force.getMagnitude(),
                                1f // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        );
                        geometry.addShape(lineSegment.getUniformDistributedForce());
                    }
                    if (null != linearShape) {
                        linearShape.setUniformDistributedForce(
                                force.getMagnitude(),
                                1f // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        );
                        geometry.addShape(linearShape.getUniformForce());
                    }
                    break;
            }
        }

        public void unExecute() {
            switch (shapeType) {
                case POINT:
                    geometry.deleteShape(vertex.getForce());
                    vertex.clearForce();
                    break;

                case LINEAR:
                    if (null != lineSegment) {
                        geometry.deleteShape(lineSegment.getUniformDistributedForce());
                        lineSegment.clearForce();
                    }
                    if (null != linearShape) {
                        geometry.deleteShape(linearShape.getUniformForce());
                        linearShape.clearForce();
                    }
                    break;
            }
        }
    }

    public static class applyDisplacementOnElement implements ICommand{
        private Vertex vertex;
        private LineSegment lineSegment;
        private LinearShape linearShape;
        private FixedDisplacement fixedDisplacement;
        private ShapeType shapeType;
        private GeometrySet geometry;

        private void initializeVariables() {
            vertex = null;
            lineSegment = null;
            linearShape = null;
            fixedDisplacement = null;
            shapeType = ShapeType.INVALID_VALUE;
            geometry = null;
        }

        public applyDisplacementOnElement(Vertex vertex,
                                          FixedDisplacement fixedDisplacement,
                                          GeometrySet geometry) {
            initializeVariables();
            this.vertex = vertex;
            this.fixedDisplacement = fixedDisplacement;
            this.geometry = geometry;
            shapeType = vertex.getShapeType();
        }

        public applyDisplacementOnElement(LineSegment lineSegment,
                                          FixedDisplacement fixedDisplacement,
                                          GeometrySet geometry) {
            initializeVariables();
            this.lineSegment = lineSegment;
            this.fixedDisplacement = fixedDisplacement;
            this.geometry = geometry;
            shapeType = lineSegment.getShapeType();
        }

        public applyDisplacementOnElement(LinearShape linearShape,
                                          FixedDisplacement fixedDisplacement,
                                          GeometrySet geometry) {
            initializeVariables();
            this.linearShape = linearShape;
            this.fixedDisplacement = fixedDisplacement;
            this.geometry = geometry;
            shapeType = linearShape.getShapeType();
        }

        public void execute() {
            switch (shapeType) {
                case POINT:
                    vertex.setFixedDisplacement(fixedDisplacement);
                    geometry.addShape(vertex.getFixedDisplacement());
                    break;
                case LINEAR:
                    if (null != lineSegment) {
                        lineSegment.setFixedDisplacement(fixedDisplacement);
                        lineSegment.getLinearFixedDisplacement().build();
                        geometry.addShape(lineSegment.getLinearFixedDisplacement());
                    }
                    if (null != linearShape) {
                        linearShape.setFixedDisplacement(fixedDisplacement);
                        linearShape.getFixedDisplacement().build();
                        geometry.addShape(linearShape.getFixedDisplacement());
                    }
                    break;
            }
        }

        public void unExecute() {
            switch (shapeType) {
                case POINT:
                    geometry.deleteShape(vertex.getFixedDisplacement());
                    vertex.clearFixedDisplacement();
                    break;

                case LINEAR:
                    if (null != lineSegment) {
                        geometry.deleteShape(lineSegment.getUniformDistributedForce());
                        lineSegment.clearFixedDisplacement();
                    }
                    if (null != linearShape) {
                        geometry.deleteShape(linearShape.getUniformForce());
                        linearShape.clearFixedDisplacement();
                    }
                    break;
            }
        }
    }
    //*/

    /*
    public static class applyColorToElement implements ICommand {
        private Geometry.SurfaceShape element;
        private float[] previous_color;
        private float[] current_color;

        private void initializeVariables() {
            element = null;
            previous_color = null;
            current_color = new float[3];

            current_color[0] = current_color[1] = current_color[2] = 0f;
        }

        applyColorToElement(float r, float g, float b,
                            Geometry.SurfaceShape surface) {
            initializeVariables();
            if (null == surface) return;
            element = surface;
            previous_color = surface.getColor();

            current_color[0] = r;
            current_color[1] = g;
            current_color[2] = b;
        }

        public void execute() {
            if (null == element) return;

            element.setColor(
                    current_color[0],
                    current_color[1],
                    current_color[2]
            );
        }

        public void unExecute() {
            if (null == element) return;
            if (null == previous_color) return;

            element.setColor(
                    previous_color[0],
                    previous_color[1],
                    previous_color[3]
            );
        }
    }
    //*/

    public static class deleteShape implements ICommand {
        private GeometrySet geometry;
        private Shape shape;
        private ICommand dragIP;

        private void initializeVariables() {
            geometry = null;
            shape = null;
            dragIP = null;
        }

        deleteShape(Shape shape,
                    GeometrySet geometry,
                    float initialX,
                    float initialY,
                    InteractivePoint ip) {
            initializeVariables();
            this.shape = shape;
            this.geometry = geometry;
            dragIP = new conformDragInteractivePointCommand(initialX, initialY, ip);
        }

        public void execute() {
            if (null == geometry) return;
            if (null == shape) return;

            //shape.deleteBoundaryConditions(geometry); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            geometry.deleteShape(shape);
        }

        public void unExecute() {
            if (null == geometry) return;
            if (null == shape) return;

            geometry.addShape(shape);
            dragIP.unExecute();
        }
    }
}
