package com.cimat.solidbox.Utilities.Preprocess;

import android.util.Log;

import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.LoggerConfig;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import nb.geometricBot.Model;
import nb.geometricBot.ModelStatus;

import static nb.geometricBot.ModelStatus.OK;

/**
 * Created by ernesto on 6/1/16.
 */
public class MyModel extends Model {

    private boolean isAValidModel;

    public MyModel() {
        super();
        isAValidModel = false;
    }

    public void printModel(String name, String TAG) {

        Log.i(TAG, name);
        Log.i(TAG, "--> Vertices");
        for (int i = 0; i < getNVertices(); i++) {
            Log.i(TAG, "" + getVerticesRef()[i*2] + "  " + getVerticesRef()[i*2+1] );
        }
        Log.i(TAG, "--> Connectivity");
        for (int i = 0; i < getNEdges(); i++) {
            Log.i(TAG, "" + getEdgesRef()[i*2] + "  " + getEdgesRef()[i*2+1] );
        }

    }

    public boolean isAValidModel() { return isAValidModel; }

    public void create(ArrayList<Point> boundary,
                       ArrayList<ArrayList<Point>> holeBoundaries) {
        isAValidModel = false;

        if (null == boundary) return;
        if (0 == boundary.size()) return;

        // Number of edges and vertices
        int nEdges, nBoundaryVertices, nHoleVertices;
        nEdges = boundary.size();
        nBoundaryVertices = boundary.size();
        nHoleVertices = 0;
        for (ArrayList<Point> hole : holeBoundaries) {
            nEdges += hole.size();
            nHoleVertices += hole.size();
        }

        edges = new int[(nEdges + nHoleVertices)*2];
        vertices = new float[nBoundaryVertices*2];
        holes = new float[nHoleVertices*2];

        // Sets the vertices coordinates
        int i = 0;
        for (Point v : boundary) {
            vertices[i*2] = v.x;
            vertices[i*2+1] = v.y;
            i++;
        }
        i = 0;
        for (ArrayList<Point> hole : holeBoundaries) {
            for (Point v : hole) {
                holes[i*2] = v.x;
                holes[i*2+1] = v.y;
                i++;
            }
        }

        // Sets the edges
        for (i = 0; i < nBoundaryVertices - 1; i++) {
            edges[2*i] = i;
            edges[2*i+1] = i + 1;
        }
        edges[2*i] = i;
        edges[2*i+1] = 0;
        i++;
        // I'm not sure about how the hole vertices are labeled, I have to ask it to Victor!!!
        for (ArrayList<Point> hole : holeBoundaries) {
            int start = i;
            for (int j = 0; j < hole.size() - 1; j++) {
                edges[2*i] = i;
                edges[2*i+1] = i + 1;
            }
            edges[2*i] = i;
            edges[2*i+1] = start;
            i++;
        }

        if (LoggerConfig.ON)
        printModel("Surface", "Model before check");

        isAValidModel = checkModel();

        if (LoggerConfig.ON)
        printModel("MODEL", "Model after check");
    }

    private boolean checkModel() {
        Boolean isValid = false;
        ModelStatus modelStatus = verify();

        if (LoggerConfig.ON)
            Log.i("ModelHelper", "Model status = " + modelStatus);

        if (modelStatus == OK) {
            isValid = true;
        } else {
            switch (modelStatus) {
                case REPEATED_VERTICES:
                    // Collapse model
                    break;
            }
            // Check again
            modelStatus = verify();
            if (OK == modelStatus) isValid = true;
        }

        return isValid;
    }

    private static void collapseModel(ArrayList<Point> newVertices,
                                     ArrayList<Point> newLines,
                                     ArrayList<Point> vertices,
                                     ArrayList<Point> lines) {

        /*
        float minDistance = Constants.SMALLEST_DISTANCE;
        // To store the group each vertex belongs to
        java.util.Vector<Integer> groups = new java.util.Vector();
        // To store the center of each group
        java.util.Vector<Point> groupCenters = new java.util.Vector();

        // The minDistance has to be in accordance to the maximum zoom in value and also
        // has to be changed until the model is valid. In this case we are only testing with
        // one minDistance value.
        identifyGroupOfPoints(minDistance, groups, groupCenters, vertices);

        // New set of points without "repeated" points
        Iterator<Point> pointIterator = groupCenters.iterator();
        Point point;
        int count = 0;
        while (pointIterator.hasNext()) {
            point = pointIterator.next();
            newVertices.add(new Vertex(point.getX(), point.getY(), point.getZ(), count));
            count++;
        }

        // New set of lines
        Iterator<Point> lineIterator = lines.iterator();
        Point line;
        int g1, g2;
        while (lineIterator.hasNext()) {
            line = lineIterator.next();
            g1 = groups.get(line.getFirstVertex().getId());
            g2 = groups.get(line.getLastVertex().getId());
            if (g1 != g2)
                newLines.add(new Point(newVertices.get(g1), newVertices.get(g2)));
        }
        //*/
        // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        /*
        Log.i("NEW MODEL", "New points size = " + newPoints.size());
        for (int i = 0; i < newPoints.size(); i++) {
            Log.i("NEW MODEL", "" + newPoints.get(i).getId() + "   " + newPoints.get(i).getX() + "   " + newPoints.get(i).getY() + "   " + newPoints.get(i).getZ());
        }

        Log.i("NEW MODEL", "New lines size = " + newLines.size());
        for (int i = 0; i < newLines.size(); i++) {
            Log.i("NEW MODEL", "" + newLines.get(i).getFirstPoint().getId() + "   " + newLines.get(i).getLastPoint().getId());
        }
        */
    }

    private static void identifyGroupOfPoints(final float minDistance,
                                              java.util.Vector<Integer> groups,
                                              java.util.Vector<Point> groupCenters,
                                              ArrayList<Point> vertices) {
        /*
        int currentGroup;
        float distance;
        Geometry.SimplePoint center = new Geometry.SimplePoint();
        Geometry.SimplePoint simplePoint = new Geometry.SimplePoint();
        List<Point> listPoints = new LinkedList();
        ListIterator<Point> iterator;
        Point point;

        // Initialize some variables
        groups.setSize(vertices.size());
        for (int i = 0; i < vertices.size(); i++) {
            point = vertices.get(i);
            listPoints.add(point);
            groups.set(point.getId(), -1);
        }

        // Find the vertices very close to each other and make groups. Each vertex will belong
        // to a group so may be some groups with one vertex
        currentGroup = 0;
        while (listPoints.size() > 0) {
            iterator = listPoints.listIterator();
            point = iterator.next();

            // A new group
            groupCenters.add(point);
            center.setCoordinates(point.getX(), point.getY(), point.getZ());
            // The first group's point
            groups.set(point.getId(), currentGroup);

            // Adds the vertices close enough to the center of this group
            iterator.remove();
            while (iterator.hasNext()){
                point = iterator.next();
                simplePoint.setCoordinates(point.getX(), point.getY(), point.getZ());
                distance = Geometry.distanceBetweenPoints(center, simplePoint);
                if (distance < minDistance) {
                    groups.set(point.getId(), currentGroup);
                    iterator.remove();
                }
            }

            currentGroup++;
        }
        //*/ // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }

    private void collapseBoundaryConditions(java.util.Vector<Integer> groups) {

    }


}
