package com.cimat.solidbox.Utilities;

import com.cimat.solidbox.Utilities.Preprocess.Conditions;
import com.cimat.solidbox.Utilities.Preprocess.Conditions.Force;
import com.cimat.solidbox.Utilities.Preprocess.Conditions.FixedDisplacement;
import com.cimat.solidbox.Utilities.Preprocess.Geometry;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.InteractivePoint;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Shape;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Zoom;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.Vertex;
//import com.cimat.solidbox.Utilities.Preprocess.Geometry.LineSegment;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.LinearShape;
import com.cimat.solidbox.Utilities.Preprocess.GeometrySet;
import com.cimat.solidbox.Utilities.Constants.BooleanOperation;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by ernesto on 11/08/16.
 */

public class UndoRedo {
    private final int MAX_UNDO_LEVELS = 10;
    Stack<ICommand> undo;
    Stack<ICommand> redo;

    private void initializeVariables() {
        undo = new Stack();
        redo = new Stack();
    }

    public UndoRedo() {
        initializeVariables();
    }

    public void undo(int levels) {
        ICommand iCommand;

        if (levels < 0) return;
        for (int i = 0; i < levels; i++) {
            if (undo.isEmpty()) return;
            iCommand = undo.pop();
            redo.push(iCommand);
            iCommand.unExecute();
        }
    }

    public void redo(int levels) {
        ICommand iCommand;

        if (levels < 0) return;
        for (int i = 0; i < levels; i++) {
            if (redo.isEmpty()) return;
            iCommand = redo.pop();
            undo.push(iCommand);
            iCommand.execute();
        }
    }

    public void insertInUndoRedoForConformDragInteractivePoint(
            float initialX, float initialY, InteractivePoint ip) {
        ICommand cmd = new Commands.conformDragInteractivePointCommand(
                initialX, initialY, ip);
        undo.push(cmd);
        redo.clear();
    }

    public void insertInUndoRedoForCreateRectangle(float x1, float y1,
                                                   float x2, float y2,
                                                   GeometrySet geometry) {
        ICommand cmd = new Commands.createRectangleCommand(x1, y1, x2, y2, geometry);
        cmd.execute();
        undo.push(cmd);
        redo.clear();
    }

    public void insertInUndoRedoForCreateRegularPolygon(int numberOfSides,
                                                        float centerX,
                                                        float centerY,
                                                        float apothemX,
                                                        float apothemY,
                                                        GeometrySet geometry) {
        /*
        ICommand cmd = new Commands.createRegularPolygonCommand(numberOfSides,
                centerX, centerY, apothemX, apothemY, geometry);
        cmd.execute();
        if (undo.size() <= MAX_UNDO_LEVELS) undo.push(cmd);
        redo.clear();
        //*/
    }

    public void insertInUndoRedoForCreateCircle(float centerX,
                                              float centerY,
                                              float radiusPointX,
                                              float radiusPointY,
                                              GeometrySet geometry) {
        /*
        ICommand cmd = new Commands.createCircleCommand(centerX, centerY,
                radiusPointX, radiusPointY, geometry);
        cmd.execute();
        undo.push(cmd);
        redo.clear();
        //*/
    }

    public void insertInUndoRedoForBooleanOperation(Shape a,
                                                  Shape b,
                                                  BooleanOperation booleanOperation,
                                                  GeometrySet geometry) {
        if (a == b) return;
        /*
        ICommand cmd = new Commands.booleanOperationCommand(a, b, booleanOperation,
                geometry);
        cmd.execute();
        undo.push(cmd);
        redo.clear();
        //*/
    }

    public void insertInUndoRedoForExtrudeLinearShape() {
        // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ICommand cmd = new Commands.extrudeLinearShapeCommand();
        undo.push(cmd);
        redo.clear();
    }

    public void insertInUndoRedoForSplitLinearShape() {
        // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ICommand cmd = new Commands.splitLinearShapeCommand();
        undo.push(cmd);
        redo.clear();
    }

    public void insertInUndoRedoForSendToBack(Shape shape,
                                            ArrayList<Shape> depthList) {
        ICommand cmd = new Commands.sendToBackCommand(shape, depthList);
        undo.push(cmd);
        redo.clear();
    }

    public void insertInUndoRedoForBringToFront(Shape shape,
                                              ArrayList<Shape> depthList) {
        ICommand cmd = new Commands.bringToFrontCommand(shape, depthList);
        undo.push(cmd);
        redo.clear();
    }

    public void insertInUndoRedoForSendToBAckOneLevel(Shape shape,
                                                    ArrayList<Shape> depthList) {
        ICommand cmd = new Commands.sendToBackOneLevelCommand(shape, depthList);
        undo.push(cmd);
        redo.clear();
    }

    public void insertInUndoRedoForBringToFrontOneLevel(Shape shape,
                                                      ArrayList<Shape> depthList) {
        ICommand cmd = new Commands.bringToFrontOneLevelCommand(shape, depthList);
        undo.push(cmd);
        redo.clear();
    }

    public void insertInUndoRedoForZoomConform(float initialScaleFactor,
                                             Zoom zoom,
                                             PlotParameters plotParameters,
                                             float[] projectionMatrix) {
        ICommand cmd = new Commands.ZoomConformCommand(initialScaleFactor,
                zoom, plotParameters, projectionMatrix);
        undo.push(cmd);
        redo.clear();
    }

    public void insertInUndoRedoForPanConform(float initialX, float initialY,
                                              float finalX, float finalY,
                                              PlotParameters plotParameters,
                                              float[] projectionMatrix) {
        ICommand cmd = new Commands.panConformCommand(initialX, initialY,
                finalX, finalY, plotParameters, projectionMatrix);
        undo.push(cmd);
        redo.clear();
    }

    public void insertInUndoRedoForSplitBooleanBuiltShape(Shape booleanBuiltShape,
                                                          GeometrySet geometry) {
        /*
        ICommand cmd = new Commands.splitBooleanBuiltShapeCommand(
                booleanBuiltShape, geometry);
        cmd.execute();
        undo.push(cmd);
        redo.clear();
        //*/
    }

    /*
    public void insertInUndoRedoForApplyForceOnElement(Vertex vertex,
                                                       Force force,
                                                       GeometrySet geometry) {
        ICommand cmd =
                new Commands.applyForceOnElement(vertex, force, geometry);
        cmd.execute();
        undo.push(cmd);
        redo.clear();
    }
    //*/

    /*
    public void insertInUndoRedoForApplyForceOnElement(LineSegment lineSegment,
                                                       Force force,
                                                       GeometrySet geometry) {
        ICommand cmd =
                new Commands.applyForceOnElement(lineSegment, force, geometry);
        cmd.execute();
        undo.push(cmd);
        redo.clear();
    }
    //*/

    public void insertInUndoRedoForApplyForceOnElement(LinearShape linearShape,
                                                       Force force,
                                                       GeometrySet geometry) {
        /*
        ICommand cmd =
                new Commands.applyForceOnElement(linearShape, force, geometry);
        cmd.execute();
        undo.push(cmd);
        redo.clear();
        //*/
    }

    /*
    public void insertInUndoRedoForApplyDisplacementOnElement(Vertex vertex,
                                                              FixedDisplacement displ,
                                                              GeometrySet geometry) {
        ICommand cmd =
                new Commands.applyDisplacementOnElement(vertex, displ, geometry);
        cmd.execute();
        undo.push(cmd);
        redo.clear();
    }
    //*/

    /*
    public void insertInUndoRedoForApplyDisplacementOnElement(LineSegment lineSegment,
                                                              FixedDisplacement displ,
                                                              GeometrySet geometry) {
        ICommand cmd =
                new Commands.applyDisplacementOnElement(lineSegment, displ, geometry);
        cmd.execute();
        undo.push(cmd);
        redo.clear();
    }
    //*/

    public void insertInUndoRedoForApplyDisplacementOnElement(LinearShape linearShape,
                                                              FixedDisplacement displ,
                                                              GeometrySet geometry) {
        /*
        ICommand cmd =
                new Commands.applyDisplacementOnElement(linearShape, displ, geometry);
        cmd.execute();
        undo.push(cmd);
        redo.clear();
        //*/
    }

    public void insertInUndoRedoForApplyColorToElement(float r,
                                                       float g,
                                                       float b,
                                                       Geometry.SurfaceShape surface) {
        /*
        ICommand cmd =
                new Commands.applyColorToElement(r, g, b, surface);
        cmd.execute();
        undo.push(cmd);
        redo.clear();
        //*/
    }

    public void insertInUndoRedoForDeleteShape(Shape shape,
                                               GeometrySet geometry,
                                               float initialX,
                                               float initialY,
                                               InteractivePoint ip) {
        ICommand cmd =
                new Commands.deleteShape(shape, geometry, initialX, initialY, ip);
        cmd.execute();
        undo.push(cmd);
        redo.clear();
    }
}
