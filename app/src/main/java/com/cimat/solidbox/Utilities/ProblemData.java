package com.cimat.solidbox.Utilities;

import com.cimat.solidbox.Utilities.Preprocess.Geometry;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by ernesto on 24/02/16.
 */
public class ProblemData {
    private int typeOfProblem;
    private int numberOfMeshNodes;
    private int typeOfModel;

    public class Plate {
        private float thickness;

        Plate() {
            thickness = 0.2f;
        }

        public void setThickness(float thickness){
            this.thickness = thickness;
        }

        public float getThickness(){
            return thickness;
        }

        public boolean writeToFile(DataOutputStream dos) {
            try {
                dos.writeFloat(thickness);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis) {
            try {
                thickness = dis.readFloat();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    public class SolidOfRevolution {
        private Geometry.Ray axis;
        private int numberOfSlices;

        SolidOfRevolution() {
            axis = new Geometry.Ray(new Geometry.Point(),
                    new Geometry.Vector());
            numberOfSlices = 20;
        }

        public void setAxis(Geometry.Ray axis){
            this.axis.point.setCoordinates(axis.point);
            this.axis.vector.setComponents(axis.vector);
        }

        public void clearAxis(){
            axis.point.setCoordinates(0f, 0f, 0f);
            axis.vector.setComponents(0f, 0f, 0f);
        }

        public Geometry.Ray getAxis() { return axis; }

        public void setNumberOfSlices(int numberOfSlices) {
            this.numberOfSlices = numberOfSlices;
        }

        public int getNumberOfSlices() {
            return numberOfSlices;
        }

        /*
        public boolean writeToFile(DataOutputStream dos) {
            try {
                if (null != axis) dos.writeInt(axis.getId());
                else              dos.writeInt(Constants.INVALID_ID);
                dos.writeInt(numberOfSlices);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis, Lines lines) {
            int id;
            try {
                id = dis.readInt();
                numberOfSlices = dis.readInt();
                if (Constants.INVALID_ID != id) axis = lines.getElementById(id);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        //*/
    }

    public class Material {
        private float youngsModulus; // Kilo-Pascals KPa
        private float poissonsRatio;
        private Constants.MaterialType materialType;

        private void initializeVariables() {
            youngsModulus = 0f;
            poissonsRatio = 0f;
            materialType = Constants.MaterialType.GENERAL;

            // Default material
            setIronProperties();
        }

        Material() {
            initializeVariables();
        }

        public void setIronProperties() {
            materialType = Constants.MaterialType.IRON;
            youngsModulus = 211e6f;
            poissonsRatio = 0.3f;
        }

        public void setSteelProperties() {
            materialType = Constants.MaterialType.STEEL;
            youngsModulus = 200e6f;
            poissonsRatio = 0.3f;
        }

        public void setAluminumProperties() {
            materialType = Constants.MaterialType.ALUMINUM;
            youngsModulus = 69e6f;
            poissonsRatio = 0.334f;
        }

        public void setYoungsModulus(float youngsModulus) {
            this.youngsModulus = youngsModulus;
        }

        public void setPoissonsRatio(float poissonsRatio) {
            this.poissonsRatio = poissonsRatio;
        }

        public float getYoungsModulus() {
            return youngsModulus;
        }

        public float getPoissonsRatio() {
            return poissonsRatio;
        }
    }

    public Plate plate;
    public SolidOfRevolution solidOfRevolution;
    public Material material;

    public ProblemData() {
        typeOfProblem = Constants.PLANE_STRESS;
        typeOfModel = Constants.ELASTIC_PROBLEM;
        numberOfMeshNodes = 50;
        plate = new Plate();
        solidOfRevolution = new SolidOfRevolution();
        material = new Material();
    }

    public void setProblemType(int type) {
        typeOfProblem = type;
    }
    public int getTypeOfProblem() {
        return typeOfProblem;
    }
    public int getNumberOfMeshNodes() {
        return numberOfMeshNodes;
    }
    public int setMeshSize(int numberOfMeshNodes) {
        return this.numberOfMeshNodes = numberOfMeshNodes;
    }
    public void setModelType(int modelType){ typeOfModel = modelType;}
    public int getTypeOfModel() { return typeOfModel; }


    /*
    public boolean writeToFile(DataOutputStream dos) {
        try {
            dos.writeInt(typeOfProblem);
            dos.writeInt(numberOfMeshNodes);

            Log.i("READING", "Type of problem before = " + typeOfProblem);
            Log.i("READING", "Number of mesh nodes before = " + numberOfMeshNodes);

            if (!plate.writeToFile(dos)) return false;
            if (!solidOfRevolution.writeToFile(dos)) return false;

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean readFromFile(DataInputStream dis, GeometryData geometry) {
        try {
            typeOfProblem = dis.readInt();
            numberOfMeshNodes = dis.readInt();

            Log.i("READING", "Type of problem = " + typeOfProblem);
            Log.i("READING", "Number of mesh nodes = " + numberOfMeshNodes);

            if (!plate.readFromFile(dis)) return false;
            if (!solidOfRevolution.readFromFile(dis, geometry.getLines())) return false;

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    //*/
}
