package com.cimat.solidbox.Utilities;

import android.util.Log;

import com.cimat.solidbox.Utilities.Preprocess.Geometry;

import nb.pdeBot.finiteElement.MeshResults;

import static com.cimat.solidbox.Utilities.Tools.largestMagnitude;
import static com.cimat.solidbox.Utilities.Tools.largestValue;
import static com.cimat.solidbox.Utilities.Tools.smallestValue;

/**
 * Created by ernesto on 16/02/16.
 */
public class PlotParameters {
    // Main data
    private float zoomFactor;

    private float screenWidth, screenHeight;  // In pixels (I'm guessing)
    private Geometry.Point center;      // In world-space coordinates
    private float depth;                      // In world-space coordinates

    private boolean stopUpdates;

    private boolean rangesCalculated;
    private float minX, maxX;
    private float minY, maxY;
    private float minZ, maxZ;
    private float normalizedUnitLength;
    private float aspectRatio;
    private float scaleFactor;

    public PlotParameters() {
        zoomFactor = 0f;
        center = new Geometry.Point();
        minX = maxX = 0f;
        minY = maxY = 0f;
        minZ = maxZ = 0f;
        depth = 0f;
        rangesCalculated = false;

        screenWidth = screenHeight = 0f;
        normalizedUnitLength = 0f;
        aspectRatio = 0f;

        scaleFactor = 0f;

        stopUpdates = false;
    }

    public void setZoomFactor(float zoomFactor){
        if (stopUpdates) return;
        this.zoomFactor = zoomFactor;
        // We have to update the ranges
        rangesCalculated = false;
    }

    public void setCenter(float centerX, float centerY, float centerZ){
        if (stopUpdates) return;
        center.setCoordinates(centerX, centerY, centerZ);

        // We have to update the ranges
        rangesCalculated = false;
    }

    public float getZoomFactor(){
        return zoomFactor;
    }

    public float getCenterX() { return center.x; }
    public float getCenterY() {
        return center.y;
    }
    public float getCenterZ() {
        return center.z;
    }
    public Geometry.Point getCenter() { return center; }
    public float getDepth() {
        return depth;
    }

    public float getMinX() {
        if (!rangesCalculated) calculateRanges();
        return minX;
    }

    public float getMaxX() {
        if (!rangesCalculated) calculateRanges();
        return maxX;
    }

    public float getMinY() {
        if (!rangesCalculated) calculateRanges();
        return minY;
    }

    public float getMaxY() {
        if (!rangesCalculated) calculateRanges();
        return maxY;
    }

    public float getMinZ() {
        if (!rangesCalculated) calculateRanges();
        return minZ;
    }

    public float getMaxZ() {
        if (!rangesCalculated) calculateRanges();
        return maxZ;
    }

    public void setScreenValues(float screenWidth, float screenHeight){
        if (stopUpdates) return;
        rangesCalculated = false;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }

    public void setDepth(float depth) {
        rangesCalculated = false;
        this.depth = depth;
    }

    public void setDepth(MeshResults meshResults,
                         boolean isAxialSymmetry) {
        rangesCalculated = false;
        final int dimension = Constants.PROBLEM_DIMENSION;
        float[] coordinates = meshResults.getVerticesRef();
        float smallestX, smallestY, largestX, largestY;

        if (null == meshResults) return;
        if (dimension > coordinates.length) return;

        // The most distant vertex (its distance)
        if (isAxialSymmetry) {
            smallestX = smallestValue(dimension, coordinates, 0);
            largestX  = largestValue(dimension, coordinates, 0);
            smallestY = smallestValue(dimension, coordinates, 1);
            largestY  = largestValue(dimension, coordinates, 1);
            if (Math.abs(largestX - smallestX) > Math.abs(largestY - smallestY))
                depth = Math.abs(largestX - smallestX);
            else
                depth = Math.abs(largestY - smallestY);
        } else {
            depth = largestMagnitude(dimension, coordinates);
        }
        //Log.i("DEPTH", "Depth = " + depth);
    }

    public float getUnitLengthNormalizedCoordinateSystem() {
        calculateNormalizedUnitLength();
        return normalizedUnitLength;
    }

    private void calculateRanges() {
        calculateAspectRatio();
        calculateNormalizedUnitLength();

        if (screenHeight > screenWidth){
            // Portrait
            minX = center.x - 1f/normalizedUnitLength;
            maxX = center.x + 1f/normalizedUnitLength;

            minY = center.y - aspectRatio/normalizedUnitLength;
            maxY = center.y + aspectRatio/normalizedUnitLength;
        } else {
            // Landscape
            minX = center.x - aspectRatio/normalizedUnitLength;
            maxX = center.x + aspectRatio/normalizedUnitLength;

            minY = center.y - 1f/normalizedUnitLength;
            maxY = center.y + 1f/normalizedUnitLength;
        }

        minZ = center.z - depth - 2f*(float)Math.sqrt(center.x*center.x + center.y*center.y);
        maxZ = center.z + depth + 2f*(float)Math.sqrt(center.x*center.x + center.y*center.y);

        rangesCalculated = true;
    }

    private void calculateAspectRatio() {
        if (screenHeight > screenWidth) aspectRatio = screenHeight / screenWidth;
        else                            aspectRatio = screenWidth / screenHeight;
    }

    private void calculateNormalizedUnitLength() {
        // Unit length in the normalized space as function of the zoom factor.
        normalizedUnitLength = 1f/((float) Constants.REFERENCE_NUMBER_OF_UNITS)*zoomFactor;
    }

    // It calculates the distance the scene has to be moved so it is completely shown on the screen.
    // This will be used when instead of the orthographic projection we use a perspective projection.
    public float calculateDepth(float angleOfVision) {
        float depthForX, depthForY, largestDepth;
        float angleInRadians = (float)Math.toRadians(angleOfVision);
        if (!rangesCalculated) calculateRanges();

        depthForX = depthForY = 0f;
        if (screenHeight < screenWidth) {
            // Landscape
            depthForY = 0.5f*(maxY - minY) / (float)Math.tan(0.5f*angleInRadians);
            depthForX = 0.5f*(maxX - minX) / (aspectRatio*(float)Math.tan(0.5f*angleInRadians));
            Log.i("Depth", "Landscape orientation");
        } else {

        }

        largestDepth = depthForX;
        if (largestDepth < depthForY) largestDepth = depthForY;
        Log.i("Depth", "Largest depth = " + largestDepth);

        return largestDepth;
    }

    public void dontAllowUpdates() { stopUpdates = true; }
    public void allowUpdates() { stopUpdates = false; }

    public void print(String TAG, String label) {
        if (!rangesCalculated) calculateRanges();
        Log.i(TAG, "Begin --------------------------------------");
        Log.i(TAG, label);
        Log.i(TAG, "X ranges = " + minX + "   " + maxX);
        Log.i(TAG, "Y ranges = " + minY + "   " + maxY);
        Log.i(TAG, "Center   = " + center.x + "   " + center.y + "   " + center.z);
        Log.i(TAG, "Width    = " + (maxX-minX));
        Log.i(TAG, "Height   = " + (maxY-minY));
        Log.i(TAG, "Depth (+-)   = " + depth);
        Log.i(TAG, "Aspect ratio = " + aspectRatio);
        Log.i(TAG, "End ----------------------------------------");
    }

    public void copy(PlotParameters plotParameters) {
        screenWidth = plotParameters.screenWidth;
        screenHeight = plotParameters.screenHeight;
        zoomFactor = plotParameters.zoomFactor;
        center.setCoordinates(plotParameters.center);
        depth = plotParameters.depth;

        stopUpdates = false;
        calculateRanges();
        rangesCalculated = true;
    }
}
