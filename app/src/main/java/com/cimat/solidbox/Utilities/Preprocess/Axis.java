package com.cimat.solidbox.Utilities.Preprocess;

import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.PlotParameters;
import com.cimat.solidbox.OpenGL.Utilities.GLText;

/**
 * Created by ernesto on 11/02/16.
 */
public class Axis {

    public class Tics{
        float intervalSize;
        float length;
        float beginOnX, endOnX;
        float beginOnY, endOnY;
        int quantityOnX, quantityOnY;

        Tics() {
            intervalSize = 0f;
            length = 0f;
            beginOnX = endOnX = 0f;
            beginOnY = endOnY = 0f;
            quantityOnX = quantityOnY = 0;
        }

        public void initiate(float minX, float maxX, float minY, float maxY,
                             float unitLengthNormalizedCoordinateSystem) {
            length = Constants.MAIN_TICS_LENGTH / unitLengthNormalizedCoordinateSystem;

            calculateIntervalSize(minX, maxX, minY, maxY);
            calculateAxisX(minX, maxX);
            calculateAxisY(minY, maxY);
        }

        private void calculateIntervalSize(float minX, float maxX, float minY, float maxY) {
            float deltaX, deltaY, delta, tenPower;

            deltaX = maxX - minX;
            deltaY = maxY - minY;
            delta = deltaX;
            if (delta > deltaY) delta = deltaY;

            tenPower = 1f;
            if (delta > 1f) {
                while(tenPower < delta) tenPower *= 10f;
                if (delta >= 0.5f*tenPower) intervalSize = 0.1f*tenPower;
                else                        intervalSize = 0.5f*0.1f*tenPower;
            } else {
                while (tenPower > delta) tenPower *= 0.1;
                if (delta >= 5f*tenPower) intervalSize = tenPower;
                else                      intervalSize = 0.5f*tenPower;
            }
        }

        private void calculateAxisX(float minX, float maxX) {
            int begin, end;

            begin = (int) Math.floor(minX / intervalSize);
            beginOnX = (float)begin * intervalSize;

            end = (int) Math.ceil(maxX / intervalSize);
            endOnX = (float)end * intervalSize;

            quantityOnX = end - begin + 1;
        }

        private void calculateAxisY(float minY, float maxY) {
            int begin, end;

            begin = (int) Math.floor(minY / intervalSize);
            beginOnY = (float)begin * intervalSize;

            end = (int) Math.ceil(maxY / intervalSize);
            endOnY = (float)end * intervalSize;

            quantityOnY = end - begin + 1;
        }

        public float getIntervalSize() { return intervalSize; }

        public float getBeginOnX() { return beginOnX;}
        public float getBeginOnY() { return beginOnY; }

        public float getEndOnX() { return endOnX; }
        public float getEndOnY() { return endOnY; }

        public int getQuantityOnX() { return quantityOnX; }
        public int getQuantityOnY() { return quantityOnY; }

        public float getLength() { return length; }

        public void drawLabels(GLText glText) {
            drawLabelsOnHorizontalAxis(0, intervalSize, getBeginOnX(), getQuantityOnX(), glText);
            drawLabelsOnVerticalAxis(0, intervalSize, getBeginOnY(), getQuantityOnY(), glText);
        }

        private void drawLabelsOnHorizontalAxis(float y,
                                               float intervalSize,
                                               float begin,
                                               int numberOfLabels,
                                               GLText glText) {
            float x  = begin;
            String label;

            for (int i = 0; i < numberOfLabels; i++) {
                label = getLabel(x);
                glText.draw(label, x, y);
                x += intervalSize;
            }
        }

        private void drawLabelsOnVerticalAxis(float x,
                                             float intervalSize,
                                             float begin,
                                             int numberOfLabels,
                                             GLText glText) {
            float y  = begin;
            String label;

            for (int i = 0; i < numberOfLabels; i++) {
                label = getLabel(y);
                glText.draw(label, x, y);
                y += intervalSize;
            }
        }
    }

    public static String getLabel(float number) {
        String label = Float.toString(number);
        if ((number < 1e-3) ||(number > 1000)) {
            // Use exponential notation
        }
        return label;
    }

    public Tics mainTics;
    public Tics secondTics;
    private float minX, maxX, minY, maxY;
    private float unitLengthNormalizedCoordinateSystem;

    public Axis() {
        mainTics = new Tics();
        secondTics = null;
        minX = maxX = minY = maxY = 0f;
        unitLengthNormalizedCoordinateSystem = 0f;
    }

    public void setPlotParameters(PlotParameters plotParameters) {
        // Because plotParameters can change any time, we stop the updates.
        plotParameters.dontAllowUpdates();

        minX = plotParameters.getMinX();
        maxX = plotParameters.getMaxX();
        minY = plotParameters.getMinY();
        maxY = plotParameters.getMaxY();
        unitLengthNormalizedCoordinateSystem =
                plotParameters.getUnitLengthNormalizedCoordinateSystem();

        mainTics.initiate(minX, maxX, minY, maxY, unitLengthNormalizedCoordinateSystem);

        // Once we have calculated everything we allow the updates
        plotParameters.allowUpdates();
    }

    public float getMinX() {
        return minX;
    }

    public float getMaxX() {
        return maxX;
    }

    public float getMinY() {
        return minY;
    }

    public float getMaxY() {
        return maxY;
    }

}
