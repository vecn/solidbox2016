package com.cimat.solidbox.Utilities;

import android.util.Log;
import android.widget.ArrayAdapter;

import com.cimat.solidbox.OpenGL.Utilities.Utilities;
import com.cimat.solidbox.Utilities.Preprocess.Geometry;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Shape;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.InteractivePoint;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Point;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Vector;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.Ray;
import com.cimat.solidbox.OpenGL.Utilities.TouchHelper.Drag;
import com.cimat.solidbox.OpenGL.Utilities.TouchHelper.Drags;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;

import nb.pdeBot.BoundaryConditions;

import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.multiplyMV;
import static android.opengl.Matrix.setIdentityM;
import static android.opengl.Matrix.invertM;
import static android.opengl.Matrix.orthoM;

/**
 * Created by ernesto on 30/06/16.
 */

public class Tools {
    public static int getNextUpdate(BlockingQueue<Integer> updates) {
        if (updates.isEmpty()) return Constants.DOING_NOTHING;
        int currentUpdate, nextUpdate;

        currentUpdate = updates.poll();

        // We dismiss all the same updates
        while (!updates.isEmpty()) {
            nextUpdate = updates.peek();
            if (nextUpdate == currentUpdate) updates.poll();
            else                             break;
        }

        return currentUpdate;
    }

    public static float average(int dimension,
                                float[] values,
                                int startIndex) {
        if (null == values) return 0;
        if (0 > dimension) return 0;
        if (-1 > startIndex) return 0;

        float sum;
        int i, counter;

        i = startIndex;
        sum = 0;
        counter = 0;
        while (i < values.length) {
            sum += values[i];
            counter++;
            i += dimension;
        }

        return sum/(float)counter;
    }

    public static float smallestMagnitude(int dimension,
                                          float[] values) {
        float magnitude, smallestMagnitude;
        final int size = values.length / dimension;

        if (values.length < dimension) return 0f;

        smallestMagnitude = 0f;
        for (int i = 0; i < dimension; i++) smallestMagnitude += (values[i]*values[i]);
        smallestMagnitude = (float) Math.sqrt(smallestMagnitude);

        for (int i = 0; i < size; i++) {
            magnitude = 0f;
            for (int j = 0; j < dimension; j++)
                magnitude += (values[i*dimension+j]*values[i*dimension+j]);
            magnitude = (float) Math.sqrt(magnitude);
            if (magnitude < smallestMagnitude) smallestMagnitude = magnitude;
        }

        return smallestMagnitude;
    }

    public static float smallestValue(int dimension,
                                      float[] values,
                                      int relativeIndex) {
        float smallestValue;
        int size = values.length / dimension;

        if (relativeIndex > dimension - 1) return 0f;
        if (values.length < dimension) return 0f;

        smallestValue = values[relativeIndex];
        for (int i = 0; i < size; i++)
            if (smallestValue > values[i*dimension+relativeIndex])
                smallestValue = values[i*dimension+relativeIndex];

        return smallestValue;
    }

    public static float smallestValue(float[] values) {
        float smallestValue;
        if (values.length == 0) return 0f;

        smallestValue = values[0];
        for (int i = 0; i < values.length; i++)
            if (smallestValue > values[i]) smallestValue = values[i];

        return smallestValue;
    }

    public static float largestValue(float[] values) {
        float largestValue;
        if (values.length == 0) return 0f;

        largestValue = values[0];
        for (int i = 0; i < values.length; i++)
            if (largestValue < values[i]) largestValue = values[i];

        return largestValue;
    }

    public static float largestValue(int dimension,
                                     float[] values,
                                     int relativeIndex) {
        float largestValue;
        int size = values.length / dimension;

        if (relativeIndex > dimension - 1) return 0f;
        if (values.length < dimension) return 0f;

        largestValue = values[relativeIndex];
        for (int i = 0; i < size; i++)
            if (largestValue < values[i*dimension+relativeIndex])
                largestValue = values[i*dimension+relativeIndex];

        return largestValue;
    }

    public static float largestMagnitude(int dimension,
                                         float[] values) {
        float magnitude, largestMagnitude;
        final int size = values.length / dimension;

        largestMagnitude = 0f;
        if (values.length > dimension)
            for (int i = 0; i < dimension; i++) largestMagnitude += (values[i]*values[i]);
        largestMagnitude = (float) Math.sqrt(largestMagnitude);

        for (int i = 0; i < size; i++) {
            magnitude = 0f;
            for (int j = 0; j < dimension; j++) magnitude += (values[i*dimension+j]*values[i*dimension+j]);
            magnitude = (float) Math.sqrt(magnitude);
            if (magnitude > largestMagnitude) largestMagnitude = magnitude;
        }

        return largestMagnitude;
    }

    public static float distanceBetweenPoints(Point a, Point b) {
        if ((null == a) || (null == b)) return -1f;
        return (float)Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2)
                + Math.pow(a.z - b.z, 2));
    }


    public static <T> void switchElements(int indexA,
                                          int indexB,
                                          ArrayList<T> elements) {
        T a = elements.get(indexA);
        T b = elements.get(indexB);

        elements.set(indexA, b);
        elements.set(indexB, b);
    }

    public static void printBoundaryConditions(String name,
                                               BoundaryConditions boundaryConditions) {
        int[] ids = boundaryConditions.getIdsRef();
        char[] dofs = boundaryConditions.getDofRef();
        double[] values = boundaryConditions.getValuesRef();
        Log.i("Data", name);
        Log.i("Data", "Size = " + boundaryConditions.getN());
        for (int i = 0; i < boundaryConditions.getN(); i++){
            switch (dofs[i]){
                case 'a':
                    Log.i("Data", "" + ids[i] + "  " + dofs[i] + "  " + values[i*2] + "  " + values[i*2+1]);
                    break;
                case 'x':
                    Log.i("Data", "" + ids[i] + "  " + dofs[i] + "  " + values[i*2]);
                    break;
                case 'y':
                    Log.i("Data", "" + ids[i] + "  " + dofs[i] + "  " + values[i*2+1]);
                    break;
                default:
                    Log.i("Data", "No dof valid!!");
            }
        }
    }

    public static <T extends Geometry.Point> void applyTransformationMatrix(float[] M,
                                                                            ArrayList<T> elements) {
        float[] a = new float[4];
        float[] b = new float[4];
        T e;

        Iterator<T> it = elements.iterator();
        a[3] = 1f;
        while (it.hasNext()) {
            e = it.next();
            a[0] = e.getX();
            a[1] = e.getY();
            a[2] = e.getZ();
            multiplyMV(b, 0, M, 0, a, 0);
            e.setCoordinates(b[0], b[1], b[2]);
        }
    }

    public static<T extends Geometry.Point> float[] getMaxCoordinatesValues(ArrayList<T> points) {
        T e;
        float max[] = new float[3];
        max[0] = max[1] = max[2] = 0f;

        if (null == points) return max;

        Iterator<T> it = points.iterator();
        if (it.hasNext()) {
            e = it.next();
            max[0] = e.getX();
            max[1] = e.getY();
            max[2] = e.getZ();

            while (it.hasNext()) {
                e = it.next();

                if (max[0] < e.getX()) max[0] = e.getX();
                if (max[1] < e.getY()) max[1] = e.getY();
                if (max[2] < e.getZ()) max[2] = e.getZ();
            }
        }

        return max;
    }

    public static<T extends Geometry.Point> float[] getMinCoordinatesValues(ArrayList<T> points) {
        T e;
        float min[] = new float[3];
        min[0] = min[1] = min[2] = 0f;

        if (null == points) return min;

        Iterator<T> it = points.iterator();
        if (it.hasNext()) {
            e = it.next();
            min[0] = e.getX();
            min[1] = e.getY();
            min[2] = e.getZ();

            while (it.hasNext()) {
                e = it.next();

                if (min[0] > e.getX()) min[0] = e.getX();
                if (min[1] > e.getY()) min[1] = e.getY();
                if (min[2] > e.getZ()) min[2] = e.getZ();
            }
        }

        return min;
    }

    public static boolean get2DLineIntersection(float px1, float py1, float px2, float py2,
                                                float qx1, float qy1, float qx2, float qy2,
                                                float[] intersectionPoint) {
        if (null == intersectionPoint) return false;

        Geometry.Vector u = new Geometry.Vector(px2 - px1, py2 - py1, 0f);
        Geometry.Vector v = new Geometry.Vector(qx2 - qx1, qy2 - qy1, 0f);
        Geometry.Vector w = new Geometry.Vector(px1 - qx1, py1 - qy1, 0f);
        Geometry.Vector vt = v.crossProduct(new Geometry.Vector(0, 0, 1));

        float vtDotU = vt.dotProduct(u);
        if (1e-10 > Math.abs(vtDotU)) return false;

        float s = -vt.dotProduct(w)/vtDotU;
        intersectionPoint[0] = px1 + s*u.x;
        intersectionPoint[1] = py1 + s*u.y;
        return true;
    }

    public static boolean isPointInTheLine(Point p1, Point p2, Point p) {
        if (null == p1 || null == p2 || null == p) return false;
        Point projectedP = projectPointOverLine(p1, p2, p);
        float dist = Tools.distanceBetweenPoints(p, projectedP);
        if (Constants.TOLERANCE_FOR_ZER0 > dist) return true;
        return false;
    }

    public static boolean isPointInTheSegment(Point p1, Point p2, Point p) {
        if (null == p1 || null == p2 || null == p) return false;
        Point projectedP = projectPointOverLine(p1, p2, p);
        float dist = Tools.distanceBetweenPoints(p, projectedP);
        if (Constants.TOLERANCE_FOR_ZER0 < dist) return false;
        float dotProd =
                (p1.x - projectedP.x)*(p2.x - projectedP.x) +
                (p1.y - projectedP.y)*(p2.y - projectedP.y) +
                (p1.z - projectedP.z)*(p2.z - projectedP.z);
        if (dotProd > 1e-10) return false;
        return true;
    }

    public static Point projectPointOverLine(Point p1, Point p2, Point p) {
        Point projection = new Point(0f, 0f, 0f);
        if (null == p1 || null == p2 || null == p) return projection;

        Geometry.Vector a = new Geometry.Vector(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z);
        Geometry.Vector b = new Geometry.Vector(p.x - p1.x, p.y - p1.y, p.z - p1.z);
        if (1e-10 > a.length()) a.scale(1e20f);
        if (1e-10 > a.length()) {
            projection.setCoordinates(p1.x, p1.y, p1.z);
            return projection;
        }

        float f = a.dotProduct(b)/(a.length()*a.length());
        projection.setCoordinates(p1.x + f*a.x, p1.y + f*a.y, p1.z + f*a.z);

        return projection;
    }


    public static void offsetSurfaceShape2D(Geometry.SurfaceShape shape, float d) {
        if (null == shape) return;
        //offsetClosedVertices2D(shape.getVertices(), d);
    }

    public static void offsetClosedLinearShape(Geometry.LinearShape shape, float d) {
        if (null == shape) return;
        offsetClosedVertices2D(shape.getVertices(), d);
    }

    private static<T extends Point> void offsetClosedVertices2D(ArrayList<T> vertices,
                                                                final float d) {
        if (null == vertices) return;
        if (2 >= vertices.size()) return;

        Geometry.Point v1, v2, v3, offsetVertex;
        ArrayList<Point> newVertices = new ArrayList<>();
        Iterator<T> it = vertices.iterator();

        v1 = v2 = v3 = null;

        v1 = vertices.get(vertices.size()-1);
        if (it.hasNext()) v2 = it.next();
        if (it.hasNext()) v3 = it.next();

        offsetVertex = new Point();
        offsetVertex2D(v1, v2, v3, d, offsetVertex);
        newVertices.add(offsetVertex);
        v1 = v2;
        v2 = v3;

        while (it.hasNext()) {
            v3 = it.next();

            offsetVertex = new Point();
            offsetVertex2D(v1, v2, v3, d, offsetVertex);
            newVertices.add(offsetVertex);
            v1 = v2;
            v2 = v3;
        }

        v3 = vertices.get(0);
        offsetVertex = new Point();
        offsetVertex2D(v1, v2, v3, d, offsetVertex);
        newVertices.add(offsetVertex);

        it = vertices.iterator();
        Iterator<Point> it_2 = newVertices.iterator();
        while(it.hasNext() && it_2.hasNext()) {
            v1 = it.next();
            v2 = it_2.next();
            v1.setCoordinates(v2.getX(), v2.getY(), v2.getZ());
        }

        newVertices.clear();
    }

    public static<T extends Point> void offsetNonClosedVertices2D(ArrayList<T> vertices,
                                                                  final float d) {
        if (null == vertices) return;
        if (1 >= vertices.size()) return;

        Geometry.Point v1, v2, v3, offsetVertex;
        ArrayList<Geometry.Point> newVertices = new ArrayList<>();
        Iterator<T> it = vertices.iterator();
        v1 = v2 = v3 = null;

        if (it.hasNext()) v2 = it.next();
        if (it.hasNext()) v3 = it.next();
        v1 = new Point(
                v2.getX() - (v3.getX() - v2.getX()),
                v2.getY() - (v3.getY() - v2.getY()),
                v2.getZ() - (v3.getZ() - v2.getZ()));

        offsetVertex = new Geometry.Point();
        offsetVertex2D(v1, v2, v3, d, offsetVertex);
        newVertices.add(offsetVertex);
        v1 = v2;
        v2 = v3;

        while (it.hasNext()) {
            v3 = it.next();

            offsetVertex = new Point();
            offsetVertex2D(v1, v2, v3, d, offsetVertex);
            newVertices.add(offsetVertex);
            v1 = v2;
            v2 = v3;
        }

        v3 = new Point(
                v2.getX() + (v2.getX() - v1.getX()),
                v2.getY() + (v2.getY() - v1.getY()),
                v2.getZ() + (v2.getZ() - v1.getZ()));
        offsetVertex = new Geometry.Point();
        offsetVertex2D(v1, v2, v3, d, offsetVertex);
        newVertices.add(offsetVertex);

        it = vertices.iterator();
        Iterator<Geometry.Point> it_2 = newVertices.iterator();
        while(it.hasNext() && it_2.hasNext()) {
            v1 = it.next();
            v2 = it_2.next();
            v1.setCoordinates(v2.getX(), v2.getY(), v2.getZ());
        }

        newVertices.clear();
    }

    private static boolean offsetVertex2D(Point v1, Point v2, Point v3,
                                          final float d, Point offsetVertex) {
        if (null != offsetVertex) offsetVertex.setCoordinates(0f, 0f, 0f);
        if (null == v1 || null == v2 || null == v3 || null == offsetVertex) return false;

        Geometry.Vector a = new Geometry.Vector(
                v2.getX() - v1.getX(),
                v2.getY() - v1.getY(),
                v2.getZ() - v1.getZ());

        Geometry.Vector b = new Geometry.Vector(
                v3.getX() - v2.getX(),
                v3.getY() - v2.getY(),
                v3.getZ() - v2.getZ());

        Geometry.Vector up = new Geometry.Vector(0f, 0f, 1f);

        Geometry.Vector na = a.getUnitNormalVector(up);
        //if (na.dotProduct(b) > 0f) na.scale(-1f);

        Geometry.Vector nb = b.getUnitNormalVector(up);
        //if (nb.dotProduct(a) < 0f) nb.scale(-1f);

        na.scale(d);
        nb.scale(d);

        if (Tools.isPointInTheLine(v1, v2, v3)) {
            if (Tools.isPointInTheSegment(v1, v2, v3))
                return false;
            else
                offsetVertex.setCoordinates(v2.getX() + na.x, v2.getY() + na.y, v2.getZ() + na.z);
        } else {
            float[] intersectionPoint = new float[2];
            Tools.get2DLineIntersection(
                    v1.getX() + na.x, v1.getY() + na.y,
                    v2.getX() + na.x, v2.getY() + na.y,
                    v2.getX() + nb.x, v2.getY() + nb.y,
                    v3.getX() + nb.x, v3.getY() + nb.y,
                    intersectionPoint);

            offsetVertex.setCoordinates(intersectionPoint[0], intersectionPoint[1], v1.getZ());
        }
        return true;
    }

    public static Point offsetVertex2D(Point v1, Point v2, Point v3, final float d) {
        Point offsetVertex = new Point(0f, 0f, 0f);
        if (null == v1 || null == v2 || null == v3) return offsetVertex;

        Geometry.Vector a = new Geometry.Vector(
                v2.getX() - v1.getX(),
                v2.getY() - v1.getY(),
                v2.getZ() - v1.getZ());

        Geometry.Vector b = new Geometry.Vector(
                v3.getX() - v2.getX(),
                v3.getY() - v2.getY(),
                v3.getZ() - v2.getZ());

        Geometry.Vector up = new Geometry.Vector(0f, 0f, 1f);

        Geometry.Vector na = a.getUnitNormalVector(up);
        Geometry.Vector nb = b.getUnitNormalVector(up);
        na.scale(d);
        nb.scale(d);

        if (Tools.isPointInTheLine(v1, v2, v3)) {
            if (Tools.isPointInTheSegment(v1, v2, v3))
                return offsetVertex;
            else
                offsetVertex.setCoordinates(v2.getX() + na.x, v2.getY() + na.y, v2.getZ() + na.z);
        } else {
            float[] intersectionPoint = new float[2];
            Tools.get2DLineIntersection(
                    v1.getX() + na.x, v1.getY() + na.y,
                    v2.getX() + na.x, v2.getY() + na.y,
                    v2.getX() + nb.x, v2.getY() + nb.y,
                    v3.getX() + nb.x, v3.getY() + nb.y,
                    intersectionPoint);

            offsetVertex.setCoordinates(intersectionPoint[0], intersectionPoint[1], v1.getZ());
        }
        return offsetVertex;
    }

    public static<T extends Geometry.Point> ArrayList<Geometry.Point> copyVerticesCoordinates(ArrayList<T> points) {
        ArrayList<Geometry.Point> newPoints = new ArrayList<>();
        Iterator<T> it = points.iterator();
        Geometry.Point v, nv;
        while (it.hasNext()) {
            v = it.next();
            nv = new Geometry.Point(v.getX(), v.getY(), v.getZ());
            newPoints.add(nv);
        }

        return newPoints;
    }

    public static<T extends Geometry.Point> ArrayList<Geometry.Point> divideVertices(ArrayList<T> vertices,
                                                                                     final float d,
                                                                                     final boolean isClosed) {
        final float totalLength = getLengthNonClosedVertices(vertices);
        ArrayList<Geometry.Point> dividedLine = new ArrayList<>();
        Geometry.Point v1, v2;
        float length, accumulated, f, littlePiece;
        Geometry.Vector delta = new Geometry.Vector(0f, 0f, 0f);

        Iterator<T> it = vertices.iterator();
        if (it.hasNext()) {
            v1 = it.next();
            dividedLine.add(new Geometry.Point(v1.getX(), v1.getY(), v1.getZ()));
            accumulated = 0f;
            while (it.hasNext()) {
                v2 = it.next();
                length = distanceBetweenPoints(v1, v2);
                accumulated += length;
                if (accumulated > d) {
                    littlePiece = d - (accumulated - length);
                    f = littlePiece/length;

                    delta.x = v2.getX() - v1.getX();
                    delta.y = v2.getY() - v1.getY();
                    delta.z = v2.getZ() - v1.getZ();
                    dividedLine.add(new Geometry.Point(
                            v1.getX() + f*delta.x,
                            v1.getY() + f*delta.y,
                            v1.getZ() + f*delta.z));
                    accumulated = length - littlePiece;

                    while (accumulated >= d) {
                        f += d/length;
                        dividedLine.add(new Geometry.Point(
                                v1.getX() + f*delta.x,
                                v1.getY() + f*delta.y,
                                v1.getZ() + f*delta.z));
                        accumulated -= d;
                    }
                }

                // When the remaining distance is almost zero or it is a bit smaller than d
                if (Constants.TOLERANCE_FOR_ZER0 < accumulated &&
                        Constants.TOLERANCE_FOR_ZER0 < Math.abs(accumulated - d))
                    dividedLine.add(new Geometry.Point(v2.getX(), v2.getY(), v2.getZ()));

                v1 = v2;
            }

            if (isClosed) {
                v2 = vertices.get(0);

                // This code is the same as before, it should be wrapped in a function
                length = distanceBetweenPoints(v1, v2);
                accumulated += length;
                if (accumulated > d) {
                    littlePiece = d - (accumulated - length);
                    f = littlePiece/length;

                    delta.x = v2.getX() - v1.getX();
                    delta.y = v2.getY() - v1.getY();
                    delta.z = v2.getZ() - v1.getZ();
                    dividedLine.add(new Geometry.Point(
                            v1.getX() + f*delta.x,
                            v1.getY() + f*delta.y,
                            v1.getZ() + f*delta.z));
                    accumulated = length - littlePiece;

                    while (accumulated >= d) {
                        f += d/length;
                        dividedLine.add(new Geometry.Point(
                                v1.getX() + f*delta.x,
                                v1.getY() + f*delta.y,
                                v1.getZ() + f*delta.z));
                        accumulated -= d;
                    }
                }

                // In this case only matters if the remaining distance is almost zero
                if (Constants.TOLERANCE_FOR_ZER0 < accumulated)
                    dividedLine.add(new Geometry.Point(v2.getX(), v2.getY(), v2.getZ()));
            }
        }

        return dividedLine;
    }

    public static<T extends Geometry.Point> float getLengthNonClosedVertices(ArrayList<T> points) {
        float length = 0f;
        Geometry.Point p1, p2;
        Iterator<T> it = points.iterator();

        if (it.hasNext()) {
            p1 = it.next();
            while (it.hasNext()) {
                p2 = it.next();
                length += distanceBetweenPoints(p1, p2);
            }
        }

        return length;
    }

    public static<T extends Geometry.Point> void printToLog(String message, ArrayList<T> points) {
        Iterator<T> it = points.iterator();
        Geometry.Point p;
        Log.i("Lineeeeee", message);
        while (it.hasNext()) {
            p = it.next();
            Log.i("Lineeeeee", p.getX() + " " + p.getY() + " " + p.getZ());
        }
    }

    public static<T extends Geometry.Point> void translatePoints(float dx, float dy, float dz,
                                                                 ArrayList<T> points) {
        Iterator<T> it = points.iterator();
        while (it.hasNext()) {
            it.next().translate(dx, dy, dz);
        }
    }

    public static Point middlePoint(Point a, Point b) {
        if ((null == a) || (null == b)) return null;
        return new Point(0.5f * (a.x + b.x), 0.5f * (a.y + b.y), 0.5f * (a.z + b.z));
    }

    public static void addRotationAroundArbitraryAxis(float a, float b, float c,
                                                      float x, float y, float z,
                                                      float angleInRadians,
                                                      float[] rotationMatrix) {
        float[] newRotation = new float[16];
        float[] aux = new float[16];

        setIdentityM(newRotation, 0);
        getRotationMatrixAroundArbitraryAxis(a, b, c, x, y, z, angleInRadians, newRotation);
        for (int i = 0; i < 16; i++) aux[i] = rotationMatrix[i];

        //MeshRenderer.printSquareMatrixSize4("SCALEVIEW", "New rotation", newRotation);

        multiplyMM(rotationMatrix, 0, newRotation, 0, aux, 0); // Maybe the order is the opposite
    }

    public static void getRotationMatrixAroundArbitraryAxis(float a, float b, float c,
                                                            float x, float y, float z,
                                                            float theta,
                                                            float[] R) {
        String TAG = "RotatingObject";
        boolean printInfo = false;

        float l = (float)Math.sqrt(x*x + y*y + z*z);
        if (l < 1e-10) {
            Log.i(TAG, "Vector too short!!");
            return;
        }

        // In this instance we normalize the direction vector.
        float u = x / l;
        float v = y / l;
        float w = z / l;

        if (printInfo) {
            Log.i(TAG, "Point = " + a + "  " + b + "  " + c);
            Log.i(TAG, "Axis  = " + x + "  " + y + "  " + z);
            Log.i(TAG, "Norm. axis  = " + u + "  " + v + "  " + w);
            Log.i(TAG, "Angle = " + theta);
        }

        // Set some intermediate values.
        float u2 = u * u;
        float v2 = v * v;
        float w2 = w * w;
        float cosT = (float) Math.cos(theta);
        float oneMinusCosT = 1f - cosT;
        float sinT = (float) Math.sin(theta);

        // Build the matrix entries element by element.
        R[0] = u2 + (v2 + w2) * cosT;
        R[4] = u * v * oneMinusCosT - w * sinT;
        R[8] = u * w * oneMinusCosT + v * sinT;
        R[12] = (a * (v2 + w2) - u * (b * v + c * w)) * oneMinusCosT
                + (b * w - c * v) * sinT;

        R[1] = u * v * oneMinusCosT + w * sinT;
        R[5] = v2 + (u2 + w2) * cosT;
        R[9] = v * w * oneMinusCosT - u * sinT;
        R[13] = (b * (u2 + w2) - v * (a * u + c * w)) * oneMinusCosT
                + (c * u - a * w) * sinT;

        R[2] = u * w * oneMinusCosT - v * sinT;
        R[6] = v * w * oneMinusCosT + u * sinT;
        R[10] = w2 + (u2 + v2) * cosT;
        R[14] = (c * (u2 + v2) - w * (a * u + b * v)) * oneMinusCosT
                + (a * v - b * u) * sinT;

        R[3] = 0f;
        R[7] = 0f;
        R[11] = 0f;
        R[15] = 1f;
    }

    public static float cosineBetweenVectors(Vector a, Vector b) {
        return a.dotProduct(b) / (a.length() * b.length());
    }

    public static void scaleView(Point previousP1NDC, Point previousP2NDC,
                                 Point currentP1NDC, Point currentP2NDC,
                                 float[] viewMatrix,
                                 float[] projectionMatrix,
                                 PlotParameters plotParameters) {
        // We have to do the rotation and projection so the firstTouchPoint and secondTouchPoint have
        // the same position as the pointers (fingers). This means the user may be doing
        // zoom and/or rotating at the same time.

        float[] invertedViewProjectionMatrix = new float[16];
        float[] viewProjectionMatrix = new float[16];
        float[] x = new float[4];
        float[] a = new float[4];

        multiplyMM(viewProjectionMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
        invertM(invertedViewProjectionMatrix, 0, viewProjectionMatrix, 0);

        Point previousP1WSC = Utilities.convertNormalized2DPointToWorldSpace2DPoint(
                previousP1NDC.x,
                previousP1NDC.y,
                invertedViewProjectionMatrix
        );

        Vector previousNDC = new Vector(
                previousP2NDC.x - previousP1NDC.x,
                previousP2NDC.y - previousP1NDC.y,
                previousP2NDC.z - previousP1NDC.z
        );

        Vector currentNDC = new Vector(
                currentP2NDC.x - currentP1NDC.x,
                currentP2NDC.y - currentP1NDC.y,
                currentP2NDC.z - currentP1NDC.z
        );

        float cosine = cosineBetweenVectors(previousNDC, currentNDC);
        if (Math.abs(cosine) > 1) {
            if (cosine > 0) cosine = 1;
            else cosine = -1;
        }
        float angle = (float) Math.acos(cosine);
        Vector sign = previousNDC.crossProduct(currentNDC);
        if (sign.z < 0) angle *= (-1);

        addRotationAroundArbitraryAxis(
                previousP1WSC.x,
                previousP1WSC.y,
                previousP1WSC.z,
                0, 0, 1, angle, viewMatrix);

        float scaleFactor = currentNDC.length() / previousNDC.length();
        plotParameters.setZoomFactor(plotParameters.getZoomFactor() * scaleFactor);

        float w = plotParameters.getMaxX() - plotParameters.getMinX();
        float h = plotParameters.getMaxY() - plotParameters.getMinY();

        x[0] = previousP1WSC.x;
        x[1] = previousP1WSC.y;
        x[2] = previousP1WSC.z;
        x[3] = 1;
        multiplyMV(a, 0, viewMatrix, 0, x, 0);

        float cx = 0.5f * (2f * a[0] - w * currentP1NDC.x);
        float cy = 0.5f * (2f * a[1] - h * currentP1NDC.y);

        plotParameters.setCenter(cx, cy, plotParameters.getCenterZ());

        // We update the projection matrix
        orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                plotParameters.getMinY(), plotParameters.getMaxY(),
                plotParameters.getMinZ(), plotParameters.getMaxZ());
    }

    public static float getDistanceToRay(Point point, Ray ray){
        Vector p1ToPoint = vectorBetween(ray.point, point);
        Vector p2ToPoint = vectorBetween(ray.point.translate(ray.vector), point);

        // The length of the cross product gives the area of an imaginary
        // parallelogram having the two vectors as sides. A parallelogram can be
        // thought of as consisting of two triangles, so this is the same as
        // twice the area of the triangle defined by the two vectors.
        // http://en.wikipedia.org/wiki/Cross_product#Geometric_meaning
        float areaOfTriangleTimesTwo = p1ToPoint.crossProduct(p2ToPoint).length();
        float lengthOfBase = ray.vector.length();

        // The area of a triangle is also equal to (base * height) / 2. In
        // other words, the height is equal to (area * 2) / base. The height
        // of this triangle is the distance from the point to the ray.
        float distanceFromPointToRay = areaOfTriangleTimesTwo / lengthOfBase;

        // Now checks the distances from the point to the begin and end of the ray
        //float distanceFromPointToRaysBegin = p1ToPoint.length();
        //float distanceFromPointToRaysEnd = p2ToPoint.length();
        float dotProductP1ToPointAndRaysVector = p1ToPoint.dotProduct(ray.vector);
        float dotProductP2ToPointAndRaysVector = p2ToPoint.dotProduct(ray.vector.scale(-1));

        if (dotProductP1ToPointAndRaysVector < 0f) distanceFromPointToRay = p1ToPoint.length();
        if (dotProductP2ToPointAndRaysVector < 0f) distanceFromPointToRay = p2ToPoint.length();

        return distanceFromPointToRay;
    }

    public static Vector vectorBetween(Point from, Point to) {
        return new Vector(to.x - from.x, to.y - from.y, to.z - from.z);
    }

    public static<T> void addAllExceptFirstElement(ArrayList<T> a,
                                                   ArrayList<T> b) {
        if (null == a || null == b) return;
        Iterator<T> it = a.iterator();
        if (it.hasNext()) it.next();
        while (it.hasNext()) b.add(it.next());
    }

    public static<T> void addAllExceptFirstAndLastElements(ArrayList<T> a,
                                                           ArrayList<T> b) {
        if (null == a || null == b) return;
        Iterator<T> it = a.iterator();
        if (it.hasNext()) it.next();
        int i = 1;
        while (it.hasNext() && (i < a.size() - 1)) b.add(it.next());
    }


    public static Geometry.OrderOneLinearShape getBoundingBox(ArrayList<Point> vertices) {
        Geometry.OrderOneLinearShape boundingBox =
                new Geometry.OrderOneLinearShape();

        if (null == vertices) return boundingBox;
        if (0 == vertices.size()) return boundingBox;

        float[] maxValues = Tools.getMaxCoordinatesValues(vertices);
        float[] minValues = Tools.getMinCoordinatesValues(vertices);

        boundingBox.setStart(minValues[0], minValues[1], 0f);
        boundingBox.createSegmentToHere(maxValues[0], minValues[1], 0f);
        boundingBox.createSegmentToHere(maxValues[0], maxValues[1], 0f);
        boundingBox.createSegmentToHere(minValues[0], maxValues[1], 0f);
        boundingBox.close();

        return boundingBox;
    }

    public static Point getPreviousWorldSpacePointsFromDrag(Drag drag,
                                                            float[] invertedTransformationMatrix) {
        Point previousNormSpacePoint = drag.getPreviousPosition();
        Point previousWorldSpacePoint = Utilities.convertNormalized2DPointToWorldSpace2DPoint(
                previousNormSpacePoint.x,
                previousNormSpacePoint.y,
                invertedTransformationMatrix
        );
        return previousWorldSpacePoint;
    }

    public static Point getEndingWorldSpacePointsFromDrag(Drag drag,
                                                          float[] invertedTransformationMatrix) {
        Point endingNormSpacePoint = drag.getEndingPosition();
        Point endingWorldSpacePoint = Utilities.convertNormalized2DPointToWorldSpace2DPoint(
                endingNormSpacePoint.x,
                endingNormSpacePoint.y,
                invertedTransformationMatrix
        );
        return endingWorldSpacePoint;
    }

    public static Geometry.OrderThreeLinearShape buildCubicLinearShape(Geometry.LinearShape ls) {
        Point p;
        Iterator<Geometry.BezierCurve> it = ls.getCurves().iterator();
        Geometry.OrderThreeLinearShape shape =
                new Geometry.OrderThreeLinearShape();

        if (ls.isClosed()) {
            if (it.hasNext()) {
                p = it.next().getStart();
                shape.setStart(p.x, p.y, p.z);
                while (it.hasNext()) {
                    p = it.next().getStart();
                    shape.createSegmentToHere(p.x, p.y, p.z);
                }
            }
            shape.close();
        } else {
            if (it.hasNext()) {
                Geometry.BezierCurve bc = it.next();
                p = bc.getStart();
                shape.setStart(p.x, p.y, p.z);
                p = bc.getEnd();
                shape.createSegmentToHere(p.x, p.y, p.z);
                while (it.hasNext()) {
                    p = it.next().getEnd();
                    shape.createSegmentToHere(p.x, p.y, p.z);
                }
            }
        }

        return shape;
    }

    /*
    public static< T extends Point> CleanPolyLine getBoundingBox(final float d,
                                                                 final ArrayList<T> vertices) {
        float[] maxValues = Tools.<T>getMaxCoordinatesValues(vertices);
        float[] minValues = Tools.<T>getMinCoordinatesValues(vertices);

        CleanPolyLine boundingBox = new CleanPolyLine();
        boundingBox.setStart(minValues[0], minValues[1], 0f);
        boundingBox.createSegmentToHere(maxValues[0], minValues[1], 0f);
        boundingBox.createSegmentToHere(maxValues[0], maxValues[1], 0f);
        boundingBox.createSegmentToHere(minValues[0], maxValues[1], 0f);
        boundingBox.close();
        Tools.offsetClosedLinearShape(boundingBox, d);

        return boundingBox;
    }

    public static<T extends Point> void updateBoundingBox(final CleanPolyLine boundingBox,
                                                          final float d,
                                                          final ArrayList<T> vertices) {
        if (null == boundingBox) return;

        float[] maxValues = getMaxCoordinatesValues(vertices);
        float[] minValues = getMinCoordinatesValues(vertices);

        ArrayList<Vertex> verticesBB = boundingBox.getVertices();
        verticesBB.get(0).setCoordinates(minValues[0], minValues[1], 0f);
        verticesBB.get(1).setCoordinates(maxValues[0], minValues[1], 0f);
        verticesBB.get(2).setCoordinates(maxValues[0], maxValues[1], 0f);
        verticesBB.get(3).setCoordinates(minValues[0], maxValues[1], 0f);
        offsetClosedLinearShape(boundingBox, d);
        boundingBox.verticesWereMoved();
    }
    //*/
}
