package com.cimat.solidbox.Utilities.Preprocess;

import com.cimat.solidbox.OpenGL.Utilities.Colors;
import com.cimat.solidbox.Utilities.Constants;

/**
 * Created by ernesto on 31/05/17.
 */

public class Material {
    private Constants.MaterialType material;
    private DrawingProperties drawingProperties;

    private void initializeVariables() {
        material = Constants.MaterialType.IRON;
        drawingProperties = new DrawingProperties();
        drawingProperties.useColor();
    }

    public Material() {
        initializeVariables();
    }

    private void setMaterial(Constants.MaterialType material) {
        this.material = material;
        switch (material) {
            case IRON:
                drawingProperties.setColor(
                        Constants.IRON_COLOR[0],
                        Constants.IRON_COLOR[1],
                        Constants.IRON_COLOR[2]
                );
                break;
            case STEEL:
                drawingProperties.setColor(
                        Constants.STEEL_COLOR[0],
                        Constants.STEEL_COLOR[1],
                        Constants.STEEL_COLOR[2]
                );
                break;
            case ALUMINUM:
                drawingProperties.setColor(
                        Constants.ALUMINUM_COLOR[0],
                        Constants.ALUMINUM_COLOR[1],
                        Constants.ALUMINUM_COLOR[2]
                );
                break;
            case CONCRETE:
                drawingProperties.setColor(
                        Constants.CONCRETE_COLOR[0],
                        Constants.CONCRETE_COLOR[1],
                        Constants.CONCRETE_COLOR[2]
                );
                break;
        }
    }

    public Constants.MaterialType getMaterial() {
        return material;
    }
    public Colors.ColorRGB getColor() {
        return drawingProperties.getColor();
    }
    public boolean needsTexturing() {
        return drawingProperties.needsTexturing();
    }
}
