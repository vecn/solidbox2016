package com.cimat.solidbox.Utilities.Preprocess;

//import com.cimat.solidbox.Utilities.Constants.Texture;

import com.cimat.solidbox.OpenGL.Objects.ObjectBuilder;
import com.cimat.solidbox.OpenGL.Utilities.Colors;
import com.cimat.solidbox.Utilities.Constants;

/**
 * Created by ernesto on 7/05/17.
 */

public class DrawingLineProperties {
    DrawingProperties drawingProperties;

    private float thicknessNDC;
    private float lineSegmentLengthNDC;
    private boolean typeOfLineChanged;

    private void initializeVariables() {

        drawingProperties = new DrawingProperties();
        drawingProperties.setColor(
                Constants.DEFAULT_LINE_COLOR[0],
                Constants.DEFAULT_LINE_COLOR[1],
                Constants.DEFAULT_LINE_COLOR[2]
        );
        drawingProperties.setSelectedColor(
                Constants.DEFAULT_LINE_SELECTED_COLOR[0],
                Constants.DEFAULT_LINE_SELECTED_COLOR[1],
                Constants.DEFAULT_LINE_SELECTED_COLOR[2]
        );
        drawingProperties.useColor();

        thicknessNDC = Constants.DEFAULT_LINE_THICKNESS_NDC;
        lineSegmentLengthNDC = Constants.LINE_SEGMENT_LENGTH_NDC;
        typeOfLineChanged = false;
    }

    public DrawingLineProperties() {
        initializeVariables();
    }

    public void copyValues(final DrawingLineProperties dlp) {
        // ............ MISSING...
        thicknessNDC = dlp.thicknessNDC;
        lineSegmentLengthNDC = dlp.lineSegmentLengthNDC;
        typeOfLineChanged = dlp.typeOfLineChanged;
    }

    public boolean needsTexturing() {
        return drawingProperties.needsTexturing();
    }

    public Constants.TextureName getTextureName() {
        return drawingProperties.getTexture();
    }

    public void setThickness(float thicknessNDC) {
        if (thicknessNDC < Constants.MINIMUM_LINE_THICKNESS_NDC)
            thicknessNDC = Constants.MINIMUM_LINE_THICKNESS_NDC;
        if (thicknessNDC > Constants.MAXIMUM_LINE_THICKNESS_NDC)
            thicknessNDC = Constants.MAXIMUM_LINE_THICKNESS_NDC;

        this.thicknessNDC = thicknessNDC;
    }

    public float getThicknessNDC() {
        return thicknessNDC;
    }
    public float getLineSegmentLengthNDC() { return lineSegmentLengthNDC; }

    public void setColor(float r, float g, float b) {
        drawingProperties.setColor(r, g, b);
    }

    public final Colors.ColorRGB getColor() {
        return drawingProperties.getColor();
    }

    public final Colors.ColorRGB getColorWhenSelected() {
        return drawingProperties.getSelectedColor();
    }

    public void setSolid() {
        drawingProperties.setTexture(Constants.TextureName.NONE);
        drawingProperties.useTexture();
        typeOfLineChanged = true;
    }

    public void setDashed6Div() {
        drawingProperties.setTexture(Constants.TextureName.DASHED_LINE_6_DIV);
        drawingProperties.useTexture();
        typeOfLineChanged = true;
    }

    public void setHockeyTable() {
        drawingProperties.setTexture(Constants.TextureName.HOCKEY_TABLE);
        drawingProperties.useTexture();
        typeOfLineChanged = true;
    }

    public boolean hasLineTypeBeenChanged() {
        return typeOfLineChanged;
    }
    public void commitLineTypeChange() {
        typeOfLineChanged = false;
    }
}
