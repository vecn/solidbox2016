package com.cimat.solidbox.Utilities.Preprocess;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

import com.cimat.solidbox.Utilities.Preprocess.Geometry.Shape;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.InteractivePoint;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.LinearShape;
import com.cimat.solidbox.Utilities.Preprocess.Geometry.SurfaceShape;

/**
 * Created by ernesto on 2/02/16.
 */
public class GeometrySet {

    // This will assign a unique id to each shape
    private ShapesSet shapes;

    // Set of interactive points
    private ArrayList<InteractivePoint> interactivePoints;

    // To handle the shape's depth
    private ArrayList<Shape> depthList;

    // This is used to keep a reference from every element. Sometimes
    // it is necessary to iterate over them without no matter if they
    // have a parent shape!!
    //private ArrayList<Vertex> vertices;
    private ArrayList<LinearShape> linearShapes;
    private ArrayList<SurfaceShape> surfaceShapes;

    public GeometrySet() {
        shapes = new ShapesSet();
        interactivePoints = new ArrayList();
        //vertices = new ArrayList();
        depthList = new ArrayList();
        linearShapes = new ArrayList();
        surfaceShapes = new ArrayList();
    }

    public void addShape(Shape shape) {
        if (null == shape) return;
        handleDepthWhenInsertingAShape(shape);
        // Adds a unique id for the shape
        shapes.addElement(shape);
    }

    /*
    public void addLinearShape(LinearShape linearShape) {
        if (null == linearShape) return;
        handleDepthWhenInsertingAShape(linearShape);
        // To get a unique id
        shapes.addElement(linearShape);
        // Then we ask to the shape to add the important elements
        // to geometry
        linearShape.addElementsToGeometry(this);
    }

    public void addSurfaceShape(SurfaceShape surfaceShape) {
        if (null == surfaceShape) return;
        handleDepthWhenInsertingAShape(surfaceShape);
        // To get a unique id
        shapes.addElement(surfaceShape);
        // Then we ask to the shape to add the important elements
        // to geometry
        surfaceShape.addElementsToGeometry(this);
    }
    //*/

    public void addInteractivePoints(ArrayList<InteractivePoint> ips) {
        if (null != ips) interactivePoints.addAll(ips);
    }

    public void addInteractivePoint(InteractivePoint ip) {
        if (null != ip) interactivePoints.add(ip);
    }

    public void removeInteractivePoints(ArrayList<InteractivePoint> ips) {
        if (null != ips) interactivePoints.removeAll(ips);
    }

    public void removeInteractivePoint(InteractivePoint ip) {
        if (null != ip) interactivePoints.remove(ip);
    }

    /*
    public void addVertices(ArrayList<Vertex> vtx) {
        if (null != vertices) vertices.addAll(vtx);
    }
    //*/

    /*
    public void removeVertices(ArrayList<Vertex> vtx) {
        if (null != vtx) vertices.removeAll(vtx);
    }
    //*/

    public void addLinearShapes(ArrayList<LinearShape> ls) {
        if (null != ls) linearShapes.addAll(ls);
    }

    public void removeLineaShapes(ArrayList<LinearShape> ls) {
        if (null != ls) linearShapes.removeAll(ls);
    }

    public void addLinearShape(LinearShape linearShape) {
        if (null != linearShape) linearShapes.add(linearShape);
    }

    public void removeLinearShape(LinearShape linearShape) {
        if (null != linearShape) linearShapes.remove(linearShape);
    }

    public void addSurfaceShape(SurfaceShape surface) {
        if (null != surface) surfaceShapes.add(surface);
    }

    public void removeSurfaceShape(SurfaceShape surface) {
        if (null != surface) surfaceShapes.remove(surface);
    }

    public void deleteShape(Shape shape) {
        if (null == shape) return;
        shapes.removeElement(shape);
        handleDepthWhenRemovingAShape(shape);
    }

    public ArrayList<InteractivePoint> getInteractivePoints() {
        return interactivePoints;
    }
    //public ArrayList<Vertex> getVertices() { return vertices; }
    public ArrayList<LinearShape> getLinearShapes() { return linearShapes; }
    public ArrayList<SurfaceShape> getSurfaceShapes() { return surfaceShapes; }
    public CopyOnWriteArrayList<Shape> getShapes() {
        return shapes.getElements();
    }

    // For a given shape the depth and its corresponding index in
    // depthList must have the same values. This is way is easier
    // to handle the depth values when inserting, removing or
    // switching shapes.
    private void handleDepthWhenInsertingAShape(Shape shape) {
        if (null == shape) return;
        shape.setDepth(depthList.size());
        depthList.add(shape);
    }

    private void handleDepthWhenRemovingAShape(Shape shape) {
        if (null == shape) return;
        Shape s;
        Iterator<Shape> it = depthList.iterator();
        while (it.hasNext()) {
            s = it.next();
            if (s == shape) {
                it.remove();
                break;
            }
        }

        while (it.hasNext()) {
            s = it.next();
            s.setDepth(s.getDepth() - 1);
        }
    }

    public boolean writeToFile(DataOutputStream dos) {
        // MISSING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return true;
    }

    public boolean readFromFile(DataInputStream dis) {
        // MISSING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return true;
    }

    public void clear() {
        // MISSING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
}
