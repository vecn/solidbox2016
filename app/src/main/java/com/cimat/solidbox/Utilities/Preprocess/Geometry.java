package com.cimat.solidbox.Utilities.Preprocess;

import android.opengl.GLES20;
import android.util.Log;

import com.cimat.solidbox.DataStructures.DataInterface;
import com.cimat.solidbox.DataStructures.ShapeTree;
import com.cimat.solidbox.OpenGL.Programs.ColorShaderProgram;
import com.cimat.solidbox.OpenGL.Programs.TextureShaderProgram;
import com.cimat.solidbox.OpenGL.Utilities.Colors;
import com.cimat.solidbox.OpenGL.Utilities.GLText;
import com.cimat.solidbox.OpenGL.Utilities.Mesh;
import com.cimat.solidbox.OpenGL.Utilities.OpenGLTextureLineSegmentHelper;
import com.cimat.solidbox.OpenGL.Utilities.OpenGLineSegmentHelper;
import com.cimat.solidbox.OpenGL.Utilities.TouchHelper;
import com.cimat.solidbox.OpenGL.Utilities.Utilities;
import com.cimat.solidbox.OpenGL.Utilities.VisualObjectsCollection;
import com.cimat.solidbox.OpenGL.Utilities.Utilities.Texture;
import com.cimat.solidbox.Utilities.Constants;
import com.cimat.solidbox.Utilities.Constants.TextureName;
import com.cimat.solidbox.Utilities.Constants.DragStatus;
import com.cimat.solidbox.Utilities.Tools;
import com.cimat.solidbox.Utilities.UndoRedo;
import com.cimat.solidbox.OpenGL.Utilities.TouchHelper.Drags;
import com.cimat.solidbox.OpenGL.Utilities.TouchHelper.Drag;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import static android.opengl.GLES20.GL_LINES;
import static android.opengl.GLES20.glBlendFunc;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glEnable;

/**
 * Created by ernesto on 18/05/17.
 */

public class Geometry {

    /*********************************** Auxiliary classes ****************************************/

    /*
    public static class SurfaceProperties {
        private boolean isItPartOfASurface;
        //private SurfaceShape surface;

        public SurfaceProperties() {
            isItPartOfASurface = false;
            //surface = null;
        }

        public boolean isItPartOfASurface() {
            return isItPartOfASurface;
        }

        public SurfaceShape getSurface() {
            return surface;
        }

        public void setSurface(SurfaceShape surface) {
            isItPartOfASurface = true;
            this.surface = surface;
        }

        public void reset() {
            isItPartOfASurface = false;
            surface = null;
        }
    }
    //*/

    public static class PunctualForceProperties {
        private Conditions.PunctualForce punctualForce;
        boolean isIfPartOfAPunctualForce;

        private void initializeVariables() {
            punctualForce = null;
            isIfPartOfAPunctualForce = false;
        }

        public PunctualForceProperties() {
            initializeVariables();
        }

        public void setPunctualForce(Conditions.PunctualForce punctualForce) {
            isIfPartOfAPunctualForce = true;
            this.punctualForce = punctualForce;
        }

        public boolean isItPartOfAPunctualForce() {
            return isIfPartOfAPunctualForce;
        }

        public Conditions.PunctualForce getPunctualForce() {
            return punctualForce;
        }
    }

    public static class Vector {
        public float x, y, z;

        public Vector(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector() {
            x = y = z = 0f;
        }

        public float length() {
            return (float) Math.sqrt(x * x + y * y + z * z);
        }

        public Vector crossProduct(Vector other) {
            return new Vector(
                    (y * other.z) - (z * other.y),
                    (z * other.x) - (x * other.z),
                    (x * other.y) - (y * other.x));
        }

        public float dotProduct(Vector other) {
            return x * other.x + y * other.y + z * other.z;
        }

        public Vector scale(float f) {
            x *= f;
            y *= f;
            z *= f;
            return this;
        }

        public void setComponents(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public void setComponents(Vector v) {
            this.x = v.x;
            this.y = v.y;
            this.z = v.z;
        }

        public void normalize() {
            float length = length();
            if (1e-20 > length()) return;

            x /= length;
            y /= length;
            z /= length;
        }

        public Vector getUnitNormalVector(Vector up) {
            Vector n = crossProduct(up);

            if (1e-20 > n.length()) n.setComponents(0f, 0f, 0f);
            else                    n.normalize();
            return n;
        }

        public void print(final String TAG, final String label) {
            Log.i(TAG, label + x + "   " + y + "   " + z);
        }

    }

    public static class Zoom {
        // NDC -> Normalized device coordinates
        // WSC -> World space coordinates
        private final float INVALID_DISTANCE = -1f;
        private float initialRadiusInNDC;
        private float initialScaleFactor;
        private Point zoomingCenterInNDC;
        private Point zoomingCenterInWSC;
        private boolean initialData;
        float[] displFromZoomingCenterToCentersView;

        public Zoom() {
            initialRadiusInNDC = 0f;
            zoomingCenterInNDC = null;
            zoomingCenterInWSC = null;
            displFromZoomingCenterToCentersView = new float[2];
            initialData = false;
        }

        public void setData(float initialScaleFactor,
                            float initialRadiusInNDC,
                            Point zoomingCenterInNDC,
                            Point zoomingCenterInWSC,
                            Point centersViewInWSC) {
            this.initialScaleFactor = initialScaleFactor;
            this.initialRadiusInNDC = initialRadiusInNDC;
            this.zoomingCenterInNDC = zoomingCenterInNDC;
            this.zoomingCenterInWSC = zoomingCenterInWSC;
            displFromZoomingCenterToCentersView[0] = zoomingCenterInWSC.x - centersViewInWSC.x;
            displFromZoomingCenterToCentersView[1] = zoomingCenterInWSC.y - centersViewInWSC.y;
            initialData = true;
        }

        public float getScaleFactorFromPointsInNDC(Point a, Point b) {
            float radius, c, scaleFactor;

            // When the data has not been set the scale factor does nothing
            if (!initialData) return 1f;

            radius = getRadiusModelA(a, b);

            // When the points a and b are not valid the scale factor does nothing
            if (radius < 0f) return 1f;

            //c = radius / initialRadiusInNDC;
            c = initialRadiusInNDC / radius;

            scaleFactor = Constants.ZOOMING_FACTOR / c * initialScaleFactor;
            if (scaleFactor > Constants.MAXIMUM_ZOOM) scaleFactor = Constants.MAXIMUM_ZOOM;
            if (scaleFactor < Constants.MINIMUM_ZOOM) scaleFactor = Constants.MINIMUM_ZOOM;

            return scaleFactor;
        }

        private float getRadiusModelA(Point aInNDC, Point bInNDC) {
            float radius = 0.5f * Tools.distanceBetweenPoints(aInNDC, bInNDC);
            if (radius < 0) return INVALID_DISTANCE;
            return radius;
        }

        private float getRadiusModelB(Point aInNDC, Point bInNDC) {
            float radiusA, radiusB;

            if (!initialData) return INVALID_DISTANCE;

            radiusA = Tools.distanceBetweenPoints(aInNDC, zoomingCenterInNDC);
            radiusB = Tools.distanceBetweenPoints(bInNDC, zoomingCenterInNDC);

            // Invalid radius
            if ((radiusA < 0) || (radiusB < 0)) return INVALID_DISTANCE;

            return 0.5f * (radiusA + radiusB);
        }

        public Point getZoomingCenterInNDC() {
            return zoomingCenterInNDC;
        }

        public Point getZoomingCenterInWSC() {
            return zoomingCenterInWSC;
        }

        public float getDisplacementToCentersViewOnX() {
            return displFromZoomingCenterToCentersView[0];
        }

        public float getDisplacementToCentersViewOnY() {
            return displFromZoomingCenterToCentersView[1];
        }
    }

    public static class SimpleCircle {
        public Point center;
        public float radius;

        public SimpleCircle(Point center, float radius) {
            this.center = center;
            this.radius = radius;
        }

        public SimpleCircle() {
            center = new Point(0f, 0f, 0f);
            radius = 0f;
        }

        public void setCenter(float cx, float cy, float cz) {
            center.setCoordinates(cx, cy, cz);
        }

        public void setRadius(float radius) {
            this.radius = radius;
        }

        public SimpleCircle scale(float scale) {
            return new SimpleCircle(center, radius * scale);
        }
    }

    public static class Triangle {
        private ArrayList<Point> vertices;

        private void initializeVariables() {
            vertices = new ArrayList<>();
            vertices.add(new Point(0f, 0f, 0f));
            vertices.add(new Point(0f, 0f, 0f));
            vertices.add(new Point(0f, 0f, 0f));
        }

        public Triangle() {
            initializeVariables();
        }

        public void copyCoordinates(int i, Point p) {
            if (i < 0) return;
            if (i > 2) return;

            vertices.get(i).setCoordinates(p.x, p.y, p.z);
        }

        public Point getVertex(int i) {
            if (i < 0 || i > 2) return null;
            return vertices.get(i);
        }
    }

    public static class Ray {
        //public final SimplePoint point;
        //public final Vector vector;
        public final Point point;
        public final Vector vector;

        public Ray(Point point, Vector vector) {
            this.point = point;
            this.vector = vector;
        }
    }

    public static class LineSegmentHelperFlags {
        public boolean buildData;
        public boolean updateVertices;

        public LineSegmentHelperFlags(boolean buildData, boolean updateVertices) {
            this.buildData = buildData;
            this.updateVertices= updateVertices;
        }
    }

    private static class DiscretizedLinearShape {
        private ArrayList<ArrayList<Point>> verticesPerShape;
        private LinearShape shape;
        private float dt;

        private void initializeVariables() {
            verticesPerShape = new ArrayList<>();
            shape = null;
            dt = 0.1f;
        }

        public DiscretizedLinearShape(LinearShape shape) {
            initializeVariables();
            this.shape = shape;
        }

        public void build(final float dt) {
            if (null == shape) return;
            this.dt = dt;

            Iterator<BezierCurve> it = shape.getCurves().iterator();
            while (it.hasNext()) verticesPerShape.add(it.next().createVertices(dt));
        }

        public void rebuildCurve(BezierCurve curve) {
            if (null == curve) return;

            int count = 0;
            for (BezierCurve c : shape.getCurves()) {
                if (c == curve) {
                    verticesPerShape.get(count).clear();
                    verticesPerShape.get(count).addAll(c.createVertices(dt));
                    break;
                }
                count++;
            }
        }

        public ArrayList<Point> getVertices() {
            ArrayList<Point> vertices = new ArrayList<>();
            Iterator<ArrayList<Point>> it = verticesPerShape.iterator();
            if (it.hasNext()) {
                ArrayList<Point> v = it.next();
                vertices.addAll(v);

                Iterator<Point> itP;
                while (it.hasNext()) {
                    v = it.next();
                    itP = v.iterator();
                    if (itP.hasNext()) itP.next();
                    while (itP.hasNext()) vertices.add(itP.next());
                }
            }
            return vertices;
        }
    }

    /****************************************    Shapes    ****************************************/

    public interface ShapeInterface {
        // Methods that any shape must support
        void draw(ColorShaderProgram colorProgram,
                  TextureShaderProgram textureProgram,
                  GLText glText,
                  float[] viewProjectionMatrix,
                  float[] modelViewProjectionMatrix,
                  VisualObjectsCollection visualObjects,
                  Map<Constants.TextureName, Utilities.Texture> texturesID,
                  float unitLengthNDC);
        void handleDragInteractivePoint(InteractivePoint ip, float x, float y);
        void setId(int id);
        int getId();
        void select();
        void unSelect();
        void setDepth(int depth);
        int getDepth();
        float getOpenGLDepth();
        void translate(float dx, float dy, float dz);
        boolean areYouSelected(float x, float y, float selectionDistance);
        void handleTouchPress(float x, float y, float selectionDistance, UndoRedo undoRedo);
        DragStatus handleTouchDrag(Drags drags,
                                   float invertedViewProjectionMatrix,
                                   UndoRedo undoRedo);
        ArrayList<InteractivePoint> detectSelectedInteractivePoints(float x, float y, float r);
    }

    public static abstract class Shape implements DataInterface, ShapeInterface {
        private int id;
        private int depth;
        private ShapeTree shapeTree;
        private boolean isSelected;

        private void initializeVariables() {
            id = Constants.INVALID_ID;
            depth = 0;
            shapeTree = new ShapeTree();
            isSelected = false;
        }

        public Shape() {
            initializeVariables();
            //shapeTree.addNode(this);// PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }

        public Shape(int id) {
            initializeVariables();
            //shapeTree.addNode(this);// PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            this.id = id;
        }

        public void setId(int id) { this.id = id; }
        public int getId() {
            return id;
        }

        public void setDepth(int depth) { this.depth = depth; }
        public int getDepth() { return depth; }
        public float getOpenGLDepth() {
            return 1f/((float)depth + 1);
        }

        public void select() { isSelected = true; }
        public void unSelect() { isSelected = false; }
        public boolean isSelected() { return isSelected; }
    }

    // Points shapes -------------------------------------------------------------------------------

    // Basic shape !!!
    public static class Point {
        public float x, y, z;

        public Point(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Point(Point simplePoint) {
            this.x = simplePoint.x;
            this.y = simplePoint.y;
            this.z = simplePoint.z;
        }

        public Point() {
            x = y = z = 0f;
        }

        public Point translate(float dx, float dy, float dz) {
            x += dx;
            y += dy;
            z += dz;
            return this;
        }

        public Point translate(Vector v) {
            x += v.x;
            y += v.y;
            z += v.z;
            return this;
        }

        public void setCoordinates(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public void setCoordinates(final Point p) {
            x = p.x;
            y = p.y;
            z = p.z;
        }

        public void print(final String TAG, final String label) {
            Log.i(TAG, label + x + "   " + y + "   " + z);
        }

        public float getX() { return x; }
        public float getY() { return y; }
        public float getZ() { return z; }
    }

    public interface PointShapeInterface {
        void setCoordinates(float x, float y, float z);
        void translate(float dx, float dy, float dz);
        float getX();
        float getY();
        float getZ();
        Point getPoint();
        boolean areYouSelected(float x, float y, float radius);
    }

    /*
    public static class Vertex extends Shape implements PointShapeInterface {
        Point p;

        private boolean hasAppliedForce;
        private boolean hasFixedDisplacement;

        private Conditions.PunctualForce force;
        private Conditions.PunctualFixedDisplacement fixedDisplacement;

        private void initializeVariables() {
            hasAppliedForce = false;
            hasFixedDisplacement = false;
            force = null;
            fixedDisplacement = null;
        }

        public Vertex() {
            super();
            initializeVariables();
        }

        public Vertex(int id) {
            super(id);
            initializeVariables();
        }

        public void setCoordinates(float x, float y, float z) {
            p.setCoordinates(x, y, z);
            if (hasAppliedForce) force.itsVertexWasMoved();
            if (hasFixedDisplacement) fixedDisplacement.itsVertexWasMoved();
        }

        public void translate(float dx, float dy, float dz) {
            p.translate(dx, dy, dz);
            if (hasAppliedForce) force.translate(dx, dy, dz);
            if (hasFixedDisplacement) fixedDisplacement.translate(dx, dy, dz);
        }

        public float getX() { return p.x; }
        public float getY() { return p.y; }
        public float getZ() { return p.z; }

        private void createForce() {
            if (null == force) {
                force = new Conditions.PunctualForce();
                //force.setVertex(this); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            }
        }

        public void clearForce() {
            //if (null != force) force.clear(); // PENDING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            hasAppliedForce = false;
        }

        public void setForce(float value) {
            createForce();
            hasAppliedForce = true;
            force.setValue(value);
        }

        private void createDisplacement() {
            if (null == fixedDisplacement) {
                fixedDisplacement = new Conditions.PunctualFixedDisplacement();
                // fixedDisplacement.setData(this, 0.25f); // Pending!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            }
        }

        public void clearFixedDisplacement() {
            if (null != fixedDisplacement) fixedDisplacement.clear();
            hasFixedDisplacement = false;
        }

        public void setFixedDisplacement(Conditions.FixedDisplacement displacement) {
            createDisplacement();

            hasFixedDisplacement = false;
            if (displacement.isFixedX() || displacement.isFixedY() || displacement.isFixedZ()) {
                clearFixedDisplacement();
                fixedDisplacement.setDisplacement(displacement);
                hasFixedDisplacement = true;
            }
        }

        public void deleteBoundaryConditions(GeometrySet geometry) {
            if (null != force) geometry.deleteShape(force);
            if (null != fixedDisplacement) geometry.deleteShape(fixedDisplacement);
            clearForce();
            clearFixedDisplacement();
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<Constants.TextureName, Utilities.Texture> texturesID,
                         float unitLengthNDC) {

            if (hasAppliedForce) {
                force.setDepth(getDepth());
                force.draw(
                        colorProgram,
                        textureProgram,
                        glText,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        visualObjects,
                        null,
                        unitLengthNDC
                );
            }

            if (hasFixedDisplacement) {
                // PENDING THE BOUNDARY HAS TO BE A SHAPE ............................................
            }

        }

        public Conditions.PunctualForce getForce() { return force; }
        public Conditions.PunctualFixedDisplacement getFixedDisplacement() {
            return fixedDisplacement;
        }
        public boolean hasAppliedForce() { return hasAppliedForce; }
        public boolean hasFixedDisplacement() {
            return hasFixedDisplacement;
        }
    }
    //*/

    public static abstract class InteractivePoint implements PointShapeInterface {
        private Point p;
        private Shape parentShape;

        private void initializeVariables() {
            p = new Point(0f, 0f, 0f);
            parentShape = null;
        }

        public InteractivePoint(Shape parentShape) {
            super();
            initializeVariables();
            this.parentShape = parentShape;
        }

        public void setCoordinates(float x, float y, float z) {
            p.setCoordinates(x, y, z);
        }

        public void translate(float dx, float dy, float dz) {
            p.translate(dx, dy, dz);
        }

        public float getX() { return p.x; }
        public float getY() { return p.y; }
        public float getZ() { return p.z; }
        public Point getPoint() { return p; }
        public Shape getParentShape() { return  parentShape; }

        public boolean areYouSelected(float x, float y, float radius) {
            if (Math.sqrt((x - p.x)*(x - p.x) + (y - p.y)*(y - p.y)) < radius) return true;
            return false;
        }
    }

    public static class ControlPoint extends InteractivePoint {

        public ControlPoint(Shape parentShape) {
            super(parentShape);
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<Constants.TextureName, Utilities.Texture> texturesID,
                         float unitLengthNDC,
                         float depth) {

            colorProgram.useProgram();
            Utilities.drawPoint(colorProgram,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    visualObjects.getInteractivePoint(),
                    super.p,
                    depth,
                    0.22f, 0.22f, 0.78f);
        }
    }

    // Linear shapes -------------------------------------------------------------------------------

    // Basic shape !!!
    // Only supports linear, quadratic and cubic bezier curves

    public interface BezierCurveInterface {
        float getLength();
        Point getCenter();
        void offset(final float d);
    }

    public static abstract class BezierCurve extends Shape implements BezierCurveInterface {
        private int order;
        private float[] weightsX;
        private float[] weightsY;
        private float[] weightsZ;
        private boolean modifiable;
        //private boolean selected;

        public DrawingLineProperties drawingLineProperties;

        private boolean buildVertices;
        private ArrayList<Point> vertices;

        private LineSegmentHelperFlags lineHelper;
        private OpenGLineSegmentHelper openGLineSegmentHelper;

        private LineSegmentHelperFlags textureLineHelper;
        private OpenGLTextureLineSegmentHelper openGLTextureLineSegmentHelper;

        private void initializeVariables() {
            order = 0;
            weightsX = weightsY = weightsZ = null;
            modifiable = false;
            //selected = false;

            drawingLineProperties = null;

            buildVertices = true;
            vertices = new ArrayList<>();

            lineHelper = new LineSegmentHelperFlags(true, false);
            openGLineSegmentHelper = new OpenGLineSegmentHelper();

            textureLineHelper = new LineSegmentHelperFlags(true, false);
            openGLTextureLineSegmentHelper = new OpenGLTextureLineSegmentHelper();
        }

        public BezierCurve(int order) {
            initializeVariables();

            if (0 >= order) return;
            this.order = order;
            weightsX = new float[order+1];
            weightsY = new float[order+1];
            weightsZ = new float[order+1];

            for (int i = 0; i < order + 1; i++)
                weightsX[i] = weightsY[i] = weightsZ[i] = 0f;
        }

        public int getOrder() {
            return order;
        }

        protected void setAsModifiable() { this.modifiable = true; }
        public boolean isModifiable() { return  modifiable; }

        public void setDrawingLineProperties(DrawingLineProperties rhs) {
            drawingLineProperties = rhs;
        }

        protected void setWeightsOnX(float v1, float v2) {
            if (1 != order) return;
            weightsX[0] = v1;
            weightsX[1] = v2;

            buildVertices = true;
            lineHelper.buildData = true;
            textureLineHelper.buildData = true;
        }

        protected void setWeightsOnY(float v1, float v2) {
            if (1 != order) return;
            weightsY[0] = v1;
            weightsY[1] = v2;

            buildVertices = true;
            lineHelper.buildData = true;
            textureLineHelper.buildData = true;
        }

        protected void setWeightsOnX(float v1, float v2, float v3, float v4) {
            if (order != 3) return;
            weightsX[0] = v1;
            weightsX[1] = v2;
            weightsX[2] = v3;
            weightsX[3] = v4;

            buildVertices = true;
            lineHelper.buildData = true;
            textureLineHelper.buildData = true;
        }

        protected void setWeightsOnY(float v1, float v2, float v3, float v4) {
            if (order != 3) return;
            weightsY[0] = v1;
            weightsY[1] = v2;
            weightsY[2] = v3;
            weightsY[3] = v4;

            buildVertices = true;
            lineHelper.buildData = true;
            textureLineHelper.buildData = true;
        }

        protected void setWeight(int i, float x, float y, float z) {
            if (0 == order) return;
            if (i < 0 || i > order) return;
            weightsX[i] = x;
            weightsY[i] = y;
            weightsZ[i] = z;

            buildVertices = true;
            lineHelper.buildData = true;
            textureLineHelper.buildData = true;
        }

        public void setStart(float x, float y, float z) {
            if (0 == order) return;
            setWeight(0, x, y, z);
        }

        public void setEnd(float x, float y, float z) {
            if (0 == order) return;
            setWeight(order, x, y, z);
        }

        private float calculateComponent(float t, float[] w) {
            switch (order) {
                case 1: {
                    return w[0] + (w[1] - w[0]) * t;
                }
                case 2: {
                    float t2 = t * t;
                    float mt = 1 - t;
                    float mt2 = mt * mt;
                    return w[0] * mt2 + w[1] * 2 * mt * t + w[2] * t2;
                }
                case 3: {
                    float t2 = t * t;
                    float t3 = t2 * t;
                    float mt = 1 - t;
                    float mt2 = mt * mt;
                    float mt3 = mt2 * mt;
                    return w[0] * mt3 + 3 * w[1] * mt2 * t + 3 * w[2] * mt * t2 + w[3] * t3;
                }
            }
            return 0f;
        }

        public Point getPointOnCurve(float t) {
            Point p = new Point(0f, 0f, 0f);
            if (0 == order) return p;

            p.x = calculateComponent(t, weightsX);
            p.y = calculateComponent(t, weightsY);
            p.z = calculateComponent(t, weightsZ);

            return p;
        }

        public Point getStart() {
            return getPointOnCurve(0);
        }
        public Point getEnd() {
            return getPointOnCurve(1);
        }

        private void switchStartEnd(float[] w) {
            if (0 == order) return;
            if (null == w) return;

            float aux = w[0];
            w[0] = w[order-1];
            w[order-1] = aux;
        }

        protected void switchStartEnd() {
            switchStartEnd(weightsX);
            switchStartEnd(weightsY);
            switchStartEnd(weightsZ);
        }

        public void translate(float dx, float dy, float dz) {
            for (int i = 0; i < order + 1; i++) {
                weightsX[i] += dx;
                weightsY[i] += dy;
                weightsZ[i] += dz;
            }

            if (null != vertices) {
                for (Point p : vertices) p.translate(dx, dy, dz);
            }

            lineHelper.updateVertices = true;
            textureLineHelper.updateVertices = true;
        }

        private ArrayList<Point> createVertices(final float dt) {
            ArrayList<Point> vertices = new ArrayList<>();
            if (0 == order) return vertices;

            if (1 == order) {
                vertices.add(getPointOnCurve(0));
                vertices.add(getPointOnCurve(1));
            } else {
                float t = 0f;
                while (t < 1f) {
                    vertices.add(getPointOnCurve(t));
                    t += dt;
                }
                vertices.add(new Point(getPointOnCurve(1)));
            }

            return vertices;
        }

        public ArrayList<Point> getVertices(float unitLengthNDC) {
            if (buildVertices) {
                float lineSegmentLength =
                        drawingLineProperties.getLineSegmentLengthNDC() / unitLengthNDC;
                float dt = lineSegmentLength / getLength();
                vertices = createVertices(dt);
                buildVertices = false;
            }
            return vertices;
        }

        public ArrayList<Point> getVertices() {
            return vertices;
        }

        public Point getCenter() {
            Point center = new Point(0f, 0f, 0f);
            if (order == 0) return center;

            for (int i = 0; i < order + 1; i++) {
                center.x += weightsX[i];
                center.y += weightsY[i];
                center.z += weightsZ[i];
            }
            center.x /= 3f;
            center.y /= 3f;
            center.z /= 3f;

            return center;
        }

        public boolean areYouSelected(float x, float y, float selectionDistance) {
            if (null == vertices) return false;

            Point touchPoint = new Point(x, y, 0f);
            Ray ray = new Ray(new Point(0f, 0f, 0f), new Vector(0f, 0f, 0f));
            float distance;

            Point v1, v2;
            Iterator<Point> it = vertices.iterator();
            if (it.hasNext()) {
                v1 = it.next();
                while (it.hasNext()) {
                    v2 = it.next();

                    ray.point.setCoordinates(v1.x, v1.y, v1.z);
                    ray.vector.setComponents(
                            v2.x - v1.x,
                            v2.y - v1.y,
                            v2.z - v1.z );

                    distance = Tools.getDistanceToRay(touchPoint, ray);
                    if (distance < selectionDistance) return true;

                    v1 = v2;
                }
            }
            return false;
        }

        private void drawLineSegments(ColorShaderProgram colorProgram,
                                      float[] viewProjectionMatrix,
                                      float[] modelViewProjectionMatrix,
                                      float depth) {

            if (lineHelper.buildData) {
                openGLineSegmentHelper.setData(vertices, false);
                openGLineSegmentHelper.buildOpenGLData(GL_LINES);
                lineHelper.buildData = false;
            }

            if (lineHelper.updateVertices) {
                openGLineSegmentHelper.updateVertexArray();
                lineHelper.updateVertices = false;
            }

            Colors.ColorRGB color = drawingLineProperties.getColor();
            if (modifiable && isSelected())
                color = drawingLineProperties.getColorWhenSelected();

            colorProgram.useProgram();
            Utilities.drawLineSegments(
                    colorProgram,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    openGLineSegmentHelper.getVertexArray(),
                    openGLineSegmentHelper.getDrawList(),
                    depth,
                    color.getRed(),
                    color.getGreen(),
                    color.getBlue()
            );
        }

        private void drawLineSegments(TextureShaderProgram textureProgram,
                                      float[] viewProjectionMatrix,
                                      float[] modelViewProjectionMatrix,
                                      Map<TextureName, Texture> texturesID,
                                      float unitLengthNDC,
                                      float depth) {

            Texture texture = null;
            if (texturesID.containsKey(drawingLineProperties.getTextureName()))
                texture = texturesID.get(drawingLineProperties.getTextureName());
            if (null == texture) return;

            if (textureLineHelper.buildData) {
                float thickness = drawingLineProperties.getThicknessNDC()/unitLengthNDC;
                openGLTextureLineSegmentHelper.setData(thickness,
                        vertices,
                        texture.ratio,
                        false);
                openGLTextureLineSegmentHelper.buildOpenGLData();
                textureLineHelper.buildData = false;
            }

            if (textureLineHelper.updateVertices) {
                openGLTextureLineSegmentHelper.updateVertexArray();
                textureLineHelper.updateVertices = false;
            }

            Colors.ColorRGB color = drawingLineProperties.getColor();
            if (modifiable && isSelected())
                color = drawingLineProperties.getColorWhenSelected();

            glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            glEnable(GLES20.GL_BLEND);

            textureProgram.useProgram();
            Utilities.drawTextures(
                    textureProgram,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    openGLTextureLineSegmentHelper.getVertexArray(),
                    openGLTextureLineSegmentHelper.getTextureArray(),
                    openGLTextureLineSegmentHelper.getDrawList(),
                    depth,
                    texture.ID,
                    color
            );

            glDisable(GLES20.GL_BLEND);
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<Constants.TextureName, Utilities.Texture> texturesID,
                         float unitLengthNDC,
                         float depth) {

            if (null == drawingLineProperties) return;

            if (buildVertices) {
                vertices = getVertices(unitLengthNDC);
                buildVertices = false;
            }

            if (drawingLineProperties.needsTexturing())
                drawLineSegments(textureProgram, viewProjectionMatrix,
                        modelViewProjectionMatrix, texturesID,
                        unitLengthNDC, depth);
            else
                drawLineSegments(colorProgram, viewProjectionMatrix,
                        modelViewProjectionMatrix, depth);

        }
    }

    public static class LinearBezierCurve extends BezierCurve {

        private ControlPoint start, end;

        private void initializeVariables() {
            start = new ControlPoint(this);
            end = new ControlPoint(this);
        }

        public LinearBezierCurve(boolean modifiable) {
            super(1);
            initializeVariables();
            if (modifiable) super.setAsModifiable();
        }

        public void setWeightsOnX(float v1, float v2) {
            start.setCoordinates(v1, start.getY(), start.getZ());
            end.setCoordinates(v2, end.getY(), end.getZ());
            super.setWeightsOnX(v1, v2);
        }

        public void setWeightsOnY(float v1, float v2) {
            start.setCoordinates(start.getX(), v1, start.getZ());
            end.setCoordinates(end.getX(), v2, end.getZ());
            super.setWeightsOnY(v1, v2);
        }

        public void setStart(float x, float y, float z) {
            super.setStart(x, y, z);
            start.setCoordinates(x, y, z);
        }

        public void setEnd(float x, float y, float z) {
            super.setEnd(x, y, z);
            end.setCoordinates(x, y, z);
        }

        public void switchStartEnd() {
            float x = start.getX();
            float y = start.getY();
            float z = start.getZ();

            start.setCoordinates(end.getX(), end.getY(), end.getZ());
            end.setCoordinates(x, y, z);

            super.switchStartEnd();
        }

        public void translate(float dx, float dy, float dz) {
            start.translate(dx, dy, dz);
            end.translate(dx, dy, dz);
            super.translate(dx, dy, dz);
        }

        public float getLength() {
            return Tools.distanceBetweenPoints(getStart(), getEnd());
        }

        public void handleTouchPress(float x,
                                     float y,
                                     float r,
                                     UndoRedo undoRedo) {
            if (!isSelected()) return;
        }

        public DragStatus handleTouchDrag(Drags drags,
                                          float invertedViewProjectionMatrix,
                                          UndoRedo undoRedo) {
            if (!isModifiable()) return DragStatus.NOTHING_DRAGGED;
            return DragStatus.NOTHING_DRAGGED;
        }

        public ArrayList<InteractivePoint> detectSelectedInteractivePoints(float x,
                                                                           float y,
                                                                           float r) {
            ArrayList<InteractivePoint> ips = new ArrayList<>();
            Point touchPoint = new Point(x, y, 0f);
            if (Tools.distanceBetweenPoints(touchPoint, start.getPoint()) < r)
                ips.add(start);
            else if (Tools.distanceBetweenPoints(touchPoint, end.getPoint()) < r)
                ips.add(end);
            return ips;
        }

        public void offset(final float d) {
            float dx = end.getX() - start.getX();
            float dy = end.getY() - start.getY();
            float dz = end.getZ() - start.getZ();

            Point a = new Point(
                    start.getX() - dx,
                    start.getY() - dy,
                    start.getZ() - dz);

            Point b = new Point(
                    end.getX() + dx,
                    end.getY() + dy,
                    end.getZ() + dz);

            Point offsetStart = Tools.offsetVertex2D(a, start.getPoint(), end.getPoint(), d);
            Point offsetEnd = Tools.offsetVertex2D(start.getPoint(), end.getPoint(), b, d);

            setStart(offsetStart.x, offsetStart.y, offsetStart.z);
            setEnd(offsetEnd.x, offsetEnd.y, offsetEnd.z);
        }

        public void handleDragInteractivePoint(InteractivePoint ip, float x, float y) {
            if      (ip == start) handleStartDrag(x, y);
            else if (ip == end) handleEndDrag(x, y);
        }

        private void handleStartDrag(float x, float y) {
            setStart(x, y, start.getZ());
        }

        private void handleEndDrag(float x, float y) {
            setEnd(x, y, end.getZ());
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<Constants.TextureName, Utilities.Texture> texturesID,
                         float unitLengthNDC) {
            super.draw(
                    colorProgram,
                    textureProgram,
                    glText,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    visualObjects,
                    texturesID,
                    unitLengthNDC,
                    getOpenGLDepth()
            );

            if (isModifiable() && isSelected()) {
                start.draw(
                        colorProgram,
                        textureProgram,
                        glText,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        visualObjects,
                        texturesID,
                        unitLengthNDC,
                        getOpenGLDepth()
                );
                end.draw(
                    colorProgram,
                    textureProgram,
                    glText,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    visualObjects,
                    texturesID,
                    unitLengthNDC,
                    getOpenGLDepth()
                );
            }
        }
    }

    public static class CubicBezierCurve extends BezierCurve {

        private ControlPoint start, end;
        private ControlPoint control1, control2;

        private void initializeVariables() {
            start = new ControlPoint(this);
            end = new ControlPoint(this);
            control1 = new ControlPoint(this);
            control2 = new ControlPoint(this);
        }

        public CubicBezierCurve() {
            super(3);
            initializeVariables();
        }

        public void setWeightsOnX(float v1, float v2, float v3, float v4) {
            start.setCoordinates(v1, start.getY(), start.getZ());
            control1.setCoordinates(v2, control1.getY(), control1.getZ());
            control2.setCoordinates(v3, control2.getY(), control2.getZ());
            end.setCoordinates(v4, end.getY(), end.getZ());
            super.setWeightsOnX(v1, v2, v3, v4);
        }

        public void setWeightsOnY(float v1, float v2, float v3, float v4) {
            start.setCoordinates(start.getX(), v1, start.getZ());
            control1.setCoordinates(control1.getX(), v2, control1.getZ());
            control2.setCoordinates(control2.getX(), v3, control2.getZ());
            end.setCoordinates(end.getX(), v4, end.getZ());
            super.setWeightsOnY(v1, v2, v3, v4);
        }

        public void setStart(float x, float y, float z) {
            start.setCoordinates(x, y, z);
            super.setStart(x, y, z);
        }

        public void setControl1(float x, float y, float z) {
            control1.setCoordinates(x, y, z);
            setWeight(1, x, y, z);
        }

        public void setControl2(float x, float y, float z) {
            control2.setCoordinates(x, y, z);
            setWeight(2, x, y, z);
        }

        public void setEnd(float x, float y, float z) {
            end.setCoordinates(x, y, z);
            super.setEnd(x, y, z);
        }

        public void switchStartEnd() {
            float x = start.getX();
            float y = start.getY();
            float z = start.getZ();

            start.setCoordinates(end.getX(), end.getY(), end.getZ());
            end.setCoordinates(x, y, z);

            super.switchStartEnd();
        }

        public void setAsStraightLine() {
            // To do this the start and end should be already defined
            float dx = end.getX() - start.getX();
            float dy = end.getY() - start.getY();
            float dz = end.getZ() - start.getZ();

            float oneThird = 1f/3f;
            float twoThird = 2f/3f;

            control1.setCoordinates(
                    start.getX() + oneThird*dx,
                    start.getY() + oneThird*dy,
                    start.getZ() + oneThird*dz);

            control2.setCoordinates(
                    start.getX() + twoThird*dx,
                    start.getY() + twoThird*dy,
                    start.getZ() + twoThird*dz);
        }

        public void translate(float dx, float dy, float dz) {
            start.translate(dx, dy, dz);
            end.translate(dx, dy, dz);
            control1.translate(dx, dy, dz);
            control2.translate(dx, dy, dz);
            super.translate(dx, dy, dz);
        }

        private float evaluateDerivative(float t, float[] w) {
            if (null == w) return 0f;
            if (t < 0f) t = 0f;
            if (t > 1f) t = 1f;
            return 3f*(1f - t)*(1f - t)*(w[1] - w[0]) + 6f*(1f - t)*t*(w[2] - w[1]) + 3f*t*t*(w[3] - w[2]);
        }

        private float evaluateFunctionLength(float t) {
            if (t < 0f) t = 0f;
            if (t > 1f) t = 1f;

            return (float)Math.sqrt(Math.pow(evaluateDerivative(t, super.weightsX), 2f) +
                            Math.pow(evaluateDerivative(t, super.weightsY), 2f) +
                            Math.pow(evaluateDerivative(t, super.weightsZ), 2f));
        }

        public float getLength() {
            final int n = 10;
            final float dt = 1f/(float)n;
            float length, t;

            // Line integral using Simpson 1/3
            length = evaluateFunctionLength(0f);
            for (int i = 1; i < n; i++) {
                t = (float)i*dt;
                if (0 == i%2) length += 2f*evaluateFunctionLength(t);
                else          length += 4f*evaluateFunctionLength(t);
            }
            length += evaluateFunctionLength(1f);
            length *= (1f - 0f)/(3f*n);

            return length;
        }

        public void handleTouchPress(float x,
                                     float y,
                                     float r,
                                     UndoRedo undoRedo) {

        }

        public DragStatus handleTouchDrag(Drags drags,
                                          float invertedViewProjectionMatrix,
                                          UndoRedo undoRedo) {
            if (!isModifiable()) return DragStatus.NOTHING_DRAGGED;
            return DragStatus.NOTHING_DRAGGED;
        }

        public ArrayList<InteractivePoint> detectSelectedInteractivePoints(float x,
                                                                           float y,
                                                                           float r) {
            ArrayList<InteractivePoint> ips = new ArrayList<>();
            Point touchPoint = new Point(x, y, 0f);
            if (Tools.distanceBetweenPoints(touchPoint, start.getPoint()) < r) {
                ips.add(start);
            } else if (Tools.distanceBetweenPoints(touchPoint, end.getPoint()) < r) {
                ips.add(end);
            } else {
                if (isSelected() && isModifiable()) {
                    if (Tools.distanceBetweenPoints(touchPoint, control1.getPoint()) < r)
                        ips.add(control1);
                    else if (Tools.distanceBetweenPoints(touchPoint, control2.getPoint()) < r)
                        ips.add(control2);
                }
            }
            return ips;
        }

        public void offset(final float d) {
            // This operation is not supported for the cubic
            // bezier curves. It can be done but the result
            // is not a unique bezier curve.
            // Reference: https://pomax.github.io/bezierinfo/#extremities
        }

        public void handleDragInteractivePoint(InteractivePoint ip, float x, float y) {
            if      (ip == start)    handleStartDrag(x, y);
            else if (ip == end)      handleEndDrag(x, y);
            else if (ip == control1) handleControl1Drag(x, y);
            else if (ip == control2) handleControl2Drag(x, y);
        }

        private void handleStartDrag(float x, float y) {
            setStart(x, y, start.getZ());
        }

        private void handleEndDrag(float x, float y) {
            setEnd(x, y, end.getZ());
        }

        private void handleControl1Drag(float x, float y) {
            control1.setCoordinates(x, y, control1.getZ());
            super.setWeight(1, x, y, control1.getZ());
        }

        private void handleControl2Drag(float x, float y) {
            control1.setCoordinates(x, y, control2.getZ());
            super.setWeight(1, x, y, control2.getZ());
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<Constants.TextureName, Utilities.Texture> texturesID,
                         float unitLengthNDC) {
            super.draw(
                    colorProgram,
                    textureProgram,
                    glText,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    visualObjects,
                    texturesID,
                    unitLengthNDC,
                    getOpenGLDepth()
            );

            if (isModifiable()) {
                start.draw(
                        colorProgram,
                        textureProgram,
                        glText,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        visualObjects,
                        texturesID,
                        unitLengthNDC,
                        getOpenGLDepth()
                );
                end.draw(
                        colorProgram,
                        textureProgram,
                        glText,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        visualObjects,
                        texturesID,
                        unitLengthNDC,
                        getOpenGLDepth()
                );

                if (isSelected()) {
                    control1.draw(
                            colorProgram,
                            textureProgram,
                            glText,
                            viewProjectionMatrix,
                            modelViewProjectionMatrix,
                            visualObjects,
                            texturesID,
                            unitLengthNDC,
                            getOpenGLDepth()
                    );

                    control2.draw(
                            colorProgram,
                            textureProgram,
                            glText,
                            viewProjectionMatrix,
                            modelViewProjectionMatrix,
                            visualObjects,
                            texturesID,
                            unitLengthNDC,
                            getOpenGLDepth()
                    );
                }
            }
        }
    }

    public interface LinearShapeInterface {
        float getLength();
        Point getCenter();
        ArrayList<Point> getVertices(float unitLengthNDC);
        ArrayList<Point> getVertices();
        BezierCurve detectSelectedBezierCurve(float x, float y, float r);
    }

    public static abstract class LinearShape extends Shape implements LinearShapeInterface {
        private ArrayList<BezierCurve> elements;
        private boolean isClosed;
        private boolean modifiable;

        public DrawingLineProperties drawingLineProperties;

        private void initializeVariables() {
            elements = new ArrayList<>();
            isClosed = false;
            modifiable = false;

            drawingLineProperties = new DrawingLineProperties();
        }

        public LinearShape() {
            super();
            initializeVariables();
        }

        protected void close() { isClosed = true; }
        public boolean isClosed() { return isClosed; }

        protected void setAsModifiable() { modifiable = true; }
        public boolean isModifiable() { return modifiable; }

        public ArrayList<BezierCurve> getCurves() { return elements; }

        public void addCurve(BezierCurve curve) {
            if (0 < elements.size()) {
                BezierCurve previousCurve = elements.get(elements.size()-1);

                Point start1 = previousCurve.getStart();
                Point start2 = curve.getStart();

                Point end1 = previousCurve.getEnd();
                Point end2 = curve.getEnd();

                float d1 = Tools.distanceBetweenPoints(start1, start2);
                float d2 = Tools.distanceBetweenPoints(start1, end2);
                float d3 = Tools.distanceBetweenPoints(end1, start2);
                float d4 = Tools.distanceBetweenPoints(end1, end2);

                if (Constants.TOLERANCE_FOR_ZER0 > d1) {
                    previousCurve.switchStartEnd();
                } else if (Constants.TOLERANCE_FOR_ZER0 > d2) {
                    previousCurve.switchStartEnd();
                    curve.switchStartEnd();
                } else if (Constants.TOLERANCE_FOR_ZER0 > d3) {
                    // Ok
                } else if (Constants.TOLERANCE_FOR_ZER0 > d4) {
                    curve.switchStartEnd();
                } else {
                    // Then the the curve to be added is far away from the line shape
                    return;
                }
            }
            curve.setDrawingLineProperties(drawingLineProperties);
            elements.add(curve);
        }

        public void translate(float dx, float dy, float dz) {
            Iterator<BezierCurve> it = elements.iterator();
            while (it.hasNext()) {
                it.next().translate(dx, dy, dz);
            }
        }

        public float getLength() {
            float length = 0f;
            for (BezierCurve curve : elements) length += curve.getLength();
            return length;
        }

        public Point getCenter() {
            Point center = new Point(0f, 0f, 0f);
            Point c;
            for (BezierCurve curve : elements) {
                c = curve.getCenter();
                center.x += c.x;
                center.y += c.y;
                center.z += c.z;
            }
            center.x /= (float)elements.size();
            center.y /= (float)elements.size();
            center.z /= (float)elements.size();

            return center;
        }

        public ArrayList<Point> getVertices(float unitLengthNDC) {
            ArrayList<Point> vertices = new ArrayList<>();

            int count = 0;
            for (BezierCurve curve : elements) {
                if (count == 0) {
                    vertices.addAll(curve.getVertices(unitLengthNDC));
                } else if (count == elements.size() - 1) {
                    if (isClosed)
                        Tools.addAllExceptFirstAndLastElements(
                                curve.getVertices(unitLengthNDC),
                                vertices);
                    else
                        Tools.addAllExceptFirstElement(
                                curve.getVertices(unitLengthNDC),
                                vertices);
                } else {
                    Tools.addAllExceptFirstElement(
                            curve.getVertices(unitLengthNDC),
                            vertices);
                }
                count++;
            }
            return vertices;
        }

        public ArrayList<Point> getVertices() {
            ArrayList<Point> vertices = new ArrayList<>();

            int count = 0;
            for (BezierCurve curve : elements) {
                if (count == 0) {
                    vertices.addAll(curve.getVertices());
                } else if (count == elements.size() - 1) {
                    if (isClosed)
                        Tools.addAllExceptFirstAndLastElements(
                                curve.getVertices(),
                                vertices);
                    else
                        Tools.addAllExceptFirstElement(
                                curve.getVertices(),
                                vertices);
                } else {
                    Tools.addAllExceptFirstElement(
                            curve.getVertices(),
                            vertices);
                }
                count++;
            }
            return vertices;
        }

        public BezierCurve detectSelectedBezierCurve(float x,
                                                     float y,
                                                     float r) {
            if (!modifiable) return null;
            BezierCurve selectedCurve = null;
            for (BezierCurve curve : elements) {
                if (curve.areYouSelected(x, y, r)) {
                    selectedCurve = curve;
                    break;
                }
            }
            return selectedCurve;
        }

        public BezierCurve getPreviousCurve(BezierCurve myCurve) {
            if (null == myCurve) return null;
            if (1 >= elements.size()) return null;

            boolean wasFound;
            BezierCurve previousCurve = null;

            if (myCurve == elements.get(0)) {
                if (isClosed)
                    previousCurve = elements.get(elements.size() - 1);
                else
                    previousCurve = null;
            } else {
                wasFound = false;
                for (BezierCurve curve : elements) {
                    if (curve == myCurve) {
                        wasFound = true;
                        break;
                    }
                    previousCurve = curve;
                }
                if (!wasFound) previousCurve = null;
            }
            return previousCurve;
        }

        public BezierCurve getNextCurve(BezierCurve myCurve) {
            if (null == myCurve) return null;
            if (1 >= elements.size()) return null;

            boolean wasFound;
            BezierCurve nextCurve = null;

            if (myCurve == elements.get(elements.size()-1)) {
                if (isClosed)
                    nextCurve = elements.get(0);
                else
                    nextCurve = null;
            } else {
                wasFound = false;
                for (int i = elements.size() - 2; i >= 0; i--) {
                    nextCurve = elements.get(i+1);
                    if (myCurve == elements.get(i)) {
                        wasFound = true;
                        break;
                    }
                }
                if (!wasFound) nextCurve = null;
            }
            return nextCurve;
        }
    }

    public static class SameOrderLinearShape extends LinearShape {
        private int order;
        private ControlPoint center;
        private OrderOneLinearShape boundingBox;

        private BezierCurve currentSelectedCurve;

        private void initializeVariables() {
            order = 0;
            center = new ControlPoint(this);
            boundingBox = null;

            currentSelectedCurve = null;
        }

        public SameOrderLinearShape(int order) {
            super();
            initializeVariables();

            if (0 >= order) this.order = 1;
            else            this.order = order;
        }

        public void handleDragCenter(float x, float y) {
            float dx = x - center.getX();
            float dy = y - center.getY();
            float dz = 0f;

            center.translate(dx, dy, dz);
            super.translate(dx, dy, dz);

            if (null != boundingBox)
                boundingBox.translate(dx, dy, dz);
        }

        public void addCurve(BezierCurve curve) {
            if (order != curve.getOrder()) return;
            super.addCurve(curve);
            Point c = super.getCenter();
            center.setCoordinates(c.x, c.y, c.z);
        }

        public void close(BezierCurve curve) {
            addCurve(curve);
            super.close();
            Point c = super.getCenter();
            center.setCoordinates(c.x, c.y, c.z);
        }

        public boolean areYouSelected(float x, float y, float selectionsDistance) {
            for (BezierCurve curve : super.getCurves()) {
                if (curve.areYouSelected(x, y, selectionsDistance)) return true;
            }
            return false;
        }

        public void handleTouchPress(float x,
                                     float y,
                                     float selectionDistance,
                                     UndoRedo undoRedo) {
            if (!isModifiable()) return;
            if (!isSelected()) return;

            if (null == currentSelectedCurve) {
                // The user may want to select a curve
                for (BezierCurve curve : super.getCurves()) {
                    if (curve.areYouSelected(x, y, selectionDistance)) {
                        curve.select();
                        currentSelectedCurve = curve;
                        break;
                    }
                }
            } else {
                if (!currentSelectedCurve.areYouSelected(x, y, selectionDistance)) {
                    currentSelectedCurve.unSelect();
                    currentSelectedCurve = null;
                    for (BezierCurve curve : super.getCurves()) {
                        if (curve.areYouSelected(x, y, selectionDistance)) {
                            curve.select();
                            currentSelectedCurve = curve;
                            break;
                        }
                    }
                }
            }
        }

        public DragStatus handleTouchDrag(Drags drags,
                                          float invertedViewProjectionMatrix,
                                          UndoRedo undoRedo) {
            if (!isModifiable()) return DragStatus.NOTHING_DRAGGED;
            return DragStatus.NOTHING_DRAGGED;
        }

        public ArrayList<InteractivePoint> detectSelectedInteractivePoints(float x,
                                                                           float y,
                                                                           float r) {
            ArrayList<InteractivePoint> ips = new ArrayList<>();
            if (!isModifiable()) return ips;
            if (!isSelected()) return ips;
            Point touchPoint = new Point(x, y, 0f);

            if (Tools.distanceBetweenPoints(touchPoint, center.getPoint()) < r) {
                ips.add(center);
            } else {
                ArrayList<InteractivePoint> ipsCurve;
                for (BezierCurve curve : getCurves()) {
                    ipsCurve = curve.detectSelectedInteractivePoints(x, y, r);
                    ips.addAll(ipsCurve);
                    if (2 <= ips.size()) break;
                }
            }

            return ips;
        }

        public void handleDragInteractivePoint(InteractivePoint ip, float x, float y) {
            if (ip == center) handleDragCenter(x, y);
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<Constants.TextureName, Utilities.Texture> texturesID,
                         float unitLengthNDC) {

            for (BezierCurve curve : super.elements) {
                curve.draw(
                        colorProgram,
                        textureProgram,
                        glText,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        visualObjects,
                        texturesID,
                        unitLengthNDC,
                        getOpenGLDepth()
                );
            }

            if (isSelected()) {
                if (null == boundingBox) {
                    boundingBox =
                            Tools.getBoundingBox(getVertices(unitLengthNDC));
                    boundingBox.drawingLineProperties.setDashed6Div();
                    boundingBox.drawingLineProperties.setColor(
                            Constants.SELECTED_SHAPE_COLOR[0],
                            Constants.SELECTED_SHAPE_COLOR[1],
                            Constants.SELECTED_SHAPE_COLOR[2]);
                    boundingBox.offset(
                            Constants.BOUNDING_BOX_MARGIN/unitLengthNDC);
                }
                boundingBox.setDepth(getDepth());
                boundingBox.draw(
                        colorProgram,
                        textureProgram,
                        glText,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        visualObjects,
                        texturesID,
                        unitLengthNDC
                );
                if (isModifiable()) {
                    colorProgram.useProgram();
                    Utilities.drawPoint(
                            colorProgram,
                            viewProjectionMatrix,
                            modelViewProjectionMatrix,
                            visualObjects.getInteractivePoint(),
                            center.getPoint(),
                            getDepth(),
                            0.22f, 0.22f, 0.22f
                    );
                }
            }
        }
    }

    public static class OrderOneLinearShape extends SameOrderLinearShape {
        private LinearBezierCurve curve;

        private void initializeVariables() {
            curve = null;
        }

        public OrderOneLinearShape() {
            super(1);
            initializeVariables();
        }

        public void setStart(float x, float y, float z) {
            curve = new LinearBezierCurve(isModifiable());
            curve.setStart(x, y, z);
        }

        public void createSegmentToHere(float x, float y, float z) {
            ArrayList<BezierCurve> elements = getCurves();
            if (null == elements) return;

            if (0 < elements.size()) {
                curve = new LinearBezierCurve(isModifiable());
                BezierCurve previousCurve = elements.get(elements.size()-1);
                Point p = previousCurve.getEnd();
                curve.setStart(p.getX(), p.getY(), p.getZ());
            }
            curve.setEnd(x, y, z);
            super.addCurve(curve);
        }

        public void close() {
            ArrayList<BezierCurve> elements = getCurves();
            if (null == elements) return;

            if (0 == elements.size()) return;
            BezierCurve lastCurve = elements.get(elements.size()-1);
            BezierCurve firstCurve = elements.get(0);
            Point p1 = lastCurve.getEnd();
            Point p2 = firstCurve.getStart();

            curve = new LinearBezierCurve(isModifiable());
            curve.setStart(p1.getX(), p1.getY(), p1.getZ());
            curve.setEnd(p2.getX(), p2.getY(), p2.getZ());
            addCurve(curve);
            super.close();
        }

        public void offset(final float d) {
            for (BezierCurve curve : getCurves()) curve.offset(d);
        }

        public void handleDragInteractivePoint(InteractivePoint ip, float x, float y) {
            super.handleDragInteractivePoint(ip, x, y);
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<Constants.TextureName, Utilities.Texture> texturesID,
                         float unitLengthNDC) {
            super.draw(colorProgram,
                    textureProgram,
                    glText,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    visualObjects,
                    texturesID,
                    unitLengthNDC);
        }
    }

    public static class OrderThreeLinearShape extends SameOrderLinearShape {
        private CubicBezierCurve curve;

        private void initializeVariables() {
            curve = null;
        }

        public OrderThreeLinearShape() {
            super(3);
            initializeVariables();
        }

        public void setStart(float x, float y, float z) {
            curve = new CubicBezierCurve();
            curve.setStart(x, y, z);
        }

        public void createSegmentToHere(float x, float y, float z) {
            ArrayList<BezierCurve> elements = getCurves();
            if (null == elements) return;

            if (0 < elements.size()) {
                curve = new CubicBezierCurve();
                BezierCurve previousCurve = elements.get(elements.size()-1);
                Point p = previousCurve.getEnd();
                curve.setStart(p.getX(), p.getY(), p.getZ());
            }
            curve.setEnd(x, y, z);
            curve.setAsStraightLine();
            addCurve(curve);
        }

        public void close() {
            ArrayList<BezierCurve> elements = getCurves();
            if (null == elements) return;

            if (0 == elements.size()) return;
            BezierCurve lastCurve = elements.get(elements.size()-1);
            BezierCurve firstCurve = elements.get(0);
            Point p1 = lastCurve.getEnd();
            Point p2 = firstCurve.getStart();

            curve = new CubicBezierCurve();
            curve.setStart(p1.getX(), p1.getY(), p1.getZ());
            curve.setEnd(p2.getX(), p2.getY(), p2.getZ());
            curve.setAsStraightLine();
            addCurve(curve);
        }

        public void handleDragInteractivePoint(InteractivePoint ip, float x, float y) {
            super.handleDragInteractivePoint(ip, x, y);
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<Constants.TextureName, Utilities.Texture> texturesID,
                         float unitLengthNDC) {
            super.draw(colorProgram,
                    textureProgram,
                    glText,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    visualObjects,
                    texturesID,
                    unitLengthNDC);
        }
    }

    // Surface shapes ------------------------------------------------------------------------------

    public interface SurfaceShapeInterface {
        LinearShape detectSelectedLinearShape(float x, float y, float r);
    }

    public static abstract class SurfaceShape extends Shape implements SurfaceShapeInterface {
        private LinearShape boundary;
        private ArrayList<LinearShape> holes;

        private boolean buildMesh;
        private Mesh mesh;

        public Material material;

        private void initializeVariables() {
            boundary = null;
            holes = new ArrayList<>();
            buildMesh = true;
            mesh = new Mesh();
            material = new Material();
        }

        public SurfaceShape() {
            super();
            initializeVariables();
        }

        public void setBoundary(LinearShape boundary) {
            this.boundary = boundary;
        }
        public LinearShape getBoundary() { return boundary; }

        public void addHole(LinearShape hole) {
            holes.add(hole);
        }
        public ArrayList<LinearShape> getHoles() { return holes; }

        public void translate(float dx, float dy, float dz) {
            boundary.translate(dx, dy, dz);
            Iterator<LinearShape> it = holes.iterator();
            while (it.hasNext()) it.next().translate(dx, dy, dz);
            mesh.translate(dx, dy, dz);
        }

        protected MyModel getModel() {
            MyModel model = new MyModel();

            ArrayList<ArrayList<Point>> holeBoundaries = new ArrayList<>();
            for (LinearShape linearShape : holes)
                holeBoundaries.add(linearShape.getVertices());

            model.create(boundary.getVertices(), holeBoundaries);
            return model;
        }

        public boolean areYouSelected(float x,
                                      float y,
                                      float selectionDistance) {
            MyModel model = getModel();
            if (!model.isAValidModel()) return false;
            if (!model.isPointInside(x, y)) return false;
            return true;
        }

        public LinearShape detectSelectedLinearShape(float x,
                                                     float y,
                                                     float r) {
            if (boundary.isModifiable() &&
                boundary.areYouSelected(x, y, r))
                return boundary;

            for (LinearShape linearShape : holes)
                if (linearShape.isModifiable() &&
                        linearShape.areYouSelected(x, y, r))
                    return linearShape;

            return null;
        }
    }

    public static class Rectangle extends SurfaceShape {
        private ControlPoint cornerA;
        private ControlPoint cornerB;
        private ControlPoint center;
        private OrderOneLinearShape boundary;

        private void initializeVariables() {
            cornerA = new ControlPoint(this);
            cornerB = new ControlPoint(this);
            center = new ControlPoint(this);
            boundary = new OrderOneLinearShape();
            super.setBoundary(boundary);
        }

        public Rectangle(float cornerAX, float cornerAY,
                         float cornerBX, float cornerBY) {
            super();
            initializeVariables();

            cornerA.setCoordinates(cornerAX, cornerAY, 0f);
            cornerB.setCoordinates(cornerBX, cornerBY, 0f);
            center.setCoordinates(0.5f*(cornerAX + cornerBX), 0.5f*(cornerAY + cornerBY), 0f);

            build();
        }

        private void build() {

            float width = cornerB.getX() - cornerA.getX();
            float height = cornerB.getY() - cornerA.getY();

            boundary.setStart(cornerA.getX(), cornerA.getY(), cornerA.getZ());
            boundary.createSegmentToHere(cornerA.getX() + width, cornerA.getY(), 0f);
            boundary.createSegmentToHere(cornerB.getX(), cornerB.getY(), cornerB.getZ());
            boundary.createSegmentToHere(cornerA.getX(), cornerA.getY() + height, 0f);
            boundary.close();

            setBoundary(boundary);
        }

        public void handleTouchPress(float x,
                                     float y,
                                     float r,
                                     UndoRedo undoRedo) {

        }

        public DragStatus handleTouchDrag(Drags drags,
                                          float invertedViewProjectionMatrix,
                                          UndoRedo undoRedo) {
            DragStatus dragStatus = DragStatus.NOTHING_DRAGGED;
            // Identify if the user is dragging a control point
            for (int i = 0; i < drags.size(); i++) {
                Drag drag = drags.getDrag(i);
                // Only can be added at the beginning
                if (drag.hasPointsBeenSet()) continue;
                Point p = drag.getBeginningPosition();
                // Several control points may be dragged
                if (center.areYouSelected(p.x, p.y, Constants.SELECTION_DISTANCE)) {
                    drag.addPoint(center);
                    dragStatus = DragStatus.DRAGGING_SOMETHING;
                }
                if (cornerA.areYouSelected(p.x, p.y, Constants.SELECTION_DISTANCE)) {
                    drag.addPoint(cornerA);
                    dragStatus = DragStatus.DRAGGING_SOMETHING;
                }
                if (cornerB.areYouSelected(p.x, p.y, Constants.SELECTION_DISTANCE)) {
                    drag.addPoint(cornerB);
                    dragStatus = DragStatus.DRAGGING_SOMETHING;
                }
            }
            return dragStatus;
        }

        public ArrayList<InteractivePoint> detectSelectedInteractivePoints(float x,
                                                                           float y,
                                                                           float r) {
            ArrayList<InteractivePoint> ips = new ArrayList<>();
            Point touchPoint = new Point(x, y, 0f);
            if (!isSelected()) return ips;
            if (Tools.distanceBetweenPoints(touchPoint, center.getPoint()) < r)
                ips.add(center);
            else if (Tools.distanceBetweenPoints(touchPoint, cornerA.getPoint()) < r)
                ips.add(cornerA);
            else if (Tools.distanceBetweenPoints(touchPoint, cornerB.getPoint()) < r)
                ips.add(cornerB);
            return ips;
        }

        public void handleDragInteractivePoint(InteractivePoint ip, float x, float y ) {
            if (!isSelected()) return;
            if      (ip == cornerA) handleCornerADrag(x, y);
            else if (ip == cornerB) handleCornerBDrag(x, y);
            else if (ip == center)  handleCenterDrag(x, y);
        }

        private void handleCornerADrag(float x, float y) {
            cornerA.setCoordinates(x, y, cornerA.getZ());
            center.setCoordinates(0.5f*(x + cornerB.getX()), 0.5f*(y + cornerB.getY()), center.getZ());

            LinearBezierCurve c;
            Point p1, p2;

            c = (LinearBezierCurve) boundary.getCurves().get(0);
            p1 = c.getStart();
            p2 = c.getEnd();
            c.setStart(x, y, p1.getZ());
            c.setEnd(p2.getX(), y, p1.getZ());

            c = (LinearBezierCurve) boundary.getCurves().get(1);
            c.setStart(p2.getX(), y, p2.getZ());

            c = (LinearBezierCurve) boundary.getCurves().get(2);
            p2 = c.getEnd();
            c.setEnd(x, p2.getY(), p2.getZ());

            c = (LinearBezierCurve) boundary.getCurves().get(3);
            p1 = c.getStart();
            p2 = c.getEnd();
            c.setStart(x, p1.getY(), p2.getZ());
            c.setEnd(x, y, p2.getZ());

            super.buildMesh = true;
        }

        private void handleCornerBDrag(float x, float y) {
            cornerB.setCoordinates(x, y, cornerB.getZ());
            center.setCoordinates(0.5f*(x + cornerA.getX()), 0.5f*(y + cornerA.getY()),
                    center.getZ());

            LinearBezierCurve c;
            Point p1, p2;

            c = (LinearBezierCurve) boundary.getCurves().get(0);
            p2 = c.getEnd();
            c.setEnd(x, p2.getY(), p2.getZ());

            c = (LinearBezierCurve) boundary.getCurves().get(1);
            p1 = c.getStart();
            p2 = c.getEnd();
            c.setStart(x, p1.getY(), p1.getZ());
            c.setEnd(x, y, p2.getZ());

            c = (LinearBezierCurve) boundary.getCurves().get(2);
            p1 = c.getStart();
            p2 = c.getEnd();
            c.setStart(x, y, p1.getZ());
            c.setEnd(p2.getX(), y, p2.getZ());

            c = (LinearBezierCurve) boundary.getCurves().get(3);
            p1 = c.getStart();
            c.setStart(p1.getX(), y, p1.getZ());

            super.buildMesh = true;
        }

        private void handleCenterDrag(float x, float y) {
            float dx = x - center.getX();
            float dy = y - center.getY();
            float dz = 0f;

            cornerA.translate(dx, dy, dz);
            cornerB.translate(dx, dy, dz);
            center.translate(dx, dy, dz);
            super.translate(dx, dy, dz);
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<Constants.TextureName, Utilities.Texture> texturesID,
                         float unitLengthNDC) {

            if (super.buildMesh) {
                super.mesh.build(boundary.getVertices(unitLengthNDC), true);
                super.buildMesh = false;
            }

            if (!material.needsTexturing()) {
                super.mesh.draw(
                        colorProgram,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        material.getColor(),
                        getOpenGLDepth()
                );
            }

            boundary.draw(
                    colorProgram,
                    textureProgram,
                    glText,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    visualObjects,
                    texturesID,
                    unitLengthNDC
            );

            if (isSelected()) {
                center.draw(
                        colorProgram,
                        textureProgram,
                        glText,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        visualObjects,
                        texturesID,
                        unitLengthNDC,
                        getOpenGLDepth()
                );

                cornerA.draw(
                        colorProgram,
                        textureProgram,
                        glText,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        visualObjects,
                        texturesID,
                        unitLengthNDC,
                        getOpenGLDepth()
                );

                cornerB.draw(
                        colorProgram,
                        textureProgram,
                        glText,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        visualObjects,
                        texturesID,
                        unitLengthNDC,
                        getOpenGLDepth()
                );
            }
        }
    }

    public static class RegularPolygon extends SurfaceShape {
        private float innerAngle;
        private int numberOfSides;
        private ControlPoint centerControl;
        private ControlPoint apothemControl;
        private OrderOneLinearShape boundary;

        private void initializeVariables() {
            innerAngle = 0f;
            numberOfSides = 0;
            centerControl = new ControlPoint(this);
            apothemControl = new ControlPoint(this);
            boundary = new OrderOneLinearShape();
        }

        public RegularPolygon(int numberOfSides,
                              float centerX, float centerY,
                              float apothemX, float apothemY) {
            super();
            initializeVariables();
            this.numberOfSides = numberOfSides;
            centerControl.setCoordinates(centerX, centerY, 0);
            apothemControl.setCoordinates(apothemX, apothemY, 0);
            build();
        }

        private void build() {
            innerAngle = (float) (2.0 * Math.PI / (double) numberOfSides);
            buildBoundary();
            super.setBoundary(boundary);
        }

        private void buildBoundary() {
            float angle, radius, apothem, x, y;

            apothem = (float)
                    Math.sqrt(Math.pow(apothemControl.getX() - centerControl.getX(), 2) +
                            Math.pow(apothemControl.getY() - centerControl.getY(), 2));

            angle = (float) Math.atan2(apothemControl.getY() - centerControl.getY(),
                    apothemControl.getX() - centerControl.getX());
            radius = apothem / (float) Math.cos(0.5 * innerAngle);

            angle += 0.5f * innerAngle;
            x = radius * (float) Math.cos(angle) + centerControl.getX();
            y = radius * (float) Math.sin(angle) + centerControl.getY();

            boundary.setStart(x, y, 0);
            for (int i = 0; i < numberOfSides - 1; i++) {
                angle += innerAngle;
                x = radius * (float) Math.cos(angle) + centerControl.getX();
                y = radius * (float) Math.sin(angle) + centerControl.getY();
                boundary.createSegmentToHere(x, y, 0);
            }
            boundary.close();
        }

        public void handleTouchPress(float x,
                                     float y,
                                     float r,
                                     UndoRedo undoRedo) {
        }

        public DragStatus handleTouchDrag(Drags drags,
                                          float invertedViewProjectionMatrix,
                                          UndoRedo undoRedo) {
            return DragStatus.NOTHING_DRAGGED;
        }

        public ArrayList<InteractivePoint> detectSelectedInteractivePoints(float x,
                                                                           float y,
                                                                           float r) {
            ArrayList<InteractivePoint> ips = new ArrayList<>();
            Point touchPoint = new Point(x, y, 0f);
            if (Tools.distanceBetweenPoints(touchPoint, centerControl.getPoint()) < r)
                ips.add(centerControl);
            else if (Tools.distanceBetweenPoints(touchPoint, apothemControl.getPoint()) < r)
                ips.add(apothemControl);
            return ips;
        }

        public void handleDragInteractivePoint(InteractivePoint ip,
                                               float x, float y ) {
            // If the center node is being moved then, we move the circle
            if (ip == centerControl) handleCenterNodeDrag(x, y);
                // If the size node is being moved, then we increase o decrease the circle's size
            else if (ip == apothemControl) handleApothemNodeDrag(x, y);
        }

        private void handleCenterNodeDrag(float x, float y) {
            float dx, dy, dz;
            dx = x - centerControl.getX();
            dy = y - centerControl.getY();
            dz = 0f;

            centerControl.translate(dx, dy, dz);
            apothemControl.translate(dx, dy, dz);
            super.translate(dx, dy, dz);
        }

        private void handleApothemNodeDrag(float x, float y) {
            float angle, radius, apothem;

            apothemControl.setCoordinates(x, y, apothemControl.getZ());

            apothem = (float) Math.sqrt(Math.pow(apothemControl.getX() - centerControl.getX(), 2) +
                    Math.pow(apothemControl.getY() - centerControl.getY(), 2));

            angle = (float) Math.atan2(apothemControl.getY() - centerControl.getY(),
                    apothemControl.getX() - centerControl.getX());
            radius = apothem / (float) Math.cos(0.5 * innerAngle);

            angle += 0.5f * innerAngle;
            for (BezierCurve curve : boundary.getCurves()) {
                curve.setStart(
                        radius * (float) Math.cos(angle) + centerControl.getX(),
                        radius * (float) Math.sin(angle) + centerControl.getY(),
                        0f
                );
                curve.setEnd(
                        radius * (float) Math.cos(angle+innerAngle) + centerControl.getX(),
                        radius * (float) Math.sin(angle+innerAngle) + centerControl.getY(),
                        0f
                );
                angle += innerAngle;
            }

            super.buildMesh = true;
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<Constants.TextureName, Utilities.Texture> texturesID,
                         float unitLengthNDC) {

            if (super.buildMesh) {
                super.mesh.build(boundary.getVertices(unitLengthNDC), true);
                super.buildMesh = false;
            }

            if (!material.needsTexturing()) {
                super.mesh.draw(
                        colorProgram,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        material.getColor(),
                        getOpenGLDepth()
                );
            }

            boundary.draw(
                    colorProgram,
                    textureProgram,
                    glText,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    visualObjects,
                    texturesID,
                    unitLengthNDC
            );

            if (isSelected()) {
                centerControl.draw(
                        colorProgram,
                        textureProgram,
                        glText,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        visualObjects,
                        texturesID,
                        unitLengthNDC,
                        getOpenGLDepth()
                );

                apothemControl.draw(
                        colorProgram,
                        textureProgram,
                        glText,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        visualObjects,
                        texturesID,
                        unitLengthNDC,
                        getOpenGLDepth()
                );
            }
        }
    }

    public static class Circle extends SurfaceShape {
        private int numberOfSides;
        private ControlPoint centerControl;
        private ControlPoint sizeControl;
        private OrderOneLinearShape boundary;

        private void initializeVariables() {
            numberOfSides = 0;
            centerControl = new ControlPoint(this);
            sizeControl = new ControlPoint(this);
            boundary = new OrderOneLinearShape();
        }

        public Circle(float centerX, float centerY,
                      float pointX, float pointY) {
            super();
            initializeVariables();
            numberOfSides = Constants.NUMBER_OF_SIDES_TO_DRAW_CIRCLE;
            centerControl.setCoordinates(centerX, centerY, 0f);
            sizeControl.setCoordinates(pointX, pointY, 0f);
            build();
        }

        private void build() {
            buildBoundary();
            super.setBoundary(boundary);
        }

        private void buildBoundary() {
            float[][] vertices = getVerticesCoordinates();

            boundary.setStart(vertices[0][0], vertices[0][1], 0);
            for (int i = 1; i < numberOfSides; i++)
                boundary.createSegmentToHere(vertices[i][0], vertices[i][1], 0);
            boundary.close();
        }

        private float[][] getVerticesCoordinates() {
            float currentAngle, centerX, centerY, radius;
            float[][] vertices = new float[numberOfSides][Constants.PROBLEM_DIMENSION];

            final float stepAngle = (float) (2*Math.PI / (double) numberOfSides);
            currentAngle = (float) Math.atan2(sizeControl.getY() - centerControl.getY(),
                    sizeControl.getX() - centerControl.getX());

            centerX = centerControl.getX();
            centerY = centerControl.getY();
            radius = (float) Math.sqrt(
                    Math.pow(sizeControl.getX() - centerControl.getX(), 2) +
                            Math.pow(sizeControl.getY() - centerControl.getY(), 2)
            );

            for (int i = 0; i < numberOfSides; i++) {
                vertices[i][0] = centerX + radius * (float) Math.cos(currentAngle);
                vertices[i][1] = centerY + radius * (float) Math.sin(currentAngle);
                currentAngle += stepAngle;
            }

            return vertices;
        }

        public void handleTouchPress(float x,
                                     float y,
                                     float r,
                                     UndoRedo undoRedo) {

        }

        public DragStatus handleTouchDrag(Drags drags,
                                          float invertedViewProjectionMatrix,
                                          UndoRedo undoRedo) {
            return DragStatus.NOTHING_DRAGGED;
        }

        public ArrayList<InteractivePoint> detectSelectedInteractivePoints(float x,
                                                                           float y,
                                                                           float r) {
            ArrayList<InteractivePoint> ips = new ArrayList<>();
            Point touchPoint = new Point(x, y, 0f);
            if (Tools.distanceBetweenPoints(touchPoint, centerControl.getPoint()) < r)
                ips.add(centerControl);
            else if (Tools.distanceBetweenPoints(touchPoint, sizeControl.getPoint()) < r)
                ips.add(sizeControl);
            return ips;
        }

        public void handleDragInteractivePoint(InteractivePoint ip,
                                               float x, float y) {
            // If the center node is being moved then, we move the circle
            if (ip == centerControl) handleCenterDrag(x, y);
                // If the size node is being moved, then we increase o decrease the circle's size
            else if (ip == sizeControl) handleSizeControlDrag(x, y);
        }

        private void handleCenterDrag(float x, float y) {
            float dx = x - centerControl.getX();
            float dy = y - centerControl.getY();
            float dz = 0;

            centerControl.translate(dx, dy, dz);
            sizeControl.translate(dx, dy, dz);
            super.translate(dx, dy, dz);
        }

        private void handleSizeControlDrag(float x, float y) {
            sizeControl.setCoordinates(x, y, sizeControl.getZ());
            float[][] newVertices = getVerticesCoordinates();

            int i = 0;
            for (BezierCurve curve : boundary.getCurves()) {
                curve.setStart(
                        newVertices[i][0],
                        newVertices[i][1],
                        0f);
                curve.setEnd(
                        newVertices[(i+1)%newVertices.length][0],
                        newVertices[(i+1)%newVertices.length][1],
                        0f);
                i++;
            }

            super.buildMesh = true;
        }

        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<Constants.TextureName, Utilities.Texture> texturesID,
                         float unitLengthNDC) {
            if (super.buildMesh) {
                super.mesh.build(boundary.getVertices(unitLengthNDC), true);
                super.buildMesh = false;
            }

            if (!material.needsTexturing()) {
                super.mesh.draw(
                        colorProgram,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        material.getColor(),
                        getOpenGLDepth()
                );
            }

            boundary.draw(
                    colorProgram,
                    textureProgram,
                    glText,
                    viewProjectionMatrix,
                    modelViewProjectionMatrix,
                    visualObjects,
                    texturesID,
                    unitLengthNDC
            );

            if (isSelected()) {
                centerControl.draw(
                        colorProgram,
                        textureProgram,
                        glText,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        visualObjects,
                        texturesID,
                        unitLengthNDC,
                        getOpenGLDepth()
                );

                sizeControl.draw(
                        colorProgram,
                        textureProgram,
                        glText,
                        viewProjectionMatrix,
                        modelViewProjectionMatrix,
                        visualObjects,
                        texturesID,
                        unitLengthNDC,
                        getOpenGLDepth()
                );
            }
        }
    }

    public static class CubicFreeShape extends SurfaceShape {
        private ControlPoint center;
        private OrderThreeLinearShape boundary;
        private ArrayList<OrderThreeLinearShape> holes;

        private BezierCurve currentSelectedCurve;
        private BezierCurve previousCurve;
        private BezierCurve nextCurve;

        private void initializeVariables() {
            center = new ControlPoint(this);
            boundary = new OrderThreeLinearShape();
            holes = new ArrayList<>();

            currentSelectedCurve = null;
            previousCurve = null;
            nextCurve = null;
        }

        public CubicFreeShape(SurfaceShape s) {
            super();
            initializeVariables();
            build(s);
        }

        private void build(SurfaceShape s) {
            boundary = Tools.buildCubicLinearShape(s.boundary);
            boundary.setAsModifiable();
            super.setBoundary(boundary);
            for (LinearShape ls : s.getHoles()) {
                OrderThreeLinearShape hole =
                        Tools.buildCubicLinearShape(ls);
                hole.setAsModifiable();
                holes.add(Tools.buildCubicLinearShape(hole));
                super.addHole(hole);
            }
        }

        public void handleTouchPress(float x,
                                     float y,
                                     float r,
                                     UndoRedo undoRedo) {
            if (!isSelected()) return;

            BezierCurve curve;
            curve = boundary.detectSelectedBezierCurve(x, y, r);
            if (null != curve) {
                if (null != currentSelectedCurve) {
                    currentSelectedCurve.unSelect();
                    if (curve == currentSelectedCurve) {
                        currentSelectedCurve = null;
                    } else {
                        curve.select();
                        currentSelectedCurve = curve;
                    }
                } else {
                    curve.select();
                    currentSelectedCurve = curve;
                    previousCurve = boundary.getPreviousCurve(curve);
                    nextCurve = boundary.getNextCurve(curve);
                }
            } else {
                if (null != currentSelectedCurve)
                    currentSelectedCurve.unSelect();
                currentSelectedCurve = null;
                previousCurve = null;
                nextCurve = null;
            }
        }

        public DragStatus handleTouchDrag(Drags drags,
                                          float invertedViewProjectionMatrix,
                                          UndoRedo undoRedo) {
            return DragStatus.NOTHING_DRAGGED;
        }

        public ArrayList<InteractivePoint> detectSelectedInteractivePoints(float x,
                                                                           float y,
                                                                           float r) {
            ArrayList<InteractivePoint> ips = new ArrayList<>();
            Point touchPoint = new Point(x, y, 0f);
            if (!isSelected()) return ips;

            if (Tools.distanceBetweenPoints(touchPoint, center.getPoint()) < r) {
                ips.add(center);
            } else {
                if (isSelected()) {
                    if (null != currentSelectedCurve) {
                        ArrayList<InteractivePoint> aux =
                                currentSelectedCurve.detectSelectedInteractivePoints(x, y, r);
                        ips.addAll(aux);
                    }

                    if (null != previousCurve) {
                        ArrayList<InteractivePoint> aux =
                                currentSelectedCurve.detectSelectedInteractivePoints(x, y, r);
                        ips.addAll(aux);
                    }

                    if (null != nextCurve) {
                        ArrayList<InteractivePoint> aux =
                                currentSelectedCurve.detectSelectedInteractivePoints(x, y, r);
                        ips.addAll(aux);
                    }
                }
            }
            return ips;
        }

        public void handleDragInteractivePoint(InteractivePoint ip, float x, float y) {
            if (!isSelected()) return;
            if (center == ip) {
                handleCenterDrag(x, y);
            } else {
                Shape line = ip.getParentShape();
                if (null != line)
                    line.handleDragInteractivePoint(ip, x, y);
            }
        }

        private void handleCenterDrag(float x, float y) {
            float dx = x - center.getX();
            float dy = y - center.getY();
            float dz = 0f;

            center.translate(dx, dy, dz);
            super.translate(dx, dy, dz);
        }


        public void draw(ColorShaderProgram colorProgram,
                         TextureShaderProgram textureProgram,
                         GLText glText,
                         float[] viewProjectionMatrix,
                         float[] modelViewProjectionMatrix,
                         VisualObjectsCollection visualObjects,
                         Map<Constants.TextureName, Utilities.Texture> texturesID,
                         float unitLengthNDC) {

        }
    }
}


