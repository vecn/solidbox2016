package com.cimat.solidbox.Utilities;

import com.cimat.solidbox.Utilities.Preprocess.Geometry;

import java.util.EnumSet;

/**
 * Created by ernesto on 5/01/16.
 */
public class Constants {
    public static final int BYTES_PER_FLOAT = 4;

    // The dimension in which we are solving the FEM's problem.
    // In OpenGL we are always are working in 3D.
    public static final int PROBLEM_DIMENSION = 2;

    // Types of actions that can be done in the CAD
    public static final int INITIAL_VALUE_INT = -2;

    public enum CADOperation {
        DOING_NOTHING,
        DRAWING_A_NODE,
        DRAWING_A_LINE,
        DRAWING_A_SURFACE,
        SELECTING_ELEMENT_TO_APPLY_FORCE,
        SELECTING_ELEMENT_TO_FIX_DISPLACEMENT,
        SELECTING_ELEMENT_TO_FIX_MATERIAL,
        UNIFY_SHAPES,
        INTERSECT_SHAPES,
        SUBSTRACT_SHAPES,
        DIFFERENCE_SHAPES,
        COMBINE_SHAPES,
        DRAWING_A_RECTANGLE,
        DRAWING_A_REGULAR_POLYGON,
        DRAWING_A_CIRCLE,
        PANNING,
        ZOOMING,
        DRAGGING_SOMETHING,
        SPLITTING_SEGMENT,
        EXTRUDING_SEGMENT,
        SELECTING_ROTATION_AXIS,
        SELECTING_A_LINE_TO_APPLY_FORCE,
        SELECTING_A_POINT_TO_FIX_DISPLACEMENT,
        DRAWING_PUNCTUAL_FORCE,
        SHAPE_WAS_TRANSLATED,
        SHAPE_WAS_CHANGED;

        // Point shapes
        private static EnumSet<CADOperation> booleanOperations = EnumSet.of(
                UNIFY_SHAPES,
                INTERSECT_SHAPES,
                SUBSTRACT_SHAPES,
                DIFFERENCE_SHAPES,
                COMBINE_SHAPES
        );

        public static boolean isABooleanOperation(CADOperation operation) {
            return booleanOperations.contains(operation);
        }
    }

    public enum SolverStatus {
        NOT_SOLVED,
        MODEL_IS_INCOMPLETE,
        OK;
    }

    public static final int EVENT_PRESS = 0;
    public static final int EVENT_DRAG = 1;

    // Size of the nodes shown on the screen (given in the normalized device coordinates), so
    // when drawing the nodes, or something else, we have to affect this lengths by a scaling factor.
    public static final int VISUAL_NODE_POINT_NUMPOINTS = 25;
    public static final float VISUAL_NODE_RADIUS = 0.05f;
    public static final float VISUAL_VERTEX_RADIUS = 0.02f;
    public static final float VISUAL_HALO_RADIUS = 0.065f;
    public static final float VISUAL_NODE_FACTOR_SELECTION = 2f;
    public static final int NUMBER_OF_SIDES_TO_DRAW_SMALL_CIRCLE = 25;
    public static final int NUMBER_OF_SIDES_TO_DRAW_CIRCLE = 50;

    // Selection distance in NDC
    public static final float SELECTION_DISTANCE = 0.1f;

    // Size of the lines shown on the screen (normalized device coordinates)
    public static final float VISUAL_LINE_WIDTH = 0.1f;
    public static final float VISUAL_LINE_FACTOR_SELECTION = 1.0f;

    // Name of the temporary file used to save the data (can be when the app is paused, stopped, etc.)
    public static final String APP_DIRECTORY = "SolidBox";

    // Some values to determine whether the user wants to select something or drag something
    public static final float MAX_TAP_DISTANCE = 0.05f; // In normalize device coordinates
    public static final long  MAX_TAP_DURATION = 150;  // Milliseconds
    public static final long  MAX_DOUBLE_TAP_INTERVAL_TIME = 500;  // Milliseconds

    // Number of floats to define the location and color of a vertex
    // (when the color changes at each vertex, for OpenGL)
    public static final int FLOATS_PER_COLOR_VERTEX = 6;

    // Number of floats to define the location
    // (in this case the same color is used for all the vertices, for OpenGL)
    public static final int FLOATS_PER_VERTEX = 3;

    // Number of components to describe the position, color and texture coordinates of a vertex
    public static final int POSITION_COMPONENT_COUNT = 3;
    public static final int COLOR_COMPONENT_COUNT = 3;
    public static final int TEXTURE_COORDINATES_COMPONENT_COUNT = 2;

    // Length's side of the largest square drawn on the screen. The length is given in virtual units
    // (world-space).
    public static final int REFERENCE_NUMBER_OF_UNITS = 4;

    // Tics length given in the normalized device coordinates.
    public static final float MAIN_TICS_LENGTH = 0.05f;

    // Arrow length to draw the forces in normalized device coordinates
    public static final float ARROW_FORCE_LENGTH = 0.5f;

    // Line width used for OpenGL to draw several objects
    public static final float LINE_WIDTH_VISUAL_MESH = 2f;
    public static final float LINE_WIDTH_NOT_SELECTED_VISUAL_LINE = 10f;
    public static final float LINE_WIDTH_SELECTED_VISUAL_LINE = 10f;
    public static final float LINE_WIDTH_VISUAL_AXIS = 5f;
    public static final float LINE_WIDTH_AXIS_ORIENTATION = 5f;
    public static final float LINE_WIDTH_GRID = 3f;

    // Types of surface results
    public static final int NO_SURFACE_RESULT = -1;
    public static final int DISPLACEMENTS = 0;
    public static final int DISPLACEMENTS_ON_X = 1;
    public static final int DISPLACEMENTS_ON_Y = 2;
    public static final int DEFORMATIONS = 3;
    public static final int VON_MISES = 4;
    public static final int PLASTIFIED_ELEMS = 5;
    public static final int DAMAGE = 6;

    // Components to be considered
    public static final char ALL_COMPONENTS = 'A';
    public static final char X_COMPONENT = 'X';
    public static final char Y_COMPONENT = 'Y';
    public static final char Z_COMPONENT = 'Z';
    public static final char ONE_COMPONENT = 'O'; // The points just have one component

    // Type of element
    public static final int ELEMENT_TYPE_TRIANGLE = 0;
    public static final int ELEMENT_TYPE_QUADRILATERAL = 1;

    public static final int NUMBER_OF_VERTICES_TO_DRAW_ELEMENT = 3;

    // In normalized device coordinates
    public static final float DISTANCE_FOR_PI_RAD_ANGLE = 1f;

    // Types of FEM's problem
    public static final int PLANE_STRESS = 0;
    public static final int PLANE_STRAIN = 1;
    public static final int SOLID_OF_REVOLUTION = 2;

    //Type of FEM's model
    public static final int ELASTIC_PROBLEM = 0;
    public static final int PLASTIC_PROBLEM = 1;
    public static final int DAMAGE_PROBLEM = 2;

    // For the text
    // Font size when is loaded from the file. If it is bigger we get a better resolution.
    public static final int FONT_SIZE = 40;
    // Font size shown on the screen (of course in normalized device coordinates)
    public static final float FONT_SIZE_ON_SCREEN = 0.06f;

    // Supports
    public static final float SUPPORT_TRIANGLE_BASE = 0.12f;


    // A node may be part of the following shapes (the nodes are used in these figures to control
    // something)
    public static final int CONTROL_NODE_FOR_CIRCLE = 0;

    public static final float COLOR_SCALE_LEFT_MARGIN = 0.1f;
    public static final float COLOR_SCALE_WIDTH = 0.1f;
    public static final float COLOR_SCALE_HEIGHT = 0.8f;
    public static final int COLOR_SCALE_DIVISONS_ON_X = 1;
    public static final int COLOR_SCALE_DIVISONS_ON_Y = 20;

    public static final float LENGTH_LARGEST_MAGNITUDE_FORCE = 0.4f;
    public static final float LENGTH_FORCES_SIDE_LINES = 0.08f;
    public static final float LENGTH_FORCES_SIDE_LINES_II = 0.1f;
    public static final float PERPENDICULAR_DISTANCE_LINES_IN_LINEAR_FORCE = 0.08f;
    public static final float DISTANCE_BETWEEN_LINES_IN_UNIFORM_FORCE = 0.2f;
    public static final float DISTANCE_BETWEEN_LINES_IN_LINEAR_SUPPORT = 0.07f;
    public static final float LENGTH_SUPPORT_LINES = 0.07f;

    public static final float PANNING_FACTOR = 1f;
    public static final float ZOOMING_FACTOR = 1f;
    public static final float MAXIMUM_ZOOM = 100f;
    public static final float MINIMUM_ZOOM = 0.001f;
    public static final float SMALLEST_DISTANCE = 0.001f;

    public static final int MAXIMUM_QUANTITY_OF_POINTERS = 10;

    // Gestures
    public static final int GESTURE_NONE = 0;
    public static final int GESTURE_TAP = 1;
    public static final int GESTURE_DOUBLE_TAP = 2;
    public static final int GESTURE_DRAGGING = 3;

    public static final int INVALID_ID = -1;

    public static final int DOING_NOTHING = -1;
    public static final int CAD_UPDATE_LINES = 0;
    public static final int CAD_UPDATE_FORCES = 1;
    public static final int CAD_UPDATE_SURFACES = 2;
    public static final int CAD_UPDATE_AXIS = 3;
    public static final int CAD_UPDATE_INTERACTIVE_POINTS = 4;

    public static final int RESULTS_UPDATE_SET_INITIAL_SURFACE_RESULT = 5;
    public static final int RESULTS_UPDATE_SET_NEW_SURFACE_RESULTS = 6;
    public static final int RESULTS_UPDATE_SET_NEW_COORDINATES = 7;

    public enum ButtonGroup {
        BOUNDARY_CONDITIONS,
        DISPLACEMENT,
        MATERIALS,
        METALS,
        PROBLEM_TYPE,
        PARAMETRIC_SHAPES,
        BOOLEAN_OPERATIONS,
        LINEAR_SHAPE_OPERATIONS;
    }

    public enum CADShape {
        INVALID_VALUE,
        LINE_SEGMENT,
        VERTEX,
        CLEAN_POLYLINE,
        POLYLINE,
        LINE,
        ARC,
        CIRCLE,
        RECTANGLE,
        REGULAR_POLYGON,
        BOOLEAN_BUILT_SHAPE,
        COMPOUND_SURFACE;

        // Point shapes
        private static EnumSet<CADShape> pointShapes = EnumSet.of(
                VERTEX
        );

        // Linear shapes
        private static EnumSet<CADShape> linearShapes = EnumSet.of(
                LINE_SEGMENT,
                CLEAN_POLYLINE,
                POLYLINE,
                LINE,
                ARC
        );

        // Surface shapes
        private static EnumSet<CADShape> surfaceShapes = EnumSet.of(
                CIRCLE,
                RECTANGLE,
                REGULAR_POLYGON,
                COMPOUND_SURFACE,
                BOOLEAN_BUILT_SHAPE
        );

        public static boolean isAPointShape(CADShape cadShape) {
            return pointShapes.contains(cadShape);
        }

        public static boolean isALinearShape(CADShape cadShape) {
            return linearShapes.contains(cadShape);
        }

        public static boolean isASurface(CADShape cadShape) {
            return surfaceShapes.contains(cadShape);
        }

        public static boolean isABooleanBuiltShape(CADShape cadShape) {
            return BOOLEAN_BUILT_SHAPE == cadShape;
        }
    }

    public enum ShapeType {
        INVALID_VALUE,
        POINT,
        LINEAR,
        SURFACE,
        VOLUME;
    }

    public static final int CONTROL_POINT_TYPE_MOVEMENT = 0;
    public static final int CONTROL_POINT_TYPE_SIZE = 0;

    public enum BooleanOperation{
        NONE,
        UNIFY,
        SUBSTRACT,
        COMBINE,
        INTERSECT,
        DIFFERENCE;
    }

    public enum MaterialType {
        GENERAL,
        IRON,
        STEEL,
        ALUMINUM,
        CONCRETE
    }

    public enum TextureName {
        NONE,
        SOLID,
        HOCKEY_TABLE,
        DASHED_LINE_6_DIV
    }

    public enum DragStatus {
        NOTHING_DRAGGED,
        DRAGGING_SOMETHING
    }

    public static final float[] DEFAULT_SURFACE_COLOR = {200f/255f, 200f/255f, 200f/255f};
    public static final float[] IRON_COLOR = {144f/255f, 93f/255f, 211f/255f};
    public static final float[] STEEL_COLOR = {196f/255f, 182f/255f, 205f/255f};
    public static final float[] ALUMINUM_COLOR = {176f/255f, 222f/255f, 230f/255f};
    public static final float[] CONCRETE_COLOR = {176f/255f, 176f/255f, 176f/255f};

    // Deletion area (all in NDC)
    public static final float[] CENTER_DELETION_AREA = {0.9f, 0.6f, 0f}; // {x, y, z}
    public static final float RADIUS_DELETION_AREA = 0.1f;

    // Margin for bounding boxes (NDC)
    public static final float BOUNDING_BOX_MARGIN = 0.05f;

    // Limits for drawing
    public static final float MINIMUM_LINE_THICKNESS_NDC = 0.005f;
    public static final float MAXIMUM_LINE_THICKNESS_NDC = 0.5f;
    public static final float DEFAULT_LINE_THICKNESS_NDC = 0.02f;
    public static final float[] DEFAULT_LINE_COLOR = {0.22f, 0.22f, 0.78f};
    public static final float[] DEFAULT_LINE_SELECTED_COLOR = {0.2f, 0.6f, 0.2f};
    public static final float[] SELECTED_SHAPE_COLOR = {0f, 183f/255f, 12f/255f};
    public static final float LINE_SEGMENT_LENGTH_NDC = 0.02f;

    // Limits for float operations
    public static final float MINIMUM_FLOAT_NUMBER = 0.001f;
    public static final float MAXIMUM_FLOAT_NUMBER = Float.MAX_VALUE;
    public static final float TOLERANCE_FOR_ZER0 = 1e-5f;
}
