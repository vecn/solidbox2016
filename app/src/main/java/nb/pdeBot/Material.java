package nb.pdeBot;

public class Material {

    protected double E;
    protected double v;
	protected double Ep;
	protected double d;
	protected double FrE;
	protected double Tl;
	protected double Ys;
    
	public Material(double youngModulus,
					double poissonModulus,
					double plasticModulus,
					double density,
					double fractureEnergy,
					double tensionLimit,
					double yieldStress) 
	{
		E = youngModulus; /* All problem types */
		v = poissonModulus; /* All problem types */
		Ep = plasticModulus; /* Plasticity problem types */
		d = density; /* All problem types */
		FrE = fractureEnergy; /* Damage problem types */
		Tl = tensionLimit; /* Damage problem types */
		Ys = yieldStress; /* Plasticity problem types */
    }
    public void setYoungModulus(double E) {
		this.E = E;
    }
    public double getYoungModulus() {
		return E;
    }
    public void setPoissonModulus(double v) {
		this.v = v;
    }
    public double getPoissonModulus() {
		return v;
    }
	public void setPlasticityModulus(double Ep) {
		this.Ep = Ep;	
	}	
	public double getPlasticityModulus() {
		return Ep;
	}
	public void setDensity(double d) {
		this.d = d;
	}
	public double getDensity() {
		return d;
	}
	public void setFractureEnergy(double FrE) {
		this.FrE = FrE;
	}
	public double getFractureEnergy() {
		return FrE;	
	}	   
	public void setTractionLimit(double Tl) {
		this.Tl = Tl;
	}
	public double getTractionLimit() {
		return Tl;
	}
	public void setYieldStress(double Ys) {
		this.Ys = Ys;
	}
	public double getYieldStress() {
		return Ys;
	}
}
