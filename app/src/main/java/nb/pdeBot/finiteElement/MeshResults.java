package nb.pdeBot.finiteElement;

import nb.geometricBot.Mesh;

public class MeshResults extends Mesh {
    private int status;
    protected float[] displacement;   /* 2 values (u, v) */
    protected float[] strain;         /* 3 values (dux, dvy, [dvx + duy]/2) */
    protected float[] VonMisesStress; /* 1 value */
    protected float[] stress; 		  /* 3 value (sx, sy, sxy) */
	protected float[] damage; 		  /* 1 value */
	protected float[] plasticElems;   /* 1 value */
    
    private MeshResults() {
	int status = -1;
	displacement = null;
	strain = null;
	VonMisesStress = null;
	stress = null;
	damage = null;
	plasticElems = null;
    }

    public float[] getDisplacementRef() {
	return displacement;
    }
    
    public float[] getStrainRef() {
	return strain;
    }

    public float[] getVonMisesStressRef() {
	return VonMisesStress;
    }

    public float[] getStressRef() {
	return stress;
    }

    public float[] getDamageRef() {return damage; }

    public float[] getPlasticElemsRef() {
	return plasticElems;
    }

    public boolean FEMSuccess() {
	return status == 0;
    }

    private void setStatus(int status) {
	this.status = status;
    }
}
