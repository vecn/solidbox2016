/**
 * Model: Definition of the Geometry. This class is used as input for most
 * of the Geometric Bot routines.
 * 2011-2015 Victor Eduardo Cardoso Nungaray
 * Twitter: @victore_cardoso
 * email: victorc@cimat.mx
 *
 */

package nb.geometricBot;

public class Model {
    protected float[] vertices;   /* Concatenated coordinates of vertices */
    protected int[]  edges;       /* Concatenated IDs of vertices forming edges */
    protected float[] holes;      /* Concatenated coordinates of holes */

    static {
	System.loadLibrary("nbots");
	//System.loadLibrary("nbots_jni");
    }

    public static Model getCircle(float x, float y, float r, float sideLength)
    {
	float p = (float)(2.0 * Math.PI * r);

	if (sideLength <= 0.0f)
	    /* Prevent division by zero */
	    sideLength = p / 10;

	float n = p / sideLength;
	if (10 > n)
	    n = 10;
	return getPolygon(x, y, r, Math.round((float)n));
    }

    public static Model getRect(float x1, float y1, float x2, float y2)
    {	
	Model model = new Model();

	model.vertices = new float[8];
	model.edges = new int[8];
	model.holes = new float[0];

	/* Set vertices */
	model.vertices[0] = x1;
	model.vertices[1] = y1;
	model.vertices[2] = x2;
	model.vertices[3] = y1;
	model.vertices[4] = x2;
	model.vertices[5] = y2;
	model.vertices[6] = x1;
	model.vertices[7] = y2;

	/* Set edges */
	model.edges[0] = 0;
	model.edges[1] = 1;
	model.edges[2] = 1;
	model.edges[3] = 2;
	model.edges[4] = 2;
	model.edges[5] = 3;
	model.edges[6] = 3;
	model.edges[7] = 0;

	return model;
    }

    public static Model getPolygon(float x, float y, float r, int n)
    {
	Model model = new Model();
	
	float angleStep = (float)(Math.PI * 2.0 / n);
	
	model.vertices = new float[2 * n];
	model.edges = new int[2 * n];
	model.holes = new float[0];

	for (int i = 0; i < n; i++) {
	    float angle = i * angleStep;
	    model.vertices[i * 2] = x + r * (float)Math.cos(angle);
	    model.vertices[i*2+1] = y + r * (float)Math.sin(angle);
	    model.edges[i * 2] = i;
	    model.edges[i*2+1] = (i+1) % n;
	}

	return model;
    }

    protected Model() {
	vertices = new float[0];
	edges = new int[0];
	holes = new float[0];
    }

    public Model(float[] vertices, int[] edges, float[] holes) {
	this.vertices = vertices;
	this.edges = edges;
	this.holes = holes;
    }

    public static native Model combine(Model modelA, Model modelB);
    public static native Model unify(Model modelA, Model modelB);
    public static native Model intersect(Model modelA, Model modelB);
    public static native Model substract(Model modelA, Model modelB);
    public static native Model difference(Model modelA, Model modelB);
    
    public native ModelStatus verify();
    public native boolean isContinuum();
    public native boolean isPointInside(double x, double y);
    
    public int getNVertices() {
	return vertices.length / 2;
    }

    public int getNEdges() {
	return edges.length / 2;
    }

    public int getNHoles() {
	return holes.length / 2;
    }

    public boolean hasHoles() {
	return (0 < holes.length);
    }

    public float[] getVerticesRef() {
	return vertices;
    }

    public int[] getEdgesRef() {
	return edges;
    }

    public float[] getHolesRef() {
	return holes;
    }
}
