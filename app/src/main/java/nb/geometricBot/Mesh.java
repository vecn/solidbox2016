/**
 * Mesh: Mesh produced by the Geometric Bot.
 * 2011-2015 Victor Eduardo Cardoso Nungaray
 * Twitter: @victore_cardoso
 * email: victorc@cimat.mx
 *
 */

package nb.geometricBot;

public class Mesh {
    static {
	System.loadLibrary("nbots");
	//System.loadLibrary("nbots_jni");
    }

    protected float[] vertices; /* Concatenated Vertices coordinates */
    protected int[] connEdges;  /* Concatenated IDs of vtx forming edges */
    protected int[] connMtx;    /* Concatemated IDs of vtx forming elems */
    protected int[] modelVtx;   /* Concatenated IDs of mesh vtx corresponding
				 * to model vertices.
				 */
    protected int[][] modelSgm; /* An array of concatenated mesh vtx IDs for
				 * each input segment (of the model).
				 */

    protected int NAreas;
    protected int[] elemAreaID;
    


    public Mesh() {
	vertices = new float[0];
	connEdges = new int[0];
	connMtx = new int[0];
	modelVtx = new int[0];
	modelSgm = new int[0][];
	NAreas = 0;
	elemAreaID = new int[0];
    }
    public static native Mesh generateMesh(Model model);
    public static native Mesh getSimplestMesh(Model model);
    
    public native void updateMesh(Model model);
    public native void updateSimplestMesh(Model model);

    public int getNVertices() {
	return vertices.length / 2;
    }

    public int getNEdges() {
	return connEdges.length / 2;
    }

    public int getNElements() {
	return connMtx.length / 3;
    }
    
    public int getNModelVertices() {
	    return modelVtx.length;
    }

    public int getNModelSegments() {
	    return modelSgm.length;
    }

    public int getNVtxOfModelSgm(int iSgm) {
	if (iSgm < modelSgm.length)
	    return modelSgm[iSgm].length;
	else
	    return 0;
    }

    public float[] getVerticesRef() {
	return vertices;
    }

    public int[] getConnMtxRef() {
	return connMtx;
    }

    public int[] getConnEdgesRef() {
	return connEdges;
    }

    public float[] getEdges() {
	float[] edges = new float[connEdges.length];
	for (int i = 0; i < connEdges.length; i++)
	    edges[i] = vertices[connEdges[i]];
	return edges;
    }

    public int[] getModelVtxRef() {
	return modelVtx;
    }

    public int[][] getModelSgmRef() {
	return modelSgm;
    }

    public int getNAreas() {
	return NAreas;
    }

    public int getElemAreaID(int elemId) {
	    return elemAreaID[elemId];
    }
}
