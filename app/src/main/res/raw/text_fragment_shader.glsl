precision mediump float;
uniform sampler2D u_TextureUnit; // The input texture.
// Set the default precision to medium. We don't need as high of a precision in the fragment shader.
uniform vec4 u_Color;
varying vec2 v_TextureCoordinates;
			
void main()              // The entry point for our fragment shader.
{
    // texture is grayscale so take only grayscale value from it when computing color output (otherwise font is always black)
    gl_FragColor = texture2D(u_TextureUnit, v_TextureCoordinates).w * u_Color;
}