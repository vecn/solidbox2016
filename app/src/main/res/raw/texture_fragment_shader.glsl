precision mediump float;

// sampler2D: this variable refers to an array of two-dimensional texture data
uniform sampler2D u_TextureUnit;
uniform vec4 u_Color;
uniform vec4 u2_Color;

varying vec2 v_TextureCoordinates;

void main()
{
    gl_FragColor = texture2D(u_TextureUnit, v_TextureCoordinates) + u_Color;
    /*
    if (gl_FragColor.r < 0.001)
        gl_FragColor = u2_Color;
    else
        gl_FragColor = u_Color;
    //*/

}