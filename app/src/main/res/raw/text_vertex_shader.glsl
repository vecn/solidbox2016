uniform mat4 u_Matrix[24];          // An array representing the combined
									// model/view/projection matrices for each sprite
attribute float a_MatrixIndex;	    // The index of the MVPMatrix of the particular sprite
attribute vec4 a_Position;          // Per-vertex position information we will pass in.
attribute vec2 a_TextureCoordinates;// Per-vertex texture coordinate information we will pass in
varying vec2 v_TextureCoordinates;  // This will be passed into the fragment shader.
void main()                         // The entry point for our vertex shader.
{
    int matrixIndex = int(a_MatrixIndex);
    v_TextureCoordinates = a_TextureCoordinates;
    // gl_Position is a special variable used to store the final position.
    // Multiply the vertex by the matrix to get the final point in normalized screen coordinates.
    gl_Position = u_Matrix[matrixIndex] * a_Position;
}